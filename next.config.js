/** @type {import('next').NextConfig} */
const host360 = process.env.NEXT_PUBLIC_360_HOST.replace(/^https?:\/\//, '').replace(/^http?:\/\//, '');
const cdn = process.env.NEXT_PUBLIC_CDN.replace(/^https?:\/\//, '').replace(/^http?:\/\//, '');

const nextConfig = {
  reactStrictMode: false,
  swcMinify: false,
  staticPageGenerationTimeout: 1500,
  images: {
    domains: ['znews-photo.zingcdn.me', host360.replace('/', ''), cdn.replace('/', ''), 'img.youtube.com', 'static-dev.dggv.edu.vn'],
  },
}

module.exports = nextConfig
// module.exports = {
//   reactStrictMode: true,
//   swcMinify: true,
// }; 