import { SdFilterOption, SdGridColumn } from "@lib/core/components";

export interface PagingRequest {
  pageNumber: number;
  pageSize: number;
  sorts?: SortDefine[];
  filters?: FilterRequest[];
}

export interface SortDefine {
  field: string;
  direction: "asc" | "desc";
}

export interface FilterRequest {
  field: string;

  value?: any;

  from?: string;

  to?: string;

  operator?: FilterOperator;
}

export enum FilterOperator {
  EQUAL = "EQUAL",
  NOT = "NOT",
  GREATER_THAN = "GREATER_THAN",
  LESS_THAN = "LESS_THAN",
  GREATER_THAN_OR_EQUAL = "GREATER_THAN_OR_EQUAL",
  LESS_THAN_OR_EQUAL = "LESS_THAN_OR_EQUAL",
  CONTAIN = "CONTAIN",
  NOT_CONTAIN = "NOT_CONTAIN",
  START_WITH = "START_WITH",
  END_WITH = "END_WITH",
  NOT_START_WITH = "NOT_START_WITH",
  NOT_END_WITH = "NOT_END_WITH",
  BETWEEN = "BETWEEN",
  IN = "IN",
  ISNULL = "ISNULL",
  ARRAY_CONTAINS = "ARRAY_CONTAINS",
  ARRAY_CONTAINED_BY = "ARRAY_CONTAINED_BY",
  ARRAY_OVERLAP = "ARRAY_OVERLAP",
}

export const generateFromGridFilter = (args: SdFilterOption, columns: SdGridColumn[]): PagingRequest => {
  const { pageNumber, pageSize, filter, sort } = args;
  const sorts: SortDefine[] = [];
  if (typeof (sort) === 'object') {
    for (const key of Object.keys(sort)) {
      if (sort[key]) {
        sorts.push({
          field: key,
          direction: sort[key]
        })
      }
    }
  }
  const filters: FilterRequest[] = [];
  if (typeof (filter) === 'object') {
    for (const key of Object.keys(filter)) {
      const value = filter[key];
      if (value !== undefined && value !== null && value !== '') {
        const column = columns.find(e => e.field === key);
        if (column) {
          if (column.type === 'string') {
            filters.push({
              field: key,
              operator: FilterOperator.CONTAIN,
              value
            })
          }
          if (column.type === 'bool') {
            filters.push({
              field: key,
              value
            })
          }
          if (column.type === 'date') {
            filters.push({
              field: key,
              operator: FilterOperator.BETWEEN,
              from: Date.begin(value)?.toISOString(),
              to: Date.end(value)?.toISOString(),
            })
          }
          if (column.type === 'values') {
            filters.push({
              field: key,
              value
            })
          }
        }
      }
    }
  }

  return {
    pageSize,
    pageNumber,
    sorts,
    filters
  }
}