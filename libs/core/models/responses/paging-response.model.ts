import { Response } from "./response.model";

export interface PagingResponse<T> extends Response<T[]> {
  total: number;

  additionData?: Record<string, any>;
}
