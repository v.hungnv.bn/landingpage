export interface LoginReq {
  username: string;
  password: string;
  project: string;
  department: string;
}

export interface UserInfo {
  userId: string;
  username: string;
  name: string;
  email: string;
  token: string;
  tenantCode: string;
  departmentCode: string;
  projectCode: string;
  permissions: string[];
  avatar: string;
  isVNA: boolean;
}

export interface SdResponse<T = any> {
  status: number;
  success: boolean;
  message?: string;
  data?: T;
  statusCode: number;
}