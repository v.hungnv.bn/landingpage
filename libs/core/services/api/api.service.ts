import axios from 'axios';
import { NotifyService } from '../notify.service';
import { PortalPage } from '../portal/portal.model';
import { LoginReq, SdResponse, UserInfo } from './api.model';

const key = 'fc780015-16d9-4a6a-ab63-44ffec89e173';
const token = () => {
  return localStorage.getItem(key);
}

let userInfo: UserInfo;
let portalPage: PortalPage;
let adminDefaultUrl: string;
let departmentCode: string;

const userHost = process.env.NEXT_PUBLIC_USER_HOST;

const handleResponse = (response: SdResponse) => {
  if (response.statusCode === 401) {
    throw new Error(response?.message || 'Có lỗi xảy ra');
  } else if (!response.success) {
    console.log(response);
    NotifyService.error(response?.message ||'Có lỗi xảy ra' );  
    throw new Error(response?.message || 'Có lỗi xảy ra');
  }

}

const handleError = (error: any) => {
  console.log(error);
}

const login = async (req: LoginReq) => {
  const url = `${userHost}auth/login`;
  try {
    const response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(req),
      headers: {
        'Content-Type': 'application/json',
      }
    })
    const res = await response.json();
    handleResponse(res);
    localStorage.setItem(key, res?.data?.token);
    return res;
  } catch (error) {
    // NotifyService.error('Có lỗi xảy ra!', null);
  }
}

const logout = () => {
  (userInfo as any) = undefined;
  localStorage.clear();
}

const loadUserInfo = async () => {
  userInfo = await get<UserInfo>(`${userHost}auth/current`);
  return userInfo;
}

const get = async <T = any>(path: string, params?: any, noAuthorization?: boolean): Promise<T> => {
  const url = new URL(path);
  params && Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  let headers: {'Content-Type'?: string, 'Authorization'?: string} = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token()}`,
  }
  if (noAuthorization || !token()) {
    const prop = "Authorization";
    delete headers[prop];
  } 

  const response = await fetch(url.toString(), {
    method: 'GET',
    headers: headers
  });
  const res = await response.json();
  handleResponse(res);
  return res?.data;
}

const getFile = async <T = any>(path: string, params?: any): Promise<T> => {
  const url = new URL(path);
  params && Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const response = await fetch(url.toString(), {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${token()}`,
    }
  })
  const res = await response.json();
  handleResponse(res);
  return res?.data;
}


const post = async <T = any>(path: string, body: any, params?: any, noAuthorization?: boolean): Promise<T> => {
  const url = new URL(path);
  params && Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  let headers: {'Content-Type'?: string, 'Authorization'?: string} = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token()}`,
  }

  if (noAuthorization || !token()) {
    const prop = "Authorization";
    delete headers[prop];
  } 

  const response = await fetch(url.toString(), {
    method: 'POST',
    body: JSON.stringify(body),
    headers: headers
  });
  const res = await response.json();
  handleResponse(res);
  if ('total' in res) {
    return {
      items: res?.data || [],
      total: res.total || 0
    } as any;
  }
  return res?.data || res;
}

const upload = async <T = any>(path: string, file: File, params?: any): Promise<T> => {
  const url = new URL(path);
  params && Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

  let formData = new FormData();
  formData.append('file',file);

  const response = await fetch(url.toString(), {
    method: 'POST',
    body:formData,
    headers: {
      'Authorization': `Bearer ${token()}`,
    }
  });
  const res = await response.json();
  handleResponse(res);
  if ('total' in res) {
    return {
      items: res?.data || [],
      total: res.total || 0
    } as any;
  }
  return res?.data;
}

const download = async (path: string, name?: string) => {
  const url = new URL(path);
  axios({
    url: `${url}`, //your url
    method: 'GET',
    responseType: 'blob',
    headers: {
      'Authorization': `Bearer ${token()}`,
    }
  }).then((response) => {
      let fileName = name || `Export-${(new Date())}.xlsx`;
      let type = 'application/octet-stream';
      const file = new Blob([response.data], { type });
      const url = window.URL.createObjectURL(file);
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', fileName); 
      document.body.appendChild(link);
      link.click();
  }).catch((error) =>{
    console.log('error', error);
    NotifyService.warn('Không có dữ liệu' || 'Có lỗi xảy ra' );  
  }); 
}

const put = async <T = any>(path: string, body?: any, params?: any): Promise<T> => {
  const url = new URL(path);
  params && Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const response = await fetch(url.toString(), {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token()}`,
    }
  });
  const res = await response.json();
  handleResponse(res);
  return res?.data;
}

const del = async <T = any>(path: string, params?: any): Promise<T> => {
  const url = new URL(path);
  params && Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
  const response = await fetch(url.toString(), {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token()}`,
    }
  })
  const res = await response.json();
  handleResponse(res);
  return res?.data;
}

const setPortalPage = (portalPages: PortalPage) => {
  portalPage = portalPages;
  return portalPage;
}

const setAdminDefaultPage = (path: string) => {
  adminDefaultUrl = path;
  return adminDefaultUrl;
}

const setDepartmentCode = (code: string) => {
  departmentCode = code;
  return departmentCode;
}

export const ApiService = {
  get token() {
    return token()
  },
  get userInfo() {
    return userInfo;
  },
  get portalPage() {
    return portalPage;
  },
  get adminDefaultUrl() {
    return adminDefaultUrl;
  },
  get departmentCode() {
    return departmentCode;
  },
  login,
  logout,
  loadUserInfo,
  get,
  getFile,
  post,
  put,
  upload,
  delete: del,
  download,
  setPermission: setPortalPage,
  setAdminDefaultPage,
  setDepartmentCode
}