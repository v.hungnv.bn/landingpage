import { CacheKeys } from "./cache.model";

export class CacheService {
  static get<T = any>(key: CacheKeys): T | undefined {
    const cachedData = localStorage.getItem(key);
    try {
      const data = cachedData && JSON.parse(cachedData);
      return data?.value;
    } catch (error) {
      console.log(error);
    }
  }

  static set<T = any>(key: CacheKeys, value: T) {
    const data = {
      value: value,
      expire: -1
    };
    try {
      localStorage.setItem(key, JSON.stringify(data));
    } catch (error) {
      console.log(error);
    }
  }


  static remove(key: CacheKeys) {
    localStorage.removeItem(key);
  }

  static clear() {
    localStorage.clear();
  }
}
