import ScheduleOutlined from "@mui/icons-material/ScheduleOutlined";
import CheckCircle from "@mui/icons-material/CheckCircle";
import Lock from "@mui/icons-material/Lock";
// import CircleIcon from "@mui/icons-material/Inprog";
// import CircleIcon from "@mui/icons-material/Circle";
import { SdBadgeColor } from '../../components';

export enum StatusEnum {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
  DONE = 'DONE',
}

export const Status: Record<StatusEnum, {
  value: string;
  display: string;
  badgeColor: SdBadgeColor;
  badgeIcon?: any;
}> = {
  INACTIVE: {
    value: 'INACTIVE',
    display: 'Khóa',
    badgeColor: 'error',
    badgeIcon: <Lock />
  },
  ACTIVE: {
    value: 'ACTIVE',
    display: 'Hoạt động',
    badgeColor: 'primary',
    badgeIcon: <ScheduleOutlined  />
  },
  DONE: {
    value: 'DONE',
    display: 'Hoàn thành',
    badgeColor: 'success',
    badgeIcon: <CheckCircle  />
  }
}