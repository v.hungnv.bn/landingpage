import { ApiService } from "@lib/core/services";
import { PortalPage } from "./portal.model";

export class PortalService {
  static page(host: string): Promise<PortalPage> {
    return ApiService.get(`${host}portal/page`);
  }
}