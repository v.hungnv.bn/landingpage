export interface Permission {
  project: string;

  code: string;

  name: string;

  type: PermissionType;

  isActivated: boolean;
}

export enum PermissionType {
  API = "API",
  COMPONENT = "COMPONENT",
}


export type PagePermission = Partial<Record<'view' | 'create' | 'update' | 'delete' | 'other1' | 'other2', boolean>>;

export type AdminPage = 'department' | 'domain' | 'permission' | 'group' | 'role' | 'user' | 'project';
export type AdminPortal = Partial<Record<AdminPage, PagePermission>>;

export type CdsPage = 'year' | 'organization' | 'council' | 'standard' | 'evaluationConfiguration' | 'dti' | 'dtiEvaluation' | 'selfEvaluation' | 'organizationEvaluation' | 'councilEvaluation' | 'setting' | 'report';
export type CdsPortal = Partial<Record<CdsPage, PagePermission>>;

export type PortalPage = AdminPortal & CdsPortal;