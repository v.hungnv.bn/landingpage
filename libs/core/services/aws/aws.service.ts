import { ApiService } from "../api/api.service";

export class AwsService {
  static readonly API = {
    upload: 'aws/upload',
    get: 'aws'
  }
}

export async function upload(host: string, file: File): Promise<{ key: string }> {
  return ApiService.upload(`${host}aws/upload`, file);
}

export function get(host: string, key: string): string {
  return `${host}aws/${key}`;
}

export function cdn(key?: string): string {
  if(!key) {
    return '';
  }
  // TODO: Các key cũ ko có prefix nên CDN ko work, tự động append prefix 360 vào tạm
  if(!key?.startsWith('360')) {
    key = `360/${key}`;
  }
  return `${process.env.NEXT_PUBLIC_CDN || 'https://static-dev.dggv.edu.vn/'}${key}`
}


export function getUrl360(key?: string): string {
 
  return `${process.env.NEXT_PUBLIC_360_HOST + 'aws/' + key}`;
}
