import { ApiService } from "../../../core/services";
export class UploadService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly hostCds = process.env.NEXT_PUBLIC_CDS_HOST;

  static readonly API = {
    upload: 'aws/upload',
    get: 'aws',
    getSignedUrl: 'aws/getSignedUrl'
  }
}



export async function upload(file: File): Promise<{ key: string }> {
  return ApiService.upload(`${UploadService.hostCds}${UploadService.API.upload}`, file);
}

export function getFileURL(fileKey: string): string {
  return `${UploadService.hostCds}${UploadService.API.get}/${fileKey}`
}
