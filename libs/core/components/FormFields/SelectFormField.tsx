
import { FieldProps, getIn } from "formik";
import React from "react";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText
} from "@mui/material";

export const SelectFormField: React.FC<
  FieldProps & {
    label?: string;
    valueField:string;
    displayField:string;
    options: Array<any>;
  }
> = ({ field, form, label, options,valueField,displayField, ...props }) => {
  const errorText =
    getIn(form.touched, field.name) && getIn(form.errors, field.name);
  return (
    <FormControl fullWidth error={!!errorText}>
      {label && <InputLabel>{label}</InputLabel>}
      <Select fullWidth {...field} {...props}>
        {options.map(op => (
          <MenuItem  key={op[valueField]+op[displayField]} value={op[valueField]}>
            {op[displayField]}
          </MenuItem>
        ))}
      </Select>
      <FormHelperText>{errorText}</FormHelperText>
    </FormControl>
  );
};