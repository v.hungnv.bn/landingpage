import TextField from "@mui/material/TextField/TextField";
import { FieldProps, getIn } from "formik";
import React from "react";

export const TextFormField: React.FC<FieldProps & { type?: string,min?:number,max?:number }> = ({
  field,
  form,
  type,
  min,
  max,
  ...props
}) => {

  const errorText =
    getIn(form.touched, field.name) && getIn(form.errors, field.name);
  if (type === 'number') {
    return (<TextField
      fullWidth
      margin="normal"
      type="number"
      InputProps={{
        ...((type === "number" && {
          inputProps: { min: min, max: max },
        }) ||
          undefined),
      }}
      helperText={errorText}
      error={!!errorText}
      {...field}
      {...props}
    />)
  }
  else {
    return (<TextField
      fullWidth
      margin="normal"
      helperText={errorText}
      error={!!errorText}
      InputLabelProps={{ shrink: true }}
      {...field}
      {...props}
    />)
  }

};