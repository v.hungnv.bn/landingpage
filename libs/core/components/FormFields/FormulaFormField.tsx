import { FormControl, InputLabel, TextField } from "@mui/material";
import { FieldProps, getIn } from "formik";

export const FormularFormField:React.FC<FieldProps & {
    label?:string,
    options: Array<{label:string,value:string}>;
}> = ({field,form,label,options, ...props}) => {
    const errorText = getIn(form.touched,field.name) && getIn(form.errors,field.name);
    return (
        <FormControl fullWidth error={!!errorText}>
            {label && <InputLabel>{label}</InputLabel>}
            {options.map(op => (
                <TextField  sx = {{marginBottom:'24px'}}label={op.label} {...field} {...props}></TextField>
            ))}
        </FormControl>
    )
}