import TextField from "@mui/material/TextField/TextField";
import { FieldProps, getIn } from "formik";
import React from "react";

export const ViewOnlyField: React.FC<FieldProps & { type?: string, disabled?: boolean,valueField?:string,displayField?:string }> = ({
    field,
    form,
    type,
    disabled,
    ...props
}) => {

    switch(type){
        case 'FORMULAR':

        case 'SELECTION':
        default:
            return (<TextField
                fullWidth
                margin="dense"
                sx={{  "& fieldset": { border: 'none' },
                "& .Mui-disabled":{WebkitTextFillColor:'black'}}}
                disabled={true}
                {...field}
                {...props}
            />)
    }   

  

};