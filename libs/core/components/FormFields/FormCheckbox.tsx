import * as React from 'react';
import Checkbox from '@mui/material/Checkbox';


export const ControlledCheckbox:React.FC<{defaultChecked:boolean,onChange:(isCheck:boolean) => void }> =  ({
    defaultChecked,
    onChange,
}) => {

  const [checked, setChecked] = React.useState(defaultChecked);

  React.useEffect(()=>{
    setChecked(defaultChecked)
  },[defaultChecked])

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
    onChange(event.target.checked);
  };

  return (
    <Checkbox
      checked={checked}
      onChange={handleChange}
      inputProps={{ 'aria-label': 'controlled' }}
    />
  );
}