import { TextField, TextFieldProps } from '@mui/material';
import { useEffect, useState } from 'react';
import { Controller, FieldValues, UseControllerProps, useForm, UseFormReturn } from "react-hook-form";
import * as uuid from 'uuid';
export interface SdInputProps {
  sdChange: (value: string) => void;
  form?: UseFormReturn<FieldValues, any>;
  rules?: UseControllerProps['rules']; // form
}

export function SdInput(props: SdInputProps & TextFieldProps) {
  const { value, sdChange, form, rules } = props;
  const [controlName, setControlName] = useState<string>('');
  useEffect(() => {
    if (form) {
      setControlName(uuid.v4());
    } else {
      setControlName('');
    }
  }, [form]);
  useEffect(() => {
    if (form && controlName) {
      const { setValue } = form;
      setValue(controlName, value);
    }
  }, [value, controlName]);
  if (form) {
    const { formState: { errors } } = form;
    const error = errors[controlName];
    let errorMessage = '';
    if (error) {
      if (error.type === 'required') {
        errorMessage = 'Dữ liệu không được để trống'
      }
    }
    return (<Controller
      name={controlName}
      control={form.control}
      rules={rules}
      render={({ field }) => (
        <TextField
          {...props}
          {...field}
          InputLabelProps={{ shrink: !!value }}
          size={props.size || 'small'}
          id={controlName}
          error={!!errorMessage}
          helperText={errorMessage || ' '}
          required={!!rules?.required}
          onChange={(event) => {
            field.onChange(event);
            sdChange(event.target.value);
          }}
        />
      )}
    />)
  }
  return (
    <TextField
      {...props}
      onChange={(event) => {
        sdChange(event.target.value);
      }}
      size={props.size || 'small'}
    />
  );
}
