import TextField from '@mui/material/TextField';
import { useEffect, useState } from "react";
import { forwardRef, useImperativeHandle } from "react"
import InputAdornment from '@mui/material/InputAdornment'
import IconButton from '@mui/material/IconButton'
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
const CoreInput = forwardRef((props: {
  value?: any, type?: string, placeholder?: string, isPassWord?: boolean
  required?: boolean, readOnly?: boolean, name?: string, multiline?: boolean, margin?: 'dense' | 'none',
  inputProps?: any, fullWidth?: boolean, disabled?: boolean, autoFocus?: boolean, classes?: any, variant?: "standard" | "filled" | "outlined",
  size?: "small" | "medium", id?: string, label?: string, notShowError?: boolean, validateDefine?: (event: string) => string, onChange: (event: string) => void
}, ref) => {
  const [showPassword, setShowPassword] = useState(false);
  const [messError, setMessError] = useState<string>('');
  const [defaultValue, setDefaultValue] = useState<string>('');

  useImperativeHandle(ref, () => ({
    validate() {
      return checkValidate();
    }
  }))

  useEffect(() => {
    setDefaultValue(props?.value)
  }, [props?.value])

  const handlerChange = (event: any) => {
    setDefaultValue(event.target.value);
    props.onChange(event.target.value);
    checkValidate(event.target.value);
  }

  const validateEmpty = (event: any) => {
    checkValidate(event.target.value);
  }

  const checkValidate = (event?: string) => {
    setMessError('');
    const value = event || defaultValue
    if (!props.notShowError && props.required && !value) {
      setMessError('Không được phép để trống');
      return false;
    }
    if (!props.notShowError && props.validateDefine) {
      const messError = props.validateDefine(value);
      setMessError(messError);
      if (messError) {
        return false;
      }
      return true;
    }
    return true;
  }

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event: any) => {
    event.preventDefault();
  };


  return (
    <div style={!props.notShowError?{marginBottom:'28px', position:'relative'}:{}}>
      {!props.isPassWord && <TextField sx={{ marginTop: '5px' }} type={props.type} required={props.required} fullWidth={props.fullWidth} disabled={props.disabled} autoFocus={props.autoFocus}
        name={props.name} multiline={props.multiline} margin={props.margin} inputProps={props.inputProps} variant={props.variant} size={props.size} id={props.id} label={props.label}
        value={defaultValue} placeholder={props.placeholder} error={!!messError} classes={props.classes} onChange={handlerChange} onBlur={validateEmpty} >
      </TextField>}
      {props.isPassWord && <FormControl sx={{ marginTop: '5px' }} required={props.required} fullWidth={props.fullWidth}
        disabled={props.disabled} variant={props.variant} size={props.size} error={!!messError}>
        <InputLabel htmlFor="outlined-adornment-password">{props.label}</InputLabel>
        <OutlinedInput fullWidth id="password" label={props.label}
          type={showPassword ? 'text' : 'password'}
          onChange={handlerChange}
          onBlur={validateEmpty}
          value={defaultValue}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                edge="end">
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          }
        ></OutlinedInput>
      </FormControl>}
      {!!messError && <div style={{position:'absolute',bottom:'-24px'}} className='text-error'>{messError}</div>}
    </div>
  );
})

export const Input = CoreInput;


