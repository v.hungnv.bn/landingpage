import dynamic from "next/dynamic";
import { any } from "prop-types";
import React, { LegacyRef, useEffect, useRef, useMemo } from "react";

import { ReactQuillProps } from "react-quill";
interface IWrappedComponent extends ReactQuillProps {
  forwardedRef?: LegacyRef<any>;
}
const ReactQuill = dynamic(
  async () => {
    const { default: RQ } = await import("react-quill");
    return ({ forwardedRef, ...props }: IWrappedComponent) => <RQ ref={forwardedRef} {...props} />;
  },
  {
    ssr: false
  }
);

// import 'react-quill/dist/quill.snow.css'; // ES6
export interface SdEditorProps {
  sdChange?: (value: string) => void;
}

export function SdEditor(props: SdEditorProps & ReactQuillProps) {
  const { value, sdChange } = props;  

  const quillRef = useRef<IWrappedComponent | any>(null);

  const codeHandler = async () => {
    let embedHtml = prompt('Please Enter the Html Embed');
    if (embedHtml) {
      const text = embedHtml?.replaceAll('“', '"')?.replaceAll('”', '"');
      const editor = quillRef.current.getEditor()
      const index = editor.getSelection().index || 0;
      editor.clipboard.dangerouslyPasteHTML(index, text);
    }
  }

  // const modules = useMemo(() => {
  //   return {
  //     toolbar: {
  //       handlers: {
  //         code: codeHandler
  //       },
  //       container: [
  //         [{ font: [] }, { size: ['11px',2,3,4,5,6,7,8,9] }, { header: [1, 2, 3, 4, 5, 6] }],
  //         ["bold", "italic", "underline", "strike"],
  //         [{ color: [] }, { background: [] }],
  //         [{ script: "sub" }, { script: "super" }],
  //         [{ header: 1 }, { header: 2 }, "blockquote", "code-block"],
  //         [
  //           { list: "ordered" },
  //           { list: "bullet" },
  //           { indent: "-1" },
  //           { indent: "+1" }
  //         ],
  //         [{ direction: "rtl" }, { align: [] }],
  //         [ 'link', 'image', 'video', 'formula' ],
  //         ['clean'],
  //         ['code'],
  //       ],
  //       clipboard: {
  //         matchVisual: false,
  //       },
  //     }
  //   }
  // }, []);

  const modules = useMemo(() => {
    return {
      toolbar: {
        handlers: {
          code: codeHandler
        },
        container: [
          [{ size: [] }, { header: [1, 2, 3, 4, 5, 6, false] }],
          ["bold", "italic", "underline", "strike"],
          [{ color: [] }, { background: [] }],
          // [{ script: "sub" }, { script: "super" }],
          // ["blockquote", ],
          [
            { list: "ordered" },
            { list: "bullet" },
            { indent: "-1" },
            { indent: "+1" }
          ],
          [{ direction: "rtl" }, { align: [] }],
          [ 'link', 'image', 'video',  ],
          // ['clean'],
          ['code'],
        ],
        clipboard: {
          matchVisual: false,
        },
      }
    }
  }, []);
  return (
    <div data-text-editor="name">
      <ReactQuill
        {...props}
        forwardedRef={quillRef}
        theme={'snow'}
        modules={modules}
        value={value}
        bounds={`[data-text-editor="name"]`}
      />
    </div>
  );
}