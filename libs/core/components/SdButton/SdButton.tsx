import { MouseEventHandler } from 'react';
import { LoadingButton } from '@mui/lab';
import { IconButton, StyledComponentProps, SxProps } from '@mui/material';

export interface SdButtonProps {
  onClick?: MouseEventHandler<HTMLButtonElement>;
  label?: string;
  variant?: 'text' | 'contained' | 'outlined';
  color?: 'primary' | 'secondary' | 'success' | 'error' | 'info' | 'warning';
  size?: 'small' | 'medium' | 'large';
  icon?: React.ReactNode;
  loading?: boolean;
  disabled?: boolean;
  className?: string;
  sx?: SxProps;
}

export function SdButton(props: SdButtonProps) {
  const { sx, onClick, label, variant, color, size, icon, loading, disabled } = props;
  if (!icon && !label) {
    return null;
  }
  if (!label) {
    return <IconButton
      sx={sx}
      color={color}
      size={size || 'small'}
      onClick={onClick}
      disabled={disabled}>
      {icon}
    </IconButton>
  }
  return (
    <LoadingButton
      sx={{ textTransform: 'none', ...sx }}
      {...props}
      variant={variant || 'contained'}
      color={color || 'primary'}
      size={size}
      loading={loading}
      // loadingPosition='start'
      onClick={onClick}
      startIcon={icon}
      disabled={disabled}>
      {label}
    </LoadingButton>
  );
}
