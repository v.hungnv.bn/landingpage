import * as React from 'react';
import dayjs, { Dayjs } from 'dayjs';
import TextField from '@mui/material/TextField';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import Stack from '@mui/material/Stack';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { SxProps } from '@mui/material';

export interface SdDateProps {
  label?: string;
  value: string;
  sdChange: (value: string | undefined) => void;
  required?: boolean;
  disabled?: boolean;
  sx?: SxProps;
}

export default function SdDate(props: SdDateProps) {
  const [data, setData] = React.useState<Dayjs | null>(null);

  const { label, disabled, required, value, sdChange, sx } = props;
  React.useEffect(() => {
    setData(dayjs(value || null));
  }, [value]);

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Stack spacing={3}>
        <DesktopDatePicker
          label={label}
          inputFormat="DD/MM/YYYY"
          value={value || null}
          onChange={(newValue: Dayjs | null) => {
            sdChange(newValue?.format('MM/DD/YYYY'));
          }}
          disabled={disabled}
          renderInput={(params) => <TextField value={data} {...params} sx={sx} required={required} size='small'/>}
        />
      </Stack>
    </LocalizationProvider>


  );
}
