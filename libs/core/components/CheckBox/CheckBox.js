import PropTypes from 'prop-types';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';

function CoreCheckBox({
  label,
  disabled = false,
  ...passProps
}) {
  const handlerChange = (event) => {
    onChange(event.target.checked);
  }
  return (
    <FormControlLabel disabled={disabled} control={<Checkbox onChange={handlerChange} {...passProps} />} label={label} />
  );
}
CoreCheckBox.propTypes = {
  label: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
};

export default CoreCheckBox;
