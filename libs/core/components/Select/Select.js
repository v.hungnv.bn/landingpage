import { useEffect, useState } from "react";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
// import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import PropTypes from 'prop-types';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import OutlinedInput from '@mui/material/OutlinedInput';
import Checkbox from '@mui/material/Checkbox';
import ListItemText from '@mui/material/ListItemText';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function CoreSelect({
  model,
  items,
  valueDisplay,
  valueField,
  label,
  size = 'small',
  customViewItem,
  autoWidth = false,
  disabled = false,
  required = false,
  multiple = false,
  width,
  placeholder,
  onChange,
  valueNoteData
}) {
  const [listData, setListData] = useState([]);
  const [itemSelect, setItemSelect] = useState('');

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    if (!multiple) {
      setItemSelect(value);
      onChange && onChange(value);
    }
    if (multiple) {
      setItemSelect(typeof value === 'string' ? value.split(',') : value);
      onChange && onChange(value);
    }
  };

  useEffect(() => {
    if (!multiple && model) {
      setItemSelect(model);
    }
    if (multiple && model) {
      setItemSelect(typeof model === 'string' ? model.split(',') : model);
    }
  }, [model]);

  useEffect(() => {
    if (Array.isArray(items)) {
      setListData(items);
      return;
    }
    const fetchData = async () => {
      const data = await items();
      setListData(data);
    }
    fetchData().catch(console.error);
  }, [items]);

  return (
    <div style={{ width: width || '100%' }}>
      <FormControl sx={{ marginTop: '5px', minWidth: 120, width: '100%', backgroundColor: '#fff' }}
        disabled={disabled}
        required={required}
        size={size}>
        <InputLabel>{label}</InputLabel>
        {!multiple && <Select
          value={itemSelect}
          placeholder={placeholder}
          label={label}
          autoWidth={autoWidth}
          MenuProps={MenuProps}
          onChange={handleChange}>
          {listData.length ? <MenuItem key={'value-0'} value={valueNoteData||''}>
            Vui lòng chọn
          </MenuItem> : ''}
          {listData.length ? listData.map((item, index) => (
            <MenuItem key={index} value={item[valueField]}>
              {customViewItem ? <div dangerouslySetInnerHTML={{ __html: customViewItem(item[valueField], item) }}></div> : item[valueDisplay]}
            </MenuItem>
          )) : <MenuItem key={'not_data'} value={undefined}>
            Không có dữ liệu
          </MenuItem>}
        </Select>}
        {multiple && <Select
          multiple
          placeholder={placeholder}
          value={itemSelect}
          onChange={handleChange}
          MenuProps={MenuProps}
          input={<OutlinedInput label={label} />}
          renderValue={(selected) => selected.join(', ')}>
          {listData.length ? listData.map((item, index) => (
            <MenuItem key={index} value={item[valueField]}>
              <Checkbox checked={itemSelect.indexOf(item[valueField]) > -1} />
              {customViewItem ? <div dangerouslySetInnerHTML={{ __html: customViewItem(item[valueField], item) }}></div> : <ListItemText primary={item[valueDisplay]} />}
            </MenuItem>
          )) : <MenuItem key={'not_data'} value={undefined}>
            Không có dữ liệu
          </MenuItem>}
        </Select>}
        {/* <FormHelperText>Error</FormHelperText> */}
      </FormControl>
    </div >
  );
}
CoreSelect.propTypes = {
  model: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  valueDisplay: PropTypes.string.isRequired,
  valueField: PropTypes.string.isRequired,
  items: PropTypes.oneOfType([PropTypes.array, PropTypes.func]).isRequired,
  label: PropTypes.string,
  size: PropTypes.oneOf(['medium', 'small', undefined]),
  autoWidth: PropTypes.bool,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  multiple: PropTypes.bool,
  onChange: PropTypes.func,
  customViewItem: PropTypes.func,
  disabledErrorMess: PropTypes.bool,
  width: PropTypes.string,
  placeholder: PropTypes.string,
  valueNoteData: PropTypes.string,
}

export default CoreSelect;
