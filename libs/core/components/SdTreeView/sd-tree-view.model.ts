import { SxProps } from "@mui/material";

export interface SdTreeViewOption<T> {
  value: string;
  display: string;
  data: T;
  children?: SdTreeViewOption<T>[];
}

export interface SdTreeViewProps<T> {
  label?: string;
  items: SdTreeViewOption<T>[];
  sdSelect?: (item: SdTreeViewOption<T> | null) => void;
  sdMultiple?: (selectedItem: Record<string, SdTreeViewOption<T>>) => void;
  required?: boolean;
  sx?: SxProps;
  multiple?: boolean;
}
export const SdTreeViewOptionTransform = <T>(
  items: T[] | undefined,
  valueField: string,
  displayField: string,
  childrenField: string): SdTreeViewOption<T>[] => {
  if (!items?.length) {
    return [];
  }
  return items.map(item => ({
    value: (item as any)?.[valueField],
    display: (item as any)?.[displayField],
    data: item,
    children: SdTreeViewOptionTransform((item as any)?.[childrenField], valueField, displayField, childrenField)
  }))
}