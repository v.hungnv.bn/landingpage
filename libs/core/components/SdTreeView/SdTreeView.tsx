import { useEffect, useState } from "react";
import { ExpandMore, ChevronRight } from '@mui/icons-material';
import { TreeItem, TreeView } from "@mui/lab";
import { SdInput } from "../SdInput/SdInput";
import { SdTreeViewOption, SdTreeViewProps } from "./sd-tree-view.model";
import { Box, Checkbox, SxProps, Typography } from "@mui/material";

const Styles: Record<string, SxProps> = {
  container: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  search: {
    marginBottom: '8px'
  },
  tree: {
    flex: 1,
    overflow: 'auto',
  }
};

export function SdTreeView<T = any>(props: SdTreeViewProps<T>) {
  const [searchText, setSearchText] = useState<string>('');
  const [options, setOptions] = useState<SdTreeViewOption<T>[]>([]);
  const [checked, setChecked] = useState<Record<string, SdTreeViewOption<T>>>({});
  const { items, sdSelect, sdMultiple, sx, multiple } = props;
  useEffect(() => {
    setOptions(items.search(searchText, ['value', 'display'], 'children'));
  }, [items, searchText]);

  const renderTreeItem = (treeItems: SdTreeViewOption<T>[] | undefined) => {
    if (!treeItems?.length) {
      return null;
    }
    return treeItems.map(treeItem => {
      if (multiple) {
        return (
          <TreeItem
            key={treeItem.value}
            nodeId={treeItem.value}
            label={<div style={{ display: 'flex', alignItems: 'center' }}>
              <Checkbox
                color="primary"
                checked={!!checked[treeItem.value]}
                value
                onChange={(event, isChecked) => {
                  if (!isChecked) {
                    delete checked[treeItem.value];
                  } else {
                    checked[treeItem.value] = treeItem;
                  }
                  setChecked({
                    ...checked,
                  });
                  sdMultiple?.(checked);
                }} />
              <Typography variant="caption">{treeItem.display}</Typography>
            </div>}
          >
            {renderTreeItem(treeItem.children)}
          </TreeItem>
        )
      } else {
        return (
          <TreeItem
            key={treeItem.value}
            nodeId={treeItem.value}
            label={treeItem.display}
          >
            {renderTreeItem(treeItem.children)}
          </TreeItem>
        )
      }
    })
  }

  const dfs = (treeItems: SdTreeViewOption<T>[] | undefined, nodeId: string): SdTreeViewOption<T> | null => {
    if (!treeItems?.length) {
      return null;
    }
    const found = treeItems.find(e => e.value === nodeId);
    if (found) {
      return found;
    }
    for (const treeItem of treeItems) {
      const found = dfs(treeItem.children, nodeId);
      if (found) {
        return found;
      }
    }
    return null;
  }

  return (
    <Box sx={{ ...Styles.container, ...sx } as SxProps}>
      <Box sx={Styles.search}>
        <SdInput value={searchText} sdChange={(value) => setSearchText(value)} fullWidth placeholder='Tìm kiếm' />
      </Box>
      <Box sx={Styles.tree}>
        <TreeView
          aria-label="rich object"
          defaultCollapseIcon={<ExpandMore />}
          defaultExpanded={['root']}
          defaultExpandIcon={<ChevronRight />}
          defaultEndIcon={<div style={{ width: 4, height: 4, border: '1px solid #637381', backgroundColor: '#637381', borderRadius: 16 }} />}
          sx={{ paddingX: '8px', flexGrow: 1, overflowY: 'auto' }}
          onNodeSelect={(event: any, nodeId: string) => {
            if (!event.target.matches('.MuiSvgIcon-root')) {
              sdSelect?.(dfs(items, nodeId));
            }
          }}
        >
          {options?.length > 0 && renderTreeItem(options)}
          {!options?.length && <TreeItem
            disabled
            key='EMPTY'
            nodeId='EMPTY'
            label='Không có dữ liệu'>
          </TreeItem>}
        </TreeView>
      </Box>
    </Box>
  );
}
