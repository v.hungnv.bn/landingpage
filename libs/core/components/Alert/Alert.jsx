import React from 'react';
import PropTypes from 'prop-types';

// import { history } from '../_helpers';
import { NotifyService, alertType } from '../../services';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';

const AlertM = React.forwardRef((props, ref) => (
    <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />
  ));

const propTypes = {
    id: PropTypes.string,
    fade: PropTypes.bool
};

const defaultProps = {
    id: 'default-alert',
    fade: true
};

class Alert extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alerts: []
        };
    }

    componentDidMount() {
        // subscribe to new alert notifications
        this.subscription = NotifyService.onAlert(this.props.id)
            .subscribe(alert => {
                // clear alerts when an empty alert is received
                if (!alert.message) {
                    // filter out alerts without 'keepAfterRouteChange' flag
                    const alerts = this.state.alerts.filter(x => x.keepAfterRouteChange);

                    // remove 'keepAfterRouteChange' flag on the rest
                    alerts.forEach(x => delete x.keepAfterRouteChange);

                    this.setState({ alerts });
                    return;
                }

                // add alert to array
                this.setState({ alerts: [...this.state.alerts, alert] });

                // auto close alert if required
                if (alert.autoClose) {
                    setTimeout(() => this.removeAlert(alert), 3000);
                }
            });

        // clear alerts on location change
        // this.historyUnlisten = history.listen(() => {
        //     alertService.clear(this.props.id);
        // });
    }

    componentWillUnmount() {
        // unsubscribe & unlisten to avoid memory leaks
        this.subscription.unsubscribe();
        // this.historyUnlisten();
    }

    removeAlert(alert) {
        if (this.props.fade) {
            // fade out alert
            const alertWithFade = { ...alert, fade: true };
            this.setState({ alerts: this.state.alerts.map(x => x === alert ? alertWithFade : x) });

            // remove alert after faded out
            setTimeout(() => {
                this.setState({ alerts: this.state.alerts.filter(x => x !== alertWithFade) })
            }, 250);
        } else {
            // remove alert
            this.setState({ alerts: this.state.alerts.filter(x => x !== alert) })
        }
    }

    render() {
        const { alerts } = this.state;
        if (!alerts.length) return null;
        return (
            <div>
                {alerts.map((alert, index) =>
                    <div key={index} >
                        <Snackbar anchorOrigin={{vertical: alert?.vertical || 'top', horizontal: alert?.horizontal || 'right'}}  open={true} autoHideDuration={3000} onClose={() => this.removeAlert(alert)}>
                            <AlertM onClose={() => this.removeAlert(alert)} severity={alert.type} sx={{ width: '100%' }}>
                                {alert.message}
                            </AlertM>
                        </Snackbar>
                    </div>
                )}
            </div>
        );
    }
}

Alert.propTypes = propTypes;
Alert.defaultProps = defaultProps;
export { Alert };