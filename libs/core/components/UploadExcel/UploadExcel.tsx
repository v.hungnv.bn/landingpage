import * as React from 'react';
import * as XLSX from "xlsx";

import { NotifyService } from '../../services';
import Table from '../table/Table'
import { useEffect, useRef, useState } from 'react';
import { EnhancedTableProps, HeadCell } from '../table/interface/table.interface';
import { SdModal } from '../SdModal/SdModal';

type AOA = Array<Array<any>>;

export interface SdUploadExcelOption<T = any> {
  title?: string;
  fileName?: string;
  columns: SdUploadExcelColumn[];
  key?: string;
}

interface SdUploadExcelColumn<T = any> {
  field: string;
  title: string;
  description?: string;
  transform?: (value: any, item: any) => string;
}

export type SdExcelItem<T = any> = {
  data: T;
  sd?: {
    excelIndex?: number;
    origin?: T
  }
};

interface IProps {
  option: SdUploadExcelOption;
  title: string;
  onClose: () => void;
  onAgree: (item: any) => void;
}

function CoreUploadExcel(props: IProps) {
  const inputUploadFile = useRef<any>(null);
  const [items, setItems] = useState<SdExcelItem[]>([]);
  const [openModal, setOpenModal] = React.useState<boolean>(true);
  const [config, setConfig] = useState<EnhancedTableProps>();


  useEffect(() => {
    setOpenModal(true);
    const headCells: HeadCell[] = [];
    props.option.columns.forEach(item => {
      headCells.push({
        field: item.field,
        type: 'string',
        label: item.title,
        transform: item.transform
      })
    })
    setConfig({
      headCells: headCells,
      hiddenPagination: true,
      fieldKey: 'id',
    })
  }, []);

  const updateFile = async (evt: any) => {
    try {
      const res = await convertData(evt);
      const data = res.items.filter((item, index) => index >= 2);
      setItems(data);
    } catch (error) { }

  }

  const convertData = (evt: any) => {
    return new Promise<{ items: any[], file: File }>((resolve, reject) => {
      try {
        const target: DataTransfer = evt.target;
        if (target.files.length !== 1) {
          NotifyService.warn('Không thể sử dụng nhiều file');
          throw new Error('Cannot use multiple files');
        }
        const file = target.files[0];
        const lastDot = file.name.lastIndexOf('.');
        if (lastDot === -1) {
          NotifyService.warn('Invalid file extension');
          throw new Error('Invalid file extension');
        }
        const extension = file.name.substring(lastDot + 1);
        if (extension.toLowerCase() !== 'xlsx') {
          NotifyService.warn('Vui lòng chọn file .xlsx');
          throw new Error('Please upload .xlsx file');
        }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
          const bstr: string = e.target.result;
          const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
          const wsname: string = wb.SheetNames[0];
          const ws: XLSX.WorkSheet = wb.Sheets[wsname];
          const lines: AOA = (XLSX.utils.sheet_to_json(ws, { header: 1 }));
          const records: any[] = [];
          const headers = lines[0];
          lines.splice(0, 1);
          lines.forEach(line => {
            const record: any = {};
            let hasValue = false;
            headers.forEach((header, index) => {
              record[header] = line[index];
              if (line[index] !== undefined && line[index] !== null && line[index].toString()) {
                hasValue = true;
              }
              if (typeof (record[header]) === 'string') {
                record[header] = record[header].trim();
              }
              if (record[header] === '' || record[header] === undefined) {
                record[header] = null;
              }
            });
            if (hasValue) {
              records.push(record);
            }
          });
          resolve({
            items: records,
            file: target.files[0]
          });
        };
        reader.readAsBinaryString(target.files[0]);
      } catch (error) {
        reject(error);
      }
    })
  }
  const handlerUploadFile = () => {
    inputUploadFile.current.click();
  }

  const buttonFooters: any[] = [
    { onClick: handlerUploadFile, label: 'Tải file lên' },
    { onClick: () => { props.onAgree(items) }, label: 'Tiếp tục', disabled: !items.length },
    { onClick: () => { props.onClose(), setOpenModal(false) }, label: 'Hủy bỏ', variant: "text", color: 'inherit', }]
  return (
    <>
      <SdModal opened={openModal} width={'1090px'}
        title={props.title} onClose={props.onClose}>
        <input onChange={(event) => { updateFile(event); }}
          type="file" hidden ref={inputUploadFile} id="uploadFile" />
        {config ? <Table notwAutoOnChaneFilter={true} Items={items} config={config}></Table> : ''}
      </SdModal>
    </>
  );
}

export default CoreUploadExcel;