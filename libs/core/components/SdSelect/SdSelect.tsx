import { useState, useEffect } from 'react';
import { FormControl, FormHelperText, InputLabel, MenuItem, Select, SxProps } from '@mui/material';
import { Controller, FieldValues, UseControllerProps, UseFormReturn } from 'react-hook-form';
import * as uuid from 'uuid';
export interface SdSelectOption<T> {
  value: string;
  display: string;
  data?: T;
}

export type SdSelectProps<T> = SdSelectNormalProps<T> | SdSelectBoolProps<T>

export interface SdSelectNormalProps<T> {
  sx?: SxProps;
  label?: string;
  value: string | undefined | null;
  items: SdSelectOption<T>[];
  sdChange: (item: SdSelectOption<T> | null, value?: string) => void;
  disabled?: boolean;
  form?: UseFormReturn<FieldValues, any>;
  rules?: UseControllerProps['rules']; // form
}

export interface SdSelectBoolProps<T> {
  sx?: SxProps;
  label?: string;
  value: boolean | undefined | null;
  items: {
    displayOnTrue: string;
    displayOnFalse: string;
  };
  sdChange: (item: SdSelectOption<T> | null, value?: boolean) => void;
  disabled?: boolean;
  form?: UseFormReturn<FieldValues, any>;
  rules?: UseControllerProps['rules']; // form
}

export function SdSelect<T = any>(props: SdSelectProps<T>) {
  const [options, setOptions] = useState<SdSelectOption<T>[]>([]);
  const [isBoolean, setIsBoolean] = useState(false);
  const [controlName, setControlName] = useState<string>('');

  const { sx, value, items, sdChange, label, disabled, form, rules } = props;
  useEffect(() => {
    if (Array.isArray(items)) {
      setOptions(items);
      setIsBoolean(false);
    } else {
      setOptions([{
        value: '1',
        display: items.displayOnTrue
      }, {
        value: '0',
        display: items.displayOnFalse
      }]);
      setIsBoolean(true);
    }
  }, [items]);
  useEffect(() => {
    if (form) {
      setControlName(uuid.v4());
    } else {
      setControlName(uuid.v4());
    }
  }, [form]);
  useEffect(() => {
    if (form && controlName) {
      const { setValue } = form;
      setValue(controlName, value ?? '');
    }
  }, [value, controlName]);
  if (form) {
    const { formState: { errors } } = form;
    const error = errors[controlName];
    let errorMessage = '';
    if (error) {
      if (error.type === 'required') {
        errorMessage = 'Dữ liệu không được để trống'
      }
    }
    return (<Controller
      name={controlName}
      control={form.control}
      rules={rules}
      render={({ field }) => (
        <FormControl
          id={controlName}
          sx={sx}
          fullWidth
          error={!!errorMessage}
          required={!!rules?.required}>
          {label && <InputLabel>{label}</InputLabel>}
          <Select
            {...field}
            label={label}
            disabled={disabled}
            size='small'
            value={typeof (value) === 'boolean' ? (value === true ? '1' : '0') : (value || '')}
            onChange={(event) => {
              const option = options.find(e => e.value === event.target.value);
              field.onChange(event);
              if (!isBoolean) {
                sdChange(option || null, option?.value as any);
              } else {
                sdChange(option || null, option?.value === '1' ? true : (option?.value === '0' ? false : undefined) as any);
              }
            }}
          >
            {
              !rules?.required && !isBoolean && <MenuItem key={'value-0'} value={''}>
                Vui lòng chọn
              </MenuItem>
            }
            {options.map((option, index) => <MenuItem key={option.value} value={option.value}>{option.display}</MenuItem>)}
          </Select>
          <FormHelperText>{errorMessage || ' '}</FormHelperText>
        </ FormControl>
      )}
    />)
  }
  return (
    <FormControl sx={{ m: 1, minWidth: 120, margin: 0 }} fullWidth >
      {label && <InputLabel>{label}</InputLabel>}
      <Select
        value={typeof (value) === 'boolean' ? (value === true ? '1' : '0') : (value || '')}
        label={label}
        disabled={disabled}
        size='small'
        onChange={(event) => {
          const option = options.find(e => e.value === event.target.value);
          if (!isBoolean) {
            sdChange(option || null, option?.value as any);
          } else {
            sdChange(option || null, option?.value === '1' ? true : (option?.value === '0' ? false : undefined) as any);
          }
        }}
      >
        {
          !rules?.required && !isBoolean && <MenuItem key={'value-0'} value={''}>
            Vui lòng chọn
          </MenuItem>
        }
        {options.map((option, index) => <MenuItem key={option.value} value={option.value}>{option.display}</MenuItem>)}
      </Select>
    </FormControl>
    // <Autocomplete
    //   disablePortal
    //   id="combo-box-demo"
    //   options={options}
    //   getOptionLabel={(option) => option.display}
    //   onChange={(event, newValue) => {
    //     sdChange(newValue);
    //   }}
    //   onInputChange={(event, newInputValue) => {
    //     console.log(newInputValue);
    //     setInputValue(newInputValue);
    //   }}
    //   inputValue={inputValue}
    //   sx={{ width: '100%' }}
    //   size="small"
    //   renderInput={(params) => <TextField {...params} label={label} required={required} size='small' />}
    // />
  );
}
