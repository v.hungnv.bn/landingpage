export type SdGridItem<T = any> = {
  data: T,
  sd: {
    selected?: boolean;
  }
}