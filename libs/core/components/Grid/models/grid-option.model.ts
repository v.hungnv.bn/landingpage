import { SdGridColumn } from "./grid-column.model";
import { SdGridCommand, SdGridMoreCommand } from "./grid-command.model";
import { SdGridDraggableOption } from "./grid-draggable.model";
import { SdGridSelectionOption } from "./grid-selection.model";

export type SdGridOption<T = any> = SdGridOptionLocal<T> | SdGridOptionServer<T>

export interface SdGridOptionLocal<T = any> {
  pagingServer?: false;
  trackBy?: string;
  items: SdLocalItems<T>;
  columns: SdGridColumn<T>[];
  commands?: SdGridCommand<T>[];
  moreCommands?: SdGridMoreCommand<T>[];
  checksum?: string;
  maxHeight?: string;
  selection?: SdGridSelectionOption<T>;
  draggable?: SdGridDraggableOption<T>;
}

export interface SdGridOptionServer<T = any> {
  pagingServer: true;
  trackBy?: string;
  items: SdServerItems<T>;
  columns: SdGridColumn<T>[];
  commands?: SdGridCommand<T>[];
  moreCommands?: SdGridMoreCommand<T>[];
  checksum?: string;
  maxHeight?: string;
  selection?: SdGridSelectionOption<T>;
}

export type SdLocalItems<T> = (() => T[] | Promise<T[]>);

export interface SdFilterOption {
  filter: Record<string, any>;
  pageNumber: number;
  pageSize: number;
  sort?: Record<string, 'asc' | 'desc'>;
}

export type SdServerItems<T> = (args: SdFilterOption) => Promise<{ items: T[], total: number }>;