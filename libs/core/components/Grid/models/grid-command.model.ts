export interface SdGridCommand<T> {
  icon?: any;
  disabled?: boolean;
  title: string;
  onClick: (item: T) => void;
  hidden?: boolean | ((item: T) => boolean);
}

export interface SdGridMoreCommand<T> {
  icon?: any;
  title: string;
  children: SdGridCommand<T>[];
}