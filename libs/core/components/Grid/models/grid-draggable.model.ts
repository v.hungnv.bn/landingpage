export interface SdGridDraggableOption<T = any> {
  visible: boolean;
  mapping: (items: T[]) => DraggableItem[];
  onDragEnd?: (newItems: T[]) => void;
}

export interface DraggableItem {
  key: string;
  primary: string;
  secondary?: string;
}

export const reorder = <T>(
  list: T[],
  startIndex: number,
  endIndex: number
): T[] => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};