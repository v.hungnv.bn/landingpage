export interface SdGridSelectionOption<T> {
  actions: SdGridSelectionAction<T>[];
  visible?: boolean;
  onSelect?: (rowData?: T, selectedItems?: T[]) => void;
  onSelectAll?: (selectedItems: T[]) => void;
}

export interface SdGridSelectionAction<T> {
  icon?: any;
  title: string;
  onClick: (selectedItems: T[]) => void;
  hidden?: boolean | ((item: T) => boolean);
  variant?: 'text' | 'contained' | 'outlined';
  color?: 'primary' | 'secondary' | 'success' | 'error' | 'info' | 'warning';
  children?: SdGridSelectionAction<T>[];
}