import { SdBadgeColor } from "../../Badge";

export type SdGridColumn<T = any> =
  SdGridColumnString<T> |
  SdGridColumnNumber<T> |
  SdGridColumnBool<T> |
  SdGridColumnDate<T> |
  SdGridColumnSelect<T>;

export interface SdGridColumnBase<T> {
  field: string;
  label: string;
  width?: string;
  transform?: (value: any, item: T) => string | number | undefined;
  template?: (value: any, item: T) => any;
  badgeColor?: (value: any, item: T) => SdBadgeColor | undefined;
  badgeIcon?: (value: any, item: T) => any;
  filter?: {
    disabled?: boolean;
  }
}

export interface SdGridColumnString<T> extends SdGridColumnBase<T> {
  type: 'string';
}

export interface SdGridColumnNumber<T> extends SdGridColumnBase<T> {
  type: 'number';
}

export interface SdGridColumnBool<T> extends SdGridColumnBase<T> {
  type: 'bool';
  option?: {
    displayOnTrue: string;
    displayOnFalse: string;
  };
}

export interface SdGridColumnDate<T> extends SdGridColumnBase<T> {
  type: 'date';
}

export interface SdGridColumnSelect<T> extends SdGridColumnBase<T> {
  type: 'values';
  option: {
    valueField: string,
    displayField: string,
    items: any[]
  };
}