import * as React from 'react';
import { TableCell } from "@mui/material";
import { SdGridColumn, SdGridItem } from '../models';
import { SdBadge } from '../../Badge';
import { CircleOutlined, FiberManualRecordOutlined } from '@mui/icons-material';

export function GridItem<T = any>(props: { column: SdGridColumn<T>, item: SdGridItem<T> }) {
  const { column, item } = props;
  const { transform, type, field, badgeColor, badgeIcon, template } = column;
  let value = (item.data as any)?.[field];
  if (template) {
    return <TableCell>
      {template(value, item.data)}
    </TableCell>;
  }
  let display = value;
  if (transform) {
    display = transform(value, item.data);
  } else {
    if (type === 'number') {
      display = Number.toVNCurrency(value);
    }
    if (type === 'bool') {
      const displayOnTrue = column.option?.displayOnTrue || 'Có';
      const displayOnFalse = column.option?.displayOnFalse || 'Không';
      if (typeof (value) === 'boolean') {
        display = <SdBadge icon={!value && <FiberManualRecordOutlined />} color={value ? 'success' : 'secondary'} text={value ? displayOnTrue : displayOnFalse} />
      }
    }
    if (type === 'date') {
      if (Date.isDate(value)) {
        // if (typeof (value) === 'string') {
        //   value = value?.replace('-', '/');
        // }
        const date = Date.toFormat(value, 'dd/MM/yyyy');
        const time = Date.toFormat(value, 'HH:mm');
        if (time !== '00:00') {
          display = `<span class="d-block T14R text-black400">${time}</span><span class="d-block T14R">${date}</span>`;
        } else {
          display = `<span class="d-block T14R">${date}</span>`;
        }
      }
    }
    if (type === 'values') {
      const { option: { items, valueField, displayField } } = column;
      const match = items?.find(e => e[valueField] === value);
      display = match?.[displayField] || value;
    }
  }
  if (badgeColor) {
    const color = badgeColor(value, item.data);
    const icon = badgeIcon?.(value, item.data);
    display = <SdBadge color={color} icon={icon} text={display} />
  }
  return (
    <TableCell>
      {type === 'date' && <div dangerouslySetInnerHTML={{ __html: display || '' }}></div>}
      {type !== 'date' && display}
    </TableCell>
  );
}

