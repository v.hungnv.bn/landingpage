import * as React from 'react';
import { SdGridCommand, SdGridItem } from '../models';
import Tooltip from "@mui/material/Tooltip";
import IconButton from "@mui/material/IconButton";

export function SdGridCommand<T = any>(props: { command: SdGridCommand<T>, item: SdGridItem<T> }) {
  const { command, item } = props;
  let visible = true;
  const { hidden, title, icon, onClick } = command;
  if (typeof (hidden) === 'boolean') {
    visible = !hidden;
  }
  if (typeof (hidden) === 'function') {
    visible = !hidden(item.data);
  }
  if (!visible) {
    return null;
  }
  return (
    <Tooltip
      key={title}
      title={title}
    >
      <IconButton onClick={() => onClick?.(item.data)}>
        {icon}
      </IconButton>
    </Tooltip>
  );
}

