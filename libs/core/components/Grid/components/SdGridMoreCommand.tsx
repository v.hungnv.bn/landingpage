import * as React from 'react';
import Tooltip from "@mui/material/Tooltip";
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import { SdGridCommand, SdGridItem, SdGridMoreCommand } from '../models';
import IconButton from "@mui/material/IconButton";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";

export function SdGridMoreCommand<T = any>(props: { moreCommand: SdGridMoreCommand<T>, item: SdGridItem<T> }) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const { moreCommand, item } = props;
  const children: SdGridCommand<T>[] = [];
  for (const command of (moreCommand?.children || [])) {
    let visible = true;
    const { hidden } = command;
    if (typeof (hidden) === 'boolean') {
      visible = !hidden;
    }
    if (typeof (hidden) === 'function') {
      visible = !hidden(item.data);
    }
    if (visible) {
      children.push(command);
    }
  }

  if (!children.length) {
    return null;
  }
  const { title, icon } = moreCommand;
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <Tooltip
        key={title}
        title={title}
      >
        <IconButton onClick={handleClick}>
          {icon || <MoreHorizIcon />}
        </IconButton>
      </Tooltip>
      <Menu
        id={`${title}-menu`}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        {
          children.map(command => <MenuItem key={command.title} onClick={() => {
            command?.onClick(item.data);
            handleClose();
          }}>
            {command.icon && (<ListItemIcon>{command.icon}</ListItemIcon>)}
            <ListItemText>{command.title}</ListItemText>
          </MenuItem>)
        }
      </Menu>
    </div>
  )
  // return (
  //   <>
  //     <div className="position-relative">
  //       <Tooltip
  //         key={title}
  //         title={title}
  //       >
  //         <IconButton onClick={() => setOpened(!opened)}>
  //           {icon || <MoreHorizIcon />}
  //         </IconButton>
  //       </Tooltip>
  //     </div>
  //     {
  //       opened && (
  //         <div
  //           ref={refCommand}
  //           className="position-absolute item-command-table"
  //         >
  //           {children.map(command => (
  //             <div
  //               key={command.title}
  //               onClick={() => {
  //                 setOpened(false);
  //                 command.onClick(item);
  //               }}
  //               className="d-flex align-items-center item-command-table-action"
  //             >
  //               {command.icon}
  //               <span className="px-5">{command.title}</span>
  //             </div>
  //           ))}
  //         </div>
  //       )
  //     }
  //   </>
  // );
}

