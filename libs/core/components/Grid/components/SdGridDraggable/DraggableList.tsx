import * as React from 'react';
import DraggableListItem from './DraggableListItem';
import {
  DragDropContext,
  Droppable,
  DropResult,
  OnDragEndResponder
} from 'react-beautiful-dnd';
import { reorder, SdGridDraggableOption, SdGridItem } from '../../models';

export type DraggableListProps<T = any> = {
  items: SdGridItem<T>[];
  onDragEnd: (newItems: SdGridItem<T>[]) => void;
  option?: SdGridDraggableOption;
};

const DraggableList = React.memo(({ option, items, onDragEnd }: DraggableListProps) => {
  if (!option?.visible || !option?.mapping) {
    return null;
  }
  const draggableItems = option.mapping(items.map(e => e.data));
  return (
    <DragDropContext onDragEnd={({ destination, source }: DropResult) => {
      // dropped outside the list
      if (!destination) {
        return;
      }
      const newItems = reorder(items, source.index, destination.index);
      onDragEnd(newItems);
    }}>
      <Droppable droppableId="droppable-list">
        {provided => (
          <div ref={provided.innerRef} {...provided.droppableProps}>
            {draggableItems.map((item, index) => (
              <DraggableListItem item={item} index={index} key={item.key} />
            ))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
});

export default DraggableList;
