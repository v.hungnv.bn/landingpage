import * as React from 'react';
import { SdGridCommand, SdGridItem, SdGridSelectionOption } from '../models';
import SdQuickAction from '../../SdQuickAction/SdQuickAction';
import { Stack, Box, ListItemIcon, ListItemText, Menu, MenuItem, SxProps } from '@mui/material';
import { MoreHoriz, Close } from '@mui/icons-material';
import { SdButton } from '../../SdButton/SdButton';

export interface SdGridQuickActionProps<T> {
  selection: SdGridSelectionOption<T> | undefined;
  items: SdGridItem<T>[];
  checked: Record<string, boolean>;
  trackBy: string;
  onClose: () => void;
}

export function SdGridQuickAction<T = any>(props: SdGridQuickActionProps<T>) {
  const { selection, items, checked, trackBy, onClose } = props;
  const [opened, setOpened] = React.useState<boolean>(false);
  const [selectedItems, setSelectedItems] = React.useState<SdGridItem<T>[]>([]);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const openMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const closeMenu = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);
  React.useEffect(() => {
    if (!selection?.visible || !selection?.actions?.length) {
      setOpened(false);
    } else {
      const selectedItems = items.filter(e => checked[(e.data as any)?.[trackBy]]);
      setSelectedItems(selectedItems);
      setOpened(!!selectedItems?.length)
    }
  }, [selection, items, checked]);
  return (
    <SdQuickAction
      opened={opened}
      message={<div className="d-flex align-items-center">
        <Box sx={Styles.bgLength}>
          <Box sx={Styles.length}>{selectedItems.length}</Box>
        </Box>
        <Box sx={Styles.message}>dữ liệu được chọn</Box>
      </div>}
      action={<Stack sx={{ pr: '8px' }} direction="row" spacing={1}>
        {
          selection?.actions?.map(action => (
            <>
              <SdButton
                key={action.title}
                label={action.title}
                icon={action.icon || (action.children?.length ? <MoreHoriz /> : undefined)}
                size={'small'}
                variant={action.variant}
                color={action.color}
                onClick={(event) => {
                  if (action.children?.length) {
                    openMenu(event);
                  } else {
                    action.onClick?.(selectedItems.map(e => e.data));
                  }
                }} />
              {
                !!action.children?.length && <Menu
                  id={`${action.title}-menu`}
                  anchorEl={anchorEl}
                  open={open}
                  onClose={closeMenu}
                >
                  {
                    action.children.map(childAction => <MenuItem key={childAction.title} onClick={() => {
                      childAction.onClick?.(selectedItems.map(e => e.data));
                      closeMenu();
                    }}>
                      {childAction.icon && (<ListItemIcon key={childAction.title}>{childAction.icon}</ListItemIcon>)}
                      <ListItemText key={childAction.title}>{childAction.title}</ListItemText>
                    </MenuItem>)
                  }
                </Menu>
              }
            </>
          ))
        }
        <SdButton onClick={onClose} icon={<Close />} variant='outlined' />
      </Stack>
      }
    />
  );
}


const Styles: Record<string, SxProps> = {
  bgLength: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    padding: '12px 8px',
    position: 'static',
    width: 'auto',
    minWidth: '48px',
    height: '48px',
    left: 0,
    top: 0,
    background: '#2962FF',
    borderRadius: '4px 0px 0px 4px'
  },
  length: {
    position: 'static',
    width: 'auto',
    minWidth: '32px',
    height: '24px',
    left: '8px',
    top: '12px',
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 500,
    fontSize: '16px',
    lineHeight: '24px',
    textAlign: 'center',
    color: '#FFF'
  },
  message: {
    position: 'static',
    minWidth: '200px',
    height: '20px',
    left: 0,
    top: 6,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '14px',
    lineHeight: '20px',
    color: '#000000',
    margin: '0 0 0 16px'
  }
}