import * as React from 'react';
import { visuallyHidden } from '@mui/utils';

import {
  TextField,
  Box,
  TableRow,
  TableCell,
  TableSortLabel,
  TableHead,
  Select,
  MenuItem,
  SxProps
} from "@mui/material";
import { SdGridColumn } from '../models';
import SdDate from '../../SdDate/SdDate';

const Styles: Record<string, SxProps> = {
  selection: {
    backgroundColor: '#f2f3f4',
    width: '50px'
  },
  tr: {
    backgroundColor: '#f2f3f4'
  },
  th: {
    backgroundColor: '#f2f3f4',
    fontWeight: 500
  },
  bgWhite: {
    backgroundColor: '#FFF'
  }
};

interface SdGridHeaderProps<T> {
  columns: SdGridColumn<T>[];
  onFilter: (filter: Record<string, string | number | boolean>, sort?: Record<string, 'asc' | 'desc'>) => void;
  hasCommand?: boolean;
  hasSelection?: boolean;
}

export function SdGridHeader<T>(props: SdGridHeaderProps<T>) {
  const [filter, setFilter] = React.useState<Record<string, string | number | boolean>>({});
  const [sort, setSort] = React.useState<Record<string, 'asc' | 'desc'>>({});
  const { columns, onFilter, hasCommand, hasSelection } = props;
  const orderBy = Object.keys(sort)?.[0];
  const order = Object.values(sort)?.[0];
  const reload = (filter: Record<string, string | number | boolean>, sort: Record<string, 'asc' | 'desc'>) => {
    onFilter({
      ...filter
    }, sort);
  }
  return (
    <TableHead sx={Styles.tr}>
      <TableRow>
        {hasSelection && <TableCell padding="checkbox" sx={Styles.selection}></TableCell>}
        {columns.length && columns.map((column) => (
          <TableCell
            sx={Styles.th}
            key={column.field}
            align='left'
            style={{ width: column.width || undefined }}
            padding='normal'
            sortDirection={orderBy === column.field ? order : false}>
            <TableSortLabel
              active={orderBy === column.field}
              direction={!orderBy ? undefined : (orderBy === column.field ? order : 'asc')}
              onClick={(e) => {
                if (!sort[column.field]) {
                  setSort({
                    [column.field]: 'asc'
                  });
                } else if (sort[column.field] === 'asc') {
                  setSort({
                    [column.field]: 'desc'
                  });
                } else if (sort[column.field] === 'desc') {
                  setSort({});
                }
                reload(filter, sort);
              }}>
              {column.label}
              {orderBy === column.field ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
            {column.type === 'string' && <Box>
              <TextField
                sx={{ ...(!column.filter?.disabled && Styles.bgWhite) }}
                fullWidth
                variant="outlined"
                size='small'
                hiddenLabel
                value={filter[column.field]}
                onChange={(event) => {
                  filter[column.field] = event.target.value;
                  setFilter({ ...filter });
                }}
                disabled={column.filter?.disabled}
                onKeyDown={(e) => {
                  if (e.keyCode == 13) {
                    reload(filter, sort);
                  }
                }} /></Box>
            }
            {column.type === 'number' && <Box style={{ padding: '4px' }}>
              <TextField
                sx={Styles.bgWhite}
                fullWidth
                variant="outlined"
                size='small'
                hiddenLabel
                value={filter[column.field] || ''}
                type='number'
                disabled={column.filter?.disabled}
                onChange={(event) => {
                  filter[column.field] = event.target.value;
                  setFilter({ ...filter });
                  reload(filter, sort);
                }}
                onKeyDown={(e) => {
                  if (e.keyCode == 13) {
                    reload(filter, sort);
                  }
                }} /></Box>
            }
            {column.type === 'date' && <Box>
              <SdDate
                sx={{ ...(!column.filter?.disabled && Styles.bgWhite) }}
                value={filter[column.field]?.toString()}
                required={false}
                disabled={column.filter?.disabled}
                sdChange={(value => {
                  if (!value) {
                    delete filter[column.field];
                  } else {
                    filter[column.field] = value;
                  }
                  setFilter({ ...filter });
                  reload(filter, sort);
                })}
              /></Box>
            }
            {column.type === 'bool' &&
              <Select
                sx={{ ...(!column.filter?.disabled && Styles.bgWhite) }}
                fullWidth
                value={filter[column.field] === true ? '1' : (filter[column.field] === false ? '0' : '')}
                size='small'
                disabled={column.filter?.disabled}
                onChange={e => {
                  if (e.target.value === '1') {
                    filter[column.field] = true;
                  } else if (e.target.value === '0') {
                    filter[column.field] = false;
                  } else {
                    delete filter[column.field];
                  }
                  setFilter({ ...filter });
                  reload(filter, sort);
                }}
              >
                <MenuItem value={''}>Vui lòng chọn</MenuItem>
                <MenuItem value='1'>{column.option?.displayOnTrue || 'Có'}</MenuItem>
                <MenuItem value='0'>{column.option?.displayOnFalse || 'Không'}</MenuItem>
              </Select>
            }
            {column.type === 'values' &&
              <Select
                sx={{ ...(!column.filter?.disabled && Styles.bgWhite) }}
                fullWidth
                value={filter[column.field] ?? ''}
                size='small'
                onChange={e => {
                  if (e.target.value === '') {
                    delete filter[column.field];
                  } else {
                    filter[column.field] = e.target.value;
                  }
                  setFilter({ ...filter });
                  reload(filter, sort);
                }}
              >
                <MenuItem value={''}>Vui lòng chọn</MenuItem>
                {
                  column.option.items.map((item: any) => {
                    const valueField = column.option.valueField;
                    const displayField = column.option.displayField;
                    // const { badgeColor, badgeIcon } = column;
                    if (typeof (item) === 'object' && valueField && displayField) {
                      return <MenuItem key={item[valueField]}
                        value={item[valueField]}
                      >{item[displayField]}
                      </MenuItem>;
                    }
                    return <MenuItem key={item} value={item}>{item}</MenuItem>;
                  })
                }
              </Select>
            }
          </TableCell>
        ))}
        {hasCommand && <TableCell padding="checkbox" sx={Styles.th}></TableCell>}
      </TableRow>
    </TableHead>
  );
}

