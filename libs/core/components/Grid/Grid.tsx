import * as React from "react";
import { Paper, Box, Table, TableBody, TableCell, TableContainer, TableRow, TablePagination, Stack } from "@mui/material"
import { useCallback, useEffect, useRef, useState } from "react";
import { GridItem } from "./components/SdGridItem";
import { SdGridHeader } from "./components/SdGridHeader";
import { SdGridCommand } from "./components/SdGridCommand";
import { SdGridMoreCommand } from "./components/SdGridMoreCommand";
import { Checkbox, SxProps } from "@mui/material";
import { SdGridItem, SdGridOption } from "./models";
import { SdGridQuickAction } from "./components/SdGridQuickAction";
import { SdButton } from "../SdButton/SdButton";
import { Done, Sort } from "@mui/icons-material";
import { SdModal, SdModalRef } from "../SdModal/SdModal";
import DraggableList from "./components/SdGridDraggable/DraggableList";
import { DropResult } from "react-beautiful-dnd";

const Styles: Record<string, SxProps> = {
  tableContainer: {
    overflowX: 'initial'
  }
};

export interface SdGridRef<T = any> {
  reload: () => void
}

function SdGridWithRef<T>(
  props: SdGridOption<T>,
  ref: React.ForwardedRef<SdGridRef<T>>
) {
  const { pagingServer, items, columns, commands, moreCommands, checksum, maxHeight, selection } = props;
  const [localItems, setLocalItems] = React.useState<SdGridItem[]>([]);
  const [filter, setFilter] = React.useState<Record<string, string | number | boolean>>({});
  const [sort, setSort] = React.useState<Record<string, "desc" | "asc"> | undefined>(undefined);
  const [renderedItems, setRenderedItems] = React.useState<SdGridItem[]>([]);
  const [total, setTotal] = React.useState<number>(0);
  const [checked, setChecked] = React.useState<Record<string, boolean>>({});
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);
  const [showDraggable, setShowDraggable] = useState(false);
  const [isFirstLoad, setIsFirstLoad] = useState(true);
  const trackBy = props?.trackBy || 'id';
  const draggable = !pagingServer && props?.draggable?.visible;
  React.useImperativeHandle(ref, () => ({
    reload() {
      reload(true);
    },
    get localItems() {
      return localItems.map(e => e.data);
    }
  }));

  useEffect(() => {
    if(isFirstLoad) {
      reload(true);
      setIsFirstLoad(false);
    } else {
      reload();
    }
  }, [pagingServer, filter, sort, pageSize, pageNumber, checksum]);

  // useEffect(() => {
  //   reload(true);
  // }, [items]);

  // useCallback(()=> {
  //   reload(true);
  // }, [items]);

  useEffect(() => {
    if (!pagingServer) {
      reload();
    }
  }, [localItems]);

  const loadLocalItems = async () => {
    if (!pagingServer) {
      const data = items();
      if (data instanceof Promise) {
        const results = await data.catch(err => {
          console.error(err);
          return [];
        });
        const gridItems = (results || []).map(mapToGridItem);
        setLocalItems(gridItems);
        return gridItems;
      } else {
        const gridItems = (data || []).map(mapToGridItem);
        setLocalItems(gridItems);
        return gridItems;
      }
    }
    return [];
  }

  const reload = async (force = false) => {
    if (!pagingServer) {
      if (force) {
        await loadLocalItems();
      } else {
        let filteredItems = localItems;
        // Filter
        for (const key of Object.keys(filter || {})) {
          const value = filter[key];
          if (typeof (value) === 'string' || typeof (value) === 'number') {
            const temp = filteredItems.map(e => e.data).search(value, key);
            filteredItems = filteredItems.filter(e => temp.includes(e.data));
          }
          if (typeof (value) === 'boolean') {
            filteredItems = filteredItems.filter(e => e.data[key] === value);
          }
        }
        // Sort
        const orderBy = Object.keys(sort || {})?.[0];
        if (orderBy) {
          const order = sort?.[orderBy];
          filteredItems.sort((a, b) => {
            if (order === 'asc') {
              if (a.data?.[orderBy] > b.data?.[orderBy]) {
                return 1;
              }
              if (a.data?.[orderBy] < b.data?.[orderBy]) {
                return -1;
              }
              return 0;
            }
            if (order === 'desc') {
              if (a.data?.[orderBy] > b.data?.[orderBy]) {
                return -1;
              }
              if (a.data?.[orderBy] < b.data?.[orderBy]) {
                return 1;
              }
              return 0;
            }
            return 0;
          });
        }
        setTotal(filteredItems.length);
        setRenderedItems(filteredItems.filter((item, index) => {
          return index >= pageNumber * pageSize
            && index < (pageNumber + 1) * pageSize;
        }))
      }
    } else {
      // Gọi api xử lý phân trang và filter
      const res = await items({ filter, sort, pageNumber, pageSize }).catch(err => {
        console.error(err);
        return {
          items: [] as T[],
          total: 0
        }
      });
      setRenderedItems((res?.items || []).map(mapToGridItem));
      setTotal(res?.total || 0);
    }
  }

  const mapToGridItem = (item: any): SdGridItem => ({
    data: item,
    sd: {
      selected: false
    }
  })
  const hasCommand = !!commands?.length || !!moreCommands?.length;
  const hasSelection = !!selection?.visible;
  return (
    <Box sx={{ width: "100%", maxHeight: maxHeight }}>
      <Paper sx={{ width: "100%" }}>
        {
          draggable && showDraggable && <><DraggableList option={props.draggable} items={localItems} onDragEnd={(newItems) => {
            setLocalItems(newItems);
            reload();
            props.draggable?.onDragEnd?.(newItems.map(e => e.data));
          }} /><Stack paddingY={1} paddingX={1} spacing={1} direction={'row'} alignItems={'center'} justifyContent={'flex-end'}>
              <SdButton label="Đóng" size="small" onClick={() => {
                setShowDraggable(false);
              }} />
            </Stack></>
        }
        {
          !showDraggable && <><TableContainer sx={Styles.tableContainer}>
            <Table size='small' stickyHeader>
              <SdGridHeader
                columns={columns}
                hasCommand={hasCommand}
                hasSelection={hasSelection}
                onFilter={(filter, sort) => {
                  setFilter(filter);
                  setSort(sort);
                }} />
              <TableBody>
                {renderedItems.map(item => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={item.data[trackBy]}
                    >
                      {hasSelection && (
                        <TableCell padding="checkbox">
                          <Checkbox
                            color="primary"
                            checked={!!checked[item.data[trackBy]]}
                            value
                            onChange={(event, isChecked) => {
                              setChecked({
                                ...checked,
                                [item.data[trackBy]]: isChecked
                              });
                            }} />
                        </TableCell>
                      )}
                      {columns.map(column => (
                        <GridItem key={`${item.data[trackBy]}-${column.field}`} item={item} column={column} />
                      ))}
                      {hasCommand && (
                        <TableCell padding="checkbox">
                          <div className="d-flex align-items-center">
                            {commands?.length &&
                              <React.Fragment>
                                {commands?.map(command => <SdGridCommand key={command.title} command={command} item={item} />)}
                              </React.Fragment>}
                            {moreCommands?.length &&
                              <React.Fragment>
                                {moreCommands.map(moreCommand => <SdGridMoreCommand key={moreCommand.title} moreCommand={moreCommand} item={item} />)}
                              </React.Fragment>}
                          </div>
                        </TableCell>
                      )}
                    </TableRow>
                  );
                })}
              </TableBody>
              {!renderedItems?.length && (
                <TableBody>
                  <TableRow>
                    <TableCell colSpan={100}>
                      <div className="d-flex align-items-center justify-content-center py-5 w-100 text-color-dark">
                        Không có dữ liệu
                      </div>
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer><Stack direction={'row'} alignItems={'center'} justifyContent={'space-between'}>
              <Stack marginLeft={1} spacing={1} direction={'row'} alignItems={'center'}>
                {draggable && <SdButton icon={<Sort />} label="Sắp xếp" size="small" variant="text" onClick={() => {
                  setShowDraggable(true);
                }} />}
              </Stack>
              <TablePagination
                rowsPerPageOptions={[10, 20, 50]}
                // component="div"
                count={total}
                rowsPerPage={pageSize}
                page={pageNumber}
                labelRowsPerPage={''}
                onPageChange={(event: React.MouseEvent<HTMLButtonElement> | null, page: number) => {
                  setPageNumber(page);
                }}
                onRowsPerPageChange={(event) => {
                  setPageSize(+event.target.value || 10);
                }} />
            </Stack></>
        }
      </Paper>
      <SdGridQuickAction
        checked={checked}
        items={renderedItems}
        selection={selection}
        trackBy={trackBy}
        onClose={() => {
          setChecked({});
        }}
      />

    </Box>
  );
}

export const SdGrid = React.forwardRef(SdGridWithRef) as
  <T>(p: SdGridOption<T> & { ref?: React.ForwardedRef<SdGridRef<T>> }) => React.ReactElement;