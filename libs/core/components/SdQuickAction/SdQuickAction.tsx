import { Box, SxProps } from '@mui/material';
import { useState, useEffect, MouseEventHandler } from 'react';

export interface SdQuickActionProps {
  opened?: boolean;
  message?: React.ReactNode;
  action?: React.ReactNode;
}

const Styles: Record<string, SxProps> = {
  container: {
    boxShadow: '0px 0.4px 0.8px rgba(0, 0, 0, 0.1), 0px 3px 6px rgba(0, 0, 0, 0.2)',
    borderRadius: '4px',
    position: 'fixed',
    minWidth: '400px',
    width: 'max-content',
    backgroundColor: '#fff',
    bottom: '90px',
    left: 0,
    right: 0,
    margin: 'auto',
    visibility: 'hidden',
    opacity: 0,
    pointerEvents: 'none',
    transition: 'all 0.2s ease-in-out',
    transform: 'translate3d(0, 100%, 0)',
    zIndex: '99',
    alignItems: 'center',
    display: 'flex'
  },
  active: {
    transform: 'translate3d(0, 0, 0)',
    opacity: '1',
    visibility: 'visible',
    pointerEvents: 'all'
  }
}

export default function SdQuickAction(props: SdQuickActionProps) {
  const { opened, message, action } = props;
  const [sxStyle, setSxStyle] = useState<SxProps>(Styles.container);
  useEffect(() => {
    if (opened) {
      setSxStyle({
        ...Styles.container,
        ...Styles.active
      } as any);
    } else {
      setSxStyle({
        ...Styles.container
      });
    }
  }, [opened])
  return (
    <Box sx={sxStyle}>
      <Box sx={{ marginRight: '16px' }}>
        {message}
      </Box>
      {action}
    </Box>
  );
}
