import PropTypes from 'prop-types';
import MuiButton from '@mui/material/Button';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CircularProgress from '@mui/material/CircularProgress';
import { useEffect, useState } from 'react'

function CoreButton({
  variant = 'contained',
  children,
  onClick,
  disabled,
  href,
  color,
  textColor,
  bgColor,
  size,
  iconLeft,
  iconRight,
  loading,
  disableElevation,
  width,
}) {
  let theme
  if (textColor && bgColor) {
    theme = createTheme({
      palette: {
        colorCustom: {
          main: textColor,
          contrastText: bgColor,
        },
      }
    });
  }
  const [sizeLoading, setSizeLoading] = useState('blue')

  useEffect(() => {
    switch (size) {
      case 'large':
        setSizeLoading(22);
        break;
      case 'medium':
        setSizeLoading(22);
        break;
      default:
        setSizeLoading(18);
        break;
    }
  }, [])


  return (
    <div>
      {theme ? <ThemeProvider theme={theme}>
        <Button style={{ textTransform: 'none', width: width }}
          variant={variant}
          disabled={loading || disabled}
          href={href}
          disableElevation={disableElevation}
          color={'colorCustom'}
          size={size}
          startIcon={!loading && iconLeft}
          endIcon={loading ? <CircularProgress color='inherit' size={sizeLoading} /> : iconRight}
          onClick={() => {
            onClick();
          }}>
          {children}
        </Button>
      </ThemeProvider> :
        <MuiButton style={{ textTransform: 'none', width: width }}
          variant={variant}
          disabled={loading || disabled}
          href={href}
          disableElevation={disableElevation}
          color={color}
          size={size}
          startIcon={!loading && iconLeft}
          endIcon={loading ? <CircularProgress color='inherit' size={sizeLoading} /> : iconRight}
          onClick={() => {
            onClick();
          }}>
          {children}
        </MuiButton>
      }
    </div>

  );
}

CoreButton.propTypes = {
  variant: PropTypes.oneOf(['text', 'contained', 'outlined', undefined]),
  disabled: PropTypes.bool,
  disableElevation: PropTypes.bool,
  loading: PropTypes.bool,
  href: PropTypes.string,
  color: PropTypes.string,
  textColor: PropTypes.string,
  width: PropTypes.string,
  bgColor: PropTypes.string,
  size: PropTypes.oneOf(['large', 'medium', 'small', undefined]),
  iconLeft: PropTypes.node,
  iconRight: PropTypes.node,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
};

export const Button = CoreButton;
