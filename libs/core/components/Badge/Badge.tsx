import * as React from 'react';
import Box from "@mui/material/Box";
import { SdBadgeColor } from './Badge.model';
import { Stack, SxProps } from "@mui/material";
import { Circle, FiberManualRecord } from '@mui/icons-material';

const Styles: Record<string, SxProps> = {
  text: {
    color: 'rgba(0,0,0,.87);'
  }
};

export function SdBadge(props: { color?: SdBadgeColor, icon?: any, text?: string | number }) {
  const { color, icon, text } = props;
  const badgeColor = color || 'secondary';
  if (!text) {
    return null;
  }
  return (
    <Stack className={`badge ${badgeColor}`} display={'flex!important'} direction={'row'} alignItems={'center'} spacing={1}>
      {
        icon || <FiberManualRecord />
      }
      <Box sx={Styles.text} className="T14R">
        {text}
      </Box>
    </Stack>
    // <div
    //   className={`badge ${badgeColor} d-flex align-items-center`}
    // >
    //   {
    //     icon || <CircleIcon />
    //   }
    //   <Box sx={Styles.text} className="ml-8 T14R badge-title">
    //     {text}
    //   </Box>
    // </div>
  );
}

