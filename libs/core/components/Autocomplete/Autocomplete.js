import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import { v4 as uuidv4 } from 'uuid';

function CoreAutocomplete() {
  return (
    <Autocomplete {...props} />
  );
}
CoreAutocomplete.propTypes = {
  disablePortal: PropTypes.bool,
  id: PropTypes.string,
  options: PropTypes.oneOfType([PropTypes.array, PropTypes.func]).isRequired,
};

export default CoreAutocomplete;