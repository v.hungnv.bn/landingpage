import PropTypes from 'prop-types';
import * as React from 'react';
import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { useState, useEffect } from "react";
import moment from 'moment';

function CoreDate({
  label,
  type,
  model,
  onChange,
  required=false
}) {

  const [date, setDate] = useState('');

  useEffect(() => {
    setDate(model);
  }, [model]);

  const handleChange = (newValue) => {
    if(newValue){
      onChange && onChange(moment(newValue.toString()).format('yyyy-MM-DD HH:mm:ss'));
      setDate(newValue);
      return;
    }
    onChange && onChange('');
    setDate('');
  }
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      {type === 'date' ? <DatePicker
        inputFormat="DD/MM/YYYY"
        label={label}
        value={date}
        onChange={handleChange}
        renderInput={(params) => <TextField required={required} sx={{ marginTop: '8px' }} fullWidth size="small" {...params} />}
      /> : <DateTimePicker
        inputFormat="DD/MM/YYYY HH:mm:ss"
        renderInput={(props) => <TextField required={required} sx={{ marginTop: '8px' }} fullWidth size="small"  {...props} />}
        label={label}
        value={date}
        onChange={handleChange}
      />}
    </LocalizationProvider>);
}
CoreDate.propTypes = {
  type: PropTypes.string, //date or date-time
  label: PropTypes.string,
  model: PropTypes.node,
  required: PropTypes.bool,
  onChange: PropTypes.func,
};

export default CoreDate;
