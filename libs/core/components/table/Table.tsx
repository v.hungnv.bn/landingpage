import * as React from "react";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Checkbox from "@mui/material/Checkbox";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import Switch from "@mui/material/Switch";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import EnhancedTableHead from "./EnhancedTableHead";
import { EnhancedTableProps, SdFindReq } from "./interface/table.interface";
import { useEffect, useState } from "react";
import { useOnClickOutside } from "../../services";
import ChipInput from "../ChipInput/ChipInpit";
import CircleIcon from "@mui/icons-material/Circle";
import { Button } from "../Button/Button";
import { Input } from "../Input/Input";

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

type Order = "asc" | "desc";

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key
): (
  a: { [key in Key]: number | string },
  b: { [key in Key]: number | string }
) => number {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(
  array: readonly T[],
  comparator: (a: T, b: T) => number
) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

export default function coreTable(props: {
  Items: any[];
  config: EnhancedTableProps;
  pageLength?: number;
  onSwitch?: (item: any, field: string, event: boolean)=> void;
  onChangeFilter?: (filters: SdFindReq) => void;
  notwAutoOnChaneFilter?: boolean;
}) {
  const { config, Items, pageLength, notwAutoOnChaneFilter, onChangeFilter , onSwitch} = props;

  const [order, setOrder] = React.useState<Order>("asc");
  const [listData, setListData] = React.useState<any[]>([]);
  const [orderBy, setOrderBy] = React.useState("");
  const [selected, setSelected] = React.useState<readonly string[]>([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(config.pageSize || 20);
  const [filters, setFilters] = React.useState<SdFindReq>({
    pageIndex: 0,
    pageSize: rowsPerPage,
    filters: [],
    sorts: [],
  });
  const [idSelect, setIdSelect] = React.useState("");
  const refCommand = React.useRef() as React.MutableRefObject<HTMLInputElement>;

  useEffect(() => {
    setListData(Items);
    setSelected(config.arrayCheckDefault || []);
  }, [Items]);

  useEffect(() => {
    if(notwAutoOnChaneFilter){
      onChangeFilter && onChangeFilter(filters);
    }
  }, []);

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: any
  ) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
    if (config.sortLocal) {
      setListData(stableSort(listData, getComparator(order, property)));
      return;
    }
    const filterCopy = filters;
    filterCopy.sorts = [
      {
        field: property,
        direction: order,
      },
    ];
    setPage(0);
    setFilters({ ...filterCopy, pageIndex: 0, pageSize: rowsPerPage });
    onChangeFilter &&
      onChangeFilter({ ...filterCopy, pageIndex: 0, pageSize: rowsPerPage });
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelected = listData.map((n) => n[config.fieldKey]);
      setSelected(newSelected);
      config.onSetSelect && config.onSetSelect(newSelected);
      return;
    }
    setSelected([]);
    config.onSetSelect && config.onSetSelect([]);
  };

  const handleSelect = (event: React.MouseEvent<unknown>, item: any) => {
    const itemKey = item[config.fieldKey];
    const selectedIndex = selected.indexOf(itemKey);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, itemKey);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
    config.onSetSelect && config.onSetSelect(item, selectedIndex === -1 ? 'check' : 'uncheck');
  };

  const handleClick = (event: React.MouseEvent<unknown>, item: any) => {
    if (!config.checked) {
      return;
    }
    const selectedIndex = selected.indexOf(item);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, item);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
    config.onSetSelect && config.onSetSelect(newSelected);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setFilters({ ...filters, pageIndex: newPage });
    onChangeFilter && onChangeFilter({ ...filters, pageIndex: newPage });
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const pageSize = parseInt(event.target.value, rowsPerPage);
    setRowsPerPage(parseInt(event.target.value));
    setFilters({ ...filters, pageIndex: 0, pageSize: pageSize });
    onChangeFilter &&
      onChangeFilter({ ...filters, pageIndex: 0, pageSize: pageSize });
    setPage(0);
  };

  const isSelected = (item: any) => selected.indexOf(item) !== -1;

  const handleFilter = (field: string, data: any, operator?: string) => {
    const filterCopy: any = filters;
    const filterField = filterCopy.filters.find(
      (item: any) => item.field === field
    );
    const index = filterCopy.filters.indexOf(filterField);
    if (index > -1) {
      filterCopy.filters.splice(index, 1);
    }
    if (data) {
      filterCopy.filters.push({
        field: field,
        value: data,
        operator: operator,
      });
    }
    setFilters({ ...filterCopy });
    onChangeFilter && onChangeFilter({ ...filterCopy });
  };
  const handleAddItem = () => {
    let item: any = {};
    config.headCells.forEach((headCell) => {
      switch (headCell.type) {
        case "switch":
          item[headCell.field] = true;
          break;
        case "tag":
          item[headCell.field] = [];
          break;
        default:
          item[headCell.field] = "";
          break;
      }
    });
    item.isAdd = true;
    listData.unshift(item);
    setListData([...listData]);
  };

  useOnClickOutside(refCommand, () => setIdSelect(""));

  return (
    <Box sx={{ width: "100%" }}>
      <Paper sx={{ width: "100%", mb: 2 }}>
        <TableContainer>
          <Table aria-labelledby="tableTitle" size={"medium"}>
            <EnhancedTableHead
              headCells={config.headCells}
              numSelected={selected.length}
              order={order}
              isAddItem={false}
              orderBy={orderBy}
              checked={config.checked}
              isCommand={!!config?.commands?.length}
              isAction={!!config?.actions?.length}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              onChangeFilter={handleFilter}
              onAddItem={handleAddItem}
              rowCount={listData.length}
            />
            <TableBody>
              {listData.map((item, index) => {
                const isItemSelected = isSelected(item[config.fieldKey]);
                const labelId = `enhanced-table-checkbox-${index}`;
                return (
                  <TableRow
                    hover
                    onClick={(event) =>
                      handleClick(event, item[config.fieldKey])
                    }
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={item[config.fieldKey] || index + Math.random()}
                    selected={isItemSelected}
                  >
                    {config.checked && (
                      <TableCell padding="checkbox">
                        <Checkbox
                          color="primary"
                          checked={isItemSelected}
                          inputProps={{
                            "aria-labelledby": labelId,
                          }}
                        />
                      </TableCell>
                    )}

                    {config.headCells.map((headCell: any) => (
                      <TableCell key={headCell.field}>
                        {headCell.transform ? (
                          <div
                            dangerouslySetInnerHTML={{
                              __html: headCell.transform(
                                item[headCell.field],
                                item
                              ),
                            }}
                          ></div>
                        ) : (
                          <div>
                            {headCell.type === "string" ? (
                              item.isAdd ? (
                                <Input onChange={() => { }} size="small" />
                              ) : (
                                item[headCell.field]
                              )
                            ) : (
                              ""
                            )}
                            {headCell.type === "switch" && (
                              <div style={{width:'100px'}}>
                                <Switch onChange={(event:any)=>{onSwitch?.(item, headCell.field, event.target.checked)}} checked={!!item[headCell.field]} />
                              </div>
                            )}
                            {headCell.type === "tag" && (
                              <ChipInput
                                selectedTags={() => { }}
                                tags={
                                  headCell?.optionTypeTag?.Items ||
                                  item[headCell.field]
                                }
                                disabled={true}
                                notDeleteChip={true}
                                fullWidth
                                variant="outlined"
                                id="path"
                                name="path"
                                size="small"
                              />
                            )}

                            {headCell.type === "values" &&
                              headCell.option &&
                              headCell.badgeType === "circle" && (
                                <div
                                  className={
                                    `bage ${headCell.bage(
                                      item[headCell.field],
                                      item
                                    )}` + " d-flex align-items-center"
                                  }
                                >
                                  <CircleIcon
                                    fontSize="small"
                                    color={headCell.bage(
                                      item[headCell.field],
                                      item
                                    )}
                                  />
                                  <Box className=" ml-6">
                                    {headCell?.option?.items?.map(
                                      (e: any, index: number) => (
                                        <React.Fragment key={index}>
                                          {e[headCell.option.valueField] ==
                                            item[headCell.field] ? (
                                            <div>
                                              {
                                                e[
                                                headCell?.option?.displayField
                                                ]
                                              }
                                            </div>
                                          ) : null}
                                        </React.Fragment>
                                      )
                                    )}
                                  </Box>
                                </div>
                              )}

                            {/* // <Tag readOnly={!item.isAdd} valueField={headCell?.optionTypeTag?.valueField} listSelect={item[headCell.field]} Items={headCell?.optionTypeTag?.Items || item[headCell.field]} limitTags={headCell?.optionTypeTag?.limitTags}></Tag>} */}
                            {headCell.type === "checkbox" && (
                              <Checkbox
                                color="primary"
                                checked={isItemSelected}
                                onClick={(event: any) =>
                                  handleSelect(event, item)
                                }
                                inputProps={{
                                  "aria-labelledby": labelId,
                                }}
                              />
                            )}
                          </div>
                        )}
                      </TableCell>
                    ))}

                    {config?.actions?.length ? (
                      <TableCell padding="checkbox">
                        <div className="d-flex align-items-center">
                          {item[config.fieldKey] &&
                            <React.Fragment>
                              {config?.actions.map(action => {
                                return !action.hidden?.(item) ?
                                  <Tooltip
                                    key={action.title}
                                    title={action?.title || ""}
                                  >
                                    <IconButton
                                      onClick={() => {
                                        action?.hidden?.(item);
                                        action.onClick(item);
                                      }}
                                    >
                                      {action.icon}
                                    </IconButton>
                                  </Tooltip>
                                  : null
                              })
                              }
                            </React.Fragment >
                          }
                        </div>
                      </TableCell>
                    ) : (
                      ""
                    )}
                    {!!config?.commands?.length && (
                      <TableCell padding="checkbox">
                        <div className="position-relative">
                          <IconButton
                            onClick={() => {
                              setIdSelect(item[config.fieldKey]);
                            }}
                          >
                            <MoreHorizIcon />
                          </IconButton>
                        </div>
                        {item[config.fieldKey] === idSelect && (
                          <div
                            ref={refCommand}
                            className="position-absolute item-command-table"
                          >
                            {config?.commands.map((action, i) => (
                              <div
                                key={i}
                                onClick={() => {
                                  setIdSelect("");
                                  action.onClick(item);
                                }}
                                className="d-flex align-items-center item-command-table-action"
                              >
                                {action.icon}
                                <span className="px-5">{action?.title}</span>
                              </div>
                            ))}
                          </div>
                        )}
                      </TableCell>
                    )}
                  </TableRow>
                );
              })}
            </TableBody>
            {(!listData || !listData.length) && (
              <TableBody>
                <TableRow>
                  <TableCell colSpan={config.headCells.length}>
                    <div className="d-flex align-items-center justify-content-center py-5 w-100 text-color-dark">
                      Không có dữ liệu
                    </div>
                  </TableCell>
                </TableRow>
              </TableBody>
            )}
          </Table>
        </TableContainer>
        <div className="d-flex align-items-center justify-content-sm-between">
          {selected.length ? (
            <div className="px-3 d-flex align-items-center">
              <span className="pe-2 py-3">
                Bạn đang chọn {selected.length} hàng
              </span>
              {config.buttonSelects?.length &&
                config.buttonSelects?.map((buttonSelect, index) => (
                  <Button
                    key={index}
                    color={buttonSelect?.color}
                    iconRight={buttonSelect?.iconRight}
                    iconLeft={buttonSelect?.iconLeft}
                    onClick={() => {
                      buttonSelect.onClick(selected);
                    }}
                  >
                    {buttonSelect.label}
                  </Button>
                ))}
            </div>
          ) : (
            <div></div>
          )}
          {!config.hiddenPagination && listData && listData.length ? (
            <TablePagination
              rowsPerPageOptions={config.perPageOptions || [20, 30, 100]}
              component="div"
              labelRowsPerPage=""
              count={pageLength || 0}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          ) : (
            ""
          )}
        </div>
      </Paper>
    </Box>
  );
}
