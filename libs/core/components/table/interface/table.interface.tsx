import { FilterRequest, SortDefine } from "../../../models/requests/paging-request.model";

export interface EnhancedTableProps {
  headCells: HeadCell[];
  fieldKey: string;
  checked?: boolean;
  arrayCheckDefault?: string[];
  perPageOptions?: number[];
  hiddenPagination?: boolean;
  sortLocal?: boolean,
  pageSize?: number,
  commands?: TableCommands[];
  actions?: { title?: string, icon?: any, onClick: (item?: any) => void, hidden?: (item?: any) => boolean }[],
  buttonSelects?: { iconRight?: any, iconLeft?: any, label: string, color?: '', onClick: (item?: any) => void }[],
  onSetSelect?: (item?: any,isUncheck?:string) => void;
}

interface TableCommands {
  hidden?: boolean,
  icon?: any;
  disabled?: boolean;
  title: string;
  onClick: (item?: any) => void
}

export interface HeadCell {
  disablePadding?: boolean;
  field: any;
  label: string;
  type: 'string' | 'switch' | 'tag' | 'checkbox' | 'select' | 'values'
  numeric?: boolean;
  width?: string;
  onSwitch?:(item:any, event: boolean)=> void;
  optionTypeTag?: {
    limitTags?: number;
    Items?: any[];
    valueField?: string;
  },
  optionTypeSelect?:{
    valueField:string,
    displayField:string,
    items:any[],
    model?:any
  },
  option?:{
    valueField?:string,
    displayField?:string,
    items?:any[],
    model?:any
  },
  transform?: (value: any, item: any) => string;
  bage?: (value: any, item: any) => string;
  badgeType?: string;
  filter?: {
    type: 'string' | 'select';
    field: string;
    operator?: string;
    option?: {
      items: any;
      valueField: string;
      displayField: string;
    };
  };
}

export interface SdFindReq {
  pageIndex: number,
  pageSize: number,
  filters?: FilterRequest[],
  sorts?: SortDefine[]
}