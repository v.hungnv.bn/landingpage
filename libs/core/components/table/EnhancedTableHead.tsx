import * as React from 'react';
import { visuallyHidden } from '@mui/utils';
import { HeadCell } from './interface/table.interface';
import Select from '../Select/Select';
import AddIcon from '@mui/icons-material/Add';
import IconButton from '@mui/material/IconButton';

import {
    TextField,
    Box,
    Checkbox,
    TableRow,
    TableCell,
    TableSortLabel,
    TableHead
} from "@mui/material";
interface EnhancedTableProps {
    numSelected: number;
    onRequestSort: (event: React.MouseEvent<unknown>, property: any) => void;
    onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onChangeFilter: (field: string, data: any, operator?: string) => void;
    onAddItem: () => void;
    order: Order;
    orderBy: string;
    rowCount: number;
    headCells: HeadCell[];
    checked?: boolean;
    isAction?: boolean;
    isAddItem?: boolean;
    isCommand?:boolean;
}

type Order = 'asc' | 'desc';

function EnhancedTableHead(props: EnhancedTableProps) {
    const { onRequestSort, onAddItem, onChangeFilter, onSelectAllClick,isCommand, isAction, isAddItem, headCells, checked, order, orderBy, numSelected, rowCount } = props;
    const createSortHandler =
        (property: any) => (event: React.MouseEvent<unknown>) => {
            onRequestSort(event, property);
        };

    const handlerSelect = (e: any, field: string, operator?: string) => {
        onChangeFilter(field, e, operator||'EQUAL')
    }

    const handlerKeyDown = (e: any, field: string, operator?: string) => {
        if (e.keyCode == 13) {
            onChangeFilter(field, e.target.value, operator||'CONTAIN')
        }
    }

    return (
        <TableHead sx={{ backgroundColor: '#F4F6F8' }}>
            <TableRow>
                
                {checked && <TableCell padding="checkbox">
                    <Checkbox
                        color="primary"
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{
                            'aria-label': 'select all desserts',
                        }}
                    />
                </TableCell>}
                {headCells.length && headCells.map((headCell) => (
                    <TableCell
                        sx={{ padding: '10px 16px', fontWeight: 600, color: '#637381' }}
                        key={headCell.field}
                        align={'left'}
                        style={{ width: headCell.width || undefined }}
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                        sortDirection={orderBy === headCell.field ? order : false}>
                        <TableSortLabel
                            active={orderBy === headCell.field}
                            direction={orderBy === headCell.field ? order : 'asc'}
                            onClick={createSortHandler(headCell.field)}>
                            {headCell.label}
                            {orderBy === headCell.field ? (
                                <Box component="span" sx={visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </Box>
                            ) : null}
                        </TableSortLabel>
                        <div>
                            {headCell.filter ? <div>
                                {headCell.filter.type === 'string' && <Box style={{ marginBottom: '8px', marginTop: '8px' }}>
                                    <TextField sx={{ backgroundColor: '#fff' }} fullWidth variant="outlined" size='small' hiddenLabel onKeyDown={(e: any) => { handlerKeyDown(e, headCell.filter?.field || headCell.field, headCell.filter?.operator) }} /></Box>}
                                {headCell.filter.type === 'select' && headCell.filter.option && <Select size='small'
                                    onChange={(e: any) => { handlerSelect(e, headCell.filter?.field || headCell.field, headCell.filter?.operator) }}
                                    valueDisplay={headCell.filter.option.displayField}
                                    valueField={headCell.filter.option.valueField} 
                                    label='' items={headCell.filter.option?.items} />}
                            </div> : ''}
                        </div>
                    </TableCell>
                ))}
                {(isAction || isAddItem) ? <TableCell padding="checkbox">
                    {isAddItem && <IconButton onClick={() => { onAddItem() }}>
                        <AddIcon />
                    </IconButton>}
                </TableCell> : ''}
                {isCommand?<TableCell padding="checkbox"></TableCell>:''}
            </TableRow>
        </TableHead>
    );
}

export default EnhancedTableHead
