import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import { Dialog, DialogContent, DialogTitle, IconButton } from "@mui/material";
import { getIn, useFormikContext } from "formik";
import React, { Fragment, useEffect, useState } from "react";
import { ApiService, NotifyService } from "../../services";
import {  getFileURL, upload, UploadService } from "../../services/upload/upload.service";
import ClearIcon from "@mui/icons-material/Clear";
import { LoadingService } from "../../services/loading.service";
const DEFAULT_MAX_FILE_SIZE_IN_BYTES = 25000000;

interface Props {
  label?: string;
  fieldName: string;
  maxFileSizeInBytes: number;
  initialListFile?:string[];
  disabled?:boolean,
  updateFilesCb?: () => void;
}

const FileUpload: React.FC<Props> = ({
  label,
  updateFilesCb,
  maxFileSizeInBytes = DEFAULT_MAX_FILE_SIZE_IN_BYTES,
  initialListFile,
  disabled,
  fieldName,
}) => {
  const [listFileName, setListFileName] = useState<string[]>(initialListFile || []);
  const [openDialog, setOpenDialog] = useState(false);
  const { values, setFieldValue } = useFormikContext();

  

  const addNewFiles = async (newFiles: FileList): Promise<void> => {
    let newListFileName = [...listFileName];
    if (newFiles) {
      for (let i = 0; i < newFiles.length; i++) {
        const newFile = newFiles.item(i);
        if (newFile) {
            LoadingService.start();
            try{
                const res = await upload(newFile);
                if (res) {
                  newListFileName.push(res.key);
                }
            }
            catch(ex){
                console.log(ex);
            }
            finally{
                LoadingService.stop();
              }
              
            }
            // newFile && formData.append('file',newFile);
          }
          setListFileName(newListFileName)
          setFieldValue(fieldName, newListFileName);
    }
  };

  const handleNewFileUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { files: newFiles } = e.target;
    const formData = new FormData();
    if (newFiles) {
      addNewFiles(newFiles);
    }
  };

  const handleDeleteFile = (fileName: string):void => {
    if (disabled){
      NotifyService.warn('Bạn không có quyền xóa file ở tiêu chí này');
    }
    else if (listFileName) {
      const afterList = listFileName.filter((el) => el !== fileName);
      setFieldValue(fieldName, afterList);
      setListFileName((prev) => afterList);
    }
  };

  const handleGetFile = async(fileName:string):Promise<void> => {
    if (fileName){
      try{
        const url = getFileURL(fileName);
        window.open(url,"__blank");
      }
      catch{

      }
    }
  }

  return (
    <>
      <IconButton disabled={disabled} color="primary" aria-label="upload picture" component="label">
        <input
          onChange={(e) => handleNewFileUpload(e)}
          hidden
          multiple
          accept="image/*,.xlss,.xlx,.doc,.docx,.pdf"
          type="file"
        />
        <CloudUploadIcon  fontSize="small" />
      </IconButton>
      <br />
      {listFileName?.length > 0 && (
        <span
          style={{
            textDecorationLine: "underline",
            color: "#2962FF",
          }}
          onClick={() => setOpenDialog(true)}
        >
          {listFileName.length + " files"}
        </span>
      )}

      <Dialog open={openDialog} onClose={() => setOpenDialog(false)}>
        <DialogTitle>Danh sách file </DialogTitle>
        <DialogContent>
          {listFileName &&
            listFileName.length > 0 &&
            listFileName.map((fileName, idx) => {
              return (
                <Fragment key={idx}>
                  <div className="d-flex justify-content-between">
                    <div
                      style={{
                        textDecorationLine: "underline",
                        color: "#2962FF",
                        maxWidth:'200px',
                        textOverflow:'ellipsis',
                        whiteSpace: 'nowrap',
                        overflow: 'hidden',
                        cursor:'pointer'
                      }}
                    >
                      <span
                      onClick={() => handleGetFile(fileName)}
                      >
                      {fileName}
                      </span>
                    </div>
                    <ClearIcon 
                      fontSize="small"
                      onClick={() => handleDeleteFile(fileName)}
                    />
                  </div>
                </Fragment>
              );
            })}
        </DialogContent>
      </Dialog>
    </>
  );
};

export default FileUpload;
