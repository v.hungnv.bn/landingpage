import React from 'react';
import Stack from '@mui/material/Stack';
import { NotifyService } from '../../services';
import { SdModal } from '../SdModal/SdModal';
import { SdButton } from '../SdButton/SdButton';

class ModalConfirm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      config: {},
      openModal: false,
    };
  }

  componentDidMount() {
    this.subscription = NotifyService.onConfirm().subscribe(config => {
      this.setState({ openModal: true });
      this.setState({ config: config });
    });
  }

  componentWillUnmount() {
    this.subscription.unsubscribe();
  }

  handlerSave = () => {
    const { config } = this.state;
    config?.onAgree?.();
    this.setState({ openModal: false });
  }

  handlerCancel = () => {
    const { config } = this.state;
    config?.onClose?.();
    this.setState({ openModal: false });
  }
  render() {
    const { config, openModal } = this.state;
    if (!config) return null;
    return (
      <div>
        <SdModal sx={{ width: 400, borderRadius: 2 }}
          sxHeader={{ pl: 2, color: '#FFF', backgroundColor: '#2962FF', justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 5, borderTopLeftRadius: 5 }}
          sxBody={{ py: 3 }}
          hideClose
          opened={openModal}
          title={config.title} onClose={() => { this.handlerCancel() }}
          footer={
            <Stack spacing={2} direction={'row'}>
              <SdButton label='Hủy bỏ' onClick={this.handlerCancel} color='info' variant='text' />
              <SdButton label='Đồng ý' onClick={this.handlerSave} />
            </Stack>}
        >
          <div
            className='T16R'
            dangerouslySetInnerHTML={{
              __html: config.describe || '',
            }}
          ></div>
        </SdModal>
      </div >
    );
  }
}

export { ModalConfirm };