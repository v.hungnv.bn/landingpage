import { useEffect, useState } from "react";
import PropTypes from 'prop-types';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';

function CoreTag({
  Items,
  placeholder,
  limitTags,
  width,
  valueField,
  label,
  readOnly,
  listSelect,
  disabled,
  size = 'small'
}) {
  const [listSelected, setListSelected] = useState([]);
  const [isShow, setIsShow] = useState(false);
  useEffect(() => {
    const data = []
    if (listSelect?.length && valueField && typeof listSelect[0] !== 'object') {
      listSelect.forEach(element => {
        const elm = Items?.find(item => item[valueField] === element);
        if (elm) {
          data.push({ [valueField]: element })
        }
      });
      setListSelected(data);
      return;
    }
    setListSelected(listSelect);
    setIsShow(true);
  }, [])

  const handlerSelect = (event, value) => {
    onSelect(event, value)
  }

  return (
    <div style={{ width: width || '100%' }}>
      {isShow ? <Autocomplete
        size={size}
        multiple
        onChange={handlerSelect}
        readOnly={readOnly}
        disabled={disabled}
        limitTags={limitTags || 3}
        options={Items || []}
        getOptionLabel={(option) => valueField ? option[valueField] : option}
        defaultValue={listSelected || []}
        renderInput={(params) => (
          <TextField readOnly={readOnly} disabled={disabled} size={size} {...params} label={label} placeholder={placeholder} />
        )}
        sx={{ width: width || '100%' }} /> : ''}
    </div >
  );
}
CoreTag.propTypes = {
  Items: PropTypes.array,
  listSelect: PropTypes.array,
  limitTags: PropTypes.number,
  placeholder: PropTypes.string,
  width: PropTypes.string,
  valueField: PropTypes.string,
  label: PropTypes.string,
  size: PropTypes.string,
  readOnly: PropTypes.bool,
  disabled: PropTypes.bool,
  onSelect: PropTypes.func
}

export default CoreTag;
