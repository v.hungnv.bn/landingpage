import { useState, useEffect } from 'react';
import TextField from '@mui/material/TextField';
import { Autocomplete } from '@mui/material';

export interface SdAutocompleteOption<T> {
  value: string;
  display: string;
  data: T;
}

export interface SdAutocompleteProps<T> {
  label?: string;
  value: string;
  items: SdAutocompleteOption<T>[];
  sdChange: (item: SdAutocompleteOption<T> | null) => void;
  required?: boolean;
}

export function SdAutocomplete<T = any>(props: SdAutocompleteProps<T>) {
  const [inputValue, setInputValue] = useState<string>('');
  const [options, setOptions] = useState<SdAutocompleteOption<T>[]>([]);
  const { value, items, sdChange, label, required } = props;
  useEffect(() => {
    setOptions(items);
  }, [items]);
  useEffect(() => {
    const item = items.find(item => item.value === value);
    if (item?.display !== inputValue) {
      setInputValue(item?.display || '');
    }
  }, [value, items]);
  return (
    <Autocomplete
      disablePortal
      id="combo-box-demo"
      options={options}
      getOptionLabel={(option) => option.display}
      onChange={(event, newValue) => {
        sdChange(newValue);
      }}
      onInputChange={(event, newInputValue) => {
        console.log(newInputValue);
        setInputValue(newInputValue);
      }}
      inputValue={inputValue}
      sx={{ width: '100%' }}
      size="small"
      renderInput={(params) => <TextField {...params} label={label} required={required} size='small' />}
    />
  );
}
