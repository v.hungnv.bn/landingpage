export { };

declare global {
  interface Array<T = any> {
    search(searchText: string | number, fields?: string | string[], children?: string): T[];
    paging(pageSize: number, page?: number): T[];
  }
}

const search = <T = any>(items: T[], searchText: any, fields?: string | string[], children?: string): T[] => {
  if (!searchText?.toString()) {
    return items;
  }
  if (!items?.length) {
    return items;
  }
  if (Array.isArray(fields)) {
    const fs = fields.filter(e => e !== undefined && e !== null && e !== '');
    if (!fs.length) {
      return items.filter(item =>
        item !== undefined
        && item !== null
        && String.aliasIncludes(item, searchText));
    }
    return items.filter(item =>
      (item !== undefined
        && item !== null
        && fs.some(field => String.aliasIncludes((item as any)[field], searchText)))
      || (children && search((item as any)[children], searchText, fields, children)?.length));
  }
  if (!fields) {
    return items.filter(item =>
      item !== undefined
      && item !== null
      && String.aliasIncludes(item, searchText));
  }
  return items.filter(item =>
    (item !== undefined
      && item !== null
      && String.aliasIncludes((item as any)[fields], searchText))
    || (children && search((item as any)[children], searchText, fields, children)?.length));
}

Array.prototype.search = function <T = any>(searchText: any, fields?: string | string[], children?: string): T[] {
  const items: T[] = this;
  return search(items, searchText, fields, children);
};

Array.prototype.paging = function <T extends Record<string, any> = any>(pageSize: number, page: number = 0): T[] {
  const items: T[] = this;
  return items.filter((item, index) => {
    return index >= page * pageSize
      && index < (page + 1) * pageSize;
  });
};
