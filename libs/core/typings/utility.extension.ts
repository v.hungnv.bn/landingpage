export { };
// declare var Date: DateConstructor;
interface SdUtilityContructor {
  upload: (option?: {
    extensions?: string[];
    maxSizeInMb?: number;
    validator?: (fileName: string) => string;
  }) => Promise<File | null>;
  download: (filePath: string, fileName?: string) => void
  downloadBlob: (blob: Blob, fileName?: string) => void;
  changeAliasLowerCase: (alias: string) => string;
  copyToClipboard: (text: string) => void;
  allWithPaging: <T = any>(func: (pageSize: number, pageNumber: number) => Promise<{
    items: T[];
    total: number;
  }>, defaultPageSize?: number) => Promise<T[]>
}


const upload = (option?: {
  extensions?: string[],
  maxSizeInMb?: number,
  validator?: (fileName: string) => string
}) => {
  const uploadId = 'U1e09c1c0-b647-437e-995e-d7a1a1b60550';
  const promise = new Promise<File | null>((resolve, reject) => {
    const body = document.getElementsByTagName('body')?.[0];
    if (!body) {
      resolve(null);
      return;
    }
    const existedElement = document.getElementById(uploadId);
    if (existedElement) {
      existedElement.remove();
    }
    const element = document.createElement('input');
    element.setAttribute('id', uploadId);
    element.setAttribute('type', 'file');
    element.style.display = 'none';
    body.appendChild(element);
    element.addEventListener('change', (evt: any) => {
      try {
        const target = evt.target as DataTransfer;
        const file = target.files.item(0);
        if (file) {
          const lastDot = file.name.lastIndexOf('.');
          if (lastDot === -1) {
            throw new Error(`File không đúng định dạng`);
          }
          const extension = file.name.substring(lastDot + 1);
          if (option) {
            if (option.extensions?.length && !option.extensions.some(e => e.toUpperCase() === extension.toUpperCase())) {
              throw new Error(`File không đúng định dạng`);
            }
            if (option.maxSizeInMb && option.maxSizeInMb > 0 && option.maxSizeInMb * 1024 * 1024 < file.size) {
              const message = `[${file.name}] Dung lượng tối đa ${option.maxSizeInMb} Mbs`;
              throw new Error(message);
            }
            if (option.validator && option.validator(file.name)) {
              const message = option.validator(file.name);
              throw new Error(message);
            }
          }
          resolve(file);
        }
      } catch (error) {
        reject(error);
      }
    });
    element.click();
  });
  return promise;
}

const download = (filePath: string, fileName?: string) => {
  const link = document.createElement('a');
  link.download = `${fileName || 'File'}_${new Date().toFormat('yyyy_MM_dd_HH_mm')}`;
  link.href = filePath;
  link.style.visibility = 'hidden';
  document.body.appendChild(link);
  link.click();
  link.remove();
}

const downloadBlob = (blob: Blob, fileName?: string) => {
  try {
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    if (link.download !== undefined) { // feature detection
      link.download = `${fileName || 'File'}_${new Date().toFormat('yyyy_MM_dd_HH_mm')}`;
      link.href = url;
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      window.URL.revokeObjectURL(url);
      link.remove();
    }
  } catch (e) {
    console.error('BlobToSaveAs error', e);
  }
}

const changeAliasLowerCase = (alias: string) => {
  let str = alias?.toString() ?? '';
  str = str.toString().toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, ' ');
  str = str.replace(/ + /g, ' ');
  str = str.trim();
  return str;
}

const copyToClipboard = (text: string) => {
  navigator.clipboard.writeText(text);
}

const allWithPaging = async <T = any>(func: (pageSize: number, pageNumber: number) => Promise<{ items: T[], total: number }>, defaultPageSize?: number): Promise<T[]> => {
  let pageSize = defaultPageSize || 1000;
  let pageNumber = 0;
  let count = 0;
  let items: T[] = [];
  while (true) {
    const res = await func(pageSize, pageNumber);
    if (Array(res?.items) && res?.total > 0) {
      items = [...items, ...res.items];
      count += res.items.length;
      pageNumber++;
      if (count >= res.total || !res.items?.length) {
        return items;
      }
    } else {
      return [];
    }
  }
}

var SdUtility: SdUtilityContructor = {
  upload,
  download,
  downloadBlob,
  changeAliasLowerCase,
  copyToClipboard,
  allWithPaging
};

declare global {
  var SdUtility: SdUtilityContructor;
}
globalThis.SdUtility = SdUtility;