export { };

declare global {
  interface Date {
    begin(): Date;
    end(): Date;
    addMiliseconds(miliseconds: number): Date;
    addHours(hours: number): Date;
    addDays(days: number): Date;
    addMonths(months: number): Date;
    toFormat(format: string): string;
  }
  interface DateConstructor {
    equal(date1: any, date2: any): boolean;
    parseFrom(value: any, format: string): Date | null;
    isDate(value: any): boolean;
    toFormat(value: any, format: string): string;
    addMiliseconds(value: any, miliseconds: number): Date | null;
    addHours(value: any, hours: number): Date | null;
    addDays(value: any, days: number): Date | null;
    addMonths(value: any, months: number): Date | null;
    begin(value: any): Date | null;
    end(value: any): Date | null;
  }
}

Date.equal = (date1: any, date2: any) => {
  if (!Date.isDate(date1) && !Date.isDate(date2)) {
    return true;
  }
  if (!Date.isDate(date1) || !Date.isDate(date2)) {
    return false;
  }
  return new Date(date1).getTime() === new Date(date2).getTime();
};

Date.parseFrom = (value: any, format: string) => {
  if (!value) {
    return null;
  }
  if (!format) {
    return null;
  }
  value = value.toString();
  const dmy = format.indexOf('dd') > -1 && format.indexOf('MM') > -1 && format.indexOf('yyyy') > -1;
  const hms = format.indexOf('HH') > -1 || format.indexOf('mm') > -1 || format.indexOf('ss') > -1;
  let strDate = '';
  if (dmy) {
    const dd = value.substr(format.indexOf('dd'), 'dd'.length);
    const MM = value.substr(format.indexOf('MM'), 'MM'.length);
    const yyyy = value.substr(format.indexOf('yyyy'), 'yyyy'.length);
    if (+yyyy > 0 && +MM > 0 && +dd > 0) {
      strDate += `${MM}/${dd}/${yyyy}`;
    } else {
      return null;
    }
  } else {
    const today = new Date();
    const yyyy = today.getFullYear().toString();
    const MM = (today.getMonth() + 1).toString().padStart(2, '0');
    const dd = today.getDate().toString().padStart(2, '0');
    strDate += `${MM}/${dd}/${yyyy}`;
  }
  if (hms) {
    const HH = format.indexOf('HH') > -1 ? value.substr(format.indexOf('HH'), 'HH'.length) : '00';
    const mm = format.indexOf('mm') > -1 ? value.substr(format.indexOf('mm'), 'mm'.length) : '00';
    const ss = format.indexOf('ss') > -1 ? value.substr(format.indexOf('ss'), 'ss'.length) : '00';
    strDate += ` ${HH || '00'}:${mm || '00'}:${ss || '00'}`;
  }
  if (!Date.isDate(strDate)) {
    return null;
  }
  return new Date(strDate);
};

Date.isDate = (value: any) => {
  if (!value) {
    return false;
  }
  const date = new Date(value);
  return !isNaN(date.getTime());
};

Date.toFormat = (value: any, format: string): string => {
  if (!Date.isDate(value)) {
    return '';
  }
  const date: Date = new Date(value);
  return date.toFormat(format);
};

Date.addMiliseconds = (value: any, miliseconds: number) => {
  if (!Date.isDate(value)) {
    return null;
  }
  const date: Date = new Date(value);
  return date.addMiliseconds(miliseconds);
};

Date.addHours = (value: any, hours: number) => {
  if (!Date.isDate(value)) {
    return null;
  }
  const date: Date = new Date(value);
  return date.addHours(hours);
};

Date.addDays = (value: any, days: number) => {
  if (!Date.isDate(value)) {
    return null;
  }
  const date: Date = new Date(value);
  return date.addDays(days);
};

Date.addMonths = (value: any, months: number) => {
  if (!Date.isDate(value)) {
    return null;
  }
  const date: Date = new Date(value);
  return date.addMonths(months);
};

Date.begin = (value: any) => {
  if (!Date.isDate(value)) {
    return null;
  }
  return new Date(Date.toFormat(value, 'MM/dd/yyyy'));
};

Date.end = (value: any) => {
  if (!Date.isDate(value)) {
    return null;
  }
  return new Date(value).addDays(1).begin().addMiliseconds(-1);
};

Date.prototype.begin = function () {
  return new Date(this.toFormat('MM/dd/yyyy'));
};

Date.prototype.end = function () {
  const date: Date = this;
  return date.addDays(1).begin().addMiliseconds(-1);
};

Date.prototype.addMiliseconds = function (miliseconds: number) {
  const date: Date = this;
  date.setMilliseconds(date.getMilliseconds() + miliseconds);
  return date;
};

Date.prototype.addHours = function (hours: number) {
  const date: Date = this;
  date.setHours(date.getHours() + hours);
  return date;
};

Date.prototype.addDays = function (days: number) {
  const date: Date = this;
  date.setDate(date.getDate() + days);
  return date;
};

Date.prototype.addMonths = function (months: number) {
  const date: Date = this;
  date.setMonth(date.getMonth() + months);
  return date;
};

Date.prototype.toFormat = function (format: string) {
  const date: Date = this;
  let result = format;
  result = result.replace('yyyy', date.getFullYear().toString());
  result = result.replace('MM', (date.getMonth() + 1).toString().padStart(2, '0'));
  result = result.replace('dd', date.getDate().toString().padStart(2, '0'));
  result = result.replace('HH', date.getHours().toString().padStart(2, '0'));
  result = result.replace('mm', date.getMinutes().toString().padStart(2, '0'));
  result = result.replace('ss', date.getSeconds().toString().padStart(2, '0'));
  return result;
};
