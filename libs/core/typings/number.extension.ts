export { };

declare global {
  interface NumberConstructor {
    toVNCurrency(value: any): string | null;
    isNumber(value: any): boolean;
    isPositiveInteger(value: any): boolean;
    isPositiveNumber(value: any): boolean;
  }
}

Number.toVNCurrency = (value: any) => {
  value = (value ?? '').toString().replace(/,/g, '');
  if (!value) {
    return null;
  }
  const val = +value;
  if (!Number.isNaN(val)) {
    return val.toLocaleString('vi-VN', { maximumFractionDigits: 10 });
  }
  return null;
};

Number.isPositiveInteger = (value: any) => {
  if (!value) {
    return false;
  }
  const regex = /^([0-9]*)$/;
  return regex.test(value) && +value > 0;
};

Number.isPositiveNumber = (value: any) => {
  if (!value) {
    return false;
  }
  const regex = /^([0-9]*)(\.[0-9]+$){0,1}$/;
  return regex.test(value) && +value > 0;
};

Number.isNumber = (value: any) => {
  if (value === undefined || value === null || value === '') {
    return false;
  }
  return !Number.isNaN(+value);
};
