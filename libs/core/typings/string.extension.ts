export { };

declare global {
  interface StringConstructor {
    isValidEmail(value: any): boolean;
    isValidPhone(value: any): boolean;
    isValidCode(value: any): boolean;
    changeAliasLowerCase(alias: any): string;
    aliasIncludes(alias: any, searchText: any): boolean;
    format(template: string, ...arr: any[]): string;
    templateToDisplay(template: string, entity: any): string; // Convert theo format ${key}
    encrypt(obj: any): string;
    decrypt(encripted: string): any;
    isNullOrEmpty(value: any): boolean;
    isNullOrWhiteSpace(value: any): boolean;
  }
}

String.isValidEmail = (value: any) => {
  if (!value) {
    return false;
  }
  const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  return re.test(value);
};

String.isValidPhone = (value: any) => {
  if (!value) {
    return false;
  }
  const re = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
  return re.test(value);
};

String.isValidCode = (value: any) => {
  if (!value) {
    return false;
  }
  const re = /^[a-zA-Z0-9\@\_\-]{2,20}$/;
  return re.test(value);
};

String.isNullOrEmpty = (value: any) => {
  return value === undefined || value === null || value === '';
};

String.isNullOrWhiteSpace = (value: any) => {
  return value === undefined || value === null || typeof value !== 'string' || value.match(/^ *$/) !== null;
};

String.changeAliasLowerCase = (alias: any) => {
  let str: string = alias?.toString() ?? '';
  str = str.toString().toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, ' ');
  str = str.replace(/ + /g, ' ');
  str = str.trim();
  return str;
}

String.aliasIncludes = (alias: any, searchText: any) => {
  return String.changeAliasLowerCase(alias).includes(String.changeAliasLowerCase(searchText));
}

String.format = (template: string, ...arr: any[]) => {
  for (let i = 0; i < arr.length; i++) {
    const regexp = new RegExp(`\\{${i}\\}`, 'gi');
    template = template.replace(regexp, arr[i]);
  }
  return template;
}

String.templateToDisplay = (template: string, entity: any) => {
  if (!template) {
    return template;
  }
  const regex = /\$\{([A-Za-z._\-]*)\}/g;
  const matches = template.match(regex) || [];
  for (const match of matches) {
    const key = match.slice(2, match.length - 1);
    if (key) {
      template = template.replace(match, entity?.[key] || '');
    }
  }
  return template;
}

const SALT = 'cb9f4b2a-d26c-4787-a66e-e7130ee00f95';

String.encrypt = (obj: any) => {
  obj = JSON.stringify(obj).split('');
  for (var i = 0, l = obj.length; i < l; i++)
    if (obj[i] == '{')
      obj[i] = '}';
    else if (obj[i] == '}')
      obj[i] = '{';
  return encodeURI(SALT + obj.join(''));
}

String.decrypt = (encripted: string) => {
  encripted = decodeURI(encripted);
  if (SALT && encripted.indexOf(SALT) != 0)
    throw new Error('object cannot be decrypted');
  const strs = encripted.substring(SALT.length).split('');
  for (let i = 0, l = strs.length; i < l; i++)
    if (strs[i] == '{')
      strs[i] = '}';
    else if (encripted[i] == '}')
      strs[i] = '{';
  return JSON.parse(strs.join(''));
}
