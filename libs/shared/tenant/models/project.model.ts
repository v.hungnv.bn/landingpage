export enum ProjectEnum {
  ADMIN = 'ADMIN',
  CDS = 'CDS',
  P360 = '360',
}

export const Projects: {
  value: string;
  display: string;
}[] = [
  {
    value: 'ADMIN',
    display: 'Admin',
  },
  {
    value: 'CDS',
    display: 'Chuyển đổi số',
  },
  {
    value: '360',
    display: 'Cổng thông tin điện tử',
  },
];
