export enum DepartmentType {
  // Cấp 1
  UBND_TINH = 'UBND_TINH',
  DANG = 'DANG',
  // Cấp 2
  SO_BAN_NGANH = 'SO_BAN_NGANH',
  UBND_HUYEN = 'UBND_HUYEN',
  // Cấp 3
  PHONG_BAN = 'PHONG_BAN',
  UBND_XA = 'UBND_XA',
  THPT = 'THPT',
  // Cấp 4
  TIEU_HOC = 'TIEU_HOC',
  THCS = 'THCS',
  MAM_NON = 'MAM_NON',
}

export const DepartmentTypeByLevel: Record<number, { value: DepartmentType; display: string }[]> = {
  1: [
    {
      value: DepartmentType.UBND_TINH,
      display: 'UBND tỉnh',
    },
    {
      value: DepartmentType.DANG,
      display: 'Đảng',
    },
  ],
  2: [
    {
      value: DepartmentType.SO_BAN_NGANH,
      display: 'Sở ban ngành',
    },
    {
      value: DepartmentType.UBND_HUYEN,
      display: 'UBND huyện',
    },
  ],
  3: [
    {
      value: DepartmentType.UBND_XA,
      display: 'UBND xã',
    },
    {
      value: DepartmentType.PHONG_BAN,
      display: 'Phòng ban',
    },
    {
      value: DepartmentType.THPT,
      display: 'THPT',
    },
  ],
  4: [
    {
      value: DepartmentType.TIEU_HOC,
      display: 'Tiểu học',
    },
    {
      value: DepartmentType.THCS,
      display: 'THCS',
    },
    {
      value: DepartmentType.MAM_NON,
      display: 'Mầm non',
    },
  ],
};

export interface DepartmentDTO {
  id: string;
  parentId: string;
  tenantCode: string;
  code: string;
  name: string;
  phone: string;
  email: string;
  logo: string;
  type: DepartmentType;
  level: number;
  provinceCode: string;
  districtCode: string;
  wardCode: string;
  provinceName: string;
  districtName: string;
  wardName: string;
  parentCode: string;
  parentName: string;
  isActivated: boolean;
  editable: boolean;
  deletable: boolean;
  children?: DepartmentDTO[];
}
