export interface DomainDTO {
  tenantCode: string;
  projectCode: string;
  departmentCode: string;
  host: string;
}
