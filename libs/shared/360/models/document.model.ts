import { CommonDTO } from './common.model';

export interface DocumentPublicFindReq {
  pageSize: number;
  pageNumber: number;
  searchText?: string;
  documentCategoryId?: string;
  documentTypeId?: string;
  documentDepartmentId?: string;
  publishDateFrom?: string;
  publishDateTo?: string;
}

export interface DocumentSaveReq {
  documentCategoryId?: string;

  documentTypeId?: string;

  documentDepartmentId?: string;

  title?: string;

  symbol?: string;

  publishDate?: Date;

  signBy?: string;

  files?: { name: string; url: string }[];

  isActivated?: boolean;
}

export interface DocumentDTO extends CommonDTO {
  documentCategoryId: string;

  documentCategoryName: string;

  documentTypeId: string;

  documentTypeName: string;

  documentDepartmentId: string;

  documentDepartmentName: string;

  title: string;

  symbol: string;

  publishDate: Date;

  signBy: string;

  files: { name: string; url: string }[];

  isActivated: boolean;
}
