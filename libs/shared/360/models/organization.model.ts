import { CommonDTO } from "./common.model";
import { MemberDTO } from "./member.model";

export interface OrganizationSaveReq {
  name?: string;

  isActivated?: boolean;
}

export interface OrganizationDTO extends CommonDTO {
  name: string;

  isActivated: boolean;

  numberOfMembers: number;

  activatedMembers: MemberDTO[];
}