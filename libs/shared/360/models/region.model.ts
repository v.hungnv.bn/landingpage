import { CommonDTO } from './common.model';
import { PostDTO } from './post.model';

export enum RegionType {
  // Template 1
  LINK_PAGE = 'LINK_PAGE',
  CATEGORY = 'CATEGORY',
  ADVERTISE = 'ADVERTISE',
  IMAGE = 'IMAGE',
  VIDEO = 'VIDEO',
  DON_VI_LIEN_KET = 'DON_VI_LIEN_KET',
  TIN_TUC_SU_KIEN = 'TIN_TUC_SU_KIEN',
  // Template 2
  DIEM_DEN = 'DIEM_DEN',
  DIEM_DEN_DU_LICH = 'DIEM_DEN_DU_LICH',
  BLOG_DU_LICH = 'BLOG_DU_LICH',
  TOUR_NOI_BAT = 'TOUR_NOI_BAT',
  GOOGLE_MAP = 'GOOGLE_MAP',
}

export const RegionTypes = [
  {
    value: RegionType.LINK_PAGE,
    display: 'Liên kết trang',
  },
  {
    value: RegionType.CATEGORY,
    display: 'Chuyên mục',
  },
  {
    value: RegionType.ADVERTISE,
    display: 'Quảng cáo',
  },
  {
    value: RegionType.IMAGE,
    display: 'Hình ảnh',
  },
  {
    value: RegionType.VIDEO,
    display: 'Video',
  },
  {
    value: RegionType.DON_VI_LIEN_KET,
    display: 'Đơn vị liên kết',
  },
  {
    value: RegionType.TIN_TUC_SU_KIEN,
    display: 'Tin tức sự kiện',
  },
  {
    value: RegionType.DIEM_DEN,
    display: 'Điểm đến',
  },
  {
    value: RegionType.DIEM_DEN_DU_LICH,
    display: 'Điểm đến du lịch',
  },
  {
    value: RegionType.BLOG_DU_LICH,
    display: 'Blog du lịch',
  },
  {
    value: RegionType.TOUR_NOI_BAT,
    display: 'Tour nổi bật',
  },
  {
    value: RegionType.GOOGLE_MAP,
    display: 'Google map',
  },
];

export interface RegionPage {
  title?: string; // Tiêu đề
  quote?: string; // Trích dẫn
  image?: string; // Url ảnh
  link?: string; // Link
  isActivated?: boolean;
}

export interface RegionCategory {
  id: string;
  name?: string;
  isActivated?: boolean;
}

export interface RegionAdvertisement {
  title?: string; // Tiêu đề
  quote?: string; // Trích dẫn
  image?: string; // Url ảnh
  link?: string; // Link
  isActivated?: boolean;
}

export interface RegionData {
  image: string; // Link ảnh
  quote: string;
  title: string; // Tiêu đề
  link: string; // Link liên kết
  posts?: PostDTO[]; // Thông tin bài viết trong trường hợp type chuyên mục
}

export interface RegionInfo {
  title?: string; // Tiêu đề
  quote?: string; // Trích dẫn
  image?: string; // Url ảnh
  link?: string; // Link
  isActivated?: boolean;
}

export interface RegionSaveReq {
  type?: RegionType;

  name?: string;

  // Nếu type là liên kết trang, gửi lên mảng pages
  pages: RegionPage[];

  // Nếu type là chuyên mục, gửi lên mảng categories
  categories: RegionCategory[];

  // Nếu type là quảng cáo, gửi lên mảng advertisements
  advertisements: RegionAdvertisement[];

  // Infos
  infos: RegionInfo[];

  isActivated?: boolean;
}

export interface RegionDTO extends CommonDTO {
  type: RegionType;

  name: string;

  pages?: RegionPage[];

  categories?: RegionCategory[];

  advertisements?: RegionAdvertisement[];

  infos?: RegionInfo[];

  // Sử dụng cho landing page, FE chỉ cần render
  data: RegionData[];

  isActivated?: boolean;
}
