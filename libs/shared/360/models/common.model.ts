export interface CommonDTO {
  tenantCode?: string;
  departmentCode?: string;
  id: string;
  deletable?: boolean;
  editable?: boolean;
  createdDate?: Date;
}
