import { CommonDTO } from './common.model';

export enum BannerType {
  IMAGE = 'IMAGE',
  YOUTUBE = 'YOUTUBE',
  IMAGE360 = 'IMAGE360',
}

export const BannerTypes = [
  {
    value: BannerType.IMAGE,
    display: 'Ảnh',
  },
  {
    value: BannerType.YOUTUBE,
    display: 'Youtube',
  },
  {
    value: BannerType.IMAGE360,
    display: 'Ảnh 360',
  },
];

export interface BannerSaveReq {
  type?: BannerType;

  name?: string;

  title?: string; // Cổng du lịch

  subTitle?: string; // Cổng du lịch

  url?: string;

  link?: string;

  showButton?: boolean;

  isActivated?: boolean;
}

export interface BannerDTO extends CommonDTO {
  type: BannerType;

  name: string;

  title: string; // Cổng du lịch

  subTitle: string; // Cổng du lịch

  url: string;

  link: string;

  showButton: boolean;

  isActivated: boolean;
}
