import { CommonDTO } from './common.model';
import { LinkedDepartmentDTO } from './linked-department.model';

export interface LinkedCategorySaveReq {
  name?: string;

  isActivated?: boolean;
}

export interface LinkedCategoryDTO extends CommonDTO {
  name: string;

  isActivated: boolean;

  numberOfDepartments: number;

  linedDepartments: LinkedDepartmentDTO[];
}
