import { CommonDTO } from './common.model';

export interface QuestionPublicFindReq {
  pageSize: number;
  pageNumber: number;
  questionGroupId?: string;
  searchText?: string;
  fullName?: string;
  address?: string;
}

export interface QuestionSaveReq {
  questionGroupId?: string;

  title?: string;

  fullName?: string;

  email?: string;

  phone?: string;

  address?: string;

  content?: string;

  anwser?: string; // Update

  isActivated?: boolean; // Update
}

export interface QuestionDTO extends CommonDTO {
  questionGroupId: string;

  questionGroupName: string;

  title: string;

  fullName: string;

  email?: string;

  phone?: string;

  address?: string;

  content?: string;

  anwser?: string;

  isActivated?: boolean;
}
