import { CommonDTO } from './common.model';

export interface PostPublicFindReq {
  pageSize: number;
  pageNumber: number;
  categorySlugOrId?: string;
  searchText?: string;
}

export interface PostSaveReq {
  categoryId?: string;

  name?: string;

  quote?: string;

  content?: string;

  isPublished?: boolean; // Xuất bản

  synchronized?: boolean; // Đồng bộ

  outstanding?: boolean; // Nổi bật

  canComment?: boolean; // Có thể bình luận

  image?: string;
}

export interface PostDTO extends CommonDTO {
  parentId: string;

  categoryId: string;

  categoryName: string;

  name: string;

  slug: string;

  quote: string;

  content: string;

  publishDate: Date;

  isPublished: boolean; // Xuất bản

  synchronized: boolean; // Đồng bộ

  outstanding: boolean; // Nổi bật

  canComment: boolean; // Có thể bình luận

  image: string;

  creator: string;

  creatorAvatar: string;
}
