import { CommonDTO } from "./common.model";

export interface CommentSaveReq {
  postId?: string;

  fullName?: string;

  email?: string;

  phone?: string;

  title?: string;

  content?: string;
}

export interface CommentDTO extends CommonDTO {

  postId: string;

  postName: string;

  fullName: string;

  email: string;

  phone: string;

  title: string;

  content: string;

  isApproved: boolean;
}