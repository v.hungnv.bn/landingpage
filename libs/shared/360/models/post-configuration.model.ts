export enum PostConfigurationType {
  MANUAL = 'MANUAL',
  AUTO = 'AUTO'
}

export const PostConfigurationTypes = [{
  value: PostConfigurationType.MANUAL,
  display: 'Thủ công'
}, {
  value: PostConfigurationType.AUTO,
  display: 'Tự động'
}]