import { CommonDTO } from "./common.model";

export interface MemberSaveReq {
  organizationId?: string;

  name?: string;

  image?: string;

  role?: string;

  email?: string;

  phone?: string;

  description?: string;

  isActivated?: boolean;
}

export interface MemberDTO extends CommonDTO {
  organizationId: string;

  organizationName: string;

  name: string;

  image: string;

  role: string;

  email: string;

  phone: string;

  description: string;

  isActivated: boolean;
}