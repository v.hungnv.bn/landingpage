import { CommonDTO } from './common.model';

export enum ConfigurationTemplate {
  TEMPLATE1 = 'TEMPLATE1',
  TEMPLATE2 = 'TEMPLATE2',
}

export const ConfigurationTemplates = [
  {
    value: ConfigurationTemplate.TEMPLATE1,
    display: 'Cổng thông tin điện tử',
  },
  {
    value: ConfigurationTemplate.TEMPLATE2,
    display: 'Cổng thông tin du lịch',
  },
];

export enum MenuType {
  NEWS = 'NEWS',
  ORGANIZATION = 'ORGANIZATION',
  SCHEDULE = 'SCHEDULE',
  DOCUMENT = 'DOCUMENT',
  QUESTION = 'QUESTION',

  CATEGORY = 'CATEGORY', // referenceId
  PAGE = 'PAGE', // referenceId

  LINK = 'LINK',
}

export const MenuTypes = [
  {
    value: MenuType.NEWS,
    display: 'Tin tức - Sự kiện',
  },
  {
    value: MenuType.ORGANIZATION,
    display: 'Cơ cấu tổ chức',
  },
  {
    value: MenuType.SCHEDULE,
    display: 'Lịch công tác',
  },
  {
    value: MenuType.DOCUMENT,
    display: 'Văn bản',
  },
  {
    value: MenuType.QUESTION,
    display: 'Hỏi đáp',
  },
  {
    value: MenuType.CATEGORY,
    display: 'Chuyên mục',
  },
  {
    value: MenuType.PAGE,
    display: 'Trang nội dung',
  },
  {
    value: MenuType.LINK,
    display: 'Liên kết tự tạo',
  },
];

export interface MenuSaveReq {
  type?: MenuType; // Nếu là menu có children thì type = null
  title?: string;
  referenceId?: string;
  link?: string;
  children?: MenuSaveReq[];
}

export interface MenuDTO {
  type?: MenuType; // Nếu là menu có children thì type = null
  title?: string;
  referenceId?: string;
  link?: string;
  slug?: string;
  children?: MenuDTO[];
}

export interface PostConfigurationDTO {
  defaultCategoryId?: string;
  outstanding?: boolean;
}

export interface PostConfigurationSaveReq {
  defaultCategoryId?: string;
  outstanding?: boolean;
}

export interface ConfigurationSaveReq {
  template?: ConfigurationTemplate;
  image?: string; // Ảnh
  faviconInfo?: string; // Thông tin favicon
  title?: string; // Tiêu đề trang
  footer?: string; // Thông tin footer
  ipV6?: string; // Link IPV6
  menus?: MenuSaveReq[];
}

export interface ConfigurationDTO extends CommonDTO {
  template?: ConfigurationTemplate;
  image?: string; // Ảnh
  faviconInfo?: string; // Thông tin favicon
  title?: string; // Tiêu đề trang
  footer?: string; // Thông tin footer
  ipV6?: string; // Link IPV6
  menus?: MenuDTO[];
}
