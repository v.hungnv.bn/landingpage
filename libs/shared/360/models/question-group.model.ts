import { CommonDTO } from "./common.model";

export interface QuestionGroupSaveReq {
  name?: string;

  isActivated?: boolean;
}

export interface QuestionGroupDTO extends CommonDTO {
  name: string;

  isActivated: boolean;
}