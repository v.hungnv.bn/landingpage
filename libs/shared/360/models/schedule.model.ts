import { CommonDTO } from './common.model';

export interface SchedulePublicFindReq {
  pageSize: number;
  pageNumber: number;
  searchText?: string;
}

export interface ScheduleSaveReq {
  name?: string;

  fromDate?: Date;

  toDate?: Date;

  documents?: { name: string; url: string }[];

  isActivated?: boolean;
}

export interface ScheduleDTO extends CommonDTO {
  name: string;

  fromDate: Date;

  toDate: Date;

  documents: { name: string; url: string }[];

  isActivated: boolean;
}
