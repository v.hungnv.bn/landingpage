import { CommonDTO } from './common.model';

export enum CategoryType {
  STATIC = 'STATIC',
  RSS = 'RSS',
}

export const CategoryTypes = [
  {
    value: CategoryType.STATIC,
    display: 'Chuyên mục tĩnh',
  },
  {
    value: CategoryType.RSS,
    display: 'Lấy tin RSS',
  },
];

export interface CategorySaveReq {
  parentId?: string;

  name?: string;

  synchronized?: boolean;

  publicRss?: boolean;

  type?: CategoryType;

  rss?: string;

  rssDelayMinute?: number;
}

export interface RssInfo {
  title: string;
  link: string;
  description: string;
  image?: {
    url: string;
    title: string;
    link: string;
  };
  items: {
    title: string;
    link: string;
    description: string;
    pubDate?: string;
    image?: {
      url: string;
      title: string;
      link: string;
    };
  }[];
}

export interface CategoryDTO extends CommonDTO {
  parentId: string;

  parentName: string;

  name: string;

  level: number;

  slug: string;

  synchronized: boolean;

  publicRss: boolean;

  type: CategoryType;

  rss: string;

  // Thông tin rss
  rssInfo: RssInfo;

  rssDelayMinute: number;

  children: CategoryDTO[];
}
