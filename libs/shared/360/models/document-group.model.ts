import { CommonDTO } from './common.model';

export enum DocumentGroupFor {
  CATEGORY = 'CATEGORY',
  TYPE = 'TYPE',
  DEPARTMENT = 'DEPARTMENT',
}

export const DocumentGroupFors = [
  {
    value: DocumentGroupFor.CATEGORY,
    display: 'Danh mục văn bản',
  },
  {
    value: DocumentGroupFor.TYPE,
    display: 'Loại văn bản',
  },
  {
    value: DocumentGroupFor.DEPARTMENT,
    display: 'Cơ quan ban hành',
  },
];

export interface DocumentGroupSaveReq {
  groupFor?: DocumentGroupFor;

  name?: string;

  isActivated?: boolean;
}

export interface DocumentGroupDTO extends CommonDTO {
  groupFor: DocumentGroupFor;

  name: string;

  isActivated: boolean;
}
