import { CommonDTO } from './common.model';

export interface TravelLocationSaveReq {
  travelCategoryId?: string;

  name?: string; // Tên địa điểm

  address?: string; // Địa chỉ

  phone?: string; // Số điện thoại

  content?: string; // Nội dung

  lat?: number;

  lng?: number;

  image?: string; // Ảnh

  link?: string; // Link liên kết nếu dùng liên kết tự tạo

  pageId?: string; // Id của trang nếu dùng trang nội dung

  isActivated?: boolean;
}

export interface GooglePlaceResult {
  name: string;
  formattedAddress: string;
  geometry: {
    location: {
      lat: number;
      lng: number;
    };
    viewport: {
      northeast: {
        lat: number;
        lng: number;
      };
      southwest: {
        lat: number;
        lng: number;
      };
    };
  };
}

export interface TravelLocationDTO extends CommonDTO {
  travelCategoryId: string;

  travelCategoryName: string;

  travelCategoryIcon: string;

  name: string; // Tên địa điểm

  address: string; // Địa chỉ

  phone: string; // Số điện thoại

  content: string; // Nội dung

  lat: number;

  lng: number;

  image: string; // Ảnh

  link: string; // Link liên kết nếu dùng liên kết tự tạo

  pageId: string; // Id của trang nếu dùng trang nội dung

  isActivated: boolean;
}
