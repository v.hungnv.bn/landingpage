import { CommonDTO } from "./common.model";

export interface PageSaveReq {
  name?: string;

  quote?: string;

  content?: string;

  isPublished?: boolean; // Xuất bản

  image?: string;
}

export interface PageDTO extends CommonDTO {
  name: string;

  slug: string;

  quote: string;

  content: string;

  publishDate: Date;

  isPublished: boolean; // Xuất bản

  image: string;
}