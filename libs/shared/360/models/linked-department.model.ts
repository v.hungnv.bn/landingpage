import { CommonDTO } from './common.model';

export interface LinkedDepartmentSaveReq {
  linkedCategoryId?: string;

  name?: string;

  link?: string;

  isActivated?: boolean;
}

export interface LinkedDepartmentDTO extends CommonDTO {
  linkedCategoryId: string;

  linkedCategoryName: string;

  name: string;

  link: string;

  isActivated: boolean;
}
