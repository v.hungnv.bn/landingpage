import { CommonDTO } from "./common.model";

export enum MediaType {
  IMAGE = 'IMAGE',
  YOUTUBE = 'YOUTUBE',
  IMAGE360 = 'IMAGE360',
  FILE = 'FILE'
}

export const MediaTypes = [{
  value: MediaType.IMAGE,
  display: 'Ảnh'
}, {
  value: MediaType.YOUTUBE,
  display: 'Youtube'
}, {
  value: MediaType.IMAGE360,
  display: 'Ảnh 360'
}, {
  value: MediaType.FILE,
  display: 'File'
}];

export interface MediaSaveReq {
  type?: MediaType;

  name?: string;

  url?: string;

  isPublished?: boolean;
}

export interface MediaDTO extends CommonDTO {
  type: MediaType;

  name: string;

  url: string;

  isPublished: boolean;
}