import { CommonDTO } from './common.model';
import { TravelLocationDTO } from './travel-location.model';

export interface TravelCategorySaveReq {
  name?: string; // Tên danh mục

  icon?: string; // Link icon

  isActivated?: boolean;
}

export interface TravelCategoryDTO extends CommonDTO {
  name: string;

  icon?: string; // Link icon

  isActivated: boolean;

  travelLocations: TravelLocationDTO[];
}
