import { PagePermission } from './portal-base.model';

export type PortalPageCds =
  | 'year'
  | 'organization'
  | 'council'
  | 'standard'
  | 'evaluationConfiguration'
  | 'dti'
  | 'dtiEvaluation'
  | 'selfEvaluation'
  | 'organizationEvaluation'
  | 'councilEvaluation'
  | 'report'
  | 'setting';
export type PortalPageCdsPermission = Partial<
  Record<PortalPageCds, PagePermission>
>;

export const PortalPermissionCds: Record<
  string,
  {
    display: string;
    project?: string;
    component?: Record<string, string>;
  }
> = {
  CDS_G_ORGANIZATION: {
    display: 'Quản lý tổ tham mưu',
    project: 'CDS',
    component: {
      CDS_C_ORGANIZATION_VIEW: 'Xem tổ tham mưu',
      CDS_C_ORGANIZATION_CREATE: 'Tạo tổ tham mưu',
      CDS_C_ORGANIZATION_UPDATE: 'Cập nhật tổ tham mưu',
      CDS_C_ORGANIZATION_DELETE: 'Xóa tổ tham mưu',
    },
  },
  CDS_G_COUNCIL: {
    display: 'Quản lý hội đồng',
    project: 'CDS',
    component: {
      CDS_C_COUNCIL_VIEW: 'Xem hội đồng',
      CDS_C_COUNCIL_CREATE: 'Tạo hội đồng',
      CDS_C_COUNCIL_UPDATE: 'Cập nhật hội đồng',
      CDS_C_COUNCIL_DELETE: 'Xóa hội đồng',
    },
  },
  CDS_G_STANDARD: {
    display: 'Quản lý tiêu chuẩn/tiêu chí',
    project: 'CDS',
    component: {
      CDS_C_STANDARD_VIEW: 'Xem tiêu chuẩn/tiêu chí',
      CDS_C_STANDARD_CREATE: 'Tạo tiêu chuẩn/tiêu chí',
      CDS_C_STANDARD_UPDATE: 'Cập nhật tiêu chuẩn/tiêu chí',
      CDS_C_STANDARD_DELETE: 'Xóa tiêu chuẩn/tiêu chí',
    },
  },
  CDS_G_EVALUATION_CONFIGURATION: {
    display: 'Quản lý cấu hình đánh giá',
    project: 'CDS',
    component: {
      CDS_C_EVALUATION_CONFIGURATION_VIEW: 'Xem cấu hình đánh giá',
      CDS_C_EVALUATION_CONFIGURATION_CREATE: 'Tạo cấu hình đánh giá',
      CDS_C_EVALUATION_CONFIGURATION_UPDATE: 'Cập nhật cấu hình đánh giá',
      CDS_C_EVALUATION_CONFIGURATION_DELETE: 'Xóa cấu hình đánh giá',
    },
  },
  CDS_G_SETTING: {
    display: 'Quản lý thiết lập',
    project: 'CDS',
    component: {
      CDS_C_SETTING_VIEW: 'Xem thiết lập',
      CDS_C_SETTING_UPDATE: 'Cập nhật thiết lập',
    },
  },
  CDS_G_REPORT: {
    display: 'Quản lý báo cáo',
    project: 'CDS',
    component: {
      CDS_C_REPORT_VIEW: 'Xem báo cáo',
    },
  },
};
