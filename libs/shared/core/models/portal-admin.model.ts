import { PagePermission } from './portal-base.model';

export type PortalPageAdmin = 'department' | 'domain' | 'project' | 'permission' | 'group' | 'role' | 'user';
export type PortalPageAdminPermission = Partial<Record<PortalPageAdmin, PagePermission>>;

export const PortalPermissionAdmin: Record<
  string,
  {
    display: string;
    project?: string;
    component?: Record<string, string>;
  }
> = {
  ADMIN_G_DEPARTMENT: {
    display: 'Quản lý đơn vị',
    component: {
      ADMIN_C_DEPARTMENT_VIEW: 'Xem đơn vị',
      ADMIN_C_DEPARTMENT_CREATE: 'Tạo đơn vị',
      ADMIN_C_DEPARTMENT_UPDATE: 'Cập nhật đơn vị',
      ADMIN_C_DEPARTMENT_DELETE: 'Xóa đơn vị',
    },
  },
  ADMIN_G_GROUP_PERMISSION: {
    display: 'Quản lý nhóm quyền',
    component: {
      ADMIN_C_GROUP_PERMISSION_VIEW: 'Xem nhóm quyền',
      ADMIN_C_GROUP_PERMISSION_CREATE: 'Tạo nhóm quyền',
      ADMIN_C_GROUP_PERMISSION_UPDATE: 'Cập nhật nhóm quyền',
      ADMIN_C_GROUP_PERMISSION_DELETE: 'Xóa nhóm quyền',
    },
  },
  ADMIN_G_ROLE: {
    display: 'Quản lý vai trò',
    component: {
      ADMIN_C_ROLE_VIEW: 'Xem vai trò',
      ADMIN_C_ROLE_CREATE: 'Tạo vai trò',
      ADMIN_C_ROLE_UPDATE: 'Cập nhật vai trò',
      ADMIN_C_ROLE_DELETE: 'Xóa vai trò',
    },
  },
  ADMIN_G_USER: {
    display: 'Quản lý tài khoản',
    component: {
      ADMIN_C_USER_VIEW: 'Xem tài khoản',
      ADMIN_C_USER_CREATE: 'Tạo tài khoản',
      ADMIN_C_USER_UPDATE: 'Cập nhật tài khoản',
      ADMIN_C_USER_DELETE: 'Xóa tài khoản',
    },
  },
  ADMIN_G_YEAR: {
    display: 'Quản lý năm',
    component: {
      ADMIN_C_YEAR_VIEW: 'Xem năm',
      ADMIN_C_YEAR_CREATE: 'Tạo năm',
      ADMIN_C_YEAR_UPDATE: 'Cập nhật năm',
      ADMIN_C_YEAR_DELETE: 'Xóa năm',
    },
  },
};
