export type PagePermission = Partial<
  Record<'view' | 'create' | 'update' | 'delete' | 'other1' | 'other2', boolean>
>;

export interface PermissionInitial {
  display: string;
  project?: string;
  component?: Record<string, string>;
}
