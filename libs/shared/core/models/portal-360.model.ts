import { PagePermission, PermissionInitial } from './portal-base.model';

export type PortalPage360 =
  | 'category'
  | 'post'
  | 'page'
  | 'banner'
  | 'mediaImage'
  | 'mediaVideo'
  | 'linkedCategory'
  | 'linkedDepartment'
  | 'oganization'
  | 'member'
  | 'documentGroup'
  | 'document'
  | 'schedule'
  | 'region'
  | 'questionGroup'
  | 'question'
  | 'comment'
  | 'travelCategory'
  | 'travelLocation'
  | 'configuration';
export type PortalPage360Permission = Partial<
  Record<PortalPage360, PagePermission>
>;

export const PortalPermission360: Record<string, PermissionInitial> = {
  P360_C_CATEGORY: {
    display: 'Quản lý danh mục',
    project: '360',
    component: {
      P360_C_CATEGORY_VIEW: 'Xem danh mục',
      P360_C_CATEGORY_CREATE: 'Tạo mới danh mục',
      P360_C_CATEGORY_UPDATE: 'Cập nhật danh mục',
      P360_C_CATEGORY_DELETE: 'Xóa danh mục',
    },
  },
  P360_G_POST: {
    display: 'Quản lý bài viết',
    project: '360',
    component: {
      P360_C_POST_VIEW: 'Xem bài viết',
      P360_C_POST_CREATE: 'Tạo mới bài viết',
      P360_C_POST_UPDATE: 'Cập nhật bài viết',
      P360_C_POST_DELETE: 'Xóa bài viết',
    },
  },
  P360_G_PAGE: {
    display: 'Quản lý trang nội dung',
    project: '360',
    component: {
      P360_C_PAGE_VIEW: 'Xem trang nội dung',
      P360_C_PAGE_CREATE: 'Tạo mới trang nội dung',
      P360_C_PAGE_UPDATE: 'Cập nhật trang nội dung',
      P360_C_PAGE_DELETE: 'Xóa trang nội dung',
    },
  },
  P360_G_LINKED_CATEGORY: {
    display: 'Quản lý danh mục liên kết',
    project: '360',
    component: {
      P360_C_LINKED_CATEGORY_VIEW: 'Xem danh mục liên kết',
      P360_C_LINKED_CATEGORY_CREATE: 'Tạo mới danh mục liên kết',
      P360_C_LINKED_CATEGORY_UPDATE: 'Cập nhật danh mục liên kết',
      P360_C_LINKED_CATEGORY_DELETE: 'Xóa danh mục liên kết',
    },
  },
  P360_G_LINKED_DEPARTMENT: {
    display: 'Quản lý đơn vị liên kết',
    project: '360',
    component: {
      P360_C_LINKED_DEPARTMENT_VIEW: 'Xem đơn vị liên kết',
      P360_C_LINKED_DEPARTMENT_CREATE: 'Tạo mới đơn vị liên kết',
      P360_C_LINKED_DEPARTMENT_UPDATE: 'Cập nhật đơn vị liên kết',
      P360_C_LINKED_DEPARTMENT_DELETE: 'Xóa đơn vị liên kết',
    },
  },
  P360_G_ORGANIZATION: {
    display: 'Quản lý tổ chức',
    project: '360',
    component: {
      P360_C_ORGANIZATION_VIEW: 'Xem tổ chức',
      P360_C_ORGANIZATION_CREATE: 'Tạo mới tổ chức',
      P360_C_ORGANIZATION_UPDATE: 'Cập nhật tổ chức',
      P360_C_ORGANIZATION_DELETE: 'Xóa tổ chức',
    },
  },
  P360_G_MEMBER: {
    display: 'Quản lý thành viên',
    project: '360',
    component: {
      P360_C_MEMBER_VIEW: 'Xem thành viên',
      P360_C_MEMBER_CREATE: 'Tạo mới thành viên',
      P360_C_MEMBER_UPDATE: 'Cập nhật thành viên',
      P360_C_MEMBER_DELETE: 'Xóa thành viên',
    },
  },
  P360_G_BANNER: {
    display: 'Quản lý banner',
    project: '360',
    component: {
      P360_C_BANNER_VIEW: 'Xem banner',
      P360_C_BANNER_CREATE: 'Tạo mới banner',
      P360_C_BANNER_UPDATE: 'Cập nhật banner',
      P360_C_BANNER_DELETE: 'Xóa banner',
    },
  },
  P360_G_QUESTION_GROUP: {
    display: 'Quản lý loại hỏi đáp',
    project: '360',
    component: {
      P360_C_QUESTION_GROUP_VIEW: 'Xem loại hỏi đáp',
      P360_C_QUESTION_GROUP_CREATE: 'Tạo mới loại hỏi đáp',
      P360_C_QUESTION_GROUP_UPDATE: 'Cập nhật loại hỏi đáp',
      P360_C_QUESTION_GROUP_DELETE: 'Xóa loại hỏi đáp',
    },
  },
  P360_G_COMMENT: {
    display: 'Quản lý bình luận',
    project: '360',
    component: {
      P360_C_COMMENT_VIEW: 'Xem bình luận',
      P360_C_COMMENT_UPDATE: 'Cập nhật bình luận',
      P360_C_COMMENT_DELETE: 'Xóa bình luận',
    },
  },
  P360_G_QUESTION: {
    display: 'Quản lý hỏi đáp',
    project: '360',
    component: {
      P360_C_QUESTION_VIEW: 'Xem hỏi đáp',
      P360_C_QUESTION_CREATE: 'Tạo mới hỏi đáp',
      P360_C_QUESTION_UPDATE: 'Cập nhật hỏi đáp',
      P360_C_QUESTION_DELETE: 'Xóa hỏi đáp',
    },
  },
  P360_G_DOCUMENT_GROUP: {
    display: 'Quản lý nhóm văn bản',
    project: '360',
    component: {
      P360_C_DOCUMENT_GROUP_VIEW: 'Xem nhóm văn bản',
      P360_C_DOCUMENT_GROUP_CREATE: 'Tạo mới nhóm văn bản',
      P360_C_DOCUMENT_GROUP_UPDATE: 'Cập nhật nhóm văn bản',
      P360_C_DOCUMENT_GROUP_DELETE: 'Xóa nhóm văn bản',
    },
  },
  P360_G_DOCUMENT: {
    display: 'Quản lý văn bản',
    project: '360',
    component: {
      P360_C_DOCUMENT_VIEW: 'Xem văn bản',
      P360_C_DOCUMENT_CREATE: 'Tạo mới văn bản',
      P360_C_DOCUMENT_UPDATE: 'Cập nhật văn bản',
      P360_C_DOCUMENT_DELETE: 'Xóa văn bản',
    },
  },
  P360_G_SCHEDULE: {
    display: 'Quản lý lịch công tác',
    project: '360',
    component: {
      P360_C_SCHEDULE_VIEW: 'Xem lịch công tác',
      P360_C_SCHEDULE_CREATE: 'Tạo mới lịch công tác',
      P360_C_SCHEDULE_UPDATE: 'Cập nhật lịch công tác',
      P360_C_SCHEDULE_DELETE: 'Xóa lịch công tác',
    },
  },
  P360_G_MEDIA: {
    display: 'Quản lý đa phương tiện',
    project: '360',
    component: {
      P360_C_MEDIA_VIEW: 'Xem đa phương tiện',
      P360_C_MEDIA_CREATE: 'Tạo mới đa phương tiện',
      P360_C_MEDIA_UPDATE: 'Cập nhật đa phương tiện',
      P360_C_MEDIA_DELETE: 'Xóa đa phương tiện',
    },
  },
  P360_G_REGION: {
    display: 'Quản lý vùng hiển thị',
    project: '360',
    component: {
      P360_C_REGION_VIEW: 'Xem vùng hiển thị',
      P360_C_REGION_CREATE: 'Tạo mới vùng hiển thị',
      P360_C_REGION_UPDATE: 'Cập nhật vùng hiển thị',
      P360_C_REGION_DELETE: 'Xóa vùng hiển thị',
    },
  },
  P360_G_TRAVEL_CATEGORY: {
    display: 'Quản lý danh mục du lịch',
    project: '360',
    component: {
      P360_C_TRAVEL_CATEGORY_VIEW: 'Xem danh mục du lịch',
      P360_C_TRAVEL_CATEGORY_CREATE: 'Tạo mới danh mục du lịch',
      P360_C_TRAVEL_CATEGORY_UPDATE: 'Cập nhật danh mục du lịch',
      P360_C_TRAVEL_CATEGORY_DELETE: 'Xóa danh mục du lịch',
    },
  },
  P360_G_TRAVEL_LOCATION: {
    display: 'Quản lý địa điểm du lịch',
    project: '360',
    component: {
      P360_C_TRAVEL_LOCATION_VIEW: 'Xem địa điểm du lịch',
      P360_C_TRAVEL_LOCATION_CREATE: 'Tạo mới địa điểm du lịch',
      P360_C_TRAVEL_LOCATION_UPDATE: 'Cập nhật địa điểm du lịch',
      P360_C_TRAVEL_LOCATION_DELETE: 'Xóa địa điểm du lịch',
    },
  },
  P360_G_CONFIGURATION: {
    display: 'Quản lý cấu hình',
    project: '360',
    component: {
      P360_C_CONFIGURATION_VIEW: 'Xem cấu hình',
      P360_C_CONFIGURATION_UPDATE: 'Cập nhật cấu hình',
    },
  },
};
