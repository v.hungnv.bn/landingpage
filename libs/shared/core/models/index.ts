import { PortalPermission360 } from './portal-360.model';
import { PortalPermissionAdmin } from './portal-admin.model';
import { PortalPermissionCds } from './portal-cds.model';

export * from './portal-base.model';
export * from './portal-admin.model';
export * from './portal-cds.model';
export * from './portal-360.model';
export const PortalPermission = {
  ...PortalPermissionAdmin,
  ...PortalPermissionCds,
  ...PortalPermission360,
};
