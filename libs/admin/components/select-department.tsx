import { SdTreeView, SdTreeViewOptionTransform } from "@lib/core/components";
import { DepartmentDTO } from "@lib/shared/tenant";
import { useEffect, useState } from "react";
import { DepartmentService } from "../services";

export interface SelectDepartmentProps {
  onChange?: (departments: DepartmentDTO[] | null) => void;
}

export const SelectDepartment = (props: SelectDepartmentProps) => {
  const [departments, setDepartments] = useState<DepartmentDTO[]>([]);
  const { onChange } = props;
  useEffect(() => {
    DepartmentService.currentByLv1().then(department => {
      setDepartments(department.children || []);
    });
  }, []);
  return (
    <SdTreeView
      multiple
      sx={{
        height: '500px',
        width: '300px'
      }}
      sdMultiple={(item) => {
        onChange?.(Object.values(item).map(e => e.data));
      }}
      items={SdTreeViewOptionTransform(departments?.[0]?.children, 'code', 'name', 'children')}
    />
  );
}
