import type { NextPage } from 'next';
import styles from "../styles/login.module.scss";
import { useRouter } from 'next/router'
import { useEffect, useState } from "react";
import { DomainService } from '../services';
import { ApiService, NotifyService } from '@lib/core/services';
import { Button, Input } from '@lib/core/components';

interface LoginProps {
  projectCode: string;
}

export const Login: NextPage<LoginProps> = (props: LoginProps) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [projectCode, setProjectCode] = useState(props?.projectCode || 'CDS');

  const [loading, setLoading] = useState<boolean>(false);
  const router = useRouter();

  const [departmentCode, setDepartmentCode] = useState('VNA');

  useEffect(() => {
    domainByHost();
  }, []);

  const domainByHost = async () => {
    const domain = await DomainService.byHost();
    if (!domain) {
      return;
    } else {
      setDepartmentCode(domain.departmentCode);
      setProjectCode(domain.projectCode);
    }
  }

  const login = async () => {
    setLoading(true);
    const department = router.query?.departmentCode?.toString() || departmentCode;
    console.log(department);
    ApiService.login({
      username,
      password,
      department,
      project: projectCode
    }).then((res) => {
      if (res) {
        NotifyService.success(`Đăng nhập thành công`);
        router.push(ApiService.adminDefaultUrl);
      }
    }).finally(() => {
      setLoading(false);
    });
  }

  return (
    <div className={styles.login}>
      <div className={styles.login_form}>
        <div className={styles.login_form_title}>ĐĂNG NHẬP</div>
        <div style={{ fontSize: '13px' }}>Bạn hãy điền tên tài khoản và mật khẩu để đăng nhập nhé.</div>
        <div style={{ paddingTop: '16px', width: '100%' }}>
          <Input value={username} required onChange={(text: string) => {
            setUsername(text);
          }} fullWidth size="small"
            id="username" label="Tài khoản" variant="outlined"></Input>
        </div>
        <div>
          <Input value={password} required onChange={(text: any) => {
            setPassword(text);
          }} fullWidth size="small" isPassWord
            id="password" label="Mật khẩu" variant="outlined"></Input>
        </div>
        <Button width={'100%'} loading={loading} onClick={login} disabled={!username || !password}>Đăng nhập</Button>
      </div>
    </div>
  )
}
