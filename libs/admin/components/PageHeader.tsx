import { Box, SxProps } from "@mui/material";

const Styles: Record<string, SxProps> = {
  container: {
    paddingTop: '8px',
    marginBottom: '12px',
    // boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)'
  },
  header: {
    padding: '8px 16px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
    borderRadius: '8px',
    backgroundColor: '#FFF'
  }
}
export const PageHeader = (props: { title: string, action?: React.ReactNode }) => {
  const { title, action } = props;
  return (
    <Box sx={Styles.container}>
      <Box sx={Styles.header}>
        <div className='T18M'>{title}</div>
        <div style={{display: 'flex', alignItems: 'center'}}>{action}</div>
      </Box>
    </Box>
  );
}
