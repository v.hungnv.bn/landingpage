import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { ApiService } from '@lib/core/services';
import { DomainService } from '@lib/admin/services';
import { UserProvider } from '../context/user-context';
import { DepartmentProvider } from '../context/department-context';


interface Props {
  children?: any;
  routerDefault?: string;
}

function DomainGuard({ children, routerDefault }: Props) {
  const router = useRouter();
  const [authorized, setAuthorized] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  // const [departmentCode, setDepartmentCode] = useState('DAKNONG-1-01');
  const [departmentCode, setDepartmentCode] = useState('DAKNONG-2-17');

  useEffect(() => {
    domainByHost();
  }, []);

  const domainByHost = async () => {
    setIsLoading(true);
    try {
      const domain = await DomainService.byHost();
      if (domain?.departmentCode) {
        setDepartmentCode(domain?.departmentCode);
        ApiService.setDepartmentCode(domain?.departmentCode);

      } else {
        ApiService.setDepartmentCode(departmentCode);

      }
      // setIsLoading(false);
    } catch (err) {
      console.log(err);
      ApiService.setDepartmentCode(departmentCode);
    }
    finally {
      setIsLoading(false);
    }

  }

  return (!isLoading ? (
    <DepartmentProvider value={{departmentCode: departmentCode}}>
      {children}
    </DepartmentProvider>) : (null)
  )
}

export { DomainGuard };