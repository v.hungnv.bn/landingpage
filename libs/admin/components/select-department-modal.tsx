import { SdButton, SdModal, SdModalRef, SdTreeView, SdTreeViewOptionTransform } from "@lib/core/components";
import { DepartmentDTO } from "@lib/shared/tenant";
import { Dispatch, SetStateAction, useRef, useState } from "react";
import { SelectDepartment } from "./select-department";

export interface SelectDepartmentModalProps {
  opened: boolean;
  setOpened: Dispatch<SetStateAction<boolean>>;
  onAccept?: (departments: DepartmentDTO[] | null) => void;
}

export const SelectDepartmentModal = (props: SelectDepartmentModalProps) => {
  const sdModal = useRef<SdModalRef | null>(null);
  const [departments, setDepartments] = useState<DepartmentDTO[]>([]);
  const { opened, setOpened, onAccept } = props;
  return (
    <SdModal ref={sdModal} opened={opened} width={'340px'} onClose={() => {
      setOpened(false);
    }}
      footer={
        <SdButton className="mr-12" label='Xác nhận' disabled={!departments.length}
          onClick={() => {
            onAccept?.(departments);
            setOpened(false);
          }} />
      }
      title={'Chọn đơn vị'}>
      <SelectDepartment onChange={(selectedItems) => {
        setDepartments(selectedItems || []);
      }} />
    </SdModal>
  );
}
