
import { ReactNode, useEffect, useState } from "react";
import { useRouter } from 'next/router';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import styles from '../styles/menu.module.scss';

export interface Menu {
  id: string;
  icon?: ReactNode;
  title: string;
  level: number;
  path?: string;
  isExternal?: boolean;
  children?: Menu[];
  hidden?: () => boolean;
}

interface MenuItemProps {
  menus: Menu[];
  activeMenuId?: string;
  expand: Record<string, boolean>,
  onClick: (menu: Menu) => void;
}

export const MenuItem = ({ activeMenuId, expand, menus, onClick }: MenuItemProps) => {
  return (<div>
    {menus.filter(menu => !menu.hidden || !menu?.hidden()).map((menu, index) => (
      <div key={menu.id} className={menu.level === 2 ? 'divMenu2' : ''} >
        <div onClick={() => {
          onClick(menu);
        }} className={`${styles.menuItem} 
          ${menu.level === 1 ? styles.menuItem1 : menu.level === 2 ? styles.menuItem2 : styles.menuItem3}
          ${activeMenuId && menu.id === activeMenuId && (menu.children?.length ? (expand[menu.id] ? styles.menuGroupActive : '') : styles.menuActive)}`}
        >
          <div className={styles.menuItemTitle}>
            {menu.icon || <div style={{
              width: '4px',
              height: '4px',
              background: '#FFFFFF',
              flex: 'none',
              order: 0,
              flexGrow: 0
            }}></div>}
            <span style={{ marginLeft: "16px" }}>{menu.title}</span>
          </div>
          {menu.children?.length ? (expand[menu.id] ? <ExpandMoreIcon className={styles.menuItemIcon} /> : <ChevronRightIcon className={styles.menuItemIcon} />) : ''}
        </div>
        {
          expand[menu.id] && menu.children?.length && (
            <div className="menu-children">
              <MenuItem activeMenuId={activeMenuId} expand={expand} menus={menu.children} onClick={onClick}></MenuItem>
            </div>
          )
        }
      </div>
    ))
    }
  </div >);
}

interface Props {
  menus: Menu[];
}

export const Sidebar = ({ menus }: Props) => {
  const [activeMenuId, setActiveMenuId] = useState<string | undefined>();
  const [expand, setExpand] = useState<Record<string, boolean>>({});
  const [selectedMenu, setSelectedMenu] = useState<Menu | undefined>();
  const router = useRouter();
  useEffect(() => {
    const expandIds: string[] = [];
    const check = (menus: Menu[]): string | undefined => {
      if (!menus?.length) {
        return;
      }
      for (const menu of menus) {
        if (menu.children?.length) {
          const result = check(menu.children);
          if (result) {
            expandIds.push(menu.id);
            return result;
          }
        } else if (menu.path === window.location.pathname) {
          return menu.id;
        }
      }
      return;
    }
    setActiveMenuId(check(menus));
    for (const expandId of expandIds) {
      expand[expandId] = true;
    }
    setExpand({
      ...expand
    })
  }, [window.location.pathname]);

  useEffect(() => {
    if (selectedMenu) {
      if (selectedMenu?.children?.length) {
        setExpand({
          ...expand,
          [selectedMenu.id]: !expand[selectedMenu.id]
        })
      } else if (selectedMenu.path) {
        router.push(selectedMenu.path);
      }
    }
  }, [selectedMenu]);

  const onClick = (menu: Menu) => {
    if (menu?.isExternal) {
      window.open(menu.path, '__blank');

    } else {
      setSelectedMenu({
        ...menu
      });
    }

  }
  return (<div>
    {menus.length && <MenuItem activeMenuId={activeMenuId} expand={expand} menus={menus} onClick={onClick}></MenuItem>}
  </div>);
}

// const Styles = {
//   menuItem: {
//     fontSize: '14px';
//   color: '#fff';
//   borderRadius: '4px';
//   display: 'flex';
//   alignItems: 'center';
//   justifyContent: 'space-between';
//   height: '40px';
//   cursor: 'pointer';
//   &:hover{
//     background-color: hsla(0,0%,100%,.06);
//   }
//   }
// }