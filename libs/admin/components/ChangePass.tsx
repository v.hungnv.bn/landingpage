import { useEffect, useState } from "react";
import router from 'next/router';
import { AccountService } from '../services';
import { Input, SdButton, SdModal } from "@lib/core/components";
import { ApiService, NotifyService } from "@lib/core/services";
interface Props {
  onClose: () => void;
}
export const ChangePass = ({ onClose }: Props): React.ReactElement => {
  const [openModal, setOpenModal] = useState(false);
  const [oldPassword, setOldPassword] = useState<string>('');
  const [newPassword, setNewPassword] = useState<string>('');
  const [passwordConfirm, setPasswordConfirm] = useState<string>();

  useEffect(() => {
    setOpenModal(true);
  }, []);

  const handlerCancel = () => {
    onClose();
    setOpenModal(false)
  }

  const handlerSave = () => {
    if (newPassword !== passwordConfirm) {
      NotifyService.warn('Mật khẩu không khớp vui lòng xem lại.', null)
      return;
    }
    AccountService.changePassword({
      oldPassword: oldPassword,
      newPassword: newPassword
    }).then((res) => {
      NotifyService.warn(' Đổi mật khẩu thành công', null)
      onClose();
      setOpenModal(false)
      ApiService.logout();
      router.push('/login');
    }).catch(_ => { })
  }

  const buttonFooters = [{ variant: "text", color: 'inherit', onClick: handlerCancel, label: 'Hủy' }, { onClick: handlerSave, label: 'Lưu lại', disabled: !(oldPassword && newPassword && passwordConfirm) }]

  return (
    <SdModal opened={openModal} width={'500px'}
      footer={<SdButton label='Lưu' onClick={handlerSave} disabled={!(oldPassword && newPassword && passwordConfirm)} />}
      title={"Đổi mật khẩu"} onClose={onClose}>
      <Input required={true}
        onChange={(event: string) => {
          setOldPassword(event);
        }} fullWidth size="small" id="oldPassword"
        label="Mật khẩu cũ" variant="outlined"></Input>

      <Input required={true}
        onChange={(event: string) => {
          setNewPassword(event);
        }} id="newPassword"
        fullWidth size="small" isPassWord={true}
        label="Mật khẩu mới" variant="outlined"></Input>

      <Input required={true}
        onChange={(event: string) => {
          setPasswordConfirm(event);
        }} isPassWord={true} id="passwordConfirm"
        fullWidth size="small"
        label="Xác nhận mật khẩu" variant="outlined"></Input>
    </SdModal>
  )
}