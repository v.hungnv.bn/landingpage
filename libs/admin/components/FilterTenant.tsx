import { forwardRef, useEffect, useImperativeHandle, useState } from "react";
import { DepartmentService } from '../services/department/department.service';
import { Autocomplete, TextField } from '@mui/material';
import { Department, Tenant, TenantService } from "../services";
import { ApiService, CacheKeys, CacheService } from "@lib/core/services";

interface Props {
  showLv4?: boolean;
  onChange?: (deparment: Department | null, tenantCode?: string) => void;
}

const FilterTenant = forwardRef((props: Props, ref) => {
  const userInfo = ApiService.userInfo;
  const [tenants, setTenants] = useState<Tenant[]>([]);
  const [tenant, setTenant] = useState<Tenant | null>();
  const [tenantCode, setTenantCode] = useState<string>();
  const [department, setDepartment] = useState<Department | null>();
  const [root, setRoot] = useState<Department | null>();
  const [departments, setDepartments] = useState<Department[]>([]);
  const [lv1, setLv1] = useState<Department | null>(null);
  const [lv2, setLv2] = useState<Department | null>(null);
  const [lv3, setLv3] = useState<Department | null>(null);
  const [lv4, setLv4] = useState<Department | null>(null);

  const cache: Record<string, Department> = {};
  const convert = (deparments: Department[]) => {
    for (const department of (deparments || [])) {
      cache[department.code] = department;
      if (department.children?.length) {
        convert(department.children);
      }
    }
  }
  convert(departments);

  useEffect(() => {
    const tenant = CacheService.get(CacheKeys.CURRENT_TENANL);
    if (tenant) {
      setTenant(tenant);
      onChangeTenant(tenant);
    }

    if (userInfo.isVNA) {
      TenantService.all().then((tenants) => {
        setTenants(tenants);
      });
    } else {
      loadDepartments();
    }
  }, []);

  useImperativeHandle(ref, () => ({
    loadData() {
      DepartmentService.getCurrent(tenantCode || '').then(result => {
        setDepartments(result);
      });
      if (department?.code) {
        DepartmentService.getCurrent(department.code).then(result => {
          props?.onChange?.(result[0] || null, tenantCode);
        });
      }
    }
  }))

  const loadDepartments = async (code?: string) => {
    const result = await DepartmentService.getCurrent(code);
    setDepartments(result);
    setDepartment(result[0]);
    CacheService.set(CacheKeys.CURRENT_DEPARMENT, department);
    props?.onChange?.(result[0] || null, code);
    setRoot(result[0]);
  }

  const onChangeTenant = async (tenant: Tenant | null) => {
    loadDepartments(tenant?.code);
    setTenantCode(tenant?.code);
  }

  return (
    <><div className='row pt-12px'>
      {userInfo.isVNA && <div className='col-3'>
        <Autocomplete id="951ea62e-101d-427e-aaa7-20035dd3ec7d" size='small' options={tenants}
          getOptionLabel={(option) => option.name}
          value={tenant || null}
          onChange={(event: any, tenant: Tenant | null) => {
            CacheService.set(CacheKeys.CURRENT_TENANL, tenant);
            setTenant(tenant);
            onChangeTenant(tenant);
            setLv1(null);
            setLv2(null);
            setLv3(null);
            setLv4(null);
          }}
          renderInput={(params) => <TextField {...params} label="Tỉnh" />}
        />
      </div>}
      {(root?.level || 0) === 0 && <div className='col-3'>
        <Autocomplete
          id="834d79ab-3683-4d1e-b33c-87748be765b6"
          size='small'
          options={cache[root?.code || '']?.children || []} disabled={!cache[root?.code || '']?.children?.length}
          value={lv1 || null}
          getOptionLabel={(option) => option.name}
          onChange={(event: any, newValue: Department | null) => {
            setDepartment(newValue);
            CacheService.set(CacheKeys.CURRENT_DEPARMENT, department);
            setLv1(newValue);
            setLv2(null);
            setLv3(null);
            setLv4(null);
            props?.onChange?.(newValue || root || null, tenantCode);
          }}
          renderInput={(params) => <TextField {...params} label="Khối tỉnh" />}
        />
      </div>}
      {(root?.level || -1) <= 1 && <div className='col-3'>
        <Autocomplete
          id="district"
          size='small'
          options={cache[root?.level === 1 ? root?.code : lv1?.code || '']?.children || []} disabled={!cache[root?.level === 1 ? root?.code : lv1?.code || '']?.children?.length}
          value={lv2 || null}
          getOptionLabel={(option) => option.name}
          renderOption={(props, option) => {
            return (
              <li {...props} key={option.code}>
                {option.name}
              </li>
            );
          }}
          onChange={(event: any, newValue: Department | null) => {
            setDepartment(newValue);
            CacheService.set(CacheKeys.CURRENT_DEPARMENT, department);
            setLv2(newValue);
            setLv3(null);
            setLv4(null);
            props?.onChange?.(newValue || lv1 || root || null, tenantCode);
          }}
          renderInput={(params) => <TextField {...params} label="Trực thuộc tỉnh" />}
        />
      </div>}
      {(root?.level || -1) <= 2 && <div className='col-3'>
        <Autocomplete
          id="itemDistrict"
          size='small'
          options={cache[root?.level === 2 ? root?.code : lv2?.code || '']?.children || []} disabled={!cache[root?.level === 2 ? root?.code : lv2?.code || '']?.children?.length}
          value={lv3 || null}
          getOptionLabel={(option) => option.name}
          renderOption={(props, option) => {
            return (
              <li {...props} key={option.code}>
                {option.name}
              </li>
            );
          }}
          onChange={(event: any, newValue: Department | null) => {
            setDepartment(newValue);
            CacheService.set(CacheKeys.CURRENT_DEPARMENT, department);
            setLv3(newValue);
            setLv4(null);
            props?.onChange?.(newValue || lv2 || lv1 || root || null, tenantCode);
          }}
          renderInput={(params) => <TextField {...params} label="Trực thuộc huyện" />}
        />
      </div>}
      {props?.showLv4 && (root?.level || -1) <= 3 && <div style={{ marginTop: userInfo.isVNA ? '12px' : '0' }} className='col-3'>
        <Autocomplete
          id="itemDistrict"
          size='small'
          options={cache[root?.level === 3 ? root?.code : lv3?.code || '']?.children || []} disabled={!cache[root?.level === 3 ? root?.code : lv3?.code || '']?.children?.length}
          value={lv4 || null}
          getOptionLabel={(option) => option.name}
          renderOption={(props, option) => {
            return (
              <li {...props} key={option.code}>
                {option.name}
              </li>
            );
          }}
          onChange={(event: any, newValue: Department | null) => {
            setDepartment(newValue);
            CacheService.set(CacheKeys.CURRENT_DEPARMENT, department);
            setLv4(newValue);
            props?.onChange?.(newValue || lv3 || lv2 || lv1 || root || null, tenantCode);
          }}
          renderInput={(params) => <TextField {...params} label=" Sự nghiệp công lập trực thuộc huyện" />}
        />
      </div>}
    </div>
    </>
  )
})

export default FilterTenant;