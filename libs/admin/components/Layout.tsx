import { useRouter } from 'next/router'
import Image from 'next/image'
import { useState, useRef, memo, MutableRefObject, useEffect } from "react";
import LogoutIcon from '@mui/icons-material/Logout';
import PersonIcon from '@mui/icons-material/Person';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import React, { ReactNode } from "react";
import gravatar from 'gravatar';
import { Menu, Sidebar } from './Sidebar';
import styles from '../styles/layout.module.scss';
import { ChangePass } from './ChangePass';
import { LoadingService } from '../../core/services/loading.service';
import { UserInfo } from 'libs/admin/modals';
import { ApiService, PortalPage, PortalService, useOnClickOutside } from '@lib/core/services';

interface Props {
  host: string;
  children?: ReactNode;
  menus: Menu[];
  head?: ReactNode;
}

export const Layout = ({ host, children, menus, head }: Props): React.ReactElement => {
  const [isShowInfo, setIsShowInfo] = useState(false);
  const [avatar, setAvatar] = useState('');
  const [isChangePass, setIsChangePass] = useState(false);
  const [isChangeUserInfo, setIsChangeUserInfo] = useState(false);
  const [permission, setPermission] = useState<PortalPage>({});

  const userInfo = ApiService.userInfo;
  const router = useRouter();
  const ref = useRef() as MutableRefObject<HTMLDivElement>;

  useEffect(() => {
    if (userInfo.avatar) {
      setAvatar(userInfo.avatar);
    } else {
      const title = userInfo.username?.substring(0, 2) || 'VN';
      setAvatar(gravatar.url(title, {
        s: '480',
        r: 'pg',
        d: 'https://cdn.auth0.com/avatars/' + title + '.png',
        protocol: 'https'
      }));
    }
    loadPagePermission();
  }, []);
  const loadPagePermission = async () => {
    LoadingService.start();
    try {
      const res = await PortalService.page(host);
      setPermission(res);
      ApiService.setPermission(res);
    } catch (err) {
      console.log(err);
    } finally {
      LoadingService.stop();
    }
  }
  const logout = () => {
    ApiService.logout();
    router.push('/login');
    setIsShowInfo(false);
  }

  const changePassword = () => {
    setIsChangePass(true)
    setIsShowInfo(false);
  }

  const changeUser = () => {
    setIsChangeUserInfo(true);
    setIsShowInfo(false);
  }

  useOnClickOutside(ref, () => setIsShowInfo(false));

  if (!Object.keys(permission)?.length) {
    return <></>;
  }
  return (
    <>{head}
      <div className={styles.layout}>
        <div className={styles.layoutMenu}>
          <div className={styles.layoutMenuHeader}>
            <div className={styles.layoutMenuLogo}>
              {/* <img className={styles.layoutMenuLogoImg} src="/logo.png" /> */}
              <img className={styles.layoutMenuLogoImg}
                src="/logo.png"
                alt="logo"  />

            </div>
          </div>
          <div className={styles.layoutMenuBody}>
            <div style={{ height: '100%', overflow: 'auto' }}>
              <Sidebar menus={menus}></Sidebar>
            </div>
          </div>
          <div onClick={() => { setIsShowInfo(true); }} className={['d-flex', 'align-items-center', styles.layoutMenuInfo].join(' ')}>
            <img className={styles.layoutMenuInfoImg} src={avatar} />
            <div>
              <div className={styles.layoutMenuInfoName}>{userInfo?.name}</div>
              <div className={styles.layoutMenuInfoEmail}>{userInfo?.email}</div>
            </div>
          </div>
        </div>
        <div className={styles.layoutBody}>
          <div className='container-fluid px-4'>
            {children}
          </div>
        </div>
        {isShowInfo && (<div ref={ref} className={styles.menuInfoProfile}>
          <div onClick={changeUser} className={styles.menuInfoProfilePer}><PersonIcon sx={{ paddingRight: '8px' }} />Thông tin tải khoản</div>
          <div onClick={changePassword} className={styles.menuInfoProfilePer}><LockOpenIcon sx={{ paddingRight: '8px' }} />Đổi mật khẩu</div>
          <div onClick={logout} className={styles.menuInfoProfileLogout}><LogoutIcon sx={{ paddingRight: '8px' }} />Đăng xuất</div>
        </div>)}
        {isChangePass && <ChangePass
          onClose={() => {
            setIsChangePass(false);
          }} />}
        {isChangeUserInfo && <UserInfo open={isChangeUserInfo}
          onClose={() => {
            setIsChangeUserInfo(false);
          }}
          onAccept={() => {
            setIsChangeUserInfo(false);
          }} />}
      </div></>
  );
}
