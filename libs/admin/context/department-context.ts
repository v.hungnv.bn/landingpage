import React, { Component } from 'react';

const DepartmentContext = React.createContext({departmentCode: 'DAKNONG-1-01'});

export const DepartmentProvider = DepartmentContext.Provider;


export const DepartmentConsumer = DepartmentContext.Consumer;

export default DepartmentContext