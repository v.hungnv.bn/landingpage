import type { NextPage } from 'next'
import { useRef, useState } from "react";
import style from '../styles/page.module.scss';
import { AccountService } from '../services/account/accounta.service';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import KeyIcon from '@mui/icons-material/Key';
import FilterTenant from '../components/FilterTenant';
import { PageHeader } from '../components';
import { Department, UserService } from '../services';
import Switch from "@mui/material/Switch";
import { ApiService, NotifyService } from '@lib/core/services';
import { AccountDetail, AccountDetailRef } from '../modals';
import { SdButton, SdGrid, SdGridRef } from '@lib/core/components';

const Account: NextPage = () => {
  const accountDetailRef = useRef<AccountDetailRef | null>(null);
  const grid = useRef<SdGridRef | null>(null);
  const [departmentCode, setDepartmentCode] = useState<string>('');
  const [idSelect, setIdSelect] = useState('');

  const newItem = () => {
    accountDetailRef.current?.open();
    // setOpenModal(true);
    setIdSelect('');
  }
  return (
    <>
      <PageHeader
        title="Quản lý tài khoản"
        action={ApiService.portalPage?.user?.create && (
          <SdButton icon={<AddIcon />} label="Tạo mới" onClick={newItem} />
        )}
      />
      {/* <UploadExcel option={configImpot} title={'Import Tài khoản'} onAgree={(item: any)=>{}} onClose={()=>{}}/> */}
      <div className={style.filter}>
        <FilterTenant showLv4={true} onChange={(item: Department | null, tenantCode: string | undefined) => {
          setDepartmentCode(item?.code || '');
          grid?.current?.reload();
        }} />
      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} className={style.content}>
        <SdGrid
          ref={grid}
          items={() => {
            return UserService.all(departmentCode);
          }}
          columns={[{
            field: 'name',
            label: 'Họ và tên',
            type: 'string',
          }, {
            field: 'username',
            label: 'Tên đăng nhập',
            type: 'string',
          }, {
            field: 'position',
            label: 'Chức vụ',
            type: 'string',
          }, {
            field: 'email',
            label: 'Email',
            type: 'string',
          }, {
            field: 'isActivated',
            label: 'Trạng thái',
            type: 'bool',
            option: {
              displayOnTrue: 'Hoạt động',
              displayOnFalse: 'Khóa'
            },
            template: (value, item) => (<div style={{ width: '100px' }}>
              <Switch checked={value} disabled={!ApiService.portalPage?.user?.update} onChange={event => {
                const checked = event.target.checked;
                NotifyService.confirm({
                  title: checked ? 'Kích hoạt lại tài khoản' : 'Vô hiệu hóa tài khoản',
                  describe: `<div>Bạn có chắc chắn muốn ${checked ? 'kích hoạt lại' : 'vô hiệu hóa'} tài khoản: <span style="font-weight: 700;">${item.username}</span> không?</div>`,
                  yesTitle: 'Đồng ý',
                  noTitle: 'Hủy bỏ',
                  onClose: () => { },
                  onAgree: () => {
                    item.isActivated = checked;
                    UserService.update(item.id, {
                      isActivated: checked
                    }).then(res => {
                      NotifyService.success('Cập nhật tài khoản thành công', null);
                      grid?.current?.reload();
                    });
                  }
                })
              }} />
            </div>)
          }]}
          commands={[{
            title: 'Sửa',
            icon: <EditIcon />,
            onClick: (item) => {
              setIdSelect(item.id);
              accountDetailRef.current?.open();
              // setOpenModal(true);
            },
            hidden: !ApiService.portalPage?.user?.update
          }, {
            title: 'Xóa',
            icon: <DeleteIcon />,
            onClick: (item) => {
              NotifyService.confirm({
                title: 'Xóa tài khoản',
                yesTitle: 'Đồng ý',
                noTitle: 'Hủy bỏ',
                describe: 'Bạn có muốn xóa tài khoản này không?',
                onClose: () => { },
                onAgree: () => {
                  AccountService.delete(item.id).then((res) => {
                    NotifyService.success('Xóa tài khoản thành công.', null);
                    grid.current?.reload();
                  });
                }
              })
            },
            hidden: !ApiService.portalPage?.user?.delete
          }, {
            title: 'Khôi phục mật khẩu',
            icon: <KeyIcon />,
            onClick: (item) => {
              NotifyService.confirm({
                title: 'Khôi phục mật khẩu',
                describe: 'Bạn có chắc chắn muốn khôi phục mật khẩu cho tài khoản này không?',
                yesTitle: 'Đồng ý',
                noTitle: 'Hủy bỏ',
                onAgree: () => {
                  AccountService.resetPassword({ userId: item.id }).then((res) => {
                    NotifyService.confirm({
                      title: 'Khôi phục mật khẩu',
                      describe: `Mật khẩu mới sau khi khôi phục là: ${res}`,
                      yesTitle: 'Tiếp tục',
                    })
                  })
                }
              })
            }
          }]}
        />
      </div>
      <AccountDetail
        ref={accountDetailRef}
        id={idSelect}
        departmentCode={departmentCode}
        onAgree={() => {
          grid.current?.reload();
          setIdSelect('');
        }} />

    </>
  )
}

export default Account