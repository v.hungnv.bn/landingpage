import type { NextPage } from 'next'
import { useRef, useState } from "react";
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import { NotifyService } from '../../core/services/notify.service'
import style from '../styles/page.module.scss';
import ModalDepartment from '../modals/new-edit.department';
import { DepartmentService } from '../services/department/department.service';
import FilterTenant from '../components/FilterTenant';
import { Department } from '../services';
import { SdGrid, SdGridColumn, SdGridCommand } from '@lib/core/components';
import { ApiService } from '@lib/core/services';
import { SdButton } from '@lib/core/components';
import { PageHeader } from '../components';

const AgenciesUnits: NextPage = () => {
  const [departmentSelect, setDepartmentSelect] = useState<Department | null>();
  const [openModalDetail, setOpenModalDetail] = useState(false);
  const [tenantCode, setTenantCode] = useState('');
  const [idSelect, setIdSelect] = useState('');
  const [checksum, setChecksum] = useState<string | undefined>(undefined);

  const childFilterTenantRef = useRef<{ loadData: () => void }>();
  const columns: SdGridColumn<Department>[] = [
    {
      field: 'name',
      label: 'Tên cơ quan',
      type: 'string'
    }, {
      field: 'level',
      label: 'Level',
      type: 'number'
    }, {
      field: 'type',
      label: 'Loại cơ quan',
      type: 'string',
    }, {
      field: 'districtName',
      label: 'Quận/Huyện',
      type: 'string',
    }, {
      field: 'wardName',
      label: 'Phường/Xã',
      type: 'string'
    }
  ];
  const commands: SdGridCommand<Department>[] = [{
    title: 'Sửa',
    icon: <EditIcon />,
    onClick: (item) => {
      setIdSelect(item.id);
      setOpenModalDetail(true);
    },
    hidden: (item) => !item.editable || !ApiService.portalPage?.department?.update
  }, {
    title: 'Xóa',
    icon: <DeleteIcon />,
    onClick: (item: Department) => {
      NotifyService.confirm({
        title: 'Xóa cơ quan đơn vị',
        yesTitle: 'Đồng ý',
        noTitle: 'Hủy bỏ',
        describe: 'Bạn có muốn xóa cơ quan đơn vị này không?',
        onAgree: async () => {
          try {
            const res = await DepartmentService.deleteDepartment(item.id);
            if (res) {
              NotifyService.success('Xóa cơ quan đơn vị thành công.', null);
              childFilterTenantRef.current?.loadData();
            }
          } catch (error) { }
        },
      })
    },
    hidden: (item) => !item.deletable || !ApiService.portalPage?.department?.delete
  }];
  const reload = async (department?: Department | null) => {
    // setItems(() => {
    //   return DepartmentService.getCurrent(department?.code);
    // });
  }
  const newItem = () => {
    setOpenModalDetail(true);
    setIdSelect('');
  }
  const buttonHeader = !!ApiService.portalPage?.department?.create ? [{
    label: 'Tạo mới',
    color: 'primary',
    iconLeft: <AddIcon />,
    onClick: newItem,
  }] : [];
  return (
    <>
      <PageHeader
        title="Quản lý cơ quan đơn vị"
        action={ApiService.portalPage?.department?.create && (
          <SdButton icon={<AddIcon />} label="Tạo mới" onClick={newItem} />
        )}
      />
      <div>
        <div className={style.filter}>
          <FilterTenant onChange={(department: Department | null, tenantCode: string | undefined) => {
            setTenantCode(tenantCode || '');
            setDepartmentSelect(department);
            setChecksum(department?.code);
          }} ref={childFilterTenantRef} />
        </div>
        <div className={style.content} style={{ height: 'calc(100vh - 173px)' }}>
          <SdGrid
            items={async () => {
              const departments = await DepartmentService.getCurrent(departmentSelect?.code);
              return departments?.[0]?.children || [];
            }}
            columns={columns}
            commands={commands}
            checksum={checksum}
          />
        </div>
        {openModalDetail && <ModalDepartment id={idSelect} open={openModalDetail}
          parentCode={departmentSelect?.code}
          level={(departmentSelect?.level || 0) + 1}
          tenantCode={tenantCode}
          onClose={() => {
            setIdSelect('');
            setOpenModalDetail(false);
          }}
          onAgree={() => {
            setIdSelect('');
            childFilterTenantRef.current?.loadData();
            setOpenModalDetail(false);
          }} />}
      </div>
    </>
  )
}

export default AgenciesUnits
