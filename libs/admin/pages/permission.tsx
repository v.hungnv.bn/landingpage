import type { NextPage } from 'next'
import { PermissionService } from '../services/permission/permission.service';
import { PageHeader } from '../components';
import { SdGrid } from '@lib/core/components';

const Permission: NextPage = () => {

  return (
    <>
      <PageHeader title="Quản lý quyền" />
      <SdGrid
        items={() => {
          return PermissionService.apiComponents();
        }}
        columns={[{
          field: 'projectCode',
          label: 'Dự án',
          type: 'string',
          width: '150px'
        }, {
          field: 'type',
          label: 'Loại',
          type: 'string',
          width: '150px'
        }, {
          field: 'code',
          label: 'Mã quyền',
          type: 'string',
          width: '150px'
        }, {
          field: 'name',
          label: 'Tên quyền',
          type: 'string',
        }]}
      />
    </>
  )
}

export default Permission