import type { NextPage } from 'next'
import Table from '../../core/components/table/Table';
import { EnhancedTableProps, SdFindReq } from '../../core/components/table/interface/table.interface';
import style from '../styles/page.module.scss';
import { useEffect, useState } from "react";
import { ProjectService } from "../services";
import { LoadingService } from '../../core/services/loading.service';
import { PageHeader } from '../components';

const Project: NextPage = () => {
  const [config, setConfig] = useState<EnhancedTableProps>();
  const [items, setItems] = useState<any[]>([]);
  const [totalItem, setTotalItem] = useState<number>(0);

  useEffect(() => {
    setConfig({
      headCells: [
        {
          field: 'code',
          label: 'Mã',
          type: 'string',
          filter: {
            type: 'string',
            field: 'code'
          }
        }, {
          field: 'name',
          label: 'Tên phần mềm',
          type: 'string',
          filter: {
            type: 'string',
            field: 'name'
          }
        }],
      fieldKey: 'id',
    })
  }, []);

  const handleFilter = async (fields: SdFindReq) => {
    LoadingService.start();
    try {
      const res = await ProjectService.getProject(fields);
      setTotalItem(res.total);
      setItems(res.items);
    } catch (error) {} finally{
      LoadingService.stop();
    }
  }

  return (
    <>
      <PageHeader title="Quản lý phần mềm công ty" />
      <div style={{ height: 'calc(100vh - 90px)' }} className={style.content}>
        {config && items ? <Table pageLength={totalItem} Items={items} onChangeFilter={handleFilter} config={config}></Table> : ''}
      </div>
    </>
  )
}

export default Project