import type { NextPage } from 'next'
import { useEffect, useState } from "react";
import * as uuid from 'uuid';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import style from '../styles/page.module.scss';
import ModalDetailRole from '../modals/detail-edit.role';
import { RoleService } from '../services/role/role.service';
import { Department } from "../services";
import FilterTenant from '../components/FilterTenant';

import EditIcon from '@mui/icons-material/Edit';
import { ApiService, NotifyService } from '../../core/services';
import { reqFilterData } from '../services/Define.function';
import { SdButton, SdGrid } from '@lib/core/components';
import { PageHeader } from '../components';

const RolePage: NextPage = () => {
  const userInfo = ApiService.userInfo;
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState<string>('');
  const [departmentCode, setDepartmentCode] = useState<string>('');
  const [checksum, setChecksum] = useState<string | undefined>(undefined);

  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }

  const uniq = (filters: reqFilterData[], key: string, value: any, operator?: string) => {
    const elm = filters.find(elm => elm.key === key);
    if (elm) {
      filters.splice(filters.indexOf(elm), 1);
    }
    if (value) {
      filters.push({ key: key, value: value, operator: operator || 'CONTAIN' });
    }
    return filters
  }

  const buttonHeader = !ApiService.portalPage?.role?.create ? [] : [{
    label: 'Tạo mới',
    color: 'primary',
    iconLeft: <AddIcon />,
    onClick: newItem,
  }];
  return (
    <>
      <PageHeader
        title="Quản lý vai trò"
        action={ApiService.portalPage?.user?.create && (
          <SdButton icon={<AddIcon />} label="Tạo mới" onClick={newItem} />
        )}
      />
      <div>
        <div className={style.filter}>
          <FilterTenant onChange={(item: Department | null, tenantCode: string | undefined) => {
            setDepartmentCode(item?.code || '');
            setChecksum(uuid.v4());
          }} />
        </div>
        <div style={{ maxHeight: 'calc(100vh - 228px)' }} className={style.content}>
          <SdGrid
            items={() => {
              return RoleService.all(departmentCode);
            }}
            columns={[{
              field: 'name',
              label: 'Vai trò',
              width: '300px',
              type: 'string'
            }, {
              field: 'permissions',
              label: 'Chi tiết quyền',
              type: 'string',
              transform: (value, item) => item.permissions?.join(', ')
            }]}
            commands={[{
              title: 'Sửa',
              icon: <EditIcon />,
              onClick: (item) => {
                setIdSelect(item.id);
                setOpenModal(true);
              },
              hidden: (item) => !ApiService.portalPage?.role?.update || !item.editable
            }, {
              title: 'Xóa',
              icon: <DeleteIcon />,
              onClick: (item) => {
                NotifyService.confirm({
                  title: 'Xóa vai trò',
                  yesTitle: 'Đồng ý',
                  noTitle: 'Hủy bỏ',
                  describe: 'Bạn có muốn xóa role này không?',
                  onAgree: async () => {
                    await RoleService.delete(item.id);
                    NotifyService.success('Xóa role thành công.', null);
                    setChecksum(uuid.v4());
                  },
                });
              },
              hidden: (item) => !ApiService.portalPage?.role?.delete || !item.deletable
            }]}
            checksum={checksum}
          />
        </div>
        {openModal && <ModalDetailRole id={idSelect} open={openModal}
          departmentCode={departmentCode} onClose={handleClose}
          onAgree={() => {
            setOpenModal(false);
            setChecksum(uuid.v4());
          }} />}

      </div>
    </>
  )
}

export default RolePage