import type { NextPage } from 'next'
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { NotifyService } from '../../core/services/notify.service'
import style from '../styles/page.module.scss';
import { useEffect, useState, useRef } from "react";
import ModalDomain from '../modals/new-edit.domain';
import { DomainService } from '../services';
import FilterTenant from '../components/FilterTenant';
import { ApiService } from '../../core/services';
import { PageHeader } from '../components';
import { SdButton, SdGrid, SdGridRef } from '@lib/core/components';

const DomainPage: NextPage = () => {
  const userInfo = ApiService.userInfo;
  const [idSelect, setIdSelect] = useState('');
  const [openModalDetail, setOpenModalDetail] = useState(false);
  const [departmentCode, setDepartmentCode] = useState<string | undefined>(undefined);
  const grid = useRef<SdGridRef | null>(null);
  const newItem = () => {
    setIdSelect('');
    setOpenModalDetail(true);
  }

  useEffect(() => {
    grid?.current?.reload();
  }, [departmentCode]);

  return (
    <>
      <PageHeader
        title="Quản lý tên miền"
        action={(
          <SdButton icon={<AddIcon />} label="Tạo mới" onClick={newItem} />
        )}
      />
      <div>
        {userInfo.isVNA && <div className={style.filter}>
          <FilterTenant onChange={(deparment) => {
            setDepartmentCode(deparment?.code);
          }} />
        </div>}
        <div style={{ maxHeight: 'calc(100vh - 175px)' }} className={style.content}>
          <SdGrid
            ref={grid}
            items={() => {
              return DomainService.all(departmentCode).then(domains => domains).catch(err => {
                console.error(err);
                return [];
              });
            }}
            columns={[{
              field: 'projectCode',
              label: 'Mã dự án',
              type: 'string',
              width: '150px'
            }, {
              field: 'departmentCode',
              label: 'Mã đơn vị',
              type: 'string',
              width: '200px'
            }, {
              field: 'host',
              label: 'Tên miền',
              type: 'string',
            }]}
            commands={[{
              title: 'Sửa',
              icon: <EditIcon />,
              onClick: (item) => {
                setIdSelect(item.id);
                setOpenModalDetail(true);
              },
            }, {
              title: 'Xóa',
              icon: <DeleteIcon />,
              onClick: (item) => {
                NotifyService.confirm({
                  title: 'Xóa tên miền',
                  yesTitle: 'Đồng ý',
                  noTitle: 'Hủy bỏ',
                  describe: 'Bạn có muốn xóa tên miền này không?',
                  onAgree: () => {
                    DomainService.delete(item.id).then(() => {
                      NotifyService.success('Xóa tên miền thành công');
                      grid?.current?.reload();
                    }).catch(error => {
                      console.error(error);
                      NotifyService.error('Xóa tên miền thất bại');
                    });
                  },
                })
              }
            }]}
          />
        </div>
        {openModalDetail && <ModalDomain open={openModalDetail} id={idSelect}
          departmentCode={departmentCode}
          onClose={() => {
            setOpenModalDetail(false);
          }}
          onAgree={() => {
            setOpenModalDetail(false);
            grid?.current?.reload();
          }} />}
      </div>
    </>
  )
}

export default DomainPage;
