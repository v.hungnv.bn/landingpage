
import type { NextPage } from 'next'
import { useEffect, useState } from "react";
import Table from '../../core/components/table/Table';
import { EnhancedTableProps, SdFindReq } from '../../core/components/table/interface/table.interface';
import { PermissionService } from '../services/permission/permission.service';
import style from '../styles/page.module.scss';
import { ProjectService } from "../services";
import { DefineFunction, reqFilterData } from '../services/Define.function';
import { ApiService, CacheKeys, CacheService } from '../../core/services';
import { LoadingService } from '../../core/services/loading.service';
import Select from '../../core/components/Select/Select';
import { PageHeader } from '../components';
import { SdGrid } from '@lib/core/components';

const GroupPermission: NextPage = () => {
  return (
    <>
      <PageHeader title="Quản lý nhóm quyền" />
      <SdGrid
        items={() => {
          return PermissionService.groups();
        }}
        columns={[{
          field: 'projectCode',
          label: 'Dự án',
          type: 'string',
          width: '150px'
        }, {
          field: 'code',
          label: 'Mã quyền',
          type: 'string',
          width: '150px'
        }, {
          field: 'name',
          label: 'Tên quyền',
          type: 'string',
        }]}
      />
    </>
  )
}

export default GroupPermission