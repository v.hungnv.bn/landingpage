
import { useEffect, useState, useRef } from "react";
import IconButton from '@mui/material/IconButton';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import style from '../styles/new-account.module.scss'
import Select from '../../core/components/Select/Select'
import DateTime from '../../core/components/Date/Date';
import { Autocomplete, TextField } from "@mui/material";
import { Department, LocationService, SaveUserDTO, UserService } from "../services";
import { ApiService, LoadingService, NotifyService } from "@lib/core/services";
import { Input, SdButton, SdModal } from "@lib/core/components";

export const UserInfo = (props: {
  open: boolean,
  onClose?: () => void,
  onAccept?: () => void
}) => {
  const { open, onClose, onAccept } = props;
  const [isOpened, setIsOpened] = useState(false);
  const [user, setUser] = useState<Partial<SaveUserDTO>>({});
  const [urlAvatar, setUrlAvatar] = useState<string | ArrayBuffer | null>(null);
  const [provinces, setProvinces] = useState<Department[]>([]);
  const [districts, setDistricts] = useState<Department[]>([]);
  const [wards, setWards] = useState<Department[]>([]);
  const [province, setProvince] = useState<Department | null>();
  const [district, setDistrict] = useState<Department | null>();
  const [ward, setWard] = useState<Department | null>();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [fileAvatar, setFileAvatar] = useState<File>();

  const childFullNameRef = useRef<{ validate: () => boolean }>();
  const childPositionRef = useRef<{ validate: () => boolean }>();
  const childEmailRef = useRef<{ validate: () => boolean }>();

  const hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  const userInfo = ApiService.userInfo;
  useEffect(() => {
    UserService.get(userInfo.userId).then(user => {
      setUser(user);
      loadLocation(user);
      setUrlAvatar(user.avatar || null);
    });
  }, []);

  useEffect(() => {
    setIsOpened(open);
  }, [open]);

  const loadLocation = async (args?: { provinceCode?: string, districtCode?: string, wardCode?: string }) => {
    try {
      const dataProvinces = await LocationService.get();
      setProvinces(dataProvinces);
      setWards([]);
      setDistricts([]);
      if (args?.provinceCode) {
        const province = dataProvinces.find((item: any) => item.code === args.provinceCode);
        setProvince(province);
        const dataDistricts = await LocationService.getChildren(args?.provinceCode);
        setDistricts(dataDistricts);
        if (args?.districtCode) {
          const district = dataDistricts.find((item: any) => item.code === args.districtCode);
          setDistrict(district);
          const dataWards = await LocationService.getChildren(args?.districtCode);
          setWards(dataWards);
          if (args?.wardCode) {
            const ward = dataWards.find((item: any) => item.code === args.wardCode);
            setWard(ward);
          }
        }
      }
    } catch (error) { }
    finally {
      LoadingService.stop();
      setIsLoading(false)
    }
  }

  const handlerSelectLocation = async (event: any, type: string) => {
    if (type === 'provinceCode') {
      const res = await LocationService.getChildren(event?.code);
      setProvince(event)
      setDistricts(res);
      setDistrict(null);
      setWards([]);
      setWard(null);
    }
    if (type === 'districtCode') {
      const res = await LocationService.getChildren(event?.code);
      setDistrict(event);
      setWards(res);
      setWard(null);
    }
    if (type === 'wardCode') {
      setWard(event);
    }
  }

  const handlerCancel = () => {
    setIsOpened(true);
  }

  const handlerSave = async () => {
    user.provinceCode = province?.code || '';
    user.districtCode = district?.code || '';
    user.wardCode = ward?.code || '';
    const isFull = childFullNameRef?.current?.validate();
    const isPosition = childPositionRef?.current?.validate();
    const isEmail = childEmailRef?.current?.validate();
    if (!isFull || !isPosition || !isEmail) {
      NotifyService.warn('Vui lòng kiêm trả lại thông tin.', null);
      return;
    };
    LoadingService.start();
    try {
      if (fileAvatar) {
        const resFile = await UserService.upload(fileAvatar);
        if (resFile.key) {
          user.avatar = `${hostUser}aws/${resFile.key}`;
        }
      }
      await UserService.updateCurrent(user);
      NotifyService.success('Cập nhật tài khoản thành công', null);
      setIsOpened(true);
      onAccept?.();
    } catch (error) { }
    finally {
      LoadingService.stop();
    }
    return;
  }

  const handlerChange = async (event: any, type: string) => {
    setUser({ ...user, [type]: event });
  }

  return (
    <SdModal opened={isOpened} width={'1090px'}
      footer={<SdButton label='Lưu' onClick={handlerSave} disabled={!(user.username && user.name && user.position)} />}
      title='Cập nhật thông tin'
      onClose={onClose}>
      {!isLoading && <div className='w-100'>
        <div className={style.perInfoText}>Thông tin cá nhân</div>
        <div className="d-flex ">
          <div className={style.perInfoAvatar}>
            <div className={style.perInfoAvatarFile}>
              <IconButton color="primary" aria-label="upload picture" component="label" onClick={() => {
                SdUtility.upload({
                  extensions: ['png', 'jpeg', 'jpg'],
                  maxSizeInMb: 5
                }).then(file => {
                  if (file) {
                    setFileAvatar(file);
                    const reader = new FileReader();
                    reader.onloadend = () => {
                      setUrlAvatar(reader.result);
                    }
                    reader.readAsDataURL(file);
                  }
                });
              }}>
                <div className={style.perInfoAvatarUpload}>
                  {!!urlAvatar ? <img src={urlAvatar as any} /> : <div className={style.perInfoUpload}><PhotoCamera />
                    <div>Tải ảnh đại diện</div></div>}
                </div>
              </IconButton>
              <div> *.jpeg, *.jpg, *.png.</div>
              <div>Kích thước tối đa 5 MB</div>
            </div>
          </div>
          <div className={style.perInfoContent}>
            <div className="row">
              <div className="col">
                <Input required={true} onChange={(event: string) => {
                  handlerChange(event, 'username');
                }} value={user.username} disabled
                  fullWidth size="small"
                  label="Tên đăng nhập" variant="outlined" />
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <Input required={true} onChange={(event: string) => {
                  handlerChange(event, 'name');
                }} fullWidth size="small" ref={childFullNameRef} value={user.name}
                  label="Họ và tên" variant="outlined"></Input>
              </div>
              <div className="col-6">
                <Input required={true} onChange={(event: string) => {
                  handlerChange(event, 'position');
                }} value={user.position}
                  fullWidth size="small" id="position" ref={childPositionRef}
                  label="Chức danh" variant="outlined"></Input>
              </div>
            </div>
            <div className="row mb-16">
              <div className="col-6">
                <Select valueDisplay="name" valueField="code" label='Giới tính'
                  items={[{ code: 'MALE', name: 'Nam' }, { code: 'FEMALE', name: 'Nữ' }]}
                  model={user.gender} onChange={(event: string) => {
                    handlerChange(event, 'gender');
                  }}></Select>
              </div>
              <div className="col-6">
                <DateTime type='date' label="Ngày sinh" model={user.birthday as any} onChange={(event: string) => {
                  if (!event) {
                    user.birthday = null;
                    return;
                  }
                  handlerChange(event, 'birthday');
                }}></DateTime>
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <Input onChange={(event: string) => {
                  handlerChange(event, 'email');
                }} fullWidth size="small" ref={childEmailRef}
                  validateDefine={(value) => {
                    if (value && !value.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
                      return 'Email không đúng định dạng'
                    }
                    return ''
                  }} value={user.email}
                  label="Email" variant="outlined"></Input>
                <div><span className={style.textDescribe}>Dữ liệu email sẽ được sử dụng trong trường hợp cần đặt lại mật khẩu</span></div>
              </div>
              <div className="col-6">
                <Input onChange={(event: string) => {
                  setUser({ ...user, phone: event });
                }} fullWidth
                  size="small"
                  value={user.phone}
                  label="Số điện thoại" variant="outlined" />
              </div>
            </div>
          </div>
        </div>
        <div className={style.perInfoContactText}>Thông tin liên hệ </div>
        <div style={{ paddingBottom: '12px' }} className="d-flex justify-content-end">
          <div className={style.perInfoContact}>
            <div className="row">
              <div className="col-6">
                <Autocomplete id="951ea62e-101d-427e-aaa7-20035dd3ec7c" size='small' options={provinces}
                  getOptionLabel={(option) => option.name} value={province}
                  onChange={(event: any, province: any | null) => {
                    handlerSelectLocation(province, 'provinceCode');
                  }}
                  renderInput={(params) => <TextField {...params} label="Tỉnh/ Thành phố" />}
                />
              </div>
              <div className="col-6">
                <Autocomplete id="951ea62e-101d-427e-aaa7-20035dd3ec71" size='small' options={districts}
                  getOptionLabel={(option) => option.name} value={district}
                  onChange={(event: any, districts: any | null) => {
                    handlerSelectLocation(districts, 'districtCode');
                  }} disabled={!districts.length}
                  renderInput={(params) => <TextField {...params} label="Quận / Huyện" />}
                />
              </div>
            </div>
            <div style={{ marginTop: '24px' }} className="row">
              <div className="col-6">
                <Autocomplete id="951ea62e-101d-427e-aaa7-20035dd3ec73" size='small' options={wards}
                  sx={{ marginTop: '5px' }}
                  getOptionLabel={(option) => option.name} value={ward}
                  onChange={(event: any, wards: any | null) => {
                    handlerSelectLocation(wards, 'wardCode');
                  }} disabled={!wards.length}
                  renderInput={(params) => <TextField {...params} label="Phường/ Xã" />}
                />
              </div>
              <div className="col-6">
                <Input onChange={(event: string) => {
                  handlerChange(event, 'address');
                }} value={user.address} disabled={!districts.length}
                  fullWidth size="small" notShowError={true}
                  label="Tổ/ Ấp" variant="outlined"></Input>
              </div>
            </div>
          </div>
        </div>
      </div>}
    </SdModal>
  )
}
