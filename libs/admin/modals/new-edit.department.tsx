
import { useEffect, useRef, useState } from "react";
import { DepartmentService } from '../services/department/department.service';
import { LocationService } from '../services/location/location.service';
import { NotifyService } from '../../core/services/notify.service'
import { ApiService } from "../../core/services";
import { LoadingService } from "../../core/services/loading.service";
import { Autocomplete, TextField } from "@mui/material";
import { Department, typeDepartment } from "../services";
import Select from '../../core/components/Select/Select';
import { Input, SdButton, SdModal } from "@lib/core/components";

const ModalDepartment = (props: { open: boolean, onClose: () => void, onAgree: () => void, tenantCode?: string, parentCode?: string, level?: number, id?: string }) => {
  const [openModal, setOpenModal] = useState(false);
  const [body, setBody] = useState<any>({});
  const [isUpdate, setIsUpdate] = useState<boolean>(false);

  const [provinces, setProvinces] = useState<Department[]>([]);
  const [types, setTypes] = useState<{ code: string, name: string }[]>([]);
  const [districts, setDistricts] = useState<Department[]>([]);
  const [wards, setWards] = useState<Department[]>([]);
  const [province, setProvince] = useState<Department | null>();
  const [district, setDistrict] = useState<Department | null>();
  const [ward, setWard] = useState<Department | null>();
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const childNameRef = useRef<{ validate: () => boolean }>();

  useEffect(() => {
    const types = typeDepartment.find(item => item.lev === props.level);
    setTypes(types?.data || []);
    setOpenModal(props.open);
  }, [props.open]);

  useEffect(() => {
    getDetail();
    setIsUpdate(props.id ? true : false);
  }, [props.id]);

  const getDetail = async () => {
    if (props.id) {
      LoadingService.start();
      DepartmentService.getById(props.id).then(res => {
        getProvince(res);
        setBody({ ...res });
      }).catch(_ => { })
      return;
    }
    setBody({});
    getProvince();
    setIsLoading(false);
  }

  const handlerSave = async () => {
    if (!childNameRef.current?.validate()) {
      return;
    }
    console.log('province', province);

    body.provinceCode = province?.code || '';
    body.districtCode = district?.code || '';
    body.wardCode = ward?.code || '';
    LoadingService.start();
    if (isUpdate) {
      try {
        const res = await DepartmentService.update(body);
        if (res) {
          NotifyService.success('Cập nhật cơ quan đơn vị mới thành công', null);
          setOpenModal(true);
          props.onAgree();
        }
      } catch (error) { }
      finally {
        LoadingService.stop();
      }
      return;
    }
    const info = ApiService.userInfo;
    body.tenantCode = info.isVNA ? props.tenantCode : info.tenantCode;
    body.isActivated = true;
    body.parentCode = props.parentCode || info.tenantCode;
    try {
      const res = await DepartmentService.create(body);
      if (res) {
        NotifyService.success('Tạo cơ quan đơn vị mới thành công', null);
        setOpenModal(true);
        props.onAgree();
      }
    } catch (error) { } finally {
      LoadingService.stop();
    }
  }

  const getProvince = async (info?: any) => {
    try {
      const dataProvinces = await LocationService.get();
      setProvinces(dataProvinces);
      setWards([]);
      setDistricts([]);
      if (info?.provinceCode) {
        const province = dataProvinces.find((item: Department) => item.code === info.provinceCode);
        setProvince(province);
        const dataDistricts = await LocationService.getChildren(info?.provinceCode);
        setDistricts(dataDistricts);
        if (info?.districtCode) {
          const district = dataDistricts.find((item: Department) => item.code === info.districtCode);
          setDistrict(district);
          const dataWards = await LocationService.getChildren(info?.districtCode);
          setWards(dataWards);
          if (info?.wardCode) {
            const ward = dataWards.find((item: Department) => item.code === info.wardCode);
            setWard(ward);
          }
        }
      }
    } catch (error) { }
    finally {
      LoadingService.stop();
      setIsLoading(false);
    }
  }

  const handlerCancel = () => {
    setOpenModal(false);
    props.onClose();
  }

  const handlerSelectLocation = async (event: Department, type: string) => {
    if (type === 'provinceCode') {
      const res = await LocationService.getChildren(event?.code);
      setProvince(event)
      setDistricts(res);
      setDistrict(null);
      setWards([]);
      setWard(null);
    }
    if (type === 'districtCode') {
      const res = await LocationService.getChildren(event?.code);
      setDistrict(event);
      setWards(res);
      setWard(null);
    }
    if (type === 'wardCode') {
      setWard(event);
    }
  }
  return (
    <SdModal opened={openModal} width={'1090px'}
      footer={<SdButton label='Lưu' onClick={handlerSave} disabled={!(body.name && body.type)} />}
      title={isUpdate ? 'Cập nhật cơ quan đơn vị' : 'Tạo mới cơ quan đơn vị'} onClose={props.onClose}>
      {!isLoading && <div>
        <div className="row pt-12px">
          <div className="col-12">
            <Input required={true} onChange={(event: string) => {
              setBody({ ...body, name: event });
            }} value={body.name}
              fullWidth size="small" ref={childNameRef}
              label="Tên đơn vị" variant="outlined" />
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <Select size='small' items={types}
              model={body.type} required={true}
              onChange={(event: any) => {
                setBody({ ...body, type: event });
              }} label="Loại cơ quan" valueDisplay={"name"} valueField={"code"} />
          </div>
          <div className="col-6">
            <Autocomplete id="951ea62e-101d-427e-aaa7-20035dd3ec7c" size='small' options={provinces}
              getOptionLabel={(option) => option.name} value={province}
              onChange={(event: any, province: any | null) => {
                handlerSelectLocation(province, 'provinceCode');
              }}
              renderInput={(params) => <TextField {...params} label="Tỉnh/ Thành phố" />}
            />
          </div>
        </div>
        <div style={{ marginTop: '24px' }} className="row">
          <div className="col-6">
            <Autocomplete id="951ea62e-101d-427e-aaa7-20035dd3ec71" size='small' options={districts}
              getOptionLabel={(option) => option.name} value={district}
              onChange={(event: any, districts: any | null) => {
                handlerSelectLocation(districts, 'districtCode');
              }} disabled={!districts.length}
              renderInput={(params) => <TextField {...params} label="Quận / Huyện" />}
            />
          </div>
          <div className="col-6">
            <Autocomplete id="951ea62e-101d-427e-aaa7-20035dd3ec73" size='small' options={wards}
              getOptionLabel={(option) => option.name} value={ward}
              onChange={(event: any, wards: any | null) => {
                handlerSelectLocation(wards, 'wardCode');
              }} disabled={!wards.length}
              renderInput={(params) => <TextField {...params} label="Phường/ Xã" />}
            />
          </div>
        </div>
      </div>}
    </SdModal>
  )
}

export default ModalDepartment