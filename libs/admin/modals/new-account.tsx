
import { useEffect, useState, useRef } from "react";
import IconButton from '@mui/material/IconButton';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import Switch from '@mui/material/Switch';
import style from '../styles/new-account.module.scss'
import Select from '../../core/components/Select/Select'
import DateTime from '../../core/components/Date/Date';
import CheckBox from "../../core/components/CheckBox/CheckBox";
import { LocationService } from '../services/location/location.service';
import { NotifyService } from '../../core/services/notify.service'
import { ApiService } from "../../core/services";
import { LoadingService } from "../../core/services/loading.service";
import { Autocomplete, TextField } from "@mui/material";
import { Department, RoleService, SaveUserDTO, UserService } from "../services";
import { Role } from "../services";
import { Input, SdButton, SdModal } from "@lib/core/components";

const ModalNewAccount = (props: {
  open: boolean,
  onClose: () => void,
  onAgree: () => void,
  tenantCode?: string,
  departmentCode?: string,
  id?: string
}) => {
  const { id, tenantCode, departmentCode } = props;
  const [isOpened, setIsOpened] = useState(false);
  const [user, setUser] = useState<Partial<SaveUserDTO>>({});
  const [roles, setRoles] = useState<Partial<Role[]>>([]);
  const [isActivated, setIsActivated] = useState<boolean>(true);
  const [urlAvatar, setUrlAvatar] = useState<string | ArrayBuffer | null>(null);
  const [provinces, setProvinces] = useState<Department[]>([]);
  const [districts, setDistricts] = useState<Department[]>([]);
  const [wards, setWards] = useState<Department[]>([]);
  const [province, setProvince] = useState<Department | null>();
  const [district, setDistrict] = useState<Department | null>();
  const [ward, setWard] = useState<Department | null>();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [isUpdate, setIsUpdate] = useState<boolean>(false);
  const [fileAvatar, setFileAvatar] = useState<File>();

  const childUserRef = useRef<{ validate: () => boolean }>();
  const childFullNameRef = useRef<{ validate: () => boolean }>();
  const childPositionRef = useRef<{ validate: () => boolean }>();
  const childEmailRef = useRef<{ validate: () => boolean }>();

  const hostUser = process.env.NEXT_PUBLIC_USER_HOST;

  useEffect(() => {
    RoleService.all(departmentCode).then(roles => {
      setRoles(roles);
    });
  }, [departmentCode]);

  useEffect(() => {
    setIsOpened(props.open);
  }, [props.open]);

  useEffect(() => {
    getDetail();
    setIsUpdate(!!props.id);
  }, [props.id]);


  const getDetail = async () => {
    if (props.id) {
      LoadingService.start();
      try {
        const res = await UserService.get(props.id);
        getProvince(res);
        setIsActivated(!!res?.isActivated);
        setUser({ ...res });
        setUrlAvatar(res.avatar || null);
      } catch (error) { } finally {
      }
      return;
    }
    setUser({});
    getProvince();
  }

  const getProvince = async (info?: any) => {
    try {
      const dataProvinces = await LocationService.get();
      setProvinces(dataProvinces);
      setWards([]);
      setDistricts([]);
      if (info?.provinceCode) {
        const province = dataProvinces.find((item: any) => item.code === info.provinceCode);
        setProvince(province);
        const dataDistricts = await LocationService.getChildren(info?.provinceCode);
        setDistricts(dataDistricts);
        if (info?.districtCode) {
          const district = dataDistricts.find((item: any) => item.code === info.districtCode);
          setDistrict(district);
          const dataWards = await LocationService.getChildren(info?.districtCode);
          setWards(dataWards);
          if (info?.wardCode) {
            const ward = dataWards.find((item: any) => item.code === info.wardCode);
            setWard(ward);
          }
        }
      }
    } catch (error) { }
    finally {
      LoadingService.stop();
      setIsLoading(false)
    }
  }

  const handlerSelectLocation = async (event: any, type: string) => {
    if (type === 'provinceCode') {
      const res = await LocationService.getChildren(event?.code);
      setProvince(event)
      setDistricts(res);
      setDistrict(null);
      setWards([]);
      setWard(null);
    }
    if (type === 'districtCode') {
      const res = await LocationService.getChildren(event?.code);
      setDistrict(event);
      setWards(res);
      setWard(null);
    }
    if (type === 'wardCode') {
      setWard(event);
    }
  }

  const handlerCancel = () => {
    setIsOpened(true);
    props.onClose();
  }

  const handlerSave = async () => {
    user.provinceCode = province?.code || '';
    user.districtCode = district?.code || '';
    user.wardCode = ward?.code || '';
    user.isActivated = isActivated;
    if (isUpdate) {
      const isFull = childFullNameRef?.current?.validate();
      const isPosition = childPositionRef?.current?.validate();
      const isEmail = childEmailRef?.current?.validate();

      if (!isFull || !isPosition || !isEmail) {
        NotifyService.warn('Vui lòng kiêm trả lại thông tin.', null);
        return;
      };
      delete user.password;
      LoadingService.start();
      try {
        if (fileAvatar) {
          const resFile = await UserService.upload(fileAvatar);
          if (resFile.key) {
            user.avatar = `${hostUser}aws/${resFile.key}`;
          }
        }
        if (props.id) {
          await UserService.update(props.id, user);
        }
        NotifyService.success('Cập nhật tài khoản thành công', null);
        setIsOpened(true);
        props.onAgree();
      } catch (error) { }
      finally {
        LoadingService.stop();
      }
      return;
    }
    const isUser = childUserRef?.current?.validate();
    const isFull = childFullNameRef?.current?.validate();
    const isPosition = childPositionRef?.current?.validate();
    const isEmail = childEmailRef?.current?.validate();

    if (!isUser || !isFull || !isPosition || !isEmail) {
      NotifyService.warn('Vui lòng kiêm trả lại thông tin.', null);
      return;
    };
    const info = ApiService.userInfo;
    user.password = '12345678';
    user.tenantCode = info.isVNA ? props.tenantCode : info.tenantCode;
    user.departmentCode = props.departmentCode || info.departmentCode;
    LoadingService.start();
    try {
      if (fileAvatar) {
        const resFile = await UserService.upload(fileAvatar);
        if (resFile.key) {
          user.avatar = `${hostUser}aws/${resFile.key}`;
        }
      }
      const res = await UserService.create(user);
      NotifyService.success('Tạo tài khoản mới thành công', null);
      setIsOpened(true);
      props.onAgree();
    } catch (error) { } finally {
      LoadingService.stop();
    }
  }
  const handlerAct = (event: any) => {
    setIsActivated(event.target.checked);
  }

  const handlerChange = async (event: any, type: string) => {
    setUser({ ...user, [type]: event });
  }

  return (
    <SdModal opened={isOpened} width={'1090px'}
      footer={<SdButton label='Lưu' onClick={handlerSave} disabled={!(user.username && user.name && user.position)} />}
      title={props.id ? 'Cập nhật tài khoản' : "Tạo mới tài khoản"} onClose={props.onClose}>
      {!isLoading && <div className='w-100'>
        <div className={style.perInfoText}>Thông tin cá nhân</div>
        <div className="d-flex ">
          <div className={style.perInfoAvatar}>
            <div className={style.perInfoAvatarFile}>
              <IconButton color="primary" aria-label="upload picture" component="label" onClick={() => {
                SdUtility.upload({
                  extensions: ['png', 'jpeg', 'jpg'],
                  maxSizeInMb: 5
                }).then(file => {
                  if (file) {
                    setFileAvatar(file);
                    const reader = new FileReader();
                    reader.onloadend = () => {
                      setUrlAvatar(reader.result);
                    }
                    reader.readAsDataURL(file);
                  }
                });
              }}>
                <div className={style.perInfoAvatarUpload}>
                  {!!urlAvatar ? <img src={urlAvatar as any} /> : <div className={style.perInfoUpload}><PhotoCamera />
                    <div>Tải ảnh đại diện</div></div>}
                </div>
              </IconButton>
              <div> *.jpeg, *.jpg, *.png.</div>
              <div>Kích thước tối đa 5 MB</div>
            </div>
            <div className="d-flex align-items-center justify-content-sm-between">
              <div className={style.perInfoTextAct}>Kích hoạt</div>
              <Switch onChange={handlerAct} checked={isActivated} />
            </div>
          </div>
          <div className={style.perInfoContent}>
            <div className="row">
              <div className="col">
                <Input required={true} onChange={(event: string) => {
                  handlerChange(event, 'username');
                }} value={user.username} disabled={isUpdate}
                  fullWidth size="small" ref={childUserRef}
                  validateDefine={(value) => {
                    if (value && !value.match(/^[a-zA-Z0-9\-\_]+$/)) {
                      return 'Tên đăng nhập không đúng định dạng'
                    }
                    return ''
                  }}
                  label="Tên đăng nhập" variant="outlined" />
              </div>
              {!isUpdate && <div className="col">
                <Input required={true} onChange={(event: string) => {
                  handlerChange(event, 'password');
                }} value={'12345678'} disabled={true}
                  fullWidth size="small" isPassWord={true}
                  label="Mật khẩu" variant="outlined"></Input>
              </div>}
            </div>
            <div className="row">
              <div className="col-6">
                <Input required={true} onChange={(event: string) => {
                  handlerChange(event, 'name');
                }} fullWidth size="small" ref={childFullNameRef} value={user.name}
                  label="Họ và tên" variant="outlined"></Input>
              </div>
              <div className="col-6">
                <Input required={true} onChange={(event: string) => {
                  handlerChange(event, 'position');
                }} value={user.position}
                  fullWidth size="small" id="position" ref={childPositionRef}
                  label="Chức danh" variant="outlined"></Input>
              </div>
            </div>
            <div className="row mb-16">
              <div className="col-6">
                <Select valueDisplay="name" valueField="code" label='Giới tính'
                  items={[{ code: 'MALE', name: 'Nam' }, { code: 'FEMALE', name: 'Nữ' }]}
                  model={user.gender} onChange={(event: string) => {
                    handlerChange(event, 'gender');
                  }}></Select>
              </div>
              <div className="col-6">
                <DateTime type='date' label="Ngày sinh" model={user.birthday as any} onChange={(event: string) => {
                  if (!event) {
                    user.birthday = null;
                    return;
                  }
                  handlerChange(event, 'birthday');
                }}></DateTime>
              </div>
            </div>
            <div className="row mb-16">
              <div className="col-6">
                <Select valueDisplay="name" valueField="code" label='Vai trò'
                  items={roles}
                  model={user.roleCode}
                  onChange={(event: string) => {
                    setUser({ ...user, roleCode: event });
                  }} />
              </div>
              <div className="col-6">
                <Input onChange={(event: string) => {
                  setUser({ ...user, phone: event });
                }} fullWidth
                  size="small"
                  value={user.phone}
                  label="Số điện thoại" variant="outlined" />
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <Input onChange={(event: string) => {
                  handlerChange(event, 'email');
                }} fullWidth size="small" ref={childEmailRef}
                  validateDefine={(value) => {
                    if (value && !value.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
                      return 'Email không đúng định dạng'
                    }
                    return ''
                  }} value={user.email}
                  label="Email" variant="outlined"></Input>
                <div><span className={style.textDescribe}>Dữ liệu email sẽ được sử dụng trong trường hợp cần đặt lại mật khẩu</span></div>
              </div>
              <div className="col-6">
                <CheckBox checked={!!user.isLeader} label="Lãnh đạo cơ quan" onChange={(event: any) => {
                  handlerChange(event.target.checked, 'isLeader');
                }} />
              </div>
            </div>
          </div>
        </div>
        <div className={style.perInfoContactText}>Thông tin liên hệ </div>
        <div style={{ paddingBottom: '12px' }} className="d-flex justify-content-end">
          <div className={style.perInfoContact}>
            <div className="row">
              <div className="col-6">
                <Autocomplete id="951ea62e-101d-427e-aaa7-20035dd3ec7c" size='small' options={provinces}
                  getOptionLabel={(option) => option.name} value={province}
                  onChange={(event: any, province: any | null) => {
                    handlerSelectLocation(province, 'provinceCode');
                  }}
                  renderInput={(params) => <TextField {...params} label="Tỉnh/ Thành phố" />}
                />
              </div>
              <div className="col-6">
                <Autocomplete id="951ea62e-101d-427e-aaa7-20035dd3ec71" size='small' options={districts}
                  getOptionLabel={(option) => option.name} value={district}
                  onChange={(event: any, districts: any | null) => {
                    handlerSelectLocation(districts, 'districtCode');
                  }} disabled={!districts.length}
                  renderInput={(params) => <TextField {...params} label="Quận / Huyện" />}
                />
              </div>
            </div>
            <div style={{ marginTop: '24px' }} className="row">
              <div className="col-6">
                <Autocomplete id="951ea62e-101d-427e-aaa7-20035dd3ec73" size='small' options={wards}
                  sx={{ marginTop: '5px' }}
                  getOptionLabel={(option) => option.name} value={ward}
                  onChange={(event: any, wards: any | null) => {
                    handlerSelectLocation(wards, 'wardCode');
                  }} disabled={!wards.length}
                  renderInput={(params) => <TextField {...params} label="Phường/ Xã" />}
                />
              </div>
              <div className="col-6">
                <Input onChange={(event: string) => {
                  handlerChange(event, 'address');
                }} value={user.address} disabled={!districts.length}
                  fullWidth size="small" notShowError={true}
                  label="Tổ/ Ấp" variant="outlined"></Input>
              </div>
            </div>
          </div>
        </div>
      </div>}
    </SdModal>
  )
}

export default ModalNewAccount