
import { useEffect, useRef, useState } from "react";
import Select from '../../core/components/Select/Select';
import { DomainService } from '../services/domain/domain.service';
import { NotifyService } from '../../core/services/notify.service';
import { Department, DepartmentService, ProjectService } from "../services";
import { LoadingService } from "../../core/services/loading.service";
import { Autocomplete, TextField } from "@mui/material";
import { Input, SdButton, SdModal } from "@lib/core/components";

const ModalDomain = (props: { open: boolean, onClose: () => void, onAgree: () => void, departmentCode?: string, id?: string }) => {
  const [openModal, setOpenModal] = useState(false);
  const [projects, setProject] = useState<any[]>([]);
  const [body, setBody] = useState<any>({});
  const [departments, setDepartments] = useState<Department[]>([]);
  const [isUpadte, setIsUpadte] = useState<boolean>(false);
  const [department, setDepartment] = useState<Department | null>();

  const childHostRef = useRef<{ validate: () => boolean }>();

  useEffect(() => {
    setOpenModal(props.open);
    getProject();
  }, [props.open]);

  useEffect(() => {
    DepartmentService.getCurrent(props.departmentCode || 'VNA').then(res => {
      setDepartment(res?.[0]);
      setDepartments(res?.[0]?.children || []);
    }).catch(_ => { })
  }, [props.departmentCode]);

  const getProject = async () => {
    const projects = await ProjectService.all();
    setProject(projects);
  }

  useEffect(() => {
    getDetail();
    setIsUpadte(props.id ? true : false);
  }, [props.id]);

  const getDetail = async () => {
    if (props.id) {
      LoadingService.start();
      try {
        const res = await DomainService.getById(props.id);
        setBody({ ...res });
      } catch (error) { } finally {
        LoadingService.stop();
      }
      return;
    }
    setBody({});
  }

  const handlerSave = async () => {
    if (!childHostRef?.current?.validate() || !body.projectCode || !body.departmentCode) {
      NotifyService.warn('Kiểm tra lại thông tin', null);
      return;
    }
    LoadingService.start();
    if (isUpadte) {
      try {
        const res = await DomainService.update(body);
        if (res) {
          NotifyService.success('Cap nhat tên miền mới thành công', null);
          setOpenModal(false);
          props.onAgree();
        }
      } catch (error) { } finally {
        LoadingService.stop();
      }
      return;
    }
    body.isActivated = true;
    try {
      const res = await DomainService.create({
        projectCode: body.projectCode,
        departmentCode: body.departmentCode || props.departmentCode || 'VNA',
        host: body.host
      });
      if (res) {
        NotifyService.success('Tạo tên miền mới thành công', null);
        setOpenModal(false);
        props.onAgree();
      }
    } catch (error) { } finally {
      LoadingService.stop();
    }
  }

  const handlerChangeHost = (event: string) => {
    setBody({ ...body, host: event });
  }

  const handlerCancel = () => {
    setOpenModal(false);
    props.onClose();
  }

  const buttonFooters = [{ variant: "text", color: 'inherit', onClick: handlerCancel, label: 'Hủy' }, { onClick: handlerSave, label: 'Lưu lại', disabled: !(body.departmentCode && body.projectCode && body.host) }]

  return (
    <SdModal opened={openModal} width={'1090px'}
      footer={<SdButton label='Lưu' onClick={handlerSave} disabled={!(body.departmentCode && body.projectCode && body.host)} />}
      title={isUpadte ? 'Cập nhật tên miền' : 'Tạo mới tên miền'} onClose={props.onClose}>
      <div>
        <div className="row mb-16">
          <div className="col-6">
            {isUpadte ? <Input required={true} onChange={() => { }}
              fullWidth size="small" notShowError={true}
              value={department?.name || 'VNA'} disabled={true}
              label="Cơ quan đơn vị" variant="outlined" /> : <Autocomplete id="951ea62e-101d-427e-aaa7-20035dd3ec89" size='small'
                options={departments} sx={{ marginTop: '5px' }}
                getOptionLabel={(option) => option.name}
                onChange={(event: any, department: Department | null) => {
                  setBody({ ...body, departmentCode: department?.code || props.departmentCode })
                }}
                renderInput={(params) => <TextField {...params} label="Cơ quan đơn vị" />}
            />}
          </div>
          <div className="col-6">
            <Select label='Phần mềm công ty' size='small' items={projects}
              valueField='code' valueDisplay='name'
              model={body.projectCode} disabled={isUpadte}
              onChange={(e) => setBody({ ...body, projectCode: e })} />
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <Input required={true} onChange={handlerChangeHost}
              fullWidth size="small" ref={childHostRef}
              value={body.host}
              validateDefine={(value) => {
                if (value && !value.match(/^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/)) {
                  return 'Tên miền không đúng định dạng'
                }
                return ''
              }}
              label="Tên miền" variant="outlined" />
          </div>
        </div>
      </div>
    </SdModal>
  )
}

export default ModalDomain