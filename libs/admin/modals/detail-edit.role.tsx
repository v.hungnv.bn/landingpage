
import { useEffect, useState, useRef } from "react";
import style from '../styles/detail-edit.role.module.scss'
import Table from '../../core/components/table/Table';
import { EnhancedTableProps } from "../../core/components/table/interface/table.interface";
import { Permission, PermissionService, PermissionType, RoleService } from "../services";
import { Input, SdButton, SdModal } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";

const ModalDetailRole = (props: { open: boolean, onClose: () => void, onAgree: () => void, departmentCode: string, projectCode?: string, id: string }) => {
  const [openModal, setOpenModal] = useState(false);
  const [roleDetail, setRoleDetail] = useState<any>({});
  const [groupPermissions, setGroupPermissions] = useState<Permission[]>([]);
  // const [itemSelect, setItemSelect] = useState<string>('');
  // const [dataPermission, setDataPermission] = useState<any[]>([]);
  const [config, setConfig] = useState<EnhancedTableProps>();
  const [childrenPermission, setChildrenPermission] = useState<string[]>([]);

  const childName = useRef<{ validate: () => boolean }>();
  const childCode = useRef<{ validate: () => boolean }>();

  useEffect(() => {
    getPermissionGroup();
    setOpenModal(props.open);
  }, [props.open]);

  useEffect(() => {
    getDetail()
  }, [props.id]);

  const getDetail = async () => {
    if (props.id) {
      try {
        const res = await RoleService.getById(props.id);
        setRoleDetail({ ...res });
        getConfig(res);
      } catch (error) { }
      return;
    }
    getConfig();
  }

  const getPermissionGroup = async () => {
    LoadingService.start();
    try {
      const permissions = await PermissionService.all();
      setGroupPermissions(permissions.filter(e => e.type === PermissionType.COMPONENT || e.type === PermissionType.GROUP));
    } catch (error) { }
    finally {
      LoadingService.stop();
    }

  }

  const handlerSave = async () => {
    if (!childrenPermission?.length) {
      NotifyService.warn('Vui lòng chọn quyền con', null)
      return;
    }
    roleDetail.permissions = childrenPermission;
    childName?.current?.validate();
    childCode?.current?.validate();
    if (!roleDetail.name || !roleDetail.permissions.length) {
      NotifyService.warn('Vui lòng kiểm tra lại thông tin', null)
      return;
    }
    LoadingService.start();
    if (props.id) {
      const res = await RoleService.update(roleDetail);
      if (res) {
        NotifyService.success('Cập nhật Role mới thành công', null);
        setOpenModal(true);
        props.onAgree();
      }
      LoadingService.stop();
      return;
    }
    roleDetail.isActivated = true;
    roleDetail.projectCode = props?.projectCode;
    roleDetail.departmentCode = props?.departmentCode;
    const res = await RoleService.create(roleDetail);
    if (res) {
      NotifyService.success('Tạo Role mới thành công', null);
      setOpenModal(true);
      props.onAgree();
    }
    LoadingService.stop();
  }

  const handlerCancel = () => {
    setOpenModal(false);
    props.onClose();
  }

  const getConfig = (data?: any) => {
    if (data) {
      setChildrenPermission(data?.permissions);
    }
    setConfig({
      checked: true,
      headCells: [{
        field: 'code',
        label: 'Mã',
        type: 'string'
      }, {
        field: 'name',
        label: 'Tên quyền',
        type: 'string'
      }],
      fieldKey: 'code',
      hiddenPagination: true,
      arrayCheckDefault: childrenPermission?.length ? childrenPermission : (data?.permissions || []),
      onSetSelect: (item) => {
        if (item?.length) {
          item.forEach((element: string) => {
            if (!childrenPermission.some(id => id === element)) {
              childrenPermission.push(element);
            }
          });
          setChildrenPermission(childrenPermission);
        }
      }
    })
  }

  // const handlerPermission = (item: any) => {
  //   setItemSelect(item.id);
  //   getRole(item.id);
  //   getConfig();
  // }
  const buttonFooters = [{ variant: "text", color: 'inherit', onClick: handlerCancel, label: 'Hủy' }, { onClick: handlerSave, label: 'Lưu lại', disabled: !roleDetail.name }]

  return (
    <SdModal opened={openModal} 
    width={'1090px'} 
    footer={<SdButton label='Lưu' onClick={handlerSave} disabled={!roleDetail.name} />}
    title="Tạo mới role" 
    onClose={props.onClose}>
      <div className='w-100 d-flex'>
        <div className={style.contentLeft}>
          <div>
            <Input required={true} onChange={(event: string) => {
              setRoleDetail({ ...roleDetail, name: event });
            }} fullWidth size="small" value={roleDetail.name}
              label="Tên vai trò" variant="outlined" ref={childName}></Input>
          </div>
          {/* <div className={style.contentLeftInfo}>
            <div className={style.contentLeftInfoTitle}>Thông tin Nhóm quyền</div>
            {!!dataRole.length && dataRole.map((item) => (<div key={item.id} className={item.id === itemSelect ? style.contentLeftInfoAc : style.contentLeftInfoNotAc}>
              <div onClick={() => handlerPermission(item)} className={style.contentLeftInfoPer}>{item.name}</div>
            </div>))}
          </div> */}
        </div>
        <div className={style.contentRight}>
          <div className={style.contentRightTable}>
            {config && <Table Items={groupPermissions} config={config}></Table>}
          </div>
        </div>
      </div>
    </SdModal>
  )
}

export default ModalDetailRole