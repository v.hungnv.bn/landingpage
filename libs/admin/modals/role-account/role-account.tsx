import { useEffect, useState } from "react";
import { NotifyService } from "../../../core/services";
import { AccountService } from "../../services/account/accounta.service";
import { RoleService } from "../../services/role/role.service";
import { Role } from "../../services/role/role.model";
import CheckBox from "../../../core/components/CheckBox/CheckBox";
import { LoadingService } from "../../../core/services/loading.service";
import style from './style.module.scss';
import { SdButton, SdModal } from "@lib/core/components";

interface Props {
  onClose: () => void;
  id: string;
  departmentCode: string;
}

export const ModalRoleAccount = ({ onClose, id, departmentCode }: Props): React.ReactElement => {
  const [openModal, setOpenModal] = useState(false);
  const [roles, setRoles] = useState<Role[]>([]);
  const [roleSelects, setRoleSelects] = useState<Role[]>([]);
  const [permissions, setPermissions] = useState<string[]>([]);

  useEffect(() => {
    setOpenModal(true);
    getAllRole();
  }, []);

  useEffect(() => {
    LoadingService.start();
    AccountService.getById(id).then(info => {
      const data: string[] = [];
      info?.roles?.forEach(role => {
        role?.permissions?.forEach((id: string) => {
          if (!data.some(code => code === id)) {
            data.push(id)
          }
        });
      });
      setPermissions(data);
      setRoleSelects(info.roles || []);
    }).catch(_ => { }).finally(() => {
      LoadingService.stop();
    });
  }, [id]);

  const getAllRole = () => {
    RoleService.all(departmentCode).then(res => {
      setRoles(res);
    }).catch(_ => { }).finally(() => {
      LoadingService.stop();
    });
  }

  const handlerCancel = () => {
    onClose();
    setOpenModal(false)
  }

  const handlerSave = () => {
    const body = {
      userId: id,
      roleIds: roleSelects?.map?.(item => item.id) || []
    };
    AccountService.saveRole(body).then(res => {
      if (res) {
        NotifyService.success('Phân quyền cho tài khoản thành công', null);
        onClose();
        setOpenModal(false)
      }
    }).catch(_ => { }).finally(() => {
      LoadingService.stop();
    });
  }

  const handerSelectRole = (item: Role, e: string) => {
    if (e) {
      roleSelects.push(item);
      setRoleSelects(roleSelects);
      if (!permissions.length) {
        setPermissions(item?.permissions);
        return;
      }
      item?.permissions.forEach((id: string) => {
        if (!permissions.some(code => code === id)) {
          permissions.push(id)
        }
      });
      setPermissions([...permissions]);
      return;
    }

    let index = -1
    roleSelects.forEach((role, i) => {
      if (role.id === item.id) {
        index = i;
      }
    })
    if (index > -1) {
      const data: string[] = [];
      roleSelects.splice(index, 1);
      setRoleSelects([...roleSelects]);
      roleSelects.forEach(role => {
        role?.permissions?.forEach((id: string) => {
          if (!data.some(code => code === id)) {
            data.push(id)
          }
        });
      })
      setPermissions(data);
    }
  }

  const buttonFooters = [{ variant: "text", color: 'inherit', onClick: handlerCancel, label: 'Hủy' }, { onClick: handlerSave, label: 'Lưu lại', disabled: !permissions.length }]

  return (
    <SdModal opened={openModal} width={'1090px'}
      footer={<SdButton label='Lưu' onClick={handlerSave} disabled={!permissions.length} />}
      title={"Phân quyền tài khoản"} onClose={onClose}>
      <div className="row pt-12px">
        <div className={['col-6', style.contenRole].join(' ')}>
          <div className={style.title_b}>Danh sách vai trò</div>
          <div className="pt-12px">
            {!!roles?.length && roles.map((item) => (
              <div key={item.id}>
                <CheckBox label={item.name}
                  checked={roleSelects.some(role => role.id === item.id)}
                  onChange={(event: any) => {
                    handerSelectRole(item, event.target.checked)
                  }} />
              </div>
            ))}
          </div>
        </div>
        <div className="col-6">
          <div className={style.title_b}>Quyền phân bổ tương ứng </div>
          <div className="pt-12px">
            {!!permissions?.length && permissions.map((code, index) => (
              <div className={style.permission} key={code}>
                {index + 1}. {code}
              </div>
            ))}
          </div>
        </div>
      </div>
    </SdModal>
  )
}