import { IUserRes } from '../account/account.model';
import { ChangePasswordRequest, UserProfile } from './auth.model';
import { Response } from '../models/responses/response.model'
import { ApiService } from '@lib/core/services';

class AuthService {
  protected readonly hostUser: string = process.env.NEXT_PUBLIC_USER_HOST ?? '';
  protected readonly hostTenant: string = process.env.NEXT_PUBLIC_TENANT_HOST ?? '';

  changePassword(request: ChangePasswordRequest): Promise<Response<IUserRes>> {
    return ApiService.post(`${this.hostUser}auth/change-password`, request)
  }

  getProfile(): Promise<UserProfile> {
    return ApiService.get(`${this.hostUser}auth/current`)
  }
}

export default new AuthService();