export interface ChangePasswordRequest {
  username: string;
  
  departmentCode: string;

  password: string;
}

export interface UserProfile {
  username?: string;

  name?: string;

  email?: string;

  isAdmin?: boolean;

  tenantCode?: string;

  projectCode?: string;

  departmentCode?: string;

  permissions?: string[];

  position?:string;

  departmentName?:string;
}