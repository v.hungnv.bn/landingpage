import { ApiService } from '@lib/core/services';
import { Role, RoleDTO } from './role.model';

export class RoleService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;

  static delete(id: string) {
    return ApiService.delete(`${RoleService.hostUser}role/${id}`);
  }
  static create(body: any) {
    return ApiService.post(`${RoleService.hostUser}role`, body);
  }
  static getById(id: string): Promise<Role> {
    return ApiService.get(`${RoleService.hostUser}role/${id}`);
  }
  static update(body: any) {
    return ApiService.put(`${RoleService.hostUser}role/${body.id}`, body);
  }
  static all(departmentCode?: string) {
    if(!departmentCode) {
      return ApiService.get<RoleDTO[]>(`${RoleService.hostUser}role/all`);
    }
    return ApiService.get<RoleDTO[]>(`${RoleService.hostUser}role/all/${departmentCode}`);
  }
  entityPath: string = 'role';
}

export default new RoleService();
