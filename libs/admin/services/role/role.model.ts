export interface RoleDTO {
  id: string;
  tenantCode: string;
  departmentCode: string;
  projectCode: string;
  code: string;
  name: string;
  permissions: string[];
  editable: boolean;
  deletable: boolean;
}
export interface Role {
  id: string;

  code: string;

  projectCode?: string;

  name?: string;

  permissions: string[];
  
  isActivated?: boolean;
}
