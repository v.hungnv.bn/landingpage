export class DefineFunction {
  static setFilterField = (field: string, val: string, filter: any) => {
    let filterCopy: any = filter;
    const filterField = filterCopy?.filters?.find((item: any) => item.field === field);
    if (filterField) {
      const index = filterCopy.filters.indexOf(filterField);
      if (index > -1) {
        filterCopy.filters.splice(index, 1);
      }
    }
    if (val) {
      if (filterCopy?.filters) {
        filterCopy.filters.push({ field: field, value: val });
      } else {
        filterCopy = {
          filters: [{
            field: field, value: val
          }]
        }
      }
    }
    return filterCopy
  }

  static filterData = (data: any[], filters: reqFilterData[]) => {
    let dataFilter:any[] = data;
    if (filters.length && data.length) {
      filters.forEach(filter => {
        dataFilter = filterDataChilder(dataFilter, filter);
      })
    }
    return dataFilter;
  }
}

export interface reqFilterData {
  key: string,
  value: any,
  operator: string
}

const filterDataChilder=(data: any[], filter: reqFilterData)=>{
  const dataFilter:any[] = [];
  data.forEach(item => {
    if (filter.operator === 'CONTAIN' && (item[filter.key]?.toLowerCase().search(filter.value.toLowerCase())>=0)) {
      dataFilter.push(item)
    }
    if (filter.operator === 'NOT' && item[filter.key] !== filter.value) {
      dataFilter.push(item)
    }
    if (filter.operator === 'EQUAL' && item[filter.key] === filter.value) {            
      dataFilter.push(item)
    }
  })
  return dataFilter;
}
