import { ApiService } from '@lib/core/services';
import { Project } from './project.model';

export class ProjectService {
  static readonly hostTenant = process.env.NEXT_PUBLIC_TENANT_HOST;

  static getProject(body: any) {
    return ApiService.post(`${ProjectService.hostTenant}project/find`, body);
  }
  static all() {
    return ApiService.post<Project[]>(`${ProjectService.hostTenant}project/all`, []);
  }
}

export default new ProjectService();
