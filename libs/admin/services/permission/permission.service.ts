import { ApiService } from '@lib/core/services';
import { Permission, PermissionType } from './permission.model';

export class PermissionService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;

  static all() {
    return ApiService.get<Permission[]>(`${PermissionService.hostUser}permission/all`);
  }

  static async apiComponents() {
    const permissions = await this.all();
    return permissions.filter(e=>e.type !== PermissionType.GROUP);
  }

  static async groups() {
    const permissions = await this.all();
    return permissions.filter(e=>e.type === PermissionType.GROUP);
  }
}

export default new PermissionService();
