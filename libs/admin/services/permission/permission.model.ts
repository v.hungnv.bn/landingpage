export interface Permission {
  id?: string;

  project?: string;

  code?: string;

  name?: string;

  description?: string;

  type?: PermissionType;

  isActivated?: boolean;

  /**
   * danh sách permision con nếu `type` === PermissionType.GROUP
   * 
   * danh sách api con nếu `type` === PermissionType.API
   * @see PermissionType
   */
  children?: string[];
}

export enum PermissionType {
  API = "API",
  COMPONENT = "COMPONENT",
  GROUP = "GROUP",
}

export const PermissionTypes = [{
  code: 'COMPONENT',
  name: 'COMPONENT'
}, {
  code: 'API',
  name: 'API'
}]