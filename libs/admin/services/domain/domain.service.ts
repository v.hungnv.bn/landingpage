import { ApiService } from '@lib/core/services';
import { Domain } from './domain.model';
export class DomainService {
  static readonly hostTenant = process.env.NEXT_PUBLIC_TENANT_HOST;

  static find(body: any) {
    return ApiService.post<{ items: Domain[], total: number }>(`${DomainService.hostTenant}domain/find`, body);
  }
  static delete(id: string) {
    return ApiService.delete(`${DomainService.hostTenant}domain/${id}`);
  }

  static update(body: Partial<Domain>) {
    return ApiService.put(`${DomainService.hostTenant}domain/${body.id}`, body);
  }

  static create(body: Partial<Domain>) {
    return ApiService.post(`${DomainService.hostTenant}domain`, body);
  }

  static all(departmentCode?: string) {
    if (!departmentCode) {
      return ApiService.get<Domain[]>(`${DomainService.hostTenant}domain/all`);
    }
    return ApiService.get<Domain[]>(`${DomainService.hostTenant}domain/all/${departmentCode}`);
  }

  static getById(id: string) {
    return ApiService.get(`${DomainService.hostTenant}domain/${id}`);
  }

  static byHost() {
    return ApiService.get<Domain>(`${DomainService.hostTenant}domain/byHost?host=${window.location.host}`)
  }
}
