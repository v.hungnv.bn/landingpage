export interface Domain {
  id: string;
  tenantCode: string;
  departmentCode: string;
  projectCode: string;
  host: string;
}