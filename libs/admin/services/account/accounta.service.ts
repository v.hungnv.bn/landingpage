import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { IUserRes } from './account.model';

export class AccountService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;

  static getUser(body: PagingRequest | undefined) {
    return ApiService.post<{ items: IUserRes[], total: number }>(`${AccountService.hostUser}user/find`, body);
  }

  static register(body: any) {
    return ApiService.post(`${AccountService.hostUser}auth/register`, body);
  }

  static delete(id: string) {
    return ApiService.delete(`${AccountService.hostUser}user/${id}`);
  }

  static getById(id: string) {
    return ApiService.get<IUserRes>(`${AccountService.hostUser}user/${id}`);
  }

  static update(body: any) {
    return ApiService.put(`${AccountService.hostUser}user/${body?.id}`, body);
  }

  static resetPassword(body: { userId: string }) {
    return ApiService.post(`${AccountService.hostUser}user/reset-password`, body);
  }

  static changePassword(body: { oldPassword: string, newPassword: string }) {
    return ApiService.put(`${AccountService.hostUser}auth/change-password`, body);
  }

  static saveRole(body: { userId: string, roleIds: string[] }) {
    return ApiService.post(`${AccountService.hostUser}user/save-role`, body);
  }

  static upload(file: File): Promise<{ key: string }> {
    return ApiService.upload(`${AccountService.hostUser}aws/upload`, file);
  }
}
