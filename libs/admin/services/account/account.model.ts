import { Role } from "../role/role.model";

export interface IUserRes {
  id: string,
  tenantCode: string,
  departmentCode: string,
  username: string,
  name: string,
  email: string,
  birthday: string,
  gender: string,
  position: string,
  isLeader: boolean,
  province: string,
  district: string,
  ward: string,
  address: string,
  isActivated: boolean,
  roles:Role[],
}
export interface IUserReq {
  pageIndex: number,
  pageSize: number,
  sorts?: {
    field: string,
    direction: string
  }[],
  filters?: {
    field: string,
    value: string,
    operator: string
  }[]
}
export interface IRegisterReq {
  username: string;
  password: string;
  email: string;
  name: string;
  gender: string;
  isActivated: boolean;
  departmentCode: string;
  province: string;
  district: string;
  ward: string;
  address: string;
  isLeader: boolean;
  birthday: string;
}