import { ApiService } from "@lib/core/services";
import { DepartmentDTO } from "@lib/shared/tenant";
import { Department } from "./department.model";

export class DepartmentService {
  static readonly hostTenant = process.env.NEXT_PUBLIC_TENANT_HOST;

  static getDepartment(id: string, body: any) {
    return ApiService.post(`${DepartmentService.hostTenant}department/${id}/find`, body);
  }
  static deleteDepartment(id: string) {
    return ApiService.delete(`${DepartmentService.hostTenant}department/${id}`);
  }
  entityPath: string = 'department';

  static create(body: any) {
    return ApiService.post(`${DepartmentService.hostTenant}department`, body);
  }

  static all() {
    return ApiService.post(`${DepartmentService.hostTenant}department/all`, {});
  }

  static getById(id: string) {
    return ApiService.get(`${DepartmentService.hostTenant}department/${id}`);
  }

  static update(body: any) {
    return ApiService.put(`${DepartmentService.hostTenant}department/${body.id}`, body);
  }

  static getCurrent(departmentCode?: string) {
    if (!departmentCode) {
      return ApiService.get<Department[]>(`${DepartmentService.hostTenant}department/current`);
    }
    return ApiService.get<Department[]>(`${DepartmentService.hostTenant}department/current/${departmentCode}`);
  }

  static currentByLv1() {
    return ApiService.get<DepartmentDTO>(`${DepartmentService.hostTenant}department/currentByLv1`);
  }
}

export default new DepartmentService();
