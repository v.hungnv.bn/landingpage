import { Project } from "../project/project.model";

export interface Department {
  id: string;
  tenantCode?: string;
  code: string;
  name: string;
  phone?: string;
  email?: string;
  logo?: string;
  parentId?: string;
  level: number;
  districtCode?: string;
  districtName?: string;
  wardCode?: string;
  wardname?: string;
  isActivated?: boolean;
  deletable?: boolean;
  editable?:boolean;
  children?: Department[];
}

export const typeDepartment = [{
  lev: 1,
  data: [{
    code: "UBND_TINH",
    name: "Ubnd Tỉnh"
  }, {
    code: "DANG",
    name: "Đảng"
  }]
}, {
  lev: 2,
  data: [{
    code: "UBND_HUYEN",
    name: "UBND Huyện"
  }, {
    code: "SO_BAN_NGANH",
    name: "Sở ban ngành"
  }]
}, {
  lev: 3,
  data: [{
    code: "PHONG_BAN",
    name: "Phòng ban"
  }, {
    code: "UBND_XA",
    name: "UBND Xã"
  }, {
    code: "THPT",
    name: "THPT"
  }]
}, {
  lev: 4,
  data: [{
    code: "TIEU_HOC",
    name: "Tiểu học"
  }, {
    code: "THCS",
    name: "THCS"
  }, {
    code: "MAM_NON",
    name: "MN"
  },{
    code: "NHIEU_CAP",
    name: "Nhiều cấp"
  },{
    code: "KHAC",
    name: "Khác"
  }]
}]