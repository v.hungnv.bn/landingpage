import { ApiService } from "../../core/services";
import { FilterRequest, PagingRequest } from "./models/requests/paging-request.model";
import { PagingResponse } from "./models/responses/paging-response.model";
import { Response } from './models/responses/response.model';

export abstract class EntityService<T> {

  protected readonly hostTenant: string = process.env.NEXT_PUBLIC_TENANT_HOST ?? '';

  protected readonly hostUser: string = process.env.NEXT_PUBLIC_USER_HOST ?? '';

  abstract getHost(): string;

  abstract entityPath: string;

  constructor() {

  }

  find(request: PagingRequest): Promise<PagingResponse<T>> {
    return ApiService.post(`${this.getHost()}${this.entityPath}/find`, request)
  }

  getById(id: string): Promise<Response<T>> {
    return ApiService.get(`${this.getHost()}${this.entityPath}/${id}`)
  }

  findAll(request: FilterRequest[]): Promise<T[]> {
    return ApiService.post(`${this.getHost()}${this.entityPath}/all`, request)
  }

  create(data: T): Promise<Response<T>> {
    return ApiService.post(`${this.getHost()}${this.entityPath}/create`, data)
  }

  update(data: T): Promise<Response<T>> {
    return ApiService.put(`${this.getHost()}${this.entityPath}/udpate`, data)
  }

  delete(id: string): Promise<Response<boolean>> {
    return ApiService.delete(`${this.getHost()}${this.entityPath}/${id}`)
  }
}