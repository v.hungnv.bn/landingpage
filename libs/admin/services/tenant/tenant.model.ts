export interface Tenant {
  id: string;
  
  code: string;

  name: string;

  isActivated?: boolean;
}