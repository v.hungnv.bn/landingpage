import { ApiService } from "../../../core/services";
import { Tenant } from "./tenant.model";

export class TenantService {
  static readonly hostTenant = process.env.NEXT_PUBLIC_TENANT_HOST;

  static all() {
    return ApiService.post<Tenant[]>(`${TenantService.hostTenant}tenant/all`, {});
  }
}
