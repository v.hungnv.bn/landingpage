import { ApiService } from '../../../core/services';
import { PagingRequest } from '../../../core/models';
import { SaveUserDTO, UserDTO } from './user.model';

export class UserService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;

  static all(departmentCode?: string) {
    if(!departmentCode) {
      return ApiService.get<UserDTO[]>(`${UserService.hostUser}user/all`);
    }
    return ApiService.get<UserDTO[]>(`${UserService.hostUser}user/all/${departmentCode}`);
  }

  static getUser(body: PagingRequest) {
    return ApiService.post<{ items: UserDTO[], total: number }>(`${UserService.hostUser}user/find`, body || {});
  }

  static delete(id: string) {
    return ApiService.delete(`${UserService.hostUser}user/${id}`);
  }

  static get(id: string) {
    return ApiService.get<UserDTO>(`${UserService.hostUser}user/${id}`);
  }

  static create(body: Partial<SaveUserDTO>) {
    return ApiService.post<void>(`${UserService.hostUser}auth/register`, body);
  }

  static update(id: string, body: Partial<SaveUserDTO>) {
    return ApiService.put<void>(`${UserService.hostUser}user/${id}`, body);
  }

  static updateCurrent(body: Partial<SaveUserDTO>) {
    return ApiService.post<void>(`${UserService.hostUser}auth/current`, body);
  }

  static resetPassword(body: { userId: string }) {
    return ApiService.post(`${UserService.hostUser}user/reset-password`, body);
  }

  static saveRole(body: { userId: string, roleIds: string[] }) {
    return ApiService.post(`${UserService.hostUser}user/save-role`, body);
  }

  static upload(file: File): Promise<{ key: string }> {
    return ApiService.upload(`${UserService.hostUser}aws/upload`, file);
  }
}