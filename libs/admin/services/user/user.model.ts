export enum Gender {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
  OTHER = 'OTHER',
}

export interface UserDTO {
  id: string;
  tenantCode: string;
  departmentCode: string;
  departmentName: string;
  roleCode: string;
  roleName: string;
  username: string;
  name: string;
  phone: string;
  email: string;
  avatar: string;
  birthday: Date;
  gender: Gender;
  position: string;
  isLeader: boolean;
  isAdmin: boolean;
  provinceCode: string;
  districtCode: string;
  wardCode: string;
  address: string;
  isActivated: boolean;
}

export interface SaveUserDTO {
  tenantCode: string;
  departmentCode: string;
  roleCode: string;
  username: string;
  password: string;
  name: string;
  phone: string;
  email: string;
  avatar: string;
  birthday: Date | null;
  gender: Gender;
  position: string;
  isLeader: boolean;
  isAdmin: boolean;
  provinceCode: string;
  districtCode: string;
  wardCode: string;
  address: string;
  isActivated: boolean;
}