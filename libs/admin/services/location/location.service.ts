import { ApiService } from '../../../core/services';

export class LocationService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;

  static get() {
    return ApiService.get(`${LocationService.hostUser}location?type=province`);
  }

  static getChildren(code: string) {
    return ApiService.get(`${LocationService.hostUser}location/${code}/children`);
  }

  static getByCode(code: string) {
    return ApiService.get(`${LocationService.hostUser}location/${code}`);
  }

}
