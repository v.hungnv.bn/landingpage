export interface PagingRequest {
  pageIndex: number;
  pageSize: number;
  sorts?: SortDefine[];
  filters?: FilterRequest[];
}

export interface SortDefine {
  field: string;
  direction: "asc" | "desc";
}

export interface FilterRequest {
  field: string;

  value?: any;

  from?: Date | number;

  to?: Date | number;

  operator?: FilterOperator;
}

export enum FilterOperator {
  EQUAL = "EQUAL",
  NOT = "NOT",
  GREATER_THAN = "GREATER_THAN",
  LESS_THAN = "LESS_THAN",
  GREATER_THAN_OR_EQUAL = "GREATER_THAN_OR_EQUAL",
  LESS_THAN_OR_EQUAL = "LESS_THAN_OR_EQUAL",
  CONTAIN = "CONTAIN",
  NOT_CONTAIN = "NOT_CONTAIN",
  START_WITH = "START_WITH",
  END_WITH = "END_WITH",
  NOT_START_WITH = "NOT_START_WITH",
  NOT_END_WITH = "NOT_END_WITH",
  BETWEEN = "BETWEEN",
  IN = "IN",
  ISNULL = "ISNULL",
  ARRAY_CONTAINS = "ARRAY_CONTAINS",
  ARRAY_CONTAINED_BY = "ARRAY_CONTAINED_BY",
  ARRAY_OVERLAP = "ARRAY_OVERLAP",
}
