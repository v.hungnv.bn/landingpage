import { PageHeader } from "@lib/admin/components";
import { SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import { NextPage } from "next";
import AddIcon from '@mui/icons-material/Add';
import { useState, useRef } from 'react'
import CategoryDetail from "../modals/category-detail";
import { Stack, Switch } from '@mui/material'
import { LoadingService, NotifyService } from "@lib/core/services";
import { CategoriesService } from "../services/categories/categories.service";
import { CategoryDTO } from "@lib/shared/360";
import { Delete, EditOutlined, RemoveOutlined } from "@mui/icons-material";


const Category: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const grid = useRef<SdGridRef | null>(null);

  const [idSelect, setIdSelect] = useState('');
  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }


  return (
    <>
      <PageHeader
        title="Danh sách chuyên mục"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />

      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          items={async () => {
            const res = await CategoriesService.all()
            return res || []
          }}
          draggable={{
            visible: true,
            mapping: (items: CategoryDTO[]) => {
              return items.map(item => ({
                key: item.id,
                primary: item.name
              }))
            },
            onDragEnd: (newItems) => {
              CategoriesService.sort(newItems.map(e => e.id)).then(() => {
                grid?.current?.reload();
              });
            }
          }}
          columns={[{
            field: 'name',
            label: 'Tên chuyên mục',
            type: 'string',
            template: (value, item) => <Stack display={'flex'} direction={'row'} alignItems={'center'}>
              {item.level >= 1 && <RemoveOutlined color={'secondary'} fontSize={'small'} />}
              {item.level >= 2 && <RemoveOutlined color={'secondary'} fontSize={'small'} />}
              {value}
            </Stack>
          }, {
            field: 'parentName',
            label: 'Chuyên mục cha',
            type: 'string',
            width: '200px'
          },
          {
            field: 'publicRss',
            label: 'RSS',
            type: 'bool',
            width: '150px',
            option: {
              displayOnTrue: 'Có',
              displayOnFalse: 'Không'
            }
          }, {
            field: 'synchronized',
            label: 'Trạng thái',
            type: 'bool',
            option: {
              displayOnTrue: 'Sử dụng',
              displayOnFalse: 'Không sử dụng'
            },
            width: '150px',
            template: (value, item: CategoryDTO) => (<Stack alignItems={'center'}>
              <Switch checked={value} onChange={event => {
                CategoriesService.update(item.id, {
                  synchronized: event.target.checked
                }).then(() => {
                  grid?.current?.reload();
                })
              }} /></Stack>)
          }]}
          commands={[{
            title: 'Sửa',
            icon: <EditOutlined />,
            onClick: (item) => {
              setOpenModal(true);
              setIdSelect(item.id)
            },
            hidden: (item) => !item.editable
          }]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận',
                  describe: 'Xóa các chuyên mục đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    CategoriesService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa chuyên mục thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>

      {openModal && <CategoryDetail id={idSelect} open={openModal}
        onClose={handleClose}
        onAgree={() => {
          grid.current?.reload();
          setIdSelect('');
          setOpenModal(false)
        }} />}
    </>
  )
}

export default Category