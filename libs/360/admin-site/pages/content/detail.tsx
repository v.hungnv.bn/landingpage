
import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect } from "@lib/core/components";
import { Stack, TextField } from "@mui/material";
import IconButton from '@mui/material/IconButton';
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import Image from 'next/image'
import Switch from '@mui/material/Switch';
import { cdn, LoadingService, NotifyService } from "@lib/core/services";
import { SdEditor } from "@lib/core/components/SdEditor/SdEditor";
import { CategoryDTO, PageSaveReq, PostSaveReq } from "@lib/shared/360";
import { SaveOutlined } from '@mui/icons-material';
import { NextPage } from 'next';
import { PageService, PostService } from '../../services';
import { CategoriesService } from '../../services/categories/categories.service';
import { PageHeader } from '@lib/admin/components';
import SettingsIcon from '@mui/icons-material/Settings';
import { AddOutlined, Delete, EditOutlined, Sync } from "@mui/icons-material";
import router from 'next/router';

const ContentPageDetail: NextPage = () => {
    const sdModal = useRef<SdModalRef | null>(null);
    const [openModal, setOpenModal] = useState(false);
    const [slug, setSlug] = useState<string>();
    const { id } = router.query;

    const {
        register,
        handleSubmit,
        control,
        setValue,
        watch,
        formState: { errors },
    } = useForm();

    useEffect(() => {


    }, []);

    useEffect(() => {

        if (typeof id === 'string') {
            getDetail(id);
        }
    }, [id]);

    useEffect(() => {

    }, [register]);


    const getDetail = async (id: string) => {
        LoadingService.start();
        try {
            const res = await PageService.getPageDetail(id);
            setValue('name', res?.name);
            setValue('quote', res?.quote);
            setValue('content', res?.content);
            setValue('isPublished', res?.isPublished);
            console.log('res__', res);
            setSlug(res?.slug);

        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    const editorContent = watch("description");

    const handlerCancel = () => {
        setOpenModal(false)
    }

    const handlerSave = () => {

    }

    const submitHandler = async (data: any) => {
        console.log('data', data);

        LoadingService.start();
        try {
            const req: Partial<PageSaveReq> = {
                content: data?.content,
                name: data?.name,
                isPublished: data?.isPublished,
                quote: data?.quote,
            };
            if (typeof id === 'string') {
                const res = await PageService.update(id, req);
                NotifyService.success('Cập nhật Thành công!');
                router.push('/admin-360/content');
            } else {
                const res = await PageService.create(req);
                NotifyService.success('Tạo mới Thành công!');
                router.push('/admin-360/content');
            }


        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    const openInNewTab = () => {

        const newWindow = window.open(`/noi-dung/${slug}`, '_blank', 'noopener,noreferrer')
        if (newWindow) newWindow.opener = null
    }
    return (
        <>
            <PageHeader
                title="Tạo mới bài viết"
                action={(
                    <Stack spacing={1} display={'flex'} direction={'row'}>
                        <SdButton label='Hủy' variant="text" color="secondary" onClick={() => router.push('/admin-360/content')} />
                        <SdButton className="ml-12" label='Lưu' onClick={handleSubmit(submitHandler)} />
                    </Stack>
                )}
            />
            <div className='bg-white p-20'>
                <form onSubmit={handleSubmit(submitHandler)} >
                    <div className="row mt-16 mb-32">
                        <div className="col-12">
                            <div className="mb-30">
                                <Controller
                                    name="name"
                                    control={control}
                                    defaultValue=""
                                    rules={{
                                        required: true,
                                        minLength: 6,
                                    }}
                                    render={({ field }) => (
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            id="name"
                                            label="Tiêu đề bài viết"
                                            size="small"
                                            error={Boolean(errors.name)}
                                            helperText={
                                                errors.name
                                                    ? errors.name.type === 'minLength'
                                                        ? 'Tiêu đề bài viết tối thiểu 5 ký tự'
                                                        : 'Tiêu đề bài viết bắt buộc'
                                                    : ''
                                            }
                                            {...field}
                                        ></TextField>
                                    )}
                                ></Controller>
                            </div>

                            <div className="mb-30">
                                <Controller
                                    name="quote"
                                    control={control}
                                    defaultValue=""
                                    render={({ field }) => (
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            id="quote"
                                            label="Trích dẫn"
                                            multiline
                                            rows={4}
                                            {...field}
                                        />
                                    )}
                                ></Controller>
                            </div>

                            <div className='content-editor'>
                                <div>Nội dung bài viết</div>
                                <Controller
                                    name="content"
                                    control={control}
                                    render={({ field }) => (
                                        <SdEditor
                                            {...field}
                                            placeholder={"Nội dung..."}
                                            onChange={(text) => {
                                                field.onChange(text);
                                            }}
                                        />
                                    )}
                                />

                                {/* <SdEditor
                                placeholder={'Nội dung...'}
                                value={editorContent}
                                onChange={(html) => setValue("description", html)}
                            /> */}
                                {/* <p className="MuiFormHelperText-root Mui-error MuiFormHelperText-sizeSmall MuiFormHelperText-contained MuiFormHelperText-filled css-k4qjio-MuiFormHelperText-root">{errors.description && "Tối thiểu 15 ký tự"}</p> */}

                            </div>

                        </div>
                        <div className="col-12">

                            <div className="d-flex align-items-center  mt-30">
                                <div>Xuất bản</div>
                                {/* <Switch name="isPublished" defaultChecked inputRef={register} /> */}

                                <Controller
                                    name="isPublished"
                                    control={control}
                                    defaultValue={false}
                                    render={({ field }) => (
                                        <Switch inputRef={field.ref} checked={field.value} {...field} />
                                    )}
                                ></Controller>

                            </div>


                        </div>
                    </div>

                </form>

            </div>


        </>
    )
}

export default ContentPageDetail