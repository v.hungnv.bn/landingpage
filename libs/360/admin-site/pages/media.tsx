import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdBadge, SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import { ApiService, LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import { Button, Stack, Switch } from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import KeyIcon from '@mui/icons-material/Key';
import DeleteIcon from '@mui/icons-material/Delete';
import SettingsIcon from '@mui/icons-material/Settings';
import { useRef, useState } from "react";
import ModalArticle from "../modals/article-detail";
import * as uuid from 'uuid';
import ModalContent from "../modals/content-detail";
import { MediaService, PageService } from "../services";
import ModalImage from "../modals/image-detail";
import { MediaDTO, MediaType } from "@lib/shared/360";
import Image from 'next/image'
import ModalMedia from "../modals/media-detail";
import { Delete } from "@mui/icons-material";


const host360 = process.env.NEXT_PUBLIC_360_HOST;

const MediaVideo: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const [checksum, setChecksum] = useState<string | undefined>(undefined);
  const grid = useRef<SdGridRef | null>(null);

  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }

  return (
    <>
      <PageHeader
        title="Quản lý video"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />
      <div >

      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          items={async () => {
            const res = await MediaService.all(MediaType.YOUTUBE);
            return res || [];
          }}
          draggable={{
            visible: true,
            mapping: (items: MediaDTO[]) => {
              return items.map(item => ({
                key: item.id,
                primary: item.url
              }))
            },
            onDragEnd: (newItems) => {
              MediaService.sort(newItems.map(e => e.id)).then(() => {
                grid?.current?.reload();
              });
            }
          }}
          columns={[{
            field: 'name',
            label: 'Tên video',
            type: 'string',
            filter: {
              disabled: false
            }
          },{
            field: 'url',
            label: 'Link youtube',
            type: 'string',
            filter: {
              disabled: true
            }
          }, {
            field: 'createdDate',
            label: 'Ngày tạo',
            type: 'date',
            width: '180px',
          }, {
            field: "isPublished",
            label: "Trạng thái",
            type: "bool",
            width: '200px',
            option: {
              displayOnTrue: 'Sử dụng',
              displayOnFalse: 'Không sử dụng'
            },
            template: (value, item: MediaDTO) => (<Stack alignItems={'center'}>
              <Switch checked={value} onChange={event => {
                MediaService.update(item.id, {
                  isPublished: event.target.checked
                }).then(() => {
                  grid?.current?.reload();
                })
              }} /></Stack>)
          }]}
          commands={[{
            title: 'Sửa',
            icon: <EditIcon />,
            onClick: (item) => {
              setIdSelect(item.id);
              setOpenModal(true);
            },
            hidden: (item) => !item.editable
          }]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận xóa',
                  describe: 'Xóa các video đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    MediaService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa video thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
      {openModal && <ModalMedia id={idSelect} open={openModal}
        onClose={handleClose}
        onAgree={() => {
          setOpenModal(false);
          grid.current?.reload();
        }} />}
    </>
  )
}

export default MediaVideo