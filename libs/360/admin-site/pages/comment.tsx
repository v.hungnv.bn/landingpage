import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdGrid, SdGridColumn, SdGridRef } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import { useEffect, useRef, useState } from "react";
import { CommentService } from "../services";
import ModalMember from "../modals/member-detail";
import PublicIcon from '@mui/icons-material/Public';
import { Delete } from "@mui/icons-material";
import { CommentDTO } from "@lib/shared/360";
import { generateFromGridFilter } from "@lib/core/models";
import { Stack } from "@mui/material";

const CommentManager: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [columns, setColumns] = useState<SdGridColumn<CommentDTO>[]>([]);
  const grid = useRef<SdGridRef | null>(null);
  useEffect(() => {
    setColumns([{
      field: 'fullName',
      label: 'Họ tên',
      type: 'string',
      width: '250px',
    }, {
      field: 'postName',
      label: 'Bài viết',
      type: 'string',
      filter: {
        disabled: true
      },
      template: (value, item) => (<a className="link-deco" href={`/bai-viet/${item.postId}`} target={'_blank'}>{value}</a>)
    }, {
      field: 'content',
      label: 'Nội dung',
      type: 'string',
      width: '250px'
    }, {
      field: "isApproved",
      label: "Trạng thái",
      type: "bool",
      width: '150px',
      option: {
        displayOnTrue: 'Đã xuất bản',
        displayOnFalse: 'Chưa xuất bản'
      }
    }]);
  }, []);
  return (
    <>
      <PageHeader
        title="Quản lý bình luận"
      />
      <div>
      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          pagingServer
          items={async (args) => {
            return await CommentService.find(generateFromGridFilter(args, columns));
          }}
          columns={columns}
          commands={[{
            title: 'Xuất bản',
            icon: <PublicIcon />,
            onClick: (item) => {
              NotifyService.confirm({
                title: 'Xuất bản bình luận',
                yesTitle: 'Đồng ý',
                noTitle: 'Hủy',
                describe: 'Bạn có muốn Xuất bản bình luận này không?',
                onAgree: async () => {
                  CommentService.approve(item.id).then((res) => {
                    NotifyService.success('Xuất bản bình luận thành công.', null);
                    grid.current?.reload();
                  });
                },
              });
            },
            hidden: (item) => !item.editable
          }]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận',
                  describe: 'Xóa các bình luận đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    CommentService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa bình luận thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
    </>
  )
}

export default CommentManager