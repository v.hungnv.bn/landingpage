import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdGridRef, SdSelect } from "@lib/core/components";
import { cdn, LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useEffect, useRef, useState } from "react";
import { PublishList, RegionCategoryExtend, RegionService } from "../../services";

import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import { Controller, useForm } from "react-hook-form";
import { IconButton, Stack, Switch, TextField } from "@mui/material";
import { RegionInfo, RegionSaveReq, RegionType, RegionTypes } from "@lib/shared/360";
import ModalRegionNewCategory from "../../modals/region-new-category";
import router from "next/router";
import ModalRegionNewAds, { RegionAdvertisementExtend } from "../../modals/region-new-ads";
import Image from 'next/image'
import ModalRegionNewLinkPage, { RegionPageExtend } from "../../modals/region-new-link-page";
import * as uuid from 'uuid';
import { SaveOutlined } from "@mui/icons-material";
import { DragAndDrop } from "../../components/drag-and-drop";
import { Draggable, DraggableProvided, DraggableStateSnapshot, Droppable, DroppableProvided } from "react-beautiful-dnd";
import DragIndicatorIcon from '@mui/icons-material/DragIndicator';
import ModalRegionNewInfo, { RegionInfoExtend } from "../../modals/region-new-info";


const host360 = process.env.NEXT_PUBLIC_360_HOST;


export const reorder = (list: any[], startIndex: number, endIndex: number) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const RegionForm: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const grid = useRef<SdGridRef | null>(null);
  const [regionType, setRegionType] = useState<RegionType>(RegionType.CATEGORY);
  const [category, setCategory] = useState<RegionCategoryExtend | undefined>();
  const [page, setPage] = useState<RegionPageExtend | undefined>();
  const [advertisement, setAdvertisement] = useState<RegionAdvertisementExtend | undefined>();
  const [info, setInfo] = useState<RegionInfoExtend | undefined>();


  const [buttonTitle, setButtonTitle] = useState<string>();
  const [isActivated, setisActivated] = useState<string>('1');


  const [advertisements, setAdvertisements] = useState<RegionAdvertisementExtend[]>([]);
  const [pages, setPages] = useState<RegionPageExtend[]>([]);
  const [categories, setCategories] = useState<RegionCategoryExtend[]>([]);


  const [infos, setInfos] = useState<RegionInfoExtend[]>([]);


  const { id } = router.query;

  const {
    register,
    handleSubmit,
    control,
    setValue,
    watch,
    formState: { errors },
  } = useForm();

  React.useEffect(() => {
    if (typeof id === 'string') {
      getDetail(id);
    }
  }, [id]);

  useEffect(() => {
    handleSetTitleButton(regionType);

  }, []);

  const getDetail = async (id: string) => {
    LoadingService.start();
    try {
      const res = await RegionService.detail(id);
      res?.categories?.forEach((e: RegionCategoryExtend) => e.uuid = uuid.v4());
      res.advertisements?.forEach((e: RegionAdvertisementExtend) => e.uuid = uuid.v4());
      res?.pages?.forEach((e: RegionPageExtend) => e.uuid = uuid.v4());
      res?.infos?.forEach((e: RegionPageExtend) => e.uuid = uuid.v4());

      setValue('name', res?.name);
      setValue('isActivated', res?.isActivated);
      setRegionType(res.type);
      setCategories(res.categories || []);
      setAdvertisements(res.advertisements || []);
      setPages(res.pages || []);
      setInfos(res.infos || []);

      handleSetTitleButton(res.type);


    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }

  const handleSetTitleButton = (type: string) => {
    if (type === RegionType.CATEGORY) {
      setButtonTitle('Tạo mới chuyên mục');
    }
    if (type === RegionType.ADVERTISE) {
      setButtonTitle('Tạo mới quảng cáo');
    }
    if (type === RegionType.LINK_PAGE) {
      setButtonTitle('Tạo mới liên kết');
    }
    if (type === RegionType.DIEM_DEN_DU_LICH) {
      setButtonTitle('Tạo mới điểm đến');
    }
    if (type === RegionType.BLOG_DU_LICH) {
      setButtonTitle('Tạo mới blog du lịch');
    }
    if (type === RegionType.TOUR_NOI_BAT) {
      setButtonTitle('Tạo mới tour nổi bật');
    }
    if (type === RegionType.DIEM_DEN) {
      setButtonTitle('Tạo mới điểm đến');
    }
  }

  const newItem = () => {
    setOpenModal(true);
    setCategory(undefined);
    setAdvertisement(undefined);
    setPage(undefined);
  }

  const handleClose = () => {
    setOpenModal(false);
    setCategory(undefined);
    setAdvertisement(undefined);
    setPage(undefined);
  }

  const handleChangeRegionType = (item) => {
    setRegionType(item?.value);

    handleSetTitleButton(item?.value);
  }


  const submitHandler = async (data: any) => {
    console.log('data', data);
    LoadingService.start();
    try {
      const req: Partial<RegionSaveReq> = {
        name: data?.name,
        type: regionType,
        isActivated: isActivated === '1',

      };
      if (regionType === RegionType.CATEGORY) {
        req.categories = categories;
      }
      if (regionType === RegionType.ADVERTISE) {
        req.advertisements = advertisements;
      }
      if (regionType === RegionType.LINK_PAGE) {
        req.pages = pages;
      }

      if (regionType === RegionType.DIEM_DEN || regionType === RegionType.DIEM_DEN_DU_LICH || regionType === RegionType.BLOG_DU_LICH || regionType === RegionType.TOUR_NOI_BAT) {
        req.infos = infos;
      }
      if (typeof id === 'string') {
        const res = await RegionService.update(id, req);
        NotifyService.success('Cập nhật Thành công!');
        router.push('/admin-360/region');

      } else {
        const res = await RegionService.create(req);
        NotifyService.success('Tạo mới Thành công!');
        router.push('/admin-360/region');
      }


    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }


  }


  const handleDragEnd = (result: { type: any; source: any; destination: any; }) => {
    console.log('result', result);

    const { type, source, destination } = result;
    if (!destination) return;

    const sourceCategoryId = source.droppableId;
    const destinationCategoryId = destination.droppableId;

    // Reordering categories
    if (type === "droppable-page") {
      const updatedPages = reorder(
        pages,
        source.index,
        destination.index
      );
      setPages(updatedPages);
    }
    if (type === "droppable-category") {
      const updatedCategoies = reorder(
        categories,
        source.index,
        destination.index
      );
      setCategories(updatedCategoies);
    }

    if (type === "droppable-ads") {
      const updatedAdvertisements = reorder(
        advertisements,
        source.index,
        destination.index
      );
      setAdvertisements(updatedAdvertisements);
    }

    if (type === "droppable-info") {
      const updatedInfos = reorder(
        infos,
        source.index,
        destination.index
      );
      setInfos(updatedInfos);
    }
  };

  return (
    <>
      <PageHeader
        title="Tạo mới vùng hiển thị"
        action={(
          <Stack spacing={1} display={'flex'} direction={'row'}>
            <SdButton label='Hủy' variant="text" color="secondary" onClick={() => router.push('/admin-360/region')} />
            <SdButton icon={<SaveOutlined />} label='Lưu' onClick={handleSubmit(submitHandler)} />
          </Stack>
        )}
      />
      <div className="bg-white p-16 rounded-8 mb-16 shadow-f1">
        <form onSubmit={handleSubmit(submitHandler)} >
          <div className="row mb-16">
            <div className="col-12">
              <div className="">
                <Controller
                  name="name"
                  control={control}
                  defaultValue=""
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <TextField
                      variant="outlined"
                      fullWidth
                      id="name"
                      label="Tên vùng"
                      size="small"
                      error={Boolean(errors.name)}
                      helperText={
                        errors.name
                          ? 'Vui lòng nhập tên vùng'
                          : ''
                      }
                      {...field}
                    ></TextField>
                  )}
                ></Controller>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <div className="">
                <SdSelect
                  label="Loại hiển thị"
                  value={regionType}
                  items={RegionTypes}
                  disabled={!!id}
                  sdChange={(item => {
                    handleChangeRegionType(item)
                  })}
                />
              </div>
            </div>
            <div className="col-6">
              <div className="w-100 mb-16">
                <SdSelect
                  label="Trạng thái"
                  value={isActivated}
                  items={PublishList}
                  sdChange={(item => {
                    setisActivated(item?.value || '0')
                  })}

                />
              </div>
            </div>
          </div>


        </form>
      </div>
      <DragAndDrop onDragEnd={handleDragEnd}>
        <div>
          {(regionType === RegionType.CATEGORY || regionType === RegionType.LINK_PAGE || regionType === RegionType.ADVERTISE || regionType === RegionType.DIEM_DEN || regionType === RegionType.DIEM_DEN_DU_LICH || regionType === RegionType.BLOG_DU_LICH || regionType === RegionType.TOUR_NOI_BAT) && (
            <SdButton className="mb-8" icon={<AddIcon />} color={'primary'} label={buttonTitle} onClick={newItem} />
          )}
          {regionType === RegionType.CATEGORY && (
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <colgroup>
                  <col style={{ width: "5%" }} />
                  <col style={{ width: "55%" }} />
                  <col style={{ width: "20%" }} />
                  <col style={{ width: "20%" }} />
                </colgroup>
                <TableHead>
                  <TableRow>
                    <TableCell align="left"></TableCell>
                    <TableCell align="left">Chuyên mục</TableCell>
                    <TableCell align="left">Trạng thái</TableCell>
                    <TableCell align="left"></TableCell>
                  </TableRow>
                </TableHead>

                <Droppable droppableId="droppable" direction="vertical" type="droppable-category">
                  {(droppableProvided: DroppableProvided) => (
                    <TableBody ref={droppableProvided.innerRef}
                      {...droppableProvided.droppableProps}>

                      {categories?.map((item: RegionCategoryExtend, index: number) => (
                        <Draggable
                          key={item.uuid}
                          draggableId={item.uuid || ''}
                          index={index}
                        >
                          {(
                            draggableProvided: DraggableProvided,
                            snapshot: DraggableStateSnapshot
                          ) => {
                            return (
                              <TableRow
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}

                                ref={draggableProvided.innerRef}
                                {...draggableProvided.draggableProps}
                                style={{
                                  ...draggableProvided.draggableProps.style,
                                  background: snapshot.isDragging
                                    ? "rgba(245,245,245, 0.75)"
                                    : "none"
                                }}
                              >
                                <TableCell align="center">
                                  <div {...draggableProvided.dragHandleProps}>
                                    <DragIndicatorIcon />

                                  </div>
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  {item.name || item.id}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  <Switch disabled checked={item.isActivated} onChange={event => {
                                    const checked = event.target.checked;

                                  }} />
                                </TableCell>
                                <TableCell align="right" component="th" scope="row">
                                  <IconButton aria-label="edit" onClick={() => {
                                    setCategory(item);
                                    setOpenModal(true);
                                  }}>
                                    <EditIcon />
                                  </IconButton>
                                  <IconButton aria-label="delete" onClick={() => {
                                    const updateCategories = categories?.filter(e => e.uuid !== item.uuid);
                                    setCategories(updateCategories);
                                  }}>
                                    <DeleteIcon />
                                  </IconButton>

                                </TableCell>
                              </TableRow>
                            );
                          }}
                        </Draggable>
                      ))}
                      {droppableProvided.placeholder}
                    </TableBody>
                  )}
                </Droppable>

                {/* <TableBody>
                  {categories?.map((item: RegionCategoryExtend, index: number) => {
                    return (
                      <TableRow
                        key={item?.uuid}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell component="th" scope="row">
                          {item.name || item.id}
                        </TableCell>
                        <TableCell component="th" scope="row">
                          <Switch disabled checked={item.isActivated} onChange={event => {
                            const checked = event.target.checked;

                          }} />
                        </TableCell>
                        <TableCell align="right" component="th" scope="row">
                          <IconButton aria-label="edit" onClick={() => {
                            setCategory(item);
                            setOpenModal(true);
                          }}>
                            <EditIcon />
                          </IconButton>
                          <IconButton aria-label="delete" onClick={() => {
                            const updateCategories = categories?.filter(e => e.uuid !== item.uuid);
                            setCategories(updateCategories);
                          }}>
                            <DeleteIcon />
                          </IconButton>

                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody> */}

              </Table>
              {openModal && <ModalRegionNewCategory id={idSelect} open={openModal}
                data={category}
                onClose={handleClose}
                onAgree={(data: RegionCategoryExtend) => {
                  setOpenModal(false);
                  if (!categories?.some((e: RegionCategoryExtend) => e.uuid === data.uuid)) {
                    setCategories([
                      ...categories,
                      data
                    ])
                  } else {
                    const updateCategories = categories?.map((ev: RegionCategoryExtend) => {
                      if (ev.uuid == data.uuid) {
                        ev.isActivated = data.isActivated;
                        ev.display = data.display;
                        ev.name = data.name;
                        ev.uuid = data.uuid;
                        ev.id = data.id;
                      }
                      return ev;
                    });
                    setCategories(updateCategories);
                  }
                }} />}
            </TableContainer>


          )}
          {regionType === RegionType.LINK_PAGE && (

            <TableContainer component={Paper}>
              <colgroup>
                <col style={{ width: "5%" }} />
                <col style={{ width: "20%" }} />
                <col style={{ width: "20%" }} />
                <col style={{ width: "35%" }} />
                <col style={{ width: "10%" }} />
                <col style={{ width: "10%" }} />
              </colgroup>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Ảnh</TableCell>
                    <TableCell align="left">Tiêu đề</TableCell>
                    <TableCell align="left">Link</TableCell>
                    <TableCell align="left">Trạng thái</TableCell>
                    <TableCell align="left"></TableCell>
                  </TableRow>
                </TableHead>


                <Droppable droppableId="droppable" direction="vertical" type="droppable-page">
                  {(droppableProvided: DroppableProvided) => (
                    <TableBody ref={droppableProvided.innerRef}
                      {...droppableProvided.droppableProps}>

                      {pages?.map((item: RegionPageExtend, index: number) => (
                        <Draggable
                          key={item.uuid}
                          draggableId={item.uuid || ''}
                          index={index}
                        >
                          {(
                            draggableProvided: DraggableProvided,
                            snapshot: DraggableStateSnapshot
                          ) => {
                            return (
                              <TableRow
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}

                                ref={draggableProvided.innerRef}
                                {...draggableProvided.draggableProps}
                                style={{
                                  ...draggableProvided.draggableProps.style,
                                  background: snapshot.isDragging
                                    ? "rgba(245,245,245, 0.75)"
                                    : "none"
                                }}
                              >
                                <TableCell align="center">
                                  <div {...draggableProvided.dragHandleProps}>
                                    <DragIndicatorIcon />

                                  </div>
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  <Image src={`${host360 + 'aws/' + item.image}`} alt="photo" width="80" height="80" />

                                </TableCell>
                                <TableCell align="left">{item.title}</TableCell>
                                <TableCell align="left">{item.link}</TableCell>
                                <TableCell component="th" scope="row">
                                  <Switch disabled checked={item.isActivated} onChange={event => {
                                    const checked = event.target.checked;

                                  }} />
                                </TableCell>
                                <TableCell align="right" component="th" scope="row">
                                  <IconButton aria-label="edit" onClick={() => {
                                    setOpenModal(true);
                                    setPage(item);
                                  }}>
                                    <EditIcon />
                                  </IconButton>
                                  <IconButton aria-label="delete" onClick={() => {
                                    // handleRemoveItem(item)
                                    const updatePages = pages?.filter(e => e.uuid !== item.uuid);
                                    setPages(updatePages);
                                  }}>
                                    <DeleteIcon />
                                  </IconButton>

                                </TableCell>
                              </TableRow>
                            );
                          }}
                        </Draggable>
                      ))}
                      {droppableProvided.placeholder}
                    </TableBody>
                  )}
                </Droppable>

              </Table>

              {openModal && <ModalRegionNewLinkPage open={openModal}
                data={page}
                onClose={handleClose}
                onAgree={(data: RegionPageExtend) => {
                  setOpenModal(false);
                  if (!pages?.some((e: RegionPageExtend) => e.uuid === data.uuid)) {
                    setPages([
                      ...pages,
                      data
                    ])
                  } else {
                    const updatePages = pages?.map((ev: RegionPageExtend) => {
                      if (ev.uuid == data.uuid) {
                        ev.title = data.title;
                        ev.isActivated = data.isActivated;
                        ev.uuid = data.uuid;
                        ev.link = data.link;
                        ev.quote = data.quote;
                        ev.image = data.image;
                      }
                      return ev;
                    });
                    setPages(updatePages);
                  }


                }} />}
            </TableContainer>

          )}
          {regionType === RegionType.ADVERTISE && (
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <colgroup>
                  <col style={{ width: "5%" }} />
                  <col style={{ width: "20%" }} />
                  <col style={{ width: "20%" }} />
                  <col style={{ width: "35%" }} />
                  <col style={{ width: "10%" }} />
                  <col style={{ width: "10%" }} />
                </colgroup>
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Ảnh</TableCell>
                    <TableCell align="left">Tiêu đề</TableCell>
                    <TableCell align="left">Link</TableCell>
                    <TableCell align="left">Trạng thái</TableCell>
                    <TableCell align="left"></TableCell>
                  </TableRow>
                </TableHead>

                <Droppable droppableId="droppable" direction="vertical" type="droppable-ads">
                  {(droppableProvided: DroppableProvided) => (
                    <TableBody ref={droppableProvided.innerRef}
                      {...droppableProvided.droppableProps}>

                      {advertisements?.map((item: RegionAdvertisementExtend, index: number) => (
                        <Draggable
                          key={item.uuid}
                          draggableId={item.uuid || ''}
                          index={index}
                        >
                          {(
                            draggableProvided: DraggableProvided,
                            snapshot: DraggableStateSnapshot
                          ) => {
                            return (
                              <TableRow
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}

                                ref={draggableProvided.innerRef}
                                {...draggableProvided.draggableProps}
                                style={{
                                  ...draggableProvided.draggableProps.style,
                                  background: snapshot.isDragging
                                    ? "rgba(245,245,245, 0.75)"
                                    : "none"
                                }}
                              >
                                <TableCell align="center">
                                  <div {...draggableProvided.dragHandleProps}>
                                    <DragIndicatorIcon />

                                  </div>
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  <Image src={cdn(item.image)} alt="photo" width="80" height="80" />

                                </TableCell>
                                <TableCell align="left">{item.title}</TableCell>
                                <TableCell align="left">{item.link}</TableCell>
                                <TableCell component="th" scope="row">
                                  <Switch disabled checked={item.isActivated} onChange={event => {
                                    const checked = event.target.checked;

                                  }} />
                                </TableCell>
                                <TableCell align="right" component="th" scope="row">
                                  <IconButton aria-label="edit" onClick={() => {
                                    setOpenModal(true);
                                    setAdvertisement(item);
                                  }}>
                                    <EditIcon />
                                  </IconButton>
                                  <IconButton aria-label="delete" onClick={() => {
                                    const updateAdvertisements = advertisements?.filter(e => e.uuid !== item.uuid);
                                    setAdvertisements(updateAdvertisements);
                                  }}>
                                    <DeleteIcon />
                                  </IconButton>

                                </TableCell>
                              </TableRow>
                            );
                          }}
                        </Draggable>
                      ))}
                      {droppableProvided.placeholder}
                    </TableBody>
                  )}
                </Droppable>
                {/* <TableBody>

                  {advertisements?.map((item: RegionAdvertisementExtend, index: number) => {
                    return (
                      <TableRow key={item.uuid}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell component="th" scope="row">
                          <Image src={cdn(item.image)} alt="photo" width="80" height="80" />

                        </TableCell>
                        <TableCell align="left">{item.title}</TableCell>
                        <TableCell align="left">{item.link}</TableCell>
                        <TableCell component="th" scope="row">
                          <Switch disabled checked={item.isActivated} onChange={event => {
                            const checked = event.target.checked;

                          }} />
                        </TableCell>
                        <TableCell align="right" component="th" scope="row">
                          <IconButton aria-label="edit" onClick={() => {
                            setOpenModal(true);
                            setAdvertisement(item);
                          }}>
                            <EditIcon />
                          </IconButton>
                          <IconButton aria-label="delete" onClick={() => {
                            const updateAdvertisements = advertisements?.filter(e => e.uuid !== item.uuid);
                            setAdvertisements(updateAdvertisements);
                          }}>
                            <DeleteIcon />
                          </IconButton>

                        </TableCell>
                      </TableRow>


                    );
                  })}
                </TableBody> */}
              </Table>
              {openModal && <ModalRegionNewAds open={openModal}
                data={advertisement}
                onClose={handleClose}
                onAgree={(data: RegionAdvertisementExtend) => {
                  setOpenModal(false);

                  if (!advertisements?.some((e: RegionAdvertisementExtend) => e.uuid === data.uuid)) {
                    setAdvertisements([
                      ...advertisements,
                      data
                    ])
                  } else {
                    const updateAdvertisements = advertisements?.map((ev: RegionAdvertisementExtend) => {
                      if (ev.uuid == data.uuid) {
                        ev.title = data?.title,
                          ev.isActivated = data?.isActivated,
                          ev.quote = data?.quote,
                          ev.link = data?.link,
                          ev.image = data.image,
                          ev.uuid = data.uuid;
                      }
                      return ev;
                    });
                    setAdvertisements(updateAdvertisements);
                  }

                }} />}
            </TableContainer>
          )}

          {(regionType === RegionType.DIEM_DEN || regionType === RegionType.DIEM_DEN_DU_LICH || regionType === RegionType.BLOG_DU_LICH || regionType === RegionType.TOUR_NOI_BAT) && (
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <colgroup>
                  <col style={{ width: "5%" }} />
                  <col style={{ width: "20%" }} />
                  <col style={{ width: "20%" }} />
                  <col style={{ width: "35%" }} />
                  <col style={{ width: "10%" }} />
                  <col style={{ width: "10%" }} />
                </colgroup>
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Ảnh</TableCell>
                    <TableCell align="left">Tiêu đề</TableCell>
                    <TableCell align="left">Link</TableCell>
                    <TableCell align="left">Trạng thái</TableCell>
                    <TableCell align="left"></TableCell>
                  </TableRow>
                </TableHead>

                <Droppable droppableId="droppable" direction="vertical" type="droppable-info">
                  {(droppableProvided: DroppableProvided) => (
                    <TableBody ref={droppableProvided.innerRef}
                      {...droppableProvided.droppableProps}>

                      {infos?.map((item: RegionInfoExtend, index: number) => (
                        <Draggable
                          key={item.uuid}
                          draggableId={item.uuid || ''}
                          index={index}
                        >
                          {(
                            draggableProvided: DraggableProvided,
                            snapshot: DraggableStateSnapshot
                          ) => {
                            return (
                              <TableRow
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}

                                ref={draggableProvided.innerRef}
                                {...draggableProvided.draggableProps}
                                style={{
                                  ...draggableProvided.draggableProps.style,
                                  background: snapshot.isDragging
                                    ? "rgba(245,245,245, 0.75)"
                                    : "none"
                                }}
                              >
                                <TableCell align="center">
                                  <div {...draggableProvided.dragHandleProps}>
                                    <DragIndicatorIcon />

                                  </div>
                                </TableCell>
                                <TableCell component="th" scope="row">
                                  <Image src={cdn(item.image)} alt="photo" width="80" height="80" />

                                </TableCell>
                                <TableCell align="left">{item.title}</TableCell>
                                <TableCell align="left">{item.link}</TableCell>
                                <TableCell component="th" scope="row">
                                  <Switch disabled checked={item.isActivated} onChange={event => {
                                    const checked = event.target.checked;

                                  }} />
                                </TableCell>
                                <TableCell align="right" component="th" scope="row">
                                  <IconButton aria-label="edit" onClick={() => {
                                    setOpenModal(true);
                                    setInfo(item);
                                  }}>
                                    <EditIcon />
                                  </IconButton>
                                  <IconButton aria-label="delete" onClick={() => {
                                    const updateInfos = infos?.filter(e => e.uuid !== item.uuid);
                                    setInfos(updateInfos);
                                  }}>
                                    <DeleteIcon />
                                  </IconButton>

                                </TableCell>
                              </TableRow>
                            );
                          }}
                        </Draggable>
                      ))}
                      {droppableProvided.placeholder}
                    </TableBody>
                  )}
                </Droppable>
                {/* <TableBody>

                  {advertisements?.map((item: RegionAdvertisementExtend, index: number) => {
                    return (
                      <TableRow key={item.uuid}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell component="th" scope="row">
                          <Image src={cdn(item.image)} alt="photo" width="80" height="80" />

                        </TableCell>
                        <TableCell align="left">{item.title}</TableCell>
                        <TableCell align="left">{item.link}</TableCell>
                        <TableCell component="th" scope="row">
                          <Switch disabled checked={item.isActivated} onChange={event => {
                            const checked = event.target.checked;

                          }} />
                        </TableCell>
                        <TableCell align="right" component="th" scope="row">
                          <IconButton aria-label="edit" onClick={() => {
                            setOpenModal(true);
                            setAdvertisement(item);
                          }}>
                            <EditIcon />
                          </IconButton>
                          <IconButton aria-label="delete" onClick={() => {
                            const updateAdvertisements = advertisements?.filter(e => e.uuid !== item.uuid);
                            setAdvertisements(updateAdvertisements);
                          }}>
                            <DeleteIcon />
                          </IconButton>

                        </TableCell>
                      </TableRow>


                    );
                  })}
                </TableBody> */}
              </Table>
              {openModal && <ModalRegionNewInfo open={openModal}
                data={info}
                title={buttonTitle}
                onClose={handleClose}
                onAgree={(data: RegionInfoExtend) => {
                  setOpenModal(false);
console.log('data', data)
                  if (!infos?.some((e: RegionInfoExtend) => e.uuid === data.uuid)) {
                    setInfos([
                      ...infos,
                      data
                    ])
                  } else {
                    const updateInfos = infos?.map((ev: RegionInfoExtend) => {
                      if (ev.uuid == data.uuid) {
                        ev.title = data?.title,
                          ev.isActivated = data?.isActivated,
                          ev.quote = data?.quote,
                          ev.link = data?.link,
                          ev.image = data.image,
                          ev.uuid = data.uuid;
                      }
                      return ev;
                    });
                    setInfos(updateInfos);
                  }

                }} />}
            </TableContainer>
          )}
        </div>
      </DragAndDrop>
    </>
  )
}

export default RegionForm


