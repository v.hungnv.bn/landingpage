import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdBadge, SdButton, SdGrid, SdGridColumn, SdGridRef } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useEffect, useRef, useState } from "react";
import { ScheduleService } from "../services";
import ModalSchedule from "../modals/schedule-detail";
import { Delete } from "@mui/icons-material";
import { ScheduleDTO } from "@lib/shared/360";
import { generateFromGridFilter } from "@lib/core/models";

const ScheduleManage: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const [columns, setColumns] = useState<SdGridColumn<ScheduleDTO>[]>([]);
  const grid = useRef<SdGridRef | null>(null);

  useEffect(() => {
    setColumns([{
      field: 'name',
      label: 'Tiêu đề',
      type: 'string',
    }, {
      field: 'fromDate',
      label: 'Từ ngày',
      type: 'date',
      width: '200px',
    }, {
      field: 'toDate',
      label: 'Đến ngày',
      type: 'date',
      width: '200px',
    }, {
      field: "isActivated",
      label: "Trạng thái",
      type: "bool",
      width: '150px',
      option: {
        displayOnTrue: 'Sử dụng',
        displayOnFalse: 'Không sử dụng'
      }
    }])
  }, []);

  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }

  return (
    <>
      <PageHeader
        title="Danh sách lịch công tác"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />
      <div >

      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          pagingServer
          items={async (args) => {
            return await ScheduleService.find(generateFromGridFilter(args, columns));
          }}
          columns={columns}
          commands={[{
            title: 'Sửa',
            icon: <EditIcon />,
            onClick: (item) => {
              setIdSelect(item.id);
              setOpenModal(true);
            },
            hidden: (item) => !item.editable
          }]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận xóa',
                  describe: 'Xóa các lịch công tác đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    ScheduleService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa lịch công tác thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
      {openModal && <ModalSchedule id={idSelect} open={openModal}
        onClose={handleClose}
        onAgree={() => {
          setOpenModal(false);
          grid.current?.reload();
          setIdSelect('');
        }} />}
    </>
  )
}

export default ScheduleManage