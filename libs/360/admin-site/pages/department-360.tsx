import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import { useRef, useState } from "react";
import { LinkedDepartmentService } from "../services/linked-department/linked-department.service";
import LinkedDepartmentDetail from "../modals/linked-department-detail";
import { Delete, EditOutlined } from "@mui/icons-material";
import { LinkedDepartmentDTO } from "@lib/shared/360";

const Department360: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const grid = useRef<SdGridRef | null>(null);

  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }

  return (
    <>
      <PageHeader
        title="Đơn vị liên kết"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          items={async () => {
            return await LinkedDepartmentService.all()
          }}
          draggable={{
            visible: true,
            mapping: (items: LinkedDepartmentDTO[]) => {
              return items.map(item => ({
                key: item.id,
                primary: item.name
              }))
            },
            onDragEnd: (newItems) => {
              LinkedDepartmentService.sort(newItems.map(e => e.id)).then(() => {
                grid?.current?.reload();
              });
            }
          }}
          columns={[{
            field: 'linkedCategoryName',
            label: 'Danh mục',
            type: 'string',
            width: '300px',
          }, {
            field: 'name',
            label: 'Tên đơn vị',
            type: 'string',
          }, {
            field: 'link',
            label: 'Liên kết',
            type: 'string',
            width: '300px',
          }, {
            field: 'isActivated',
            label: 'Trạng thái',
            type: 'bool',
            width: '200px',
            option: {
              displayOnTrue: 'Sử dụng',
              displayOnFalse: 'Không sử dụng'
            }
          }]}
          commands={[{
            title: 'Sửa',
            icon: <EditOutlined />,
            onClick: (item) => {
              setOpenModal(true);
              setIdSelect(item.id)
            },
            hidden: (item) => !item.editable
          }]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận',
                  describe: 'Xóa các đơn vị đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    LinkedDepartmentService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa đơn vị thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
      {openModal && <LinkedDepartmentDetail id={idSelect} open={openModal}
        onClose={handleClose}
        onAgree={() => {
          grid.current?.reload();
          setOpenModal(false);
        }} />}
    </>
  )
}

export default Department360