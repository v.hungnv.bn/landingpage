import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdFilterOption, SdGrid, SdGridColumn, SdGridRef } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next";
import { Stack, Switch } from "@mui/material";
import { useEffect, useRef, useState } from "react";
import ModalArticle from "../modals/article-detail";
import { PostService } from "../services";
import { CategoryDTO, PostDTO, RegionTypes } from "@lib/shared/360";
import { generateFromGridFilter } from "@lib/core/models";
import { AddOutlined, Delete, EditOutlined, Sync } from "@mui/icons-material";
import { SelectDepartmentModal } from "@lib/admin/components";
import SettingsIcon from '@mui/icons-material/Settings';
import ModalPostConfig from "../modals/post-config";
import { CategoriesService } from "../services/categories/categories.service";
import VisibilityIcon from '@mui/icons-material/Visibility';
import router from "next/router";


const Article: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [openPostConfig, setOpenPostConfig] = useState(false);

  const [opened, setOpened] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const [selectedItems, setSelectedItems] = useState<PostDTO[]>([]);
  const [columns, setColumns] = useState<SdGridColumn<PostDTO>[]>([]);
  const grid = useRef<SdGridRef | null>(null);

  useEffect(() => {
    init();
  }, []);

  const init = async () => {
    LoadingService.start();
    try {
      const categories = await CategoriesService.all();
      initGrid(categories);
    } catch (err) {
      console.log(err);
    } finally {
      LoadingService.stop();
    }
  }

  const initGrid = (cats: CategoryDTO[]) => {
    setColumns([{
      field: 'name',
      label: 'Tiêu đề',
      type: 'string',
    }, {
      field: 'categoryId',
      label: 'Chuyên mục',
      width: '200px',
      type: 'values',
      option: {
        items: cats,
        valueField: 'id',
        displayField: 'name'
      },
    }, {
      field: 'publishDate',
      label: 'Ngày xuất bản',
      type: 'date',
      width: '200px',
    }, {
      field: 'synchronized',
      label: 'Đồng bộ',
      type: 'bool',
      width: '150px',
      option: {
        displayOnTrue: 'Có',
        displayOnFalse: 'Không'
      }
    }, {
      field: 'outstanding',
      label: 'Nổi bật',
      type: 'bool',
      width: '150px',
      option: {
        displayOnTrue: 'Nổi bật',
        displayOnFalse: 'Không'
      },
      template: (value, item: PostDTO) => (<Stack alignItems={'center'}>
        <Switch checked={value} onChange={event => {
          PostService.update(item.id, {
            outstanding: event.target.checked
          }).then(() => {
            grid?.current?.reload();
          })
        }} /></Stack>)
    }, {
      field: "isPublished",
      label: "Trạng thái",
      type: 'bool',
      width: '200px',
      option: {
        displayOnTrue: 'Đã xuất bản',
        displayOnFalse: 'Chưa xuất bản'
      },
      template: (value, item: PostDTO) => (<Stack alignItems={'center'}>
        <Switch checked={value} onChange={event => {
          PostService.update(item.id, {
            isPublished: event.target.checked
          }).then(() => {
            grid?.current?.reload();
          })
        }} /></Stack>)
    }])
  }

  const addArticle = () => {
    router.push('/admin-360/article/detail');
  }
  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }
  const handleClose = () => {
    setOpenModal(false);
    setOpenPostConfig(false);
  }
  return (
    <>
      <PageHeader
        title="Danh sách bài viết"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<SettingsIcon />} variant={'outlined'} color={'primary'} label="Cấu hình" onClick={() => setOpenPostConfig(true)} />
            </div>
            <div className="ml-20">
              <SdButton icon={<AddOutlined />} color={'primary'} label="Tạo mới" onClick={addArticle} />
            </div>
          </>
        )}
      />
      <div>
      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          pagingServer
          ref={grid}
          items={(args: SdFilterOption) => PostService.find(generateFromGridFilter(args, columns))}
          columns={columns}
          commands={[{
            title: 'Sửa',
            icon: <EditOutlined />,
            onClick: (item) => {
              // setIdSelect(item.id);
              // setOpenModal(true);
              router.push({
                pathname: '/admin-360/article/detail',
                query: { id: item.id }
              })
            },
            hidden: (item) => !item.editable
          }, {
            title: 'Xem trước',
            icon: <VisibilityIcon />,
            onClick: (item) => {
              const newWindow = window.open(`/bai-viet/${item.slug}`, '_blank', 'noopener,noreferrer')
              if (newWindow) newWindow.opener = null
            },
          },]}
          selection={{
            visible: true,
            actions: [{
              title: 'Đồng bộ',
              icon: <Sync />,
              hidden: (item) => !item.editable,
              onClick: (items) => {
                setSelectedItems(items);
                setOpened(true);
              },
            }, {
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận',
                  describe: 'Xóa các bài viết đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    PostService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa bài viết thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
      {
        openModal && <ModalArticle id={idSelect} open={openModal}
          onClose={handleClose}
          onAgree={() => {
            grid.current?.reload();
            setIdSelect('');
          }} />
      }
      {
        openPostConfig && <ModalPostConfig open={openPostConfig}
          onClose={handleClose}
          onAgree={() => {
            setOpenPostConfig(false);
            grid.current?.reload();
          }} />
      }
      <SelectDepartmentModal opened={opened} setOpened={setOpened} onAccept={(departments) => {
        if (departments?.length) {
          LoadingService.start();
          PostService.sync(selectedItems.map(e => e.id), departments?.map(e => e.code) || []).then(() => {
            NotifyService.success('Đồng bộ thành công');
            grid?.current?.reload();
          }).catch(console.error)
            .finally(LoadingService.stop);
        }
      }} />
    </>
  )
}

export default Article