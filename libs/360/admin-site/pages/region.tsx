import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import { useRef, useState } from "react";
import { RegionService } from "../services";
import ModalSchedule from "../modals/schedule-detail";
import router from "next/router";
import { RegionDTO, RegionTypes } from "@lib/shared/360";
import { Stack, Switch } from "@mui/material";
import { EditOutlined, DeleteOutlined, Delete } from "@mui/icons-material";

const RegionManager: NextPage = () => {
  const grid = useRef<SdGridRef | null>(null);

  const newItem = () => {
    router.push('/admin-360/region/detail');
  }

  return (
    <>
      <PageHeader
        title="Thiết lập chi tiết vùng hiển thị"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />
      <div >
      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          items={() => {
            return RegionService.all();
          }}
          draggable={{
            visible: true,
            mapping: (items: RegionDTO[]) => {
              return items.map(item => ({
                key: item.id,
                primary: item.name
              }))
            },
            onDragEnd: (newItems) => {
              RegionService.sort(newItems.map(e => e.id)).then(() => {
                grid?.current?.reload();
              });
            }
          }}
          columns={[{
            field: 'name',
            label: 'Tên vùng',
            type: 'string',
          }, {
            field: 'type',
            label: 'Loại hiển thị',
            type: 'values',
            option: {
              items: RegionTypes,
              valueField: 'value',
              displayField: 'display'
            },
            width: '200px',
          }, {
            field: "isActivated",
            label: "Trạng thái",
            type: 'bool',
            width: '200px',
            option: {
              displayOnTrue: 'Đang sử dụng',
              displayOnFalse: 'Không sử dụng'
            },
            template: (value, item) => (<Stack alignItems={'center'}>
              <Switch checked={value} onChange={event => {
                RegionService.update(item.id, {
                  isActivated: event.target.checked
                }).then(() => {
                  grid?.current?.reload();
                })
              }} /></Stack>)
          }]}
          commands={[{
            title: 'Sửa',
            icon: <EditOutlined />,
            onClick: (item) => {
              router.push({
                pathname: '/admin-360/region/detail',
                query: { id: item.id }
              })
            },
            hidden: (item) => !item.editable
          }]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận',
                  describe: 'Xóa các vùng hiển thị đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    RegionService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa vùng hiển thị thành công', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
    </>
  )
}

export default RegionManager