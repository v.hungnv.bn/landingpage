import { PageHeader } from "@lib/admin/components";
import { SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import { NextPage } from "next";
import AddIcon from '@mui/icons-material/Add';
import { useState, useRef } from 'react'
import CategoryDetail from "../modals/category-detail";
import { Stack } from '@mui/material'
import { LoadingService, NotifyService } from "@lib/core/services";
import { CategoriesService } from "../services/categories/categories.service";
import { CategoryDTO } from "@lib/shared/360";
import { Delete, EditOutlined, RemoveOutlined } from "@mui/icons-material";
import { TravelCategoryService } from "../services";
import ModalTravelCategory from "../modals/travel-category-detail";
import Image from 'next/image'


const TravelCategory: NextPage = () => {
    const [openModal, setOpenModal] = useState(false);
    const grid = useRef<SdGridRef | null>(null);

    const [idSelect, setIdSelect] = useState('');
    const newItem = () => {
        setOpenModal(true);
        setIdSelect('');
    }

    const handleClose = () => {
        setOpenModal(false);
    }


    return (
        <>
            <PageHeader
                title="Danh sách danh mục du lịch"
                action={(
                    <>
                        <div className="ml-20">
                            <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
                        </div>
                    </>
                )}
            />

            <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
                <SdGrid
                    ref={grid}
                    items={async () => {
                        const res = await TravelCategoryService.all()
                        return res || []
                    }}

                    columns={[{
                        field: 'icon',
                        label: 'Icon',
                        type: 'string',
                        width: '200px',
                        filter: {
                            disabled: true
                        },
                        template: (value, item) => (<div style={{ width: '100px' }}>
                            <Image src={`/admin/icons/${value}.svg`} alt="photo" width="24" height="34" />
                        </div>)
                    }, {
                        field: 'name',
                        label: 'Tên lĩnh vực',
                        type: 'string',
                        template: (value, item) => <Stack display={'flex'} direction={'row'} alignItems={'center'}>
                            {item.level >= 1 && <RemoveOutlined color={'secondary'} fontSize={'small'} />}
                            {item.level >= 2 && <RemoveOutlined color={'secondary'} fontSize={'small'} />}
                            {value}
                        </Stack>
                    }, {
                        field: 'isActivated',
                        label: 'Trạng thái',
                        type: 'bool',
                        option: {
                            displayOnTrue: 'Sử dụng',
                            displayOnFalse: 'Không sử dụng'
                        },
                        width: '150px'
                    },

                    ]}
                    commands={[{
                        title: 'Sửa',
                        icon: <EditOutlined />,
                        onClick: (item) => {
                            setOpenModal(true);
                            setIdSelect(item.id)
                        },
                        hidden: (item) => !item.editable
                    }]}
                    selection={{
                        visible: true,
                        actions: [{
                            title: 'Xóa',
                            icon: <Delete />,
                            color: 'error',
                            hidden: (item) => !item.deletable,
                            onClick: (items) => {
                                NotifyService.confirm({
                                    title: 'Xác nhận',
                                    describe: 'Xóa các chuyên mục đã chọn',
                                    onAgree: () => {
                                        LoadingService.start()
                                        TravelCategoryService.delete(items.map(e => e.id).join()).then(() => {
                                            NotifyService.success('Xóa chuyên mục thành công.', null);
                                            grid.current?.reload();
                                        }).catch(console.error)
                                            .finally(LoadingService.stop);
                                    }
                                })
                            },
                        }]
                    }}
                />
            </div>

            {openModal && <ModalTravelCategory id={idSelect} open={openModal}
                onClose={handleClose}
                onAgree={() => {
                    grid.current?.reload();
                    setIdSelect('');
                    setOpenModal(false)
                }} />}
        </>
    )
}

export default TravelCategory