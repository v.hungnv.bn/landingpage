
import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect } from "@lib/core/components";
import { Stack, TextField } from "@mui/material";
import IconButton from '@mui/material/IconButton';
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import Image from 'next/image'
import Switch from '@mui/material/Switch';
import { cdn, LoadingService, NotifyService } from "@lib/core/services";
import { SdEditor } from "@lib/core/components/SdEditor/SdEditor";
import { CategoryDTO, PostSaveReq } from "@lib/shared/360";
import { SaveOutlined } from '@mui/icons-material';
import { NextPage } from 'next';
import { PostService } from '../../services';
import { CategoriesService } from '../../services/categories/categories.service';
import { PageHeader } from '@lib/admin/components';
import SettingsIcon from '@mui/icons-material/Settings';
import { AddOutlined, Delete, EditOutlined, Sync } from "@mui/icons-material";
import router from 'next/router';

const ArticleDetail: NextPage = () => {
    const sdModal = useRef<SdModalRef | null>(null);
    const [categories, setCategories] = useState<CategoryDTO[]>([]);
    const [slug, setSlug] = useState<string>();
    const [categoryId, setCategoryId] = useState<string>();

    const [openModal, setOpenModal] = useState(false);
    const [canComment, setCanComment] = useState<string>('0');
    const [file, setFile] = useState<any>();
    const [image, setImage] = useState<any>({
        key: '',
    });
    const [allows, setAllows] = useState<{
        value: string;
        display: string;
        data: any
    }[]>([]);

    const { id } = router.query;

    const form = useForm();

    const {
        handleSubmit,
        register,
        control,
        setValue,
        watch,
        formState: { errors },
    } = form;


    // const {
    //   register,
    //   handleSubmit,
    //   control,
    //   setValue,
    //   watch,
    //   formState: { errors },
    // } = useForm();

    useEffect(() => {
        setAllows([
            {
                value: '1',
                display: 'Có',
                data: null,
            },
            {
                value: '0',
                display: 'Không',
                data: null,
            }
        ])
        getAllCategory();
    }, []);

    useEffect(() => {

        if (typeof id === 'string') {
            getDetail(id);
        }
    }, [id]);

    useEffect(() => {
        // register("description", { minLength: 15 });


    }, [register]);


    const getAllCategory = async () => {
        CategoriesService.all().then(categories => {
            const updateCategories = categories.map((e) => {
                if (e?.level == 1) {
                    return { ...e, name: `- ${e?.name}` }
                } else if (e?.level == 2) {
                    return { ...e, name: `-- ${e?.name}` }
                } else return { ...e }
            })
            setCategories(updateCategories);
        })
    }

    const getDetail = async (id: string) => {
        LoadingService.start();
        try {
            const res = await PostService.getPostDetail(id);
            setValue('name', res?.name);
            setValue('quote', res?.quote);
            setValue('content', res?.content);
            setValue('isPublished', res?.isPublished);
            setValue('outstanding', res?.outstanding);
            setValue('categoryId', res?.categoryId);
            setCanComment(res?.canComment ? '1' : '0');
            setFile(cdn(res?.image));
            setCategoryId(res?.categoryId);
            setSlug(res?.slug);


        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    const editorContent = watch("description");

    const handlerCancel = () => {
        setOpenModal(false);
    }

    const handlerSave = () => {

    }

    const handleChangeFile = async (e: any) => {
        if (e?.target?.files[0]) {
            const file = e.target.files[0];
            if (!["image/png", "image/jpeg", "image/jpg"].some(item => item == file?.type)) {
                NotifyService.warn('file tải lên không đúng định dạng.', null);
                setFile(null);
                return;
            }
            if (file.size > 20000000) {
                NotifyService.warn('Dung lượng ảnh không được quá 20MB.', null);
                setFile(null);
                return;
            }
            setFile(URL.createObjectURL(file));
            LoadingService.start();
            try {
                const res = await PostService.upload(file);
                setImage({
                    ...image,
                    key: res.key
                });

                NotifyService.success('Thành công!');

            } catch (err) {
                console.log(err)

            } finally {
                LoadingService.stop();

            }
        }

    }

    const submitHandler = async (data: any) => {

        LoadingService.start();
        try {
            const req: PostSaveReq = {
                canComment: canComment === '1' ? true : false,
                categoryId: categoryId,
                content: data?.content,
                name: data?.name,
                image: image?.key || undefined,
                isPublished: data?.isPublished,
                outstanding: data?.outstanding,
                quote: data?.quote,
                synchronized: false
            };
            if (typeof id === 'string') {
                const res = await PostService.update(id, req);
                NotifyService.success('Cập nhật Thành công!');
                router.push('/admin-360/article');
            } else {
                const res = await PostService.create(req);
                NotifyService.success('Tạo mới Thành công!');
                router.push('/admin-360/article');
            }


        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }
    const openInNewTab = () => {

        const newWindow = window.open(`/bai-viet/${slug}`, '_blank', 'noopener,noreferrer')
        if (newWindow) newWindow.opener = null
    }
    return (
        <>
            <PageHeader
                title="Tạo mới bài viết"
                action={(

                    <Stack spacing={1} display={'flex'} direction={'row'}>
                        <SdButton label='Hủy' variant="text" color="secondary" onClick={() => router.push('/admin-360/article')} />
                        <SdButton className="ml-12" label='Lưu' onClick={handleSubmit(submitHandler)} />
                    </Stack>
                )}
            />
            <div className='bg-white px-16 py-8'>
                <form onSubmit={handleSubmit(submitHandler)} >
                    <div className="row mt-16 mb-32">
                        <div className="col-9">
                            <div className="mb-30">
                                <Controller
                                    name="name"
                                    control={control}
                                    defaultValue=""
                                    rules={{
                                        required: true,
                                        minLength: 6,
                                    }}
                                    render={({ field }) => (
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            id="name"
                                            label="Tiêu đề bài viết"
                                            size="small"
                                            error={Boolean(errors.name)}
                                            helperText={
                                                errors.name
                                                    ? errors.name.type === 'minLength'
                                                        ? 'Tiêu đề bài viết tối thiểu 5 ký tự'
                                                        : 'Tiêu đề bài viết bắt buộc'
                                                    : ''
                                            }
                                            {...field}
                                        ></TextField>
                                    )}
                                ></Controller>
                            </div>

                            <div className="mb-30">
                                <Controller
                                    name="quote"
                                    control={control}
                                    defaultValue=""
                                    render={({ field }) => (
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            id="quote"
                                            label="Trích dẫn"
                                            multiline
                                            rows={4}
                                            {...field}
                                        />
                                    )}
                                ></Controller>
                            </div>

                            <div className='post-editor'>
                                <div>Nội dung bài viết</div>
                                <Controller
                                    name="content"
                                    control={control}
                                    render={({ field }) => (
                                        <SdEditor
                                            {...field}
                                            placeholder={"Nội dung..."}
                                            onChange={(text) => {
                                                field.onChange(text);
                                            }}
                                        />
                                    )}
                                />

                                {/* <SdEditor
                                placeholder={'Nội dung...'}
                                value={editorContent}
                                onChange={(html) => setValue("description", html)}
                            /> */}
                                {/* <p className="MuiFormHelperText-root Mui-error MuiFormHelperText-sizeSmall MuiFormHelperText-contained MuiFormHelperText-filled css-k4qjio-MuiFormHelperText-root">{errors.description && "Tối thiểu 15 ký tự"}</p> */}

                            </div>

                        </div>
                        <div className="col-3">
                            <div style={{ minHeight: '226px', borderRadius: '16px' }} className='box-shadow-2 d-flex align-items-center justify-content-center py-16'>
                                <div className='text-center'>
                                    <IconButton color="primary" aria-label="upload picture" component="label">
                                        <div className='thumb-upload'>
                                            <div className='thumb-upload__wrap'>
                                                {!file ? (
                                                    <><AddAPhotoIcon /><div className='text-upload mt-4'>Upload Photo</div></>
                                                ) :
                                                    <Image src={file} alt="photo" width="128" height="128" />
                                                }
                                                <input type="file" hidden onChange={handleChangeFile} />

                                            </div>

                                        </div>
                                    </IconButton>
                                    <div className='mt-16 text-center'>Cho phép *.jpeg, *.jpg, *.png.</div>
                                    <div className='text-center'>Kích thước tối đa 20 MB</div>
                                </div>

                            </div>
                            <div className="d-flex align-items-center justify-content-between mt-30">
                                <div>Xuất bản</div>
                                {/* <Switch name="isPublished" defaultChecked inputRef={register} /> */}

                                <Controller
                                    name="isPublished"
                                    control={control}
                                    defaultValue={false}
                                    render={({ field }) => (
                                        <Switch inputRef={field.ref} checked={field.value} {...field} />
                                    )}
                                ></Controller>

                            </div>

                            <div className="d-flex align-items-center justify-content-between my-20">
                                <div>Nổi bật</div>
                                <Controller
                                    name="outstanding"
                                    control={control}
                                    defaultValue={false}
                                    render={({ field }) => (
                                        <Switch inputRef={field.ref} checked={field.value} {...field} />
                                    )}
                                ></Controller>
                            </div>


                            <div className="mb-16">
                                <SdSelect
                                    form={form}
                                    value={categoryId}
                                    label="Chọn chuyên mục"
                                    rules={{
                                        required: true
                                    }}
                                    items={categories.map(e => ({
                                        value: e.id,
                                        display: e.name,
                                        data: e
                                    }))}
                                    sdChange={(item, value) => setCategoryId(value)}

                                />

                            </div>

                            <div className="w-100">
                                <SdSelect
                                    label="Cho phép bình luận"
                                    value={canComment}
                                    items={allows}
                                    sdChange={(item => {
                                        setCanComment(item?.value || '')
                                    })}
                                />
                            </div>

                        </div>
                    </div>

                </form>
            </div>


        </>
    )
}

export default ArticleDetail