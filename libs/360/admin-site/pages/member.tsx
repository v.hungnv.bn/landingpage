import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdBadge, SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useRef, useState } from "react";
import { MemberService } from "../services";
import ModalMember from "../modals/member-detail";
import { MemberDTO } from "@lib/shared/360";
import { Stack, Switch } from "@mui/material";
import { Delete } from "@mui/icons-material";


const MemberManage: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const grid = useRef<SdGridRef | null>(null);

  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }

  return (
    <>
      <PageHeader
        title="Thành viên"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />
      <div>
      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          items={async () => {
            const res = await MemberService.all();
            return res || [];
          }}
          draggable={{
            visible: true,
            mapping: (items) => {
              return items.map(item => ({
                key: item.id,
                primary: item.name
              }))
            },
            onDragEnd: (newItems) => {
              MemberService.sort(newItems.map(e => e.id)).then(() => {
                grid?.current?.reload();
              });
            }
          }}
          columns={[{
            field: 'name',
            label: 'Tên thành viên',
            type: 'string',
          }, {
            field: 'role',
            label: 'Chức vụ',
            type: 'string',
            width: '200px'
          }, {
            field: 'organizationName',
            label: 'Phòng ban',
            type: 'string',
            width: '250px'
          }, {
            field: "isActivated",
            label: "Trạng thái",
            type: 'bool',
            width: '150px',
            option: {
              displayOnTrue: 'Hoạt động',
              displayOnFalse: 'Khóa'
            },
            template: (value, item: MemberDTO) => (<Stack alignItems={'center'}>
              <Switch checked={value} onChange={event => {
                MemberService.update(item.id, {
                  isActivated: event.target.checked
                }).then(() => {
                  grid?.current?.reload();
                })
              }} /></Stack>)
          }]}
          commands={[{
            title: 'Sửa',
            icon: <EditIcon />,
            onClick: (item) => {
              setIdSelect(item.id);
              setOpenModal(true);
            },
            hidden: (item) => !item.editable
          }]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận xóa',
                  describe: 'Xóa các thành viên đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    MemberService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa thành viên thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
      {openModal && <ModalMember id={idSelect} open={openModal}
        onClose={handleClose}
        onAgree={() => {
          setOpenModal(false);
          grid.current?.reload();
        }} />}
    </>
  )
}

export default MemberManage