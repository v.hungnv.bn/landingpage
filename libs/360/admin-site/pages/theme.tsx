import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdSelect } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import { useEffect, useState } from "react";
import { ConfigurationService } from "../services";
import { ConfigurationSaveReq, ConfigurationTemplate, ConfigurationTemplates } from "@lib/shared/360";
import Image from 'next/image'


const ThemePage: NextPage = () => {
    const [template, setTemplate] = useState<ConfigurationTemplate>(ConfigurationTemplate.TEMPLATE1);

    useEffect(() => {
        // domainByHost();

        getConfiguaration();
    }, []);

    const getConfiguaration = async () => {
        try {
            const res = await ConfigurationService.detail();
            setTemplate(res.template || template);

        } catch {
        }
        finally {
        }
    }

    const handleSave = async () => {
        LoadingService.start();
        try {
            const req: ConfigurationSaveReq = {
                template: template
            }
            const res = await ConfigurationService.save(req);
            NotifyService.success('Lưu Thành công!');

        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    const handleSelect = (theme: ConfigurationTemplate) => {
        setTemplate(theme);
    }

    return (
        <>
            <PageHeader
                title="Tuỳ chỉnh theme"
                action={(
                    <>
                        <div className="ml-20">
                            <SdButton icon={<AddIcon />} color={'primary'} label="Lưu" onClick={handleSave} />
                        </div>
                    </>
                )}
            />
            <div className="bg-white p-10 rounded-8 px-16">
                <div className="heading-line text-blue-100 text-24 font-bold mb-40 ">Mẫu</div>
                <div className="row mb-40">
                    <div className="col-4">
                        <div className="mb-16">
                            <SdSelect
                                value={template}
                                label="Chọn mẫu"
                                rules={{
                                    required: true
                                }}
                                items={ConfigurationTemplates.map(e => ({
                                    value: e.value,
                                    display: e.display,
                                    data: e
                                }))}
                                sdChange={(item, value) => setTemplate(value === ConfigurationTemplate.TEMPLATE1 ? ConfigurationTemplate.TEMPLATE1 : ConfigurationTemplate.TEMPLATE2)}
                            />

                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-4">
                        <div className="pointer" onClick={() => handleSelect(ConfigurationTemplate.TEMPLATE1)}>
                            <Image src={'/admin/frame-1.png'} alt="photo" width="317" height="164" />
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="pointer" onClick={() => handleSelect(ConfigurationTemplate.TEMPLATE2)}>
                            <Image src={'/admin/frame-2.png'} alt="photo" width="317" height="164" />

                        </div>
                    </div>
                </div>

            </div>


        </>
    )
}

export default ThemePage