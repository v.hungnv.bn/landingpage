import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import { cdn, LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import { useRef, useState } from "react";
import { MediaService } from "../services";
import ModalImage from "../modals/image-detail";
import { MediaDTO, MediaType } from "@lib/shared/360";
import Image from 'next/image'
import { Stack, Switch } from "@mui/material";
import { Delete, DeleteOutline, EditOutlined } from "@mui/icons-material";


const host360 = process.env.NEXT_PUBLIC_360_HOST;

const MediaImage: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const grid = useRef<SdGridRef | null>(null);

  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }

  return (
    <>
      <PageHeader
        title="Quản lý hình ảnh"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />
      <div >

      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          items={async () => {
            const res = await MediaService.all(MediaType.IMAGE);
            return res || [];
          }}
          draggable={{
            visible: true,
            mapping: (items: MediaDTO[]) => {
              return items.map(item => ({
                key: item.id,
                primary: item.url
              }))
            },
            onDragEnd: (newItems) => {
              MediaService.sort(newItems.map(e => e.id)).then(() => {
                grid?.current?.reload();
              });
            }
          }}
          columns={[{
            field: 'url',
            label: 'Hình ảnh',
            type: 'string',
            width: '200px',
            filter: {
              disabled: true
            },
            template: (value, item) => (<div style={{ width: '100px' }}>
              <Image src={cdn(value)} alt="photo" width="80" height="80" />
            </div>)
          }, {
            field: 'createdDate',
            label: 'Ngày tạo',
            type: 'date',
            width: '150px',
          }, {
            field: "isPublished",
            label: "Trạng thái",
            type: "bool",
            width: '150px',
            option: {
              displayOnTrue: 'Sử dụng',
              displayOnFalse: 'Không sử dụng'
            },
            template: (value, item: MediaDTO) => (<Stack alignItems={'center'}>
              <Switch checked={value} onChange={event => {
                MediaService.update(item.id, {
                  isPublished: event.target.checked
                }).then(() => {
                  grid?.current?.reload();
                })
              }} /></Stack>)
          }]}
          commands={[{
            title: 'Sửa',
            icon: <EditOutlined />,
            onClick: (item) => {
              setIdSelect(item.id);
              setOpenModal(true);
            },
            hidden: (item) => !item.editable
          }]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <DeleteOutline />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận xóa',
                  describe: 'Xóa các hình ảnh đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    MediaService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa hình ảnh thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
      {openModal && <ModalImage id={idSelect} open={openModal}
        onClose={handleClose}
        onAgree={() => {
          setOpenModal(false);
          grid.current?.reload();
        }} />}
    </>
  )
}

export default MediaImage