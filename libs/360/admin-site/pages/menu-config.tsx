import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdInput } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import { useEffect, useState } from "react";
import { ConfigurationSaveReq, MenuDTO, MenuType } from "@lib/shared/360";
import * as uuid from 'uuid';
import { IconButton, Stack } from "@mui/material";
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import DeleteIcon from '@mui/icons-material/Delete';
import { ConfigurationService, MenuDTOExtend, PageService } from "../services";
import { useForm } from "react-hook-form";
import MenuConfigChildren from "../modals/menu-config-children";
import { CategoriesService } from "../services/categories/categories.service";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import SortIcon from '@mui/icons-material/Sort';
import { Drag, DragAndDrop, Drop } from "../components/drag-and-drop";
import DragIndicatorIcon from '@mui/icons-material/DragIndicator';
const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const reorder = (list: MenuDTOExtend[], startIndex: number, endIndex: number) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const MenuConfig: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [expanded, setExpanded] = useState({
    static: true,
    page: false,
    category: false,
    link: false
  });

  const [menuLink, setMenuLink] = useState<MenuDTOExtend>({
    title: '',
    link: ''
  });
  const form = useForm();
  const [menus, setMenus] = useState<MenuDTOExtend[]>([]);
  const [staticPages, setStaticPages] = useState<MenuDTOExtend[]>([
    {
      title: 'Tin tức- sự kiện',
      type: MenuType.NEWS,
      referenceId: '2769a6cd-1464-4042-bb37-1cdc3cc4a675'
    },
    {
      title: 'Cơ cấu tổ chức',
      type: MenuType.ORGANIZATION,
      referenceId: 'ef0e4cd0-65cb-46a9-93ba-03ce569a93fa'
    },
    {
      title: 'Văn bản',
      type: MenuType.DOCUMENT,
      referenceId: '79ea4dbd-2833-4f18-bf64-f3fecb390060'
    },
    {
      title: 'Lịch công tác',
      type: MenuType.SCHEDULE,
      referenceId: '77739125-a5a7-441e-99d3-5eee3e317700'
    },
    {
      title: 'Hỏi đáp',
      type: MenuType.QUESTION,
      referenceId: '132a024f-3815-4bfa-9693-457739b1654b'
    }
  ]);
  const [categories, setCategories] = useState<MenuDTOExtend[]>([]);
  const [pages, setPages] = useState<MenuDTOExtend[]>([]);

  const [menuParent, setMenuParent] = useState<MenuDTOExtend>();

  useEffect(() => {
    getDetail();
    initData();
  }, []);

  const initData = async () => {
    LoadingService.start();
    try {
      const cates = await CategoriesService.all();
      const pags = await PageService.all();
      if (cates.length) {
        const itemCats: MenuDTOExtend[] = [];
        cates?.forEach(e => {
          itemCats.push({
            title: e.name,
            referenceId: e.id,
            slug: e.slug,
            type: MenuType.CATEGORY
          })
        })
        setCategories(itemCats);
      }
      if (pags.length) {
        const itemPages: MenuDTOExtend[] = [];
        pags?.forEach(e => {
          itemPages.push({
            title: e.name,
            referenceId: e.id,
            slug: e.slug,
            type: MenuType.PAGE
          })
        })
        setPages(itemPages);
      }

    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }

  const getDetail = async () => {
    LoadingService.start();
    try {
      const res = await ConfigurationService.detail();

      if (res?.menus?.length) {
        const ids: string[] = [];
        res.menus.forEach(e => {
          ids.push(e.referenceId || '');
          if (e.children) {
            e.children.forEach(c => {
              ids.push(c.referenceId || '');
            })
          }
        });
        let staticList = [...staticPages];
        staticList.forEach(e => {
          if (ids.includes(e.referenceId || '')) {
            e.disabled = true;
          }
        });
        setStaticPages(staticList);

        let catList = [...categories];
        catList.forEach(e => {
          if (ids.includes(e.referenceId || '')) {
            e.disabled = true;
          }
        });
        setCategories(catList);

        let pageList = [...pages];
        pageList.forEach(e => {
          if (ids.includes(e.referenceId || '')) {
            e.disabled = true;
          }
        });
        setPages(pageList);
      }

      setMenus(res?.menus || []);

      // console.log('res__', res)

    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }

  const handleClose = () => {
    setOpenModal(false);
  }
  const addMenu = (menu: MenuDTOExtend) => {
    staticPages.forEach(e => {
      if (e.referenceId === menu.referenceId) {
        e.disabled = true;
      }
    });
    setStaticPages(staticPages);

    categories.forEach(e => {
      if (e.referenceId === menu.referenceId) {
        e.disabled = true;
      }
    });
    setCategories(categories);

    pages.forEach(e => {
      if (e.referenceId === menu.referenceId) {
        e.disabled = true;
      }
    });
    setPages(pages);

    setMenus([
      ...menus,
      menu
    ]);

  }

  const removeMenu = (menu: MenuDTOExtend) => {
    const referenceIds = [menu.referenceId];
    if (menu.children) {
      menu.children.forEach(c => {
        referenceIds.push(c.referenceId);
      });
    }

    staticPages.forEach(e => {
      if (referenceIds.includes(e.referenceId)) {
        e.disabled = false;
      }

    });
    setStaticPages(staticPages);

    categories.forEach(e => {
      if (referenceIds.includes(e.referenceId)) {
        e.disabled = false;
      }

    });
    setCategories(categories);

    pages.forEach(e => {
      if (referenceIds.includes(e.referenceId)) {
        e.disabled = false;
      }

    });
    setPages(pages);
    const newMenus = menus?.filter(e => e.referenceId !== menu.referenceId)
    setMenus(newMenus);
  }

  const removeMenuChild = (child: MenuDTOExtend, menu: MenuDTOExtend) => {
    staticPages.forEach(e => {
      if (e.referenceId === child.referenceId) {
        e.disabled = false;
      }
    });
    setStaticPages(staticPages);

    categories.forEach(e => {
      if (e.referenceId === child.referenceId) {
        e.disabled = false;
      }
    });
    setCategories(categories);

    pages.forEach(e => {
      if (e.referenceId === child.referenceId) {
        e.disabled = false;
      }
    });
    setPages(pages);


    let items = [...menus];
    items?.forEach(e => {
      if (e.referenceId === menu?.referenceId) {
        const filter = e?.children?.filter(f => f.referenceId !== child.referenceId);
        e.children = filter;

      }
    })
    setMenus(items);

  }


  const handleMenuLink = () => {
    if (menuLink?.title && menuLink?.link) {
      const menu = {
        referenceId: uuid.v4(),
        type: MenuType.LINK,
        title: menuLink?.title,
        link: menuLink?.link,
      }
      setMenus([
        ...menus,
        menu
      ]);
      setMenuLink({
        title: '',
        link: ''
      });
    }
  }

  const addMenuChildren = (menu: MenuDTOExtend) => {
    setOpenModal(true);
    setMenuParent(menu);
  }

  const handlerSave = async () => {
    LoadingService.start();
    try {
      const req: ConfigurationSaveReq = {
        menus: menus
      }
      const res = await ConfigurationService.save(req);
      NotifyService.success('Lưu Thành công!');


    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }


  const handleDragEnd = (result: { type: any; source: any; destination: any; }) => {
    console.log('result', result);

    const { type, source, destination } = result;
    if (!destination) return;

    const sourceCategoryId = source.droppableId;
    const destinationCategoryId = destination.droppableId;

    // Reordering items
    if (type === "droppable-item") {
      // If drag and dropping within the same category
      if (sourceCategoryId === destinationCategoryId) {
        const menufind = menus.find((category) => category.referenceId === sourceCategoryId);
        const updatedOrder = reorder(menufind?.children || [], source.index, destination.index);
        const updatedMenus = menus.map((menu) =>
          menu.referenceId !== sourceCategoryId
            ? menu
            : { ...menu, children: updatedOrder }
        );

        // setCategories(updatedCategories);
        setMenus(updatedMenus);

      }
    }

    // Reordering categories
    if (type === "droppable-menu") {
      const updatedMenus = reorder(
        menus,
        source.index,
        destination.index
      );

      setMenus(updatedMenus);
    }



  };

  return (
    <>
      <PageHeader
        title="Thiết lập Menu"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Lưu" onClick={handlerSave} />
            </div>
          </>
        )}
      />
      <div className="bg-white rounded-8 shadow-f1 p-20">
        <div className="row">
          <div className="col-3">
            <div className="shadow-f1 rounded-8">
              <div className="bg-blue-100 text-white rounded-top-8 text-18 font-bold py-6 px-10">Danh mục </div>
              <div className="p-5 bg-white">
                <div >
                  <div className="text-16 font-semibold px-10 border-bottom py-10 d-flex justify-content-between align-items-center">
                    Trang mặc định
                    {expanded.static ? (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        static: !expanded.static
                      })}>
                        <KeyboardArrowUpIcon />
                      </IconButton>
                    ) : (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        static: !expanded.static
                      })}>
                        <KeyboardArrowDownIcon />
                      </IconButton>
                    )}

                  </div>
                  <div className={expanded.static ? 'block' : 'hidden'}>
                    {staticPages.map(e => (
                      <div key={e.referenceId} className="p-10 shadow-f1 rounded-8 d-flex justify-content-between align-items-center">
                        {e.title}
                        <IconButton disabled={e.disabled} aria-label="add" onClick={() => addMenu(e)}>
                          <ArrowRightAltIcon />
                        </IconButton>
                      </div>
                    ))}
                  </div>
                </div>

                <div>
                  <div className="text-16 font-semibold px-10 border-bottom py-10 d-flex justify-content-between align-items-center">
                    Trang nội dung
                    {expanded.page ? (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        page: !expanded.page
                      })}>
                        <KeyboardArrowUpIcon />
                      </IconButton>
                    ) : (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        page: !expanded.page
                      })}>
                        <KeyboardArrowDownIcon />
                      </IconButton>
                    )}

                  </div>
                  <div className={expanded.page ? 'block' : 'hidden'}>
                    {pages.map(e => (
                      <div key={e.referenceId} className="p-10 shadow-f1 rounded-8 d-flex justify-content-between align-items-center">
                        {e.title}
                        <IconButton disabled={e.disabled} aria-label="add" onClick={() => addMenu(e)}>
                          <ArrowRightAltIcon />
                        </IconButton>
                      </div>
                    ))}
                  </div>
                </div>

                <div>
                  <div className="text-16 font-semibold px-10 border-bottom py-10 d-flex justify-content-between align-items-center">
                    Trang nội chuyên mục
                    {expanded.category ? (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        category: !expanded.category
                      })}>
                        <KeyboardArrowUpIcon />
                      </IconButton>
                    ) : (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        category: !expanded.category
                      })}>
                        <KeyboardArrowDownIcon />
                      </IconButton>
                    )}

                  </div>
                  <div className={expanded.category ? 'block' : 'hidden'}>
                    {categories?.map(e => (
                      <div key={e.referenceId} className="p-10 shadow-f1 rounded-8 d-flex justify-content-between align-items-center">
                        {e.title}
                        <IconButton disabled={e.disabled} aria-label="add" onClick={() => addMenu(e)}>
                          <ArrowRightAltIcon />
                        </IconButton>
                      </div>
                    ))}
                  </div>
                </div>

                <div>
                  <div className="text-16 font-semibold px-10 border-bottom py-10 d-flex justify-content-between align-items-center">
                    Liên kết tự tạo
                    {expanded.link ? (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        link: !expanded.link
                      })}>
                        <KeyboardArrowUpIcon />
                      </IconButton>
                    ) : (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        link: !expanded.link
                      })}>
                        <KeyboardArrowDownIcon />
                      </IconButton>
                    )}

                  </div>
                  <div className={expanded.link ? 'block' : 'hidden'}>
                    <div>
                      <SdInput
                        className="w-100"
                        form={form}
                        label='Tên Liên kết'
                        value={menuLink.title}
                        sdChange={(value) => setMenuLink({
                          ...menuLink,
                          title: value
                        })}
                      />
                    </div>
                    <div>
                      <SdInput
                        className="w-100"
                        form={form}
                        label='Link Liên kết'
                        value={menuLink.link}
                        sdChange={(value) => setMenuLink({
                          ...menuLink,
                          link: value
                        })}
                      />
                    </div>
                    <div>
                      <SdButton label='Thêm vào menu' onClick={handleMenuLink} />
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div className="col-4">
            <DragAndDrop onDragEnd={handleDragEnd}>
              <div className="shadow-f1 rounded-8">
                <div className="bg-blue-100 text-white rounded-top-8 text-18 font-bold d-flex align-items-center justify-content-between py-6 px-10">
                  Menu
                  <IconButton component="label">
                    <SortIcon className="text-white" />

                  </IconButton>
                </div>
                <Drop id="droppable" type="droppable-menu">
                  <div className="p-5 bg-white">
                    <div className="p-10 shadow-f1 rounded-8 d-flex justify-content-between align-items-center">
                      Home
                    </div>
                    {menus?.length > 0 && (
                      <div>
                        {menus.map((menu: MenuDTOExtend, index: number) => (
                          <Drag key={menu.referenceId}
                            idx={index}
                            id={menu.referenceId}
                            index={index}>
                            <div className="p-10 shadow-f1 rounded-8 ">
                              <div className="d-flex justify-content-between align-items-center bg-white">
                                <div className="d-flex align-items-center">
                                  <DragIndicatorIcon className="mr-4" />
                                  <div>
                                    {menu.title}
                                  </div>
                                </div>


                                <Stack direction="row" spacing={1}>
                                  <IconButton onClick={() => removeMenu(menu)} aria-label="delete">
                                    <DeleteIcon />
                                  </IconButton>
                                  <IconButton onClick={() => addMenuChildren(menu)} aria-label="addChildren">
                                    <AddIcon />
                                  </IconButton>
                                </Stack>
                              </div>

                              <Drop key={menu.referenceId} id={menu.referenceId} type="droppable-item">
                                {(menu.children?.length || 0) > 0 && (
                                  <div className="pl-16">
                                    {menu.children?.map((child: MenuDTOExtend, index: number) => (
                                      <Drag className="draggable" key={child.referenceId}
                                        idx={index}
                                        id={child.referenceId}
                                        index={index}>
                                        <div className="p-10 shadow-f1 rounded-8 d-flex justify-content-between align-items-center">
                                          <div className="d-flex align-items-center">
                                            <DragIndicatorIcon className="mr-4" />
                                            <div>
                                              {child.title}
                                            </div>
                                          </div>
                                          <Stack direction="row" spacing={1}>
                                            <IconButton onClick={() => removeMenuChild(child, menu)} aria-label="delete">
                                              <DeleteIcon />
                                            </IconButton>

                                          </Stack>
                                        </div>
                                      </Drag>
                                    ))}
                                  </div>
                                )}
                              </Drop>


                            </div>
                          </Drag>
                        ))}
                      </div>
                    )}

                  </div>
                </Drop>
              </div>
            </DragAndDrop>
            {openModal && <MenuConfigChildren statics={staticPages} catList={categories} pageList={pages} open={openModal} menu={menuParent}
              onClose={handleClose}
              onAgree={(data: MenuDTOExtend[]) => {
                let items = [...menus];
                items?.forEach(e => {
                  if (e.referenceId === menuParent?.referenceId) {
                    e.children = [...e?.children || [], ...data];
                  }
                })
                setMenus(items);

                setOpenModal(false);
              }} />}

          </div>
        </div>

      </div>


    </>
  )
}

export default MenuConfig