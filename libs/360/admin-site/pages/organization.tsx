import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import { useRef, useState } from "react";
import { OrganizationService } from "../services";
import ModalOrganization from "../modals/organization-detail";
import { Delete } from "@mui/icons-material";
import { OrganizationDTO } from "@lib/shared/360";
import { Stack, Switch } from "@mui/material";


const OrganizationManage: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const grid = useRef<SdGridRef | null>(null);

  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }

  return (
    <>
      <PageHeader
        title="Phòng ban"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />
      <div >

      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          items={async () => {

            const res = await OrganizationService.all();
            return res || [];
          }}
          draggable={{
            visible: true,
            mapping: (items) => {
              return items.map(item => ({
                key: item.id,
                primary: item.name
              }))
            },
            onDragEnd: (newItems) => {
              OrganizationService.sort(newItems.map(e => e.id)).then(() => {
                grid?.current?.reload();
              });
            }
          }}
          columns={[{
            field: 'name',
            label: 'Tiêu phòng ban',
            type: 'string',
          }, {
            field: "isActivated",
            label: "Trạng thái",
            type: 'bool',
            width: '150px',
            option: {
              displayOnTrue: 'Sử dụng',
              displayOnFalse: 'Không sử dụng'
            },
            template: (value, item: OrganizationDTO) => (<Stack alignItems={'center'}>
              <Switch checked={value} onChange={event => {
                OrganizationService.update(item.id, {
                  isActivated: event.target.checked
                }).then(() => {
                  grid?.current?.reload();
                })
              }} /></Stack>)
          }]}
          commands={[{
            title: 'Sửa',
            icon: <EditIcon />,
            onClick: (item) => {
              setIdSelect(item.id);
              setOpenModal(true);
            },
            hidden: (item) => !item.editable
          }]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận xóa',
                  describe: 'Xóa các phòng ban đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    OrganizationService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa phòng ban thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
      {openModal && <ModalOrganization id={idSelect} open={openModal}
        onClose={handleClose}
        onAgree={() => {
          setOpenModal(false);
          grid.current?.reload();
          setIdSelect('');
        }} />}
    </>
  )
}

export default OrganizationManage