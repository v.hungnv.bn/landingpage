import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdBadge, SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import { ApiService, cdn, LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import { Button, Stack, Switch } from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useRef, useState } from "react";
import { BannerService } from "../services";
import { BannerDTO, BannerType, BannerTypes } from "@lib/shared/360";
import Image from 'next/image'
import ModalBanner from "../modals/banner-detail";
import { Delete } from "@mui/icons-material";

const host360 = process.env.NEXT_PUBLIC_360_HOST;

const getIdUrlYoutube = (url: string) => {
  let video_id = url?.split('v=')[1];
  const ampersandPosition = video_id?.indexOf('&');
  if (ampersandPosition != -1) {
    video_id = video_id?.substring(0, ampersandPosition);
  }
  return video_id;
}

const Banner: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const [checksum, setChecksum] = useState<string | undefined>(undefined);
  const grid = useRef<SdGridRef | null>(null);

  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }

  return (
    <>
      <PageHeader
        title="Quản lý banner"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />}  color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />
      <div >

      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          items={async () => {
            const res = await BannerService.all();
            return res || [];
          }}
          draggable={{
            visible: true,
            mapping: (items: BannerDTO[]) => {
              return items.map(item => ({
                key: item.id,
                primary: item.name
              }))
            },
            onDragEnd: (newItems) => {
              BannerService.sort(newItems.map(e => e.id)).then(() => {
                grid?.current?.reload();
              });
            }
          }}
          columns={[{
            field: 'url',
            label: 'Hình ảnh',
            type: 'string',
            width: '150px',
            filter: {
              disabled: true
            },
            template: (value, item) => (
              <Stack style={{ width: '100px' }} alignItems={'center'}>
                {item.type === BannerType.YOUTUBE ? (
                  <Image src={`https://img.youtube.com/vi/${getIdUrlYoutube(value)}/default.jpg`} alt="photo" width="80" height="80" />
                ) : (
                  <Image src={`${cdn(value)}`} alt="photo" width="80" height="80" />
                )}
              </Stack>
            )
          }, {
            field: 'name',
            label: 'Tên hiển thị',
            type: 'string',
          }, {
            field: 'type',
            label: 'Loại',
            type: 'values',
            width: '200px',
            option: {
              items: BannerTypes,
              valueField: 'value',
              displayField: 'display'
            }
          }, {
            field: 'showButton',
            label: 'Hiển thị nút',
            type: 'bool',
            width: '160px',
            option: {
              displayOnTrue: 'Hiển thị',
              displayOnFalse: 'Không hiển thị'
            },
            template: (value, item: BannerDTO) => (<Stack alignItems={'center'}>
              <Switch checked={value} onChange={event => {
                BannerService.update(item.id, {
                  showButton: event.target.checked
                }).then(() => {
                  grid?.current?.reload();
                })
              }} /></Stack>)
          }, {
            field: "isActivated",
            label: "Trạng thái",
            type: "bool",
            width: '160px',
            option: {
              displayOnTrue: 'Sử dụng',
              displayOnFalse: 'Không sử dụng'
            },
            template: (value, item: BannerDTO) => (<Stack alignItems={'center'}>
              <Switch checked={value} onChange={event => {
                BannerService.update(item.id, {
                  isActivated: event.target.checked
                }).then(() => {
                  grid?.current?.reload();
                })
              }} /></Stack>)
          }]}
          commands={[{
            title: 'Sửa',
            icon: <EditIcon />,
            onClick: (item) => {
              setIdSelect(item.id);
              setOpenModal(true);
            },
            hidden: (item) => !item.editable
          }]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận',
                  describe: 'Xóa các banner đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    BannerService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa banner thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
      {openModal && <ModalBanner id={idSelect} open={openModal}
        onClose={handleClose}
        onAgree={() => {
          setOpenModal(false);
          grid?.current?.reload();
        }} />}
    </>
  )
}

export default Banner