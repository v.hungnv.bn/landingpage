import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import {  LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import { useRef, useState } from "react";
import DepartmentCategoryDetail from "../modals/department-category-detail";
import { LinkedCategoryService } from "../services/linked-category/linked-category.service";
import { Delete, EditOutlined } from "@mui/icons-material";
import { LinkedCategoryDTO } from "@lib/shared/360";

const DepartmentCategory: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState<any>('');
  const grid = useRef<SdGridRef | null>(null);
  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }

  return (
    <>
      <PageHeader
        title="Danh mục đơn vị liên kết"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />
      <div >

      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          items={async () => {
            const res = await LinkedCategoryService.all()
            return res || []
          }}
          draggable={{
            visible: true,
            mapping: (items: LinkedCategoryDTO[]) => {
              return items.map(item => ({
                key: item.id,
                primary: item.name
              }))
            },
            onDragEnd: (newItems) => {
              LinkedCategoryService.sort(newItems.map(e => e.id)).then(() => {
                grid?.current?.reload();
              });
            }
          }}
          columns={[{
            field: 'name',
            label: 'Tên danh mục',
            type: 'string',
          }, {
            field: 'numberOfDepartments',
            label: 'Đơn vị thuộc danh mục',
            type: 'number',
            width: '300px',
          }, {
            field: 'isActivated',
            label: 'Trạng thái',
            type: 'bool',
            width: '200px',
            option: {
              displayOnTrue: 'Sử dụng',
              displayOnFalse: 'Không sử dụng'
            }
          }]}
          commands={[{
            title: 'Sửa',
            icon: <EditOutlined />,
            onClick: (item) => {
              setIdSelect(item?.id)
              setOpenModal(true);
            },
          }, 
          ]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận',
                  describe: 'Xóa các chuyên mục đã chọn', 
                  onAgree: () => {
                    LoadingService.start()
                    LinkedCategoryService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa chuyên mục thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
      {openModal && <DepartmentCategoryDetail id={idSelect} open={openModal}
        onClose={handleClose}
        onAgree={() => {
          grid.current?.reload();
          setOpenModal(false);
        }} />}
    </>
  )
}

export default DepartmentCategory