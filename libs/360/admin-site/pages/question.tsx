import { PageHeader } from '@lib/admin/components'
import { SdGrid, SdButton, SdGridRef, SdGridColumn } from '@lib/core/components'
import { NextPage } from 'next'
import AddIcon from '@mui/icons-material/Add';
import { useEffect, useRef, useState } from 'react';
import { Box, Tab, Tabs } from '@mui/material';
import { LoadingService, NotifyService } from '@lib/core/services';
import { QuestionGroupService, QuestionService } from '../services/question/question.service';
import QuestionGroupDetail from '../modals/question-group-detail';
import QuestionDetail from '../modals/question-detail';
import { DeleteOutline, EditOutlined } from '@mui/icons-material';
import { QuestionDTO, QuestionGroupDTO } from '@lib/shared/360';
import { generateFromGridFilter } from '@lib/core/models';

function TabPanel(props: any) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box >
          {children}
        </Box>
      )}
    </div>
  );
}
function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}


const Question: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const [questionColumns, setQuestionColumns] = useState<SdGridColumn<QuestionDTO>[]>([]);
  const grid = useRef<SdGridRef | null>(null);
  const [openTable1, setOpenTable1] = useState<boolean>(false)
  const [openTable2, setOpenTable2] = useState<boolean>(false)

  useEffect(() => {
    QuestionGroupService.all().then((items) => {
      setQuestionColumns([{
        field: 'title',
        label: 'Tiêu đề câu hỏi',
        type: 'string'
      }, {
        field: 'fullName',
        label: 'Người gửi',
        type: 'string',
        width: '200px'
      }, {
        field: 'phone',
        label: 'SĐT',
        type: 'string',
        width: '200px'
      }, {
        field: 'email',
        label: 'Email',
        type: 'string',
        width: '200px'
      }, {
        field: 'questionGroupId',
        label: 'Lĩnh vực',
        type: 'values',
        option: {
          items,
          valueField: 'id',
          displayField: 'name'
        },
        width: '200px'
      }, {
        field: "isActivated",
        label: "Trạng thái",
        type: 'bool',
        width: '150px',
        option: {
          displayOnTrue: 'Đã duyệt',
          displayOnFalse: 'Chưa chuyệt'
        }
      }])
    }).catch(console.error);
  }, []);

  const newItem = () => {
    setOpenTable1(true)
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
    setOpenTable1(false)
    setOpenTable2(false)
  }


  // tabs 
  const [value, setValue] = useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <PageHeader
        title="Hỏi đáp"
        action={(
          <>
            {value == 0 && (
              <div className="ml-20">
                <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
              </div>
            )}

          </>
        )}
      />
      <Box sx={{ width: '100%' }}>
        <Box sx={{
            borderBottom: 1,
            borderColor: "divider",
            height: "70px",
            background: "white",
            borderTopLeftRadius: "15px",
            borderTopRightRadius: "15px",
          }}>
          <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
            <Tab sx={{ textTransform: 'none' }} label="Lĩnh vực hỏi đáp" {...a11yProps(0)} />
            <Tab sx={{ textTransform: 'none' }} label="Trả lời câu hỏi đọc giả" {...a11yProps(1)} />
          </Tabs>
        </Box>

        <TabPanel value={value} index={0}>
          <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
            <SdGrid
              ref={grid}
              draggable={{
                visible: true,
                mapping: (items: QuestionGroupDTO[]) => {
                  return items.map(item => ({
                    key: item.id,
                    primary: item.name
                  }))
                },
                onDragEnd: (newItems) => {
                  QuestionGroupService.sort(newItems.map(e => e.id)).then(() => {
                    grid?.current?.reload();
                  });
                }
              }}
              items={async () => {
                const res = await QuestionGroupService.all()
                return res || []
              }}
              columns={[{
                field: 'name',
                label: 'Tên Lĩnh vực',
                type: 'string',
              }, {
                field: "isActivated",
                label: "Trạng thái",
                type: 'bool',
                width: '150px',
                option: {
                  displayOnTrue: 'Sử dụng',
                  displayOnFalse: 'Không sử dụng'
                }
              }]}
              commands={[{
                title: 'Sửa',
                icon: <EditOutlined />,
                onClick: (item) => {
                  setOpenModal(true);
                  setOpenTable1(true)
                  setIdSelect(item.id)
                },
                hidden: (item) => !item.editable
              }]}
              selection={{
                visible: true,
                actions: [{
                  title: 'Xóa',
                  icon: <DeleteOutline />,
                  color: 'error',
                  hidden: (item) => !item.deletable,
                  onClick: (items) => {
                    NotifyService.confirm({
                      title: 'Xác nhận xóa',
                      describe: 'Xóa các lĩnh vực đã chọn',
                      onAgree: () => {
                        LoadingService.start()
                        QuestionGroupService.delete(items.map(e => e.id).join()).then(() => {
                          NotifyService.success('Xóa lĩnh vực thành công.', null);
                          grid.current?.reload();
                        }).catch(console.error)
                          .finally(LoadingService.stop);
                      }
                    })
                  },
                }]
              }}
            />
          </div>

          {openTable1 && <QuestionGroupDetail id={idSelect} open={openTable1}
            onClose={handleClose}
            onAgree={() => {
              grid.current?.reload();
              setIdSelect('');
              setOpenModal(false)
              setOpenTable1(false)
            }} />}
        </TabPanel>

        <TabPanel value={value} index={1}>
          <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
            <SdGrid
              ref={grid}
              pagingServer
              items={async (args) => {
                return await QuestionService.find(generateFromGridFilter(args, questionColumns));
              }}
              columns={questionColumns}
              commands={[{
                title: 'Sửa',
                icon: <EditOutlined />,
                onClick: (item) => {
                  setOpenModal(true);
                  setOpenTable2(true)
                  setIdSelect(item.id)
                },
                hidden: (item) => !item.editable
              }]}
              selection={{
                visible: true,
                actions: [{
                  title: 'Xóa',
                  icon: <DeleteOutline />,
                  color: 'error',
                  hidden: (item) => !item.deletable,
                  onClick: (items) => {
                    NotifyService.confirm({
                      title: 'Xác nhận xóa',
                      describe: 'Xóa các câu hỏi đã chọn',
                      onAgree: () => {
                        LoadingService.start()
                        QuestionService.delete(items.map(e => e.id).join()).then(() => {
                          NotifyService.success('Xóa câu hỏi thành công.', null);
                          grid.current?.reload();
                        }).catch(console.error)
                          .finally(LoadingService.stop);
                      }
                    })
                  },
                }]
              }}
            />
          </div>

          {openTable2 && <QuestionDetail id={idSelect} open={openTable2}
            onClose={handleClose}
            onAgree={() => {
              grid.current?.reload();
              setIdSelect('');
              setOpenModal(false)
              setOpenTable2(false)
            }} />}
        </TabPanel>
      </Box>
    </>
  )
}

export default Question