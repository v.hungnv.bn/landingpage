import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import { Stack, Switch } from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import { useRef, useState } from "react";
import ModalContent from "../modals/content-detail";
import { PageService } from "../services";
import { Delete } from "@mui/icons-material";
import PreviewIcon from '@mui/icons-material/Preview';
import VisibilityIcon from '@mui/icons-material/Visibility';
import router from "next/router";

const PageContent: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const grid = useRef<SdGridRef | null>(null);

  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }
  const addContent = () => {
    router.push('/admin-360/content/detail');
  }
  return (
    <>
      <PageHeader
        title="Trang nội dung"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={addContent} />
            </div>
          </>
        )}
      />
      <div>
      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          items={() => PageService.all()}
          columns={[{
            field: 'name',
            label: 'Tiêu đề',
            type: 'string',
          }, 
          // {
          //   field: 'quote',
          //   label: 'Trích dẫn',
          //   type: 'string',
          // }, 
          {
            field: "isPublished",
            label: "Trạng thái",
            type: "bool",
            option: {
              displayOnTrue: "Đã xuất bản",
              displayOnFalse: "Chưa xuất bản"
            },
            width: "200px",
            template: (value, item) => (<Stack alignItems={'center'}>
              <Switch checked={value} onChange={event => {
                PageService.update(item.id, {
                  isPublished: event.target.checked
                }).then(() => {
                  grid?.current?.reload();
                })
              }} /></Stack>)
          }
          ]}
          commands={[{
            title: 'Sửa',
            icon: <EditIcon />,
            onClick: (item) => {
              // setIdSelect(item.id);
              // setOpenModal(true);
              router.push({
                pathname: '/admin-360/content/detail',
                query: { id: item.id }
              })
            },
            hidden: (item) => !item.editable
          },
          {
            title: 'Xem trước',
            icon: <VisibilityIcon />,
            onClick: (item) => {
              const newWindow = window.open(`/noi-dung/${item.slug}`, '_blank', 'noopener,noreferrer')
              if (newWindow) newWindow.opener = null
            },
          },
            // {
            //   title: 'Xóa',
            //   icon: <DeleteIcon />,
            //   onClick: async (item) => {
            //     NotifyService.confirm({
            //       title: 'Xóa trang nội dung',
            //       yesTitle: 'Đồng ý',
            //       noTitle: 'Hủy bỏ',
            //       describe: 'Bạn có muốn xóa trang nội dung này không?',
            //       onAgree: async () => {
            //         await PageService.delete(item.id);
            //         NotifyService.success('Xóa thành công');
            //         grid?.current?.reload();
            //       },
            //     });
            //   },
            //   hidden: (item) => !item.deletable
            // }
          ]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận',
                  describe: 'Xóa các trang đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    PageService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa trang thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}

        />
      </div>
      {openModal && <ModalContent id={idSelect} open={openModal}
        onClose={handleClose}
        onAgree={() => {
          grid.current?.reload();
          setOpenModal(false);
          setIdSelect('');

        }} />}
    </>
  )
}

export default PageContent