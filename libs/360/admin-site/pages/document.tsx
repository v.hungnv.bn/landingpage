import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdGrid, SdGridColumn, SdGridRef } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useEffect, useRef, useState } from "react";
import { DocumentService } from "../services/document/document.service";
import ModalDocument from "../modals/document-detail";
import React from "react";
import { DocumentDTO } from "@lib/shared/360";
import { generateFromGridFilter } from "@lib/core/models";
import { Delete } from "@mui/icons-material";

const DocumentManage: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const [columns, setColumns] = useState<SdGridColumn<DocumentDTO>[]>([]);
  const grid = useRef<SdGridRef | null>(null);
  useEffect(() => {
    setColumns([{
      field: 'title',
      label: 'Tên văn bản',
      type: 'string',
    },
    //  {
    //   field: 'documentCategoryName',
    //   label: 'Danh mục văn bản',
    //   type: 'string',
    //   width: "250px",
    // }, 
    {
      field: 'documentTypeName',
      label: 'Loại văn bản',
      type: 'string',
      width: "250px",
    }, {
      field: 'documentDepartmentName',
      label: 'Cơ quan ban hành',
      type: 'string',
      width: "250px",
    }, {
      field: "isActivated",
      label: "Trạng thái",
      type: "bool",
      width: "200px",
      option: {
        displayOnTrue: 'Sử dụng',
        displayOnFalse: 'Không sử dụng'
      }
    }])
  }, []);
  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }


  return (
    <>
      <PageHeader
        title="Danh sách văn bản"
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />
      <div>
      </div>
      <div style={{ maxHeight: 'calc(100vh - 175px)' }} >
        <SdGrid
          ref={grid}
          pagingServer
          items={async (args) => {
            return await DocumentService.find(generateFromGridFilter(args, columns));
          }}
          columns={columns}
          commands={[{
            title: 'Sửa',
            icon: <EditIcon />,
            onClick: (item) => {
              setIdSelect(item.id);
              setOpenModal(true);
            },
            hidden: (item) => !item.editable

          }]}
          selection={{
            visible: true,
            actions: [{
              title: 'Xóa',
              icon: <Delete />,
              color: 'error',
              hidden: (item) => !item.deletable,
              onClick: (items) => {
                NotifyService.confirm({
                  title: 'Xác nhận xóa',
                  describe: 'Xóa các văn bản đã chọn',
                  onAgree: () => {
                    LoadingService.start()
                    DocumentService.delete(items.map(e => e.id).join()).then(() => {
                      NotifyService.success('Xóa văn bản thành công.', null);
                      grid.current?.reload();
                    }).catch(console.error)
                      .finally(LoadingService.stop);
                  }
                })
              },
            }]
          }}
        />
      </div>
      {openModal && <ModalDocument id={idSelect} open={openModal}
        onClose={handleClose}
        onAgree={() => {
          setOpenModal(false);
          grid.current?.reload();
        }} />}
    </>
  )
}

export default DocumentManage