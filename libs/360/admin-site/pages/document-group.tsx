import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdGrid, SdGridRef } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import { useRef, useState } from "react";
import { DocumentGroupService } from "../services";

import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import 'swiper/css';
import ModalDocumentGroup from "../modals/document-group-detail";
import { DocumentGroupDTO, DocumentGroupFor } from "@lib/shared/360";
import { DeleteOutline, EditOutlined } from "@mui/icons-material";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const DocumentGroupManager: NextPage = () => {
  const [openModal, setOpenModal] = useState(false);
  const [idSelect, setIdSelect] = useState('');
  const grid = useRef<SdGridRef | null>(null);
  const [value, setValue] = React.useState(0);
  const [groupFor, setGroupFor] = React.useState<DocumentGroupFor>(DocumentGroupFor.CATEGORY);
  const [title, setTitle] = useState<string>('Danh mục văn bản');

  React.useEffect(() => {
  }, []);

  const newItem = () => {
    setOpenModal(true);
    setIdSelect('');
  }

  const handleClose = () => {
    setOpenModal(false);
  }
  const handleChange = (event, newValue) => {
    setValue(newValue);
    if (newValue === 0) {
      setGroupFor(DocumentGroupFor.CATEGORY);
      setTitle('Danh mục văn bản')
    }
    if (newValue === 1) {
      setGroupFor(DocumentGroupFor.TYPE)
      setTitle('Loại văn bản')
    }
    if (newValue === 2) {
      setGroupFor(DocumentGroupFor.DEPARTMENT)
      setTitle('Cơ quan ban hành')
    }
  };
  return (
    <>
      <PageHeader
        title={title}
        action={(
          <>
            <div className="ml-20">
              <SdButton icon={<AddIcon />} color={'primary'} label="Tạo mới" onClick={newItem} />
            </div>
          </>
        )}
      />
      <div >

      </div>
      <div className="document-group-tab" >
        <div className="p-0" >
          <Box  sx={{
            borderBottom: 1,
            borderColor: "divider",
            height: "70px",
            background: "white",
            borderTopLeftRadius: "15px",
            borderTopRightRadius: "15px",
          }}>
            <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" >
              <Tab label={'Danh mục văn bản'} {...a11yProps(0)} />
              <Tab label={'Loại văn bản'} {...a11yProps(1)} />
              <Tab label={'Cơ quan ban hành'} {...a11yProps(2)} />
            </Tabs>
          </Box>
          <TabPanel value={value} index={0} >
            <SdGrid
              ref={grid}
              draggable={{
                visible: true,
                mapping: (items: DocumentGroupDTO[]) => {
                  return items.map(item => ({
                    key: item.id,
                    primary: item.name
                  }))
                },
                onDragEnd: (newItems) => {
                  DocumentGroupService.sort(newItems.map(e => e.id)).then(() => {
                    grid?.current?.reload();
                  });
                }
              }}
              items={async () => {
                const res = await DocumentGroupService.all();
                const data = res?.filter(e => e.groupFor === groupFor);
                return data || [];
              }}
              columns={[{
                field: 'name',
                label: 'Tên danh mục',
                type: 'string',
              }, {
                field: "isActivated",
                label: "Trạng thái",
                type: "bool",
                width: "200px",
                option: {
                  displayOnTrue: 'Sử dụng',
                  displayOnFalse: 'Không sử dụng'
                }
              }]}
              commands={[{
                title: 'Sửa',
                icon: <EditOutlined />,
                onClick: (item) => {
                  setIdSelect(item.id);
                  setOpenModal(true);
                },
                hidden: (item) => !item.editable
              }]}
              selection={{
                visible: true,
                actions: [{
                  title: 'Xóa',
                  icon: <DeleteOutline />,
                  color: 'error',
                  hidden: (item) => !item.deletable,
                  onClick: (items) => {
                    NotifyService.confirm({
                      title: 'Xác nhận xóa',
                      describe: 'Xóa các danh mục đã chọn',
                      onAgree: () => {
                        LoadingService.start()
                        DocumentGroupService.delete(items.map(e => e.id).join()).then(() => {
                          NotifyService.success('Xóa danh mục thành công.', null);
                          grid.current?.reload();
                        }).catch(console.error)
                          .finally(LoadingService.stop);
                      }
                    })
                  },
                }]
              }}
            />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <SdGrid
              ref={grid}
              draggable={{
                visible: true,
                mapping: (items: DocumentGroupDTO[]) => {
                  return items.map(item => ({
                    key: item.id,
                    primary: item.name
                  }))
                },
                onDragEnd: (newItems) => {
                  DocumentGroupService.sort(newItems.map(e => e.id)).then(() => {
                    grid?.current?.reload();
                  });
                }
              }}
              items={async () => {
                const res = await DocumentGroupService.all();
                const data = res?.filter(e => e.groupFor === groupFor);
                return data || [];
              }}
              columns={[{
                field: 'name',
                label: 'Loại văn bản',
                type: 'string',
              }, {
                field: "isActivated",
                label: "Trạng thái",
                type: "bool",
                width: "200px",
                option: {
                  displayOnTrue: 'Sử dụng',
                  displayOnFalse: 'Không sử dụng'
                }
              }
              ]}
              commands={[{
                title: 'Sửa',
                icon: <EditOutlined />,
                onClick: (item) => {
                  setIdSelect(item.id);
                  setOpenModal(true);
                },
                hidden: (item) => !item.editable
              }]}
              selection={{
                visible: true,
                actions: [{
                  title: 'Xóa',
                  icon: <DeleteOutline />,
                  color: 'error',
                  hidden: (item) => !item.deletable,
                  onClick: (items) => {
                    NotifyService.confirm({
                      title: 'Xác nhận xóa',
                      describe: 'Xóa các loại văn bản đã chọn',
                      onAgree: () => {
                        LoadingService.start()
                        DocumentGroupService.delete(items.map(e => e.id).join()).then(() => {
                          NotifyService.success('Xóa loại văn bản thành công.', null);
                          grid.current?.reload();
                        }).catch(console.error)
                          .finally(LoadingService.stop);
                      }
                    })
                  },
                }]
              }}
            />

          </TabPanel>
          <TabPanel value={value} index={2}>
            <SdGrid
              ref={grid}
              draggable={{
                visible: true,
                mapping: (items: DocumentGroupDTO[]) => {
                  return items.map(item => ({
                    key: item.id,
                    primary: item.name
                  }))
                },
                onDragEnd: (newItems) => {
                  DocumentGroupService.sort(newItems.map(e => e.id)).then(() => {
                    grid?.current?.reload();
                  });
                }
              }}
              items={async () => {
                const res = await DocumentGroupService.all();
                const data = res?.filter(e => e.groupFor === groupFor);
                return data || [];
              }}
              columns={[{
                field: 'name',
                label: 'Cơ quan',
                type: 'string',
              }, {
                field: "isActivated",
                label: "Trạng thái",
                type: "bool",
                width: "200px",
                option: {
                  displayOnTrue: 'Sử dụng',
                  displayOnFalse: 'Không sử dụng'
                }
              }
              ]}
              commands={[{
                title: 'Sửa',
                icon: <EditOutlined />,
                onClick: (item) => {
                  setIdSelect(item.id);
                  setOpenModal(true);
                },
                hidden: (item) => !item.editable
              }]}
              selection={{
                visible: true,
                actions: [{
                  title: 'Xóa',
                  icon: <DeleteOutline />,
                  color: 'error',
                  hidden: (item) => !item.deletable,
                  onClick: (items) => {
                    NotifyService.confirm({
                      title: 'Xác nhận xóa',
                      describe: 'Xóa các cơ quan đã chọn',
                      onAgree: () => {
                        LoadingService.start()
                        DocumentGroupService.delete(items.map(e => e.id).join()).then(() => {
                          NotifyService.success('Xóa cơ quan thành công.', null);
                          grid.current?.reload();
                        }).catch(console.error)
                          .finally(LoadingService.stop);
                      }
                    })
                  },
                }]
              }}
            />

          </TabPanel>

        </div>


      </div>
      {openModal && <ModalDocumentGroup id={idSelect} open={openModal} groupFor={groupFor}
        onClose={handleClose}
        onAgree={() => {
          setOpenModal(false);
          grid.current?.reload();
        }} />}
    </>
  )
}

export default DocumentGroupManager