import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdButton, SdInput } from "@lib/core/components";
import { cdn, LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import { useEffect, useState } from "react";
import { ConfigurationService, PostService } from "../services";
import { ConfigurationSaveReq } from "@lib/shared/360";
import Image from 'next/image'
import { IconButton } from "@mui/material";
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import { useForm } from "react-hook-form";


const host360 = process.env.NEXT_PUBLIC_360_HOST;

const InfoPage: NextPage = () => {
    const [file, setFile] = useState<any>();

    const [req, setReq] = useState<ConfigurationSaveReq>({
        title: ''
    });
    const form = useForm();
    const {
        handleSubmit,
    } = form;


    useEffect(() => {
        getDetail();
    }, []);

    const getDetail = async () => {
        LoadingService.start();
        try {
            const res = await ConfigurationService.detail();
            setReq(res);
            setFile(cdn(res?.image));
            console.log('res__', res)

        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    const handleChangeFile = async (e: any) => {
        if (e?.target?.files[0]) {
            const file = e.target.files[0];
            if (!["image/png", "image/jpeg", "image/jpg"].some(item => item == file?.type)) {
                NotifyService.warn('file tải lên không đúng định dạng.', null);
                setFile(null);
                return;
            }
            if (file.size > 5000000) {
                NotifyService.warn('Dung lượng ảnh không được quá 5MB.', null);
                setFile(null);
                return;
            }
            setFile(URL.createObjectURL(file));
            LoadingService.start();
            try {
                const res = await PostService.upload(file);
                setReq({
                    ...req,
                    image: res?.key
                });

                NotifyService.success('Thành công!');

            } catch (err) {
                console.log(err)

            } finally {
                LoadingService.stop();

            }
        }

    }

    const submitHandler = async () => {
        LoadingService.start();
        try {
            console.log(req);
            await ConfigurationService.save(req)
            NotifyService.success('Cập nhật thành công!');
        } catch (error) {
            console.error(error)
        } finally {
            LoadingService.stop();
        }
    }


    return (
        <>
            <PageHeader
                title="Cấu hình thông tin website"
                action={(
                    <>
                        <div className="ml-20">
                            <SdButton color={'primary'} label="Lưu" onClick={handleSubmit(submitHandler)} />
                        </div>
                    </>
                )}
            />
            <div className="bg-white p-10 rounded-8 shadow-f1">
                <div className="row ">
                    <div className="col-3">
                        <div style={{ minHeight: '226px', borderRadius: '16px' }} className='box-shadow-2 d-flex align-items-center justify-content-center py-16'>
                            <div className='text-center'>
                                <IconButton color="primary" aria-label="upload picture" component="label">
                                    <div className='thumb-upload'>
                                        <div className='thumb-upload__wrap'>
                                            {!file ? (
                                                <><AddAPhotoIcon /><div className='text-upload mt-4'>Upload Photo</div></>
                                            ) :
                                                <Image src={file} alt="photo" width="128" height="128" />
                                            }
                                            <input type="file" hidden onChange={handleChangeFile} />

                                        </div>

                                    </div>
                                </IconButton>
                                <div className='mt-16 text-center'>Cho phép *.jpeg, *.jpg, *.png.</div>
                                <div className='text-center'>Kích thước tối đa 5 MB</div>
                            </div>

                        </div>
                    </div>
                    <div className="col-9">
                        <div className="my-10">
                            <SdInput
                                form={form}
                                rules={{
                                    required: true
                                }}
                                label='Tiêu đề trang'
                                value={req?.title}
                                sdChange={(value) => setReq({
                                    ...req,
                                    title: value
                                })}
                                sx={{ width: '100%' }} />
                        </div>
                        <div className="my-10">
                            <SdInput
                                form={form}
                                rules={{
                                    required: true
                                }}
                                label='Thông tin favicon'
                                value={req?.faviconInfo}
                                sdChange={(value) => setReq({
                                    ...req,
                                    faviconInfo: value
                                })}
                                sx={{ width: '100%' }} />
                        </div>
                        <div className="my-10">
                            <SdInput
                                form={form}
                                rules={{
                                    required: true
                                }}
                                label='Đường link IPv6'
                                value={req?.ipV6}
                                sdChange={(value) => setReq({
                                    ...req,
                                    ipV6: value
                                })}
                                sx={{ width: '100%' }} />
                        </div>
                        <div className="my-10">
                            <SdInput
                                form={form}
                                rules={{
                                    required: true
                                }}
                                label='Thông tin Footer'
                                multiline
                                rows={4}
                                value={req?.footer}
                                sdChange={(value) => setReq({
                                    ...req,
                                    footer: value
                                })}
                                sx={{ width: '100%' }} />
                        </div>
                    </div>
                </div>
                {/* <div className="d-flex justify-content-end">
                    <SdButton color={'primary'} label="Lưu" onClick={handleSubmit(submitHandler)} />
                </div> */}
            </div>


        </>
    )
}

export default InfoPage