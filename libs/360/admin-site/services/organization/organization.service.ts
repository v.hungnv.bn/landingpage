import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { OrganizationDTO, ScheduleSaveReq } from '@lib/shared/360';
import { OrganizationDTOExtend } from './organization.model';

export class OrganizationService {
    static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
    static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

    static create = async (body: Partial<ScheduleSaveReq>) => {
        return await ApiService.post(`${OrganizationService.host360}organization`, body);
    }

    static update = async (id: string, body:  Partial<ScheduleSaveReq>) => {
        return await ApiService.put(`${OrganizationService.host360}organization/${id}`, body);
    }

    static delete = async (id: string) => {
        return await ApiService.delete(`${OrganizationService.host360}organization/${id}`);
    }

    static upload = async (file: File): Promise<{ key: string }> => {
        return ApiService.upload(`${OrganizationService.host360}aws/upload`, file);
    }


    static find = async (body: { pageSize: number, pageNumber: number }): Promise<{ items: OrganizationDTOExtend[], total: number }> => {
        return await ApiService.post(`${OrganizationService.host360}organization/find`, body);
    }

    static all = async (): Promise<OrganizationDTOExtend[]> => {
        return await ApiService.get(`${OrganizationService.host360}organization/all`);
    }

    static detail = async (id: string): Promise<OrganizationDTO> => {
        return await ApiService.get(`${OrganizationService.host360}organization/${id}`);
    }

    static sort = async (ids: string[]): Promise<void> => {
      return await ApiService.post(`${OrganizationService.host360}organization/sort`, { ids });
    }

    static allPublish = async (): Promise<OrganizationDTOExtend[]> => {
        return await ApiService.get(`${OrganizationService.host360}organization/all/${ApiService.userInfo?.departmentCode || ApiService.departmentCode}`, null, true);

    }
}
 