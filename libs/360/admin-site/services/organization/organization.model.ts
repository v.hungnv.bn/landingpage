import { MemberDTO, OrganizationDTO } from "@lib/shared/360";

export type OrganizationDTOExtend = OrganizationDTO & { 
    id: string;
    value: string;
    display: string;
    data: OrganizationDTOExtend;
 }

