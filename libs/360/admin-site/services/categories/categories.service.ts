import { ApiService } from "@lib/core/services";
import { CategoryDTO, CategorySaveReq } from "libs/shared/360";
import { CategoryDTOExtend } from "./categories.model";


export class CategoriesService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

  static create = async (body: CategorySaveReq) => {
    return await ApiService.post(`${CategoriesService.host360}category`, body)
  }

  static update = async (id: string, body: CategorySaveReq) => {
    return await ApiService.put(`${CategoriesService.host360}category/${id}`, body)
  }

  static delete = async (id: string) => {
    return await ApiService.delete(`${CategoriesService.host360}category/${id}`);
  }

  static detail = async (id: string): Promise<CategoryDTO> => {
    return await ApiService.get(`${CategoriesService.host360}category/${id}`);
  }

  static all = async (): Promise<CategoryDTOExtend[]> => {
    return await ApiService.get(`${CategoriesService.host360}category/all`);
  }

  static sort = async (ids: string[]): Promise<void> => {
    return await ApiService.post(`${CategoriesService.host360}category/sort`, { ids });
  }
}