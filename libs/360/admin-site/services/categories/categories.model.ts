import { CategoryDTO } from "libs/shared/360";


export type CategoryDTOExtend = CategoryDTO & { 
    id: string;
    value: string;
    display: string;
 }