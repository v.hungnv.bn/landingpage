import { BannerDTO } from "@lib/shared/360";

export type BannerDTOExtend = BannerDTO & { 
    id: string;
 }