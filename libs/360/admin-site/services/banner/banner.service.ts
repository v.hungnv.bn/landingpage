import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { BannerSaveReq, MediaSaveReq, MediaType, PageSaveReq, PostDTO, PostSaveReq } from '@lib/shared/360';
import { BannerDTOExtend } from './banner.model';

export class BannerService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

  static create = async (body: Partial<BannerSaveReq>) => {
    return await ApiService.post(`${BannerService.host360}banner`, body);
  }

  static update = async (id: string, body: Partial<BannerSaveReq>) => {
    return await ApiService.put(`${BannerService.host360}banner/${id}`, body);
  }

  static delete = async (id: string) => {
    return await ApiService.delete(`${BannerService.host360}banner/${id}`);
  }

  static upload = async (file: File): Promise<{ key: string }> => {
    return ApiService.upload(`${BannerService.host360}aws/upload`, file);
  }

  static all = async (): Promise<BannerDTOExtend[]> => {
    return await ApiService.get(`${BannerService.host360}banner/all`);
  }

  static getBannerDetail = async (id: string): Promise<BannerDTOExtend> => {
    return await ApiService.get(`${BannerService.host360}banner/${id}`);
  }

  static getAllBanner = async (): Promise<BannerDTOExtend[]> => {
    return await ApiService.get(`${BannerService.host360}banner/all/${ApiService.userInfo?.departmentCode || ApiService.departmentCode}`, null, true);
  }

  static sort = async (ids: string[]): Promise<void> => {
    return await ApiService.post(`${BannerService.host360}banner/sort`, { ids });
  }
}
