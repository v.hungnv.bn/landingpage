import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { MemberSaveReq, ScheduleSaveReq } from '@lib/shared/360';
import { MemberDTOExtend } from './member.model';

export class MemberService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

  static create = async (body: Partial<MemberSaveReq>) => {
    return await ApiService.post(`${MemberService.host360}member`, body);
  }

  static update = async (id: string, body: Partial<MemberSaveReq>) => {
    return await ApiService.put(`${MemberService.host360}member/${id}`, body);
  }

  static delete = async (id: string) => {
    return await ApiService.delete(`${MemberService.host360}member/${id}`);
  }

  static upload = async (file: File): Promise<{ key: string }> => {
    return ApiService.upload(`${MemberService.host360}aws/upload`, file);
  }


  static find = async (body: { pageSize: number, pageNumber: number }): Promise<{ items: MemberDTOExtend[], total: number }> => {
    return await ApiService.post(`${MemberService.host360}member/find`, body);
  }

  static all = async (): Promise<MemberDTOExtend[]> => {
    return await ApiService.get(`${MemberService.host360}member/all`);
  }

  static detail = async (id: string): Promise<MemberDTOExtend> => {
    return await ApiService.get(`${MemberService.host360}member/${id}`);
  }

  static sort = async (ids: string[]): Promise<void> => {
    return await ApiService.post(`${MemberService.host360}member/sort`, { ids });
  }

  static allPublish = async (): Promise<MemberDTOExtend[]> => {
    return await ApiService.get(`${MemberService.host360}member/all/${ApiService.userInfo?.departmentCode || ApiService.departmentCode}`, null, true);

  }
}
