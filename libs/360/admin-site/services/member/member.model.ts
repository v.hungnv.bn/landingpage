import { MemberDTO } from "@lib/shared/360";

export type MemberDTOExtend = MemberDTO & { 
    id: string;
 }