import { DocumentDTO } from "@lib/shared/360";

export type DocumentDTOExtend = DocumentDTO & { 
    id: string;
 }