import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { DocumentDTO, DocumentGroupSaveReq, DocumentPublicFindReq, DocumentSaveReq, MemberSaveReq, ScheduleSaveReq } from '@lib/shared/360';
import { FindDTOReq } from '../common/common.model';
import { DocumentDTOExtend } from './document.model';

export class DocumentService {
    static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
    static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

    static create = async (body: Partial<DocumentSaveReq>) => {
        return await ApiService.post(`${DocumentService.host360}document`, body);
    }

    static update = async (id: string, body: Partial<DocumentSaveReq>) => {
        return await ApiService.put(`${DocumentService.host360}document/${id}`, body);
    }

    static delete = async (id: string) => {
        return await ApiService.delete(`${DocumentService.host360}document/${id}`);
    }

    static upload = async (file: File): Promise<{ key: string }> => {
        return ApiService.upload(`${DocumentService.host360}aws/upload`, file);
    }


    static find = async (body: { pageSize: number, pageNumber: number }): Promise<{ items: DocumentDTO[], total: number }> => {
        return await ApiService.post(`${DocumentService.host360}document/find`, body);
    }

    static all = async (): Promise<DocumentDTO[]> => {
        return await ApiService.get(`${DocumentService.host360}document/all`);
    }

    static detail = async (id: string): Promise<DocumentDTO> => {
        return await ApiService.get(`${DocumentService.host360}document/${id}`, null, true);
    }

    static findPublish = async (body?: DocumentPublicFindReq): Promise<{ items: DocumentDTO[], total: number }> => {
        return await ApiService.post(`${DocumentService.host360}document/find/${ApiService.departmentCode}`, body);

    }


}
