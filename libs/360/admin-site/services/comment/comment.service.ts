import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { CommentSaveReq, ScheduleSaveReq } from '@lib/shared/360';
import { CommentDTOExtend } from './comment.model';

export class CommentService {
    static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
    static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

    static create = async (body: Partial<CommentSaveReq>) => {
        return await ApiService.post(`${CommentService.host360}comment`, body, null, true);
    }

    static update = async (id: string, body:  Partial<CommentSaveReq>) => {
        return await ApiService.put(`${CommentService.host360}comment/${id}`, body);
    }

    static approve = async (id: string) => {
        return await ApiService.put(`${CommentService.host360}comment/${id}/approve`);
    }

    static delete = async (id: string) => {
        return await ApiService.delete(`${CommentService.host360}comment/${id}`);
    }

    static upload = async (file: File): Promise<{ key: string }> => {
        return ApiService.upload(`${CommentService.host360}aws/upload`, file);
    }


    static find = async (body: { pageSize: number, pageNumber: number }): Promise<{ items: CommentDTOExtend[], total: number }> => {
        return await ApiService.post(`${CommentService.host360}comment/find`, body);
    }

    static all = async (): Promise<CommentDTOExtend[]> => {
        return await ApiService.get(`${CommentService.host360}comment/all`);
    }

    static detail = async (id: string): Promise<CommentDTOExtend> => {
        return await ApiService.get(`${CommentService.host360}comment/${id}`);
    }

    static allPublish = async (postId: string): Promise<CommentDTOExtend[]> => {
        return await ApiService.get(`${CommentService.host360}comment/all/${postId}`, null, true);
    }

  
}
 