import { ApiService } from '@lib/core/services';
import { GooglePlaceResult, TravelCategoryDTO, TravelCategorySaveReq } from '@lib/shared/360';

export class TravelCategoryService {
    static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
    static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

    static create = async (body: Partial<TravelCategorySaveReq>) => {
        return await ApiService.post(`${TravelCategoryService.host360}travel-category`, body);
    }

    static update = async (id: string, body: Partial<TravelCategorySaveReq>) => {
        return await ApiService.put(`${TravelCategoryService.host360}travel-category/${id}`, body);
    }

    static delete = async (id: string) => {
        return await ApiService.delete(`${TravelCategoryService.host360}travel-category/${id}`);
    }

    static upload = async (file: File): Promise<{ key: string }> => {
        return ApiService.upload(`${TravelCategoryService.host360}aws/upload`, file);
    }


    static all = async (): Promise<TravelCategoryDTO[]> => {
        return await ApiService.get(`${TravelCategoryService.host360}travel-category/all`).catch(err => {
            console.error(err);
            return [];
        });
    }

    static detail = async (id: string): Promise<TravelCategoryDTO> => {
        return await ApiService.get(`${TravelCategoryService.host360}travel-category/${id}`);
    }


    static allPublish = async (): Promise<TravelCategoryDTO[]> => {
        return await ApiService.get(`${TravelCategoryService.host360}travel-category/all/${ApiService.departmentCode}`).catch(err => {
            console.error(err);
            return [];
        });
    }

    static searchPlace = async (searchText?: string): Promise<GooglePlaceResult[]> => {
        return await ApiService.get(`${TravelCategoryService.host360}travel-location/searchPlace/${searchText}`);

    }

}
