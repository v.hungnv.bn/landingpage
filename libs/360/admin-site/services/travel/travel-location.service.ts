import { ApiService } from '@lib/core/services';
import { GooglePlaceResult, TravelCategoryDTO, TravelCategorySaveReq, TravelLocationDTO, TravelLocationSaveReq } from '@lib/shared/360';

export class TravelLocationService {
    static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
    static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

    static create = async (body: Partial<TravelLocationSaveReq>) => {
        return await ApiService.post(`${TravelLocationService.host360}travel-location`, body);
    }

    static update = async (id: string, body: Partial<TravelLocationSaveReq>) => {
        return await ApiService.put(`${TravelLocationService.host360}travel-location/${id}`, body);
    }

    static delete = async (id: string) => {
        return await ApiService.delete(`${TravelLocationService.host360}travel-location/${id}`);
    }

    static upload = async (file: File): Promise<{ key: string }> => {
        return ApiService.upload(`${TravelLocationService.host360}aws/upload`, file);
    }


    static all = async (): Promise<TravelLocationDTO[]> => {
        return await ApiService.get(`${TravelLocationService.host360}travel-location/all`).catch(err => {
            console.error(err);
            return [];
        });
    }

    static detail = async (id: string): Promise<TravelLocationDTO> => {
        return await ApiService.get(`${TravelLocationService.host360}travel-location/${id}`);
    }

    static allPublish = async (): Promise<TravelLocationDTO[]> => {
        return await ApiService.get(`${TravelLocationService.host360}travel-location/all/${ApiService.departmentCode}`);

    }
    static searchPlace = async (searchText?: string): Promise<GooglePlaceResult[]> => {
        return await ApiService.get(`${TravelLocationService.host360}travel-location/searchPlace/${searchText}`);

    }

}
