import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { DocumentGroupSaveReq, MemberSaveReq, ScheduleSaveReq } from '@lib/shared/360';
import { DocumentGroupDTOExtend } from './document-group.model';

export class DocumentGroupService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

  static create = async (body: Partial<DocumentGroupSaveReq>) => {
    return await ApiService.post(`${DocumentGroupService.host360}document-group`, body);
  }

  static update = async (id: string, body: Partial<DocumentGroupSaveReq>) => {
    return await ApiService.put(`${DocumentGroupService.host360}document-group/${id}`, body);
  }

  static delete = async (id: string) => {
    return await ApiService.delete(`${DocumentGroupService.host360}document-group/${id}`);
  }

  static upload = async (file: File): Promise<{ key: string }> => {
    return ApiService.upload(`${DocumentGroupService.host360}aws/upload`, file);
  }


  static find = async (body: { pageSize: number, pageNumber: number }): Promise<{ items: DocumentGroupDTOExtend[], total: number }> => {
    return await ApiService.post(`${DocumentGroupService.host360}document-group/find`, body);
  }

  static all = async (): Promise<DocumentGroupDTOExtend[]> => {
    return await ApiService.get(`${DocumentGroupService.host360}document-group/all`);
  }

  static detail = async (id: string): Promise<DocumentGroupDTOExtend> => {
    return await ApiService.get(`${DocumentGroupService.host360}document-group/${id}`);
  }

  static sort = async (ids: string[]): Promise<void> => {
    return await ApiService.post(`${DocumentGroupService.host360}document-group/sort`, { ids });
  }

  static allPublish = async (): Promise<DocumentGroupDTOExtend[]> => {
    return await ApiService.get(`${DocumentGroupService.host360}document-group/all/${ApiService.userInfo?.departmentCode || ApiService.departmentCode}`, null, true);
  }
}
