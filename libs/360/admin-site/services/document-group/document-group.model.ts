import { DocumentGroupDTO } from "@lib/shared/360";

export type DocumentGroupDTOExtend = DocumentGroupDTO & { 
    id: string;
 }