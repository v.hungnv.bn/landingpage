import { ApiService } from "@lib/core/services";
import { LinkedCategorySaveReq, LinkedDepartmentDTO, LinkedDepartmentSaveReq } from "@lib/shared/360";


export class LinkedDepartmentService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

  static create = async (body: LinkedDepartmentSaveReq) => {
    return await ApiService.post(`${LinkedDepartmentService.host360}linked-department`, body)
  }

  static update = async (id: string, body: LinkedCategorySaveReq) => {
    return await ApiService.put(`${LinkedDepartmentService.host360}linked-department/${id}`, body)
  }

  static delete = async (id: string) => {
    return await ApiService.delete(`${LinkedDepartmentService.host360}linked-department/${id}`);
  }

  static detail = async (id: string): Promise<LinkedDepartmentDTO> => {
    return await ApiService.get(`${LinkedDepartmentService.host360}linked-department/${id}`);
  }

  static all = async (): Promise<LinkedDepartmentDTO[]> => {
    return await ApiService.get(`${LinkedDepartmentService.host360}linked-department/all`);
  }

  static sort = async (ids: string[]): Promise<void> => {
    return await ApiService.post(`${LinkedDepartmentService.host360}linked-department/sort`, { ids });
  }
}