import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { RegionDTO, RegionSaveReq, ScheduleSaveReq } from '@lib/shared/360';
import { RegionDTOExtend } from './region.model';

export class RegionService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

  static create = async (body: Partial<RegionSaveReq>) => {
    return await ApiService.post(`${RegionService.host360}region`, body);
  }

  static update = async (id: string, body: Partial<RegionSaveReq>) => {
    return await ApiService.put(`${RegionService.host360}region/${id}`, body);
  }

  static delete = async (id: string) => {
    return await ApiService.delete(`${RegionService.host360}region/${id}`);
  }

  static upload = async (file: File): Promise<{ key: string }> => {
    return ApiService.upload(`${RegionService.host360}aws/upload`, file);
  }


  static find = async (body: { pageSize: number, pageNumber: number }): Promise<{ items: RegionDTOExtend[], total: number }> => {
    return await ApiService.post(`${RegionService.host360}region/find`, body);
  }

  static all = async (): Promise<RegionDTO[]> => {
    return await ApiService.get(`${RegionService.host360}region/all`).catch(err => {
      console.error(err);
      return [];
    });
  }

  static detail = async (id: string): Promise<RegionDTOExtend> => {
    return await ApiService.get(`${RegionService.host360}region/${id}`, null, true);
  }

  static allPublish = async (): Promise<RegionDTOExtend[]> => {
    return await ApiService.get(`${RegionService.host360}region/all/${ApiService.departmentCode}`, null, true);

  }

  static findPublish = async (body: { pageSize: number, pageNumber: number }): Promise<{ items: RegionDTOExtend[], total: number }> => {
    return await ApiService.post(`${RegionService.host360}region/find/${ApiService.userInfo?.departmentCode || ApiService.departmentCode}`, body, null, true);
  }

  static sort = async (ids: string[]): Promise<void> => {
    return await ApiService.post(`${RegionService.host360}region/sort`, { ids });
  }
}
