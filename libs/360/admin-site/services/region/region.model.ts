import { RegionCategory, RegionDTO } from "@lib/shared/360";

export type RegionDTOExtend = RegionDTO & {
   id: string;
   categories?: RegionCategoryExtend[]
}

export type RegionCategoryExtend = RegionCategory & {
   id?: string;
   uuid?: string;
   value?: string;
   display?: string;
}