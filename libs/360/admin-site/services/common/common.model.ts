export const PublishList = [{
    value: '1',
    display: 'Sử dụng'
  }, {
    value: '0',
    display: 'Không sử dụng'
  }];

  export type Filters = { 
    field?: string;
    value?: string;
 }

 export type FindDTOReq = { 
  pageSize?: number;
  pageNumber?: number;
  filters?: Filters[];
}
