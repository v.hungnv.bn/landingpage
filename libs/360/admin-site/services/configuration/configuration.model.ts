import { MenuDTO } from "@lib/shared/360";

export type MenuDTOExtend = MenuDTO & { 
    disabled?: boolean;
    open?: boolean;
    
 }

