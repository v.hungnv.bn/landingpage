import { ApiService } from '@lib/core/services';
import { ConfigurationDTO, ConfigurationSaveReq, PostConfigurationDTO, PostConfigurationSaveReq } from '@lib/shared/360';

export class ConfigurationService {
    static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
    static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

    static save = async (body: Partial<ConfigurationSaveReq>) => {
        return await ApiService.post(`${ConfigurationService.host360}configuration`, body);
    }

    static postConfiguration = async (body: Partial<PostConfigurationSaveReq>) => {
        return await ApiService.post(`${ConfigurationService.host360}configuration/postConfiguration`, body);
    }

    static getPostConfiguration = async (): Promise<PostConfigurationDTO> => {
        return await ApiService.get(`${ConfigurationService.host360}configuration/postConfiguration`);
    }


    static upload = async (file: File): Promise<{ key: string }> => {
        return ApiService.upload(`${ConfigurationService.host360}aws/upload`, file);
    }

    static detail = async (): Promise<ConfigurationDTO> => {
        return await ApiService.get(`${ConfigurationService.host360}configuration`);
    }
    static detailPublic = async (departmentCode: string): Promise<ConfigurationDTO> => {
        return await ApiService.get(`${ConfigurationService.host360}configuration/${departmentCode}`, null, true);
    }


}
