import { PageDTO } from "@lib/shared/360";

export type PageDTOExtend = PageDTO & { 
    id: string;
 }