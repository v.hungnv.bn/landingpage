import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { PageSaveReq, PostDTO, PostSaveReq } from '@lib/shared/360';
import { PageDTOExtend } from './page.model';

export class PageService {
    static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
    static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

    static create = async (body: Partial<PageSaveReq>) => {
        return await ApiService.post(`${PageService.host360}page`, body);
    }

    static update = async (id: string, body:  Partial<PageSaveReq>) => {
        return await ApiService.put(`${PageService.host360}page/${id}`, body);
    }

    static delete = async (id: string) => {
        return await ApiService.delete(`${PageService.host360}page/${id}`);
    }

    static upload = async (file: File): Promise<{ key: string }> => {
        return ApiService.upload(`${PageService.host360}aws/upload`, file);
    }

    static all = async (): Promise<PageDTOExtend[]> => {
        return await ApiService.get(`${PageService.host360}page/all`);
    }

    static getPageDetail = async (idOrSlug: string): Promise<PageDTOExtend> => {
        return await ApiService.get(`${PageService.host360}page/${idOrSlug}`, null, true);
    }
}
