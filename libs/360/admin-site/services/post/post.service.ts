import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { CategoryDTO, PostDTO, PostPublicFindReq, PostSaveReq } from '@lib/shared/360';
import { CategoryDTOExtend } from '../categories/categories.model';

export class PostService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

  static create = async (body: PostSaveReq) => {
    return await ApiService.post(`${PostService.host360}post`, body);
  }

  static update = async (id: string, body: PostSaveReq) => {
    return await ApiService.put(`${PostService.host360}post/${id}`, body);
  }

  static delete = async (id: string) => {
    return await ApiService.delete(`${PostService.host360}post/${id}`);
  }

  static upload = async (file: File): Promise<{ key: string }> => {
    return ApiService.upload(`${PostService.host360}aws/upload`, file);
  }

  static find = async (body: PagingRequest): Promise<{ items: PostDTO[], total: number }> => {
    return await ApiService.post(`${PostService.host360}post/find`, body);
  }

  static getPostDetail = async (id: string): Promise<PostDTO> => {
    return await ApiService.get(`${PostService.host360}post/${id}`);

  }

  static all = async (categoryId: string): Promise<PostDTO[]> => {
    return await ApiService.get(`${PostService.host360}post/all/${categoryId}`);

  }

  static allPostPublish = async (categoryId?: string): Promise<PostDTO[]> => {
    return await ApiService.get(`${PostService.host360}post/all/${ApiService.departmentCode}/${categoryId}`, null, true);
  }

  static getAllCategory = async (): Promise<CategoryDTOExtend[]> => {
    const res = await ApiService.get(`${PostService.host360}category/all/${ApiService?.userInfo?.departmentCode || ApiService.departmentCode}`, null, true);
    
    return res?.filter((e:CategoryDTOExtend) => e.synchronized) || [];
  }

  static getCategory = async (id?: string): Promise<CategoryDTO> => {
    return await ApiService.get(`${PostService.host360}category/${id}`);
  }

  static sync = async (ids: string[], departmentCodes: string[]) => {
    return await ApiService.post(`${PostService.host360}post/sync`, {
      ids,
      departmentCodes
    });
  }

  static view = async (idOrSlug: string) => {
    return await ApiService.post(`${PostService.host360}post/view/${idOrSlug}`, {}, null, true);
  }

  static topView = async (departmentCode?: string): Promise<PostDTO[]> => {
    return await ApiService.get(`${PostService.host360}post/allByView/${departmentCode || ApiService.departmentCode}`, null, true);
  }

  static findPublish = async (body: PostPublicFindReq, departmentCode?: string): Promise<{ items: PostDTO[], total: number }> => {
    return await ApiService.post(`${PostService.host360}post/find/${departmentCode || ApiService.departmentCode}`, body, null, true);
  }

}
