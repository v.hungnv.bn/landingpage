import { PostDTO } from "@lib/shared/360";

export type PostDTOExtend = PostDTO & { 
    id: string;
 }