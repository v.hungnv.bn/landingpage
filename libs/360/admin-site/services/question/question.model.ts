import { QuestionDTO, QuestionGroupDTO } from "@lib/shared/360";

export type QuestionDTOExtend = QuestionDTO & { 
    id: string;
 }

 export type QuestionGroupDTOExtend = QuestionGroupDTO & {
    id: string;
    value: string;
    display: string;
 }