import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { QuestionGroupSaveReq, QuestionPublicFindReq, QuestionSaveReq } from '@lib/shared/360';
import { FindDTOReq } from '../common/common.model';
import { QuestionGroupDTOExtend, QuestionDTOExtend } from './question.model';

export class QuestionGroupService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

  static create = async (body: QuestionGroupSaveReq) => {
    return await ApiService.post(`${QuestionGroupService.host360}question-group`, body);
  }

  static update = async (id: string, body: QuestionGroupSaveReq) => {
    return await ApiService.put(`${QuestionGroupService.host360}question-group/${id}`, body);
  }

  static delete = async (id: string) => {
    return await ApiService.delete(`${QuestionGroupService.host360}question-group/${id}`);
  }

  static upload = async (file: File): Promise<{ key: string }> => {
    return ApiService.upload(`${QuestionGroupService.host360}question-group/upload`, file);
  }

  static find = async (body: { pageSize: number, pageNumber: number }): Promise<{ items: QuestionGroupDTOExtend[], total: number }> => {
    return await ApiService.get(`${QuestionGroupService.host360}question-group/find`, body);
  }

  static detail = async (id: string): Promise<QuestionGroupDTOExtend> => {
    return await ApiService.get(`${QuestionGroupService.host360}question-group/${id}`);
  }

  static all = async (): Promise<QuestionGroupDTOExtend[]> => {
    return await ApiService.get(`${QuestionGroupService.host360}question-group/all`);
  }

  static sort = async (ids: string[]): Promise<void> => {
    return await ApiService.post(`${QuestionGroupService.host360}question-group/sort`, { ids });
  }

  static allPublish = async (): Promise<QuestionGroupDTOExtend[]> => {
    return await ApiService.get(`${QuestionGroupService.host360}question-group/all/${ApiService.userInfo?.departmentCode || ApiService.departmentCode}`, null, true);
  }
}

export class QuestionService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

  static create = async (body: QuestionSaveReq) => {
    return await ApiService.post(`${QuestionService.host360}question`, body, null, true);
  }

  static update = async (id: string, body: QuestionSaveReq) => {
    return await ApiService.put(`${QuestionService.host360}question/${id}`, body);
  }

  static delete = async (id: string) => {
    return await ApiService.delete(`${QuestionService.host360}question/${id}`);
  }

  static find = async (body: { pageSize: number, pageNumber: number }): Promise<{ items: QuestionDTOExtend[], total: number }> => {
    return await ApiService.post(`${QuestionService.host360}question/find`, body);
  }

  static getQuestionDetail = async (id: string): Promise<QuestionDTOExtend> => {
    return await ApiService.get(`${QuestionService.host360}question/${id}`);
  }

  static findPublish = async (body: QuestionPublicFindReq): Promise<{ items: QuestionDTOExtend[], total: number }> => {
    return await ApiService.post(`${QuestionService.host360}question/find/${ApiService.userInfo?.departmentCode || ApiService.departmentCode}`, body);
  }

}



