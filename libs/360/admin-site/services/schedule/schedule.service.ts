import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { SchedulePublicFindReq, ScheduleSaveReq } from '@lib/shared/360';
import { FindDTOReq } from '../common/common.model';
import { ScheduleDTOExtend } from './schedule.model';

export class ScheduleService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

  static create = async (body: Partial<ScheduleSaveReq>) => {
    return await ApiService.post(`${ScheduleService.host360}schedule`, {
      ...body,
      fromDate: body.fromDate !== undefined ? Date.toFormat(body.fromDate, 'MM/dd/yyyy') : undefined,
      toDate: body.toDate !== undefined ? Date.toFormat(body.toDate, 'MM/dd/yyyy') : undefined,
    });
  }

  static update = async (id: string, body: Partial<ScheduleSaveReq>) => {
    return await ApiService.put(`${ScheduleService.host360}schedule/${id}`, {
      ...body,
      fromDate: body.fromDate !== undefined ? Date.toFormat(body.fromDate, 'MM/dd/yyyy') : undefined,
      toDate: body.toDate !== undefined ? Date.toFormat(body.toDate, 'MM/dd/yyyy') : undefined,
    });
  }

  static delete = async (id: string) => {
    return await ApiService.delete(`${ScheduleService.host360}schedule/${id}`);
  }

  static upload = async (file: File): Promise<{ key: string }> => {
    return ApiService.upload(`${ScheduleService.host360}aws/upload`, file);
  }


  static find = async (body: { pageSize: number, pageNumber: number }): Promise<{ items: ScheduleDTOExtend[], total: number }> => {
    return await ApiService.post(`${ScheduleService.host360}schedule/find`, body);
  }

  static all = async (): Promise<ScheduleDTOExtend[]> => {
    return await ApiService.get(`${ScheduleService.host360}schedule/all`);
  }

  static detail = async (id: string): Promise<ScheduleDTOExtend> => {
    return await ApiService.get(`${ScheduleService.host360}schedule/${id}`, null, true);
  }

  static allPublish = async (): Promise<ScheduleDTOExtend[]> => {
    return await ApiService.get(`${ScheduleService.host360}schedule/all/${ApiService.userInfo?.departmentCode || ApiService.departmentCode}`, null, true);

  }

  static findPublish = async (body: SchedulePublicFindReq): Promise<{ items: ScheduleDTOExtend[], total: number }> => {
    return await ApiService.post(`${ScheduleService.host360}schedule/find/${ApiService.userInfo?.departmentCode || ApiService.departmentCode}`, body);
  }


}
