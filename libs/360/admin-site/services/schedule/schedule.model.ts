import { ScheduleDTO } from "@lib/shared/360";

export type ScheduleDTOExtend = ScheduleDTO & { 
    id: string;
 }