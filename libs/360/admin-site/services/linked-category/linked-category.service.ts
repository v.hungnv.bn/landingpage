import { ApiService } from "@lib/core/services";
import { LinkedCategoryDTO, LinkedCategorySaveReq } from "@lib/shared/360";

export class LinkedCategoryService {
  static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
  static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

  static create = async (body: LinkedCategorySaveReq) => {
    return await ApiService.post(`${LinkedCategoryService.host360}linked-category`, body)
  }

  static update = async (id: string, body: LinkedCategorySaveReq) => {
    return await ApiService.put(`${LinkedCategoryService.host360}linked-category/${id}`, body)
  }

  static delete = async (id: string) => {
    return await ApiService.delete(`${LinkedCategoryService.host360}linked-category/${id}`);
  }

  static getLinkedCategoriesDetail = async (id: string) => {
    return await ApiService.get(`${LinkedCategoryService.host360}linked-category/${id}`);
  }

  static all = async (): Promise<LinkedCategoryDTO[]> => {
    return await ApiService.get(`${LinkedCategoryService.host360}linked-category/all`).catch(err=> {
      console.error(err);
      return [];
    });
  }

  static sort = async (ids: string[]): Promise<void> => {
    return await ApiService.post(`${LinkedCategoryService.host360}linked-category/sort`, { ids });
  }

  static allPublish = async (): Promise<LinkedCategoryDTO[]> => {
    return await ApiService.get(`${LinkedCategoryService.host360}linked-category/all/${ApiService.departmentCode}`).catch(err=> {
      console.error(err);
      return [];
    });
  }
}