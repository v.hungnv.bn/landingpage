import { PagingRequest } from '@lib/core/models';
import { ApiService } from '@lib/core/services';
import { MediaSaveReq, MediaType, PageSaveReq, PostDTO, PostSaveReq } from '@lib/shared/360';
import { MediaDTOExtend } from './media.model';

export class MediaService {
    static readonly hostUser = process.env.NEXT_PUBLIC_USER_HOST;
    static readonly host360 = process.env.NEXT_PUBLIC_360_HOST;

    static create = async (body: Partial<MediaSaveReq>) => {
        return await ApiService.post(`${MediaService.host360}media`, body);
    }

    static update = async (id: string, body:  Partial<MediaSaveReq>) => {
        return await ApiService.put(`${MediaService.host360}media/${id}`, body);
    }

    static delete = async (id: string) => {
        return await ApiService.delete(`${MediaService.host360}media/${id}`);
    }

    static upload = async (file: File): Promise<{ key: string }> => {
        return ApiService.upload(`${MediaService.host360}aws/upload`, file);
    }

    static all = async (type: MediaType): Promise<MediaDTOExtend[]> => {
        return await ApiService.get(`${MediaService.host360}media/all/${type}`);
    }

    static getMediaDetail = async (id: string): Promise<MediaDTOExtend> => {
        return await ApiService.get(`${MediaService.host360}media/${id}`);
    }

    static sort = async (ids: string[]): Promise<void> => {
      return await ApiService.post(`${MediaService.host360}media/sort`, { ids });
    }

    static allPublic = async (type: string): Promise<MediaDTOExtend[]> => {
        return await ApiService.get(`${MediaService.host360}media/all/${type}/${ApiService.departmentCode}`, null, true);
    }
}
 