import { MediaDTO } from "@lib/shared/360";

export type MediaDTOExtend = MediaDTO & { 
    id: string;
 }