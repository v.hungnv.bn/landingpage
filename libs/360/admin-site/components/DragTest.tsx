import { css, cx } from "@emotion/css";

import { useCallback, useReducer } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import produce from "immer";

export const data = [
    {
      id: "5f832341cc119a50d1adb972",
      picture: "http://placehold.it/32x32",
      name: {
        first: "Goff",
        last: "Robbins",
      },
    },
    {
      id: "5f832341e1d0f20fc283177a",
      picture: "http://placehold.it/32x32",
      name: {
        first: "Pickett",
        last: "Burks",
      },
    },
    {
      id: "5f832341daae2cc0af8610a4",
      picture: "http://placehold.it/32x32",
      name: {
        first: "Taylor",
        last: "Campos",
      },
    },
    {
      id: "5f832341ef54dda7b80930da",
      picture: "http://placehold.it/32x32",
      name: {
        first: "Nolan",
        last: "Bright",
      },
    },
    {
      id: "5f8323410a6b9155385bd47d",
      picture: "http://placehold.it/32x32",
      name: {
        first: "Fran",
        last: "Buchanan",
      },
    },
    {
      id: "5f8323416ecbb23bb925363a",
      picture: "http://placehold.it/32x32",
      name: {
        first: "Vonda",
        last: "Nieves",
      },
    },
    {
      id: "5f832341eee9783dfccbfa6d",
      picture: "http://placehold.it/32x32",
      name: {
        first: "Sheree",
        last: "Reynolds",
      },
    },
    {
      id: "5f832341c0b0131eeade1b00",
      picture: "http://placehold.it/32x32",
      name: {
        first: "Lilian",
        last: "Russell",
      },
    },
  ];

const dragReducer = produce((draft, action) => {
  switch (action.type) {
    case "MOVE": {
      draft[action.from] = draft[action.from] || [];
      draft[action.to] = draft[action.to] || [];
      const [removed] = draft[action.from].splice(action.fromIndex, 1);
      draft[action.to].splice(action.toIndex, 0, removed);
    }
  }
});

export const MultiTableDrag = () => {
  const [state, dispatch] = useReducer(dragReducer, {
    items: data,
  });

  const onDragEnd = useCallback((result) => {
    console.log('result', result)
    if (result.reason === "DROP") {
      if (!result.destination) {
        return;
      }
      dispatch({
        type: "MOVE",
        from: result.source.droppableId,
        to: result.destination.droppableId,
        fromIndex: result.source.index,
        toIndex: result.destination.index,
      });
    }
  }, []);

  return (
    <div className={`flex flex-row h-screen p-4`}>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="items" type="PERSON">
          {(provided, snapshot) => {
            return (
              <div
                ref={provided.innerRef}
                {...provided.droppableProps}
                className={cx(
                    'wrap-todo',
                  styles.dropper,
                  snapshot.isDraggingOver && styles.dropOver
                )}
              >
                {state.items?.map((person, index) => {
                  return (
                    <Draggable
                      key={person.id}
                      draggableId={person.id}
                      index={index}
                    >
                      {(provided, snapshot) => {
                        return (
                          <div
                            className={cx(
                              styles.dragger,
                              snapshot.isDragging && styles.dragging
                            )}
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <div className={styles.draggerContent}>
                              <img
                                src={person.picture}
                                className={styles.draggerIcon}
                              />
                              <span>
                                {person.name.first} {person.name.last}
                              </span>
                            </div>
                          </div>
                        );
                      }}
                    </Draggable>
                  );
                })}
                {provided.placeholder}
              </div>
            );
          }}
        </Droppable>
        <Droppable  droppableId="items2" type="PERSON">
          {(provided, snapshot) => {
            return (
              <div
                ref={provided.innerRef}
                {...provided.droppableProps}
                className={cx(
                    'wrap-done',
                  styles.dropper,
                  snapshot.isDraggingOver && styles.dropOver
                )}
              >
                {state?.items2?.map((person, index) => {
                  return (
                    <Draggable
                      key={person.id}
                      draggableId={person.id}
                      index={index}
                    >
                      {(provided, snapshot) => {
                        return (
                          <div
                            className={cx(
                              styles.dragger,
                              snapshot.isDragging && styles.dragging
                            )}
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <div className={styles.draggerContent}>
                              <img
                                src={person.picture}
                                className={styles.draggerIcon}
                              />
                              <span>
                                {person.name.first} {person.name.last}
                              </span>
                            </div>
                          </div>
                        );
                      }}
                    </Draggable>
                  );
                })}
                {provided.placeholder}
              </div>
            );
          }}
        </Droppable>
      </DragDropContext>
    </div>
  );
};

//
const styles = {
    dragger: (`px-4 py-4 my-2 transition-colors duration-150 ease-in-out bg-white rounded-lg shadow hover:bg-gray-100`),
    dropper: (`w-auto px-4 min-w-1/4 max-w-1/2`),
    draggerContent: (`flex items-center space-x-3 text-base`),
    draggerIcon: (
      `inline-flex items-center justify-center rounded-full p-1.5 text-white bg-teal-100 text-teal-700`
    ),
    dragging: (`bg-gray-300`),
    dropOver: (`bg-gray-100`),
  };