import { css, cx } from "@emotion/css";

import { useCallback, useReducer, useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import produce from "immer";
import { SdButton, SdInput } from "@lib/core/components";
import { MenuDTO, MenuType } from "@lib/shared/360";
import DragIndicatorIcon from '@mui/icons-material/DragIndicator';
import { useForm } from "react-hook-form";
import * as uuid from 'uuid';

export const data: MenuDTO[] = [
    {
        referenceId: "5f832341cc119a50d1adb972",
        type: MenuType.NEWS,
        title: "Tin tức sự kiện",
    },
    {
        referenceId: "đâsd",
        type: MenuType.ORGANIZATION,
        title: "Cơ cấu tổ chức",
    },

];

export const content: MenuDTO[] = [
    {
        referenceId: "dsd",
        type: MenuType.PAGE,
        title: "Giới thiệu chung",
    },
    {
        referenceId: "vdfgdg",
        type: MenuType.PAGE,
        title: "Liên hệ",
    },

];

export const categories: MenuDTO[] = [
    {
        referenceId: "đâsasd",
        type: MenuType.CATEGORY,
        title: "Tin tức từ sở",
    },
    {
        referenceId: "kjhkjhjkasd",
        type: MenuType.CATEGORY,
        title: "Tin tức từ phòng",
    },

];

export const data2: MenuDTO[] = [
    {
        referenceId: "jkhjkhjkhkj",
        type: MenuType.NEWS,
        title: "Home",
    },

];

const dragReducer = produce((draft, action) => {
    console.log('action', action)

    switch (action.type) {
        case "MOVE": {
            draft[action.from] = draft[action.from] || [];
            draft[action.to] = draft[action.to] || [];
            const [removed] = draft[action.from].splice(action.fromIndex, 1);
            draft[action.to].splice(action.toIndex, 0, removed);
        }
        case "ADD": {
            draft[action.from] = draft[action.from] || [];
            draft[action.to] = draft[action.to] || [];
            const [removed] = draft[action.from].splice(action.fromIndex, 1);
            draft[action.to].splice(action.toIndex, 0, removed);
        }
    }
});

export const DragConfigMenu = () => {
    const [menuLink, setMenuLink] = useState<any>({
        title: '',
        link: ''
    });
    const form = useForm();

    const [state, dispatch] = useReducer(dragReducer, {
        items: data,
        pages: content,
        items2: data2,
        categories: categories,
        links: []
    });

    const onDragEnd = useCallback((result) => {
        console.log('result', result)
        if (result.reason === "DROP") {
            if (!result.destination) {
                return;
            }
            if (result.destination?.droppableId !== 'items2') {
                return;
            }
            dispatch({
                type: "MOVE",
                from: result.source.droppableId,
                to: result.destination.droppableId,
                fromIndex: result.source.index,
                toIndex: result.destination.index,
            });
        }
        // console.log('state', state)

    }, []);

    const handleSubmit = () => {
        console.log('state', state)

    }
    const handleAddMenu = () => {
        if (menuLink?.title && menuLink?.link) {
            // state.links = [
            //     ...state.links,
            //     {
            //         referenceId: uuid.v4(),
            //         type: MenuType.LINK,
            //         title: menuLink?.title,
            //         link: menuLink?.link,
            //     }
            // ];
            // dispatch({ type: 'UPDATE', items2: state.items2 })
            const menu = {
                referenceId: uuid.v4(),
                type: MenuType.LINK,
                title: menuLink?.title,
                link: menuLink?.link,
            }
            dispatch({
                type: "ADD",
                from: menu,
                to: 'items2',
                toIndex: state.items2.length,
            });
            console.log('state', state)
        }

    }

    return (
        <div className={`bg-white shadow-f1 rounded-8 p-20`}>
            {/* <SdButton className="mr-12" label='Lưu' variant="outlined" onClick={handleSubmit} /> */}
            <DragDropContext onDragEnd={onDragEnd}>

                <div className="row">
                    <div className="col-3">
                        <div className="shadow-f1 rounded-8">
                            <div className="bg-blue-100 text-white rounded-top-8 text-18 font-bold py-6 px-10">Danh mục </div>
                            <div className="p-5 bg-white">
                                <Droppable droppableId="items" type="PERSON">
                                    {(provided, snapshot) => {
                                        return (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.droppableProps}
                                                className={cx(
                                                    'wrap-todo',
                                                    styles.dropper,
                                                    snapshot.isDraggingOver && styles.dropOver
                                                )}
                                            >
                                                <div className="text-16 font-semibold p-10 border-bottom">Trang mặc định</div>
                                                {state.items?.map((menu, index) => {
                                                    return (
                                                        <Draggable
                                                            key={menu.referenceId}
                                                            draggableId={menu.referenceId}
                                                            index={index}
                                                        >
                                                            {(provided, snapshot) => {
                                                                return (
                                                                    <div
                                                                        className={cx(
                                                                            styles.dragger,
                                                                            snapshot.isDragging && styles.dragging
                                                                        ) + '  p-10 shadow-f1 rounded-8'}
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                    >
                                                                        <div className={styles.draggerContent}>
                                                                            <DragIndicatorIcon />
                                                                            {menu.title}
                                                                        </div>
                                                                    </div>
                                                                );
                                                            }}
                                                        </Draggable>
                                                    );
                                                })}
                                                {provided.placeholder}
                                            </div>
                                        );
                                    }}
                                </Droppable>
                                <Droppable droppableId="pages" type="PERSON">
                                    {(provided, snapshot) => {
                                        return (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.droppableProps}
                                                className={cx(
                                                    'wrap-todo',
                                                    styles.dropper,
                                                    snapshot.isDraggingOver && styles.dropOver
                                                )}
                                            >
                                                <div className="text-16 font-semibold p-10 border-bottom">Trang nội dung</div>
                                                {state.pages?.map((menu, index) => {
                                                    return (
                                                        <Draggable
                                                            key={menu.referenceId}
                                                            draggableId={menu.referenceId}
                                                            index={index}
                                                        >
                                                            {(provided, snapshot) => {
                                                                return (
                                                                    <div
                                                                        className={cx(
                                                                            styles.dragger,
                                                                            snapshot.isDragging && styles.dragging
                                                                        ) + ' p-10 shadow-f1 rounded-8'}
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                    >
                                                                        <div className={styles.draggerContent}>
                                                                            <DragIndicatorIcon />
                                                                            {menu.title}
                                                                        </div>
                                                                    </div>
                                                                );
                                                            }}
                                                        </Draggable>
                                                    );
                                                })}
                                                {provided.placeholder}
                                            </div>
                                        );
                                    }}
                                </Droppable>

                                <Droppable droppableId="categories" type="PERSON">
                                    {(provided, snapshot) => {
                                        return (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.droppableProps}
                                                className={cx(
                                                    'wrap-todo',
                                                    styles.dropper,
                                                    snapshot.isDraggingOver && styles.dropOver
                                                )}
                                            >
                                                <div className="text-16 font-semibold p-10 border-bottom">Chuyên mục</div>
                                                {state.categories?.map((menu, index) => {
                                                    return (
                                                        <Draggable
                                                            key={menu.referenceId}
                                                            draggableId={menu.referenceId}
                                                            index={index}
                                                        >
                                                            {(provided, snapshot) => {
                                                                return (
                                                                    <div
                                                                        className={cx(
                                                                            styles.dragger,
                                                                            snapshot.isDragging && styles.dragging
                                                                        ) + ' p-10 shadow-f1 rounded-8'}
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                    >
                                                                        <div className={styles.draggerContent}>
                                                                            <DragIndicatorIcon />
                                                                            {menu.title}
                                                                        </div>
                                                                    </div>
                                                                );
                                                            }}
                                                        </Draggable>
                                                    );
                                                })}
                                                {provided.placeholder}
                                            </div>
                                        );
                                    }}
                                </Droppable>
                                <Droppable droppableId="links" type="PERSON">
                                    {(provided, snapshot) => {
                                        return (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.droppableProps}
                                                className={cx(
                                                    'wrap-todo',
                                                    styles.dropper,
                                                    snapshot.isDraggingOver && styles.dropOver
                                                )}
                                            >
                                                <div className="text-16 font-semibold p-10 border-bottom mb-16">Liên kết tự tạo</div>
                                                <div>
                                                    <SdInput
                                                        className="w-100"
                                                        form={form}
                                                        label='Tên Liên kết'
                                                        value={menuLink.title}
                                                        sdChange={(value) => setMenuLink({
                                                            ...menuLink,
                                                            title: value
                                                        })}
                                                    />
                                                </div>
                                                <div>
                                                    <SdInput
                                                        className="w-100"
                                                        form={form}
                                                        label='Link Liên kết'
                                                        value={menuLink.link}
                                                        sdChange={(value) => setMenuLink({
                                                            ...menuLink,
                                                            link: value
                                                        })}
                                                    />
                                                </div>
                                                <div>
                                                    <SdButton label='Thêm vào menu' onClick={handleAddMenu} />
                                                </div>

                                                {provided.placeholder}
                                            </div>
                                        );
                                    }}
                                </Droppable>
                                {/* <div>
                                    <div className="text-16 font-semibold p-10 border-bottom mb-16">Liên kết tự tạo</div>
                                    <div>
                                        <SdInput
                                            className="w-100"
                                            form={form}
                                            label='Tên Liên kết'
                                            value={menuLink.title}
                                            sdChange={(value) => setMenuLink({
                                                ...menuLink,
                                                title: value
                                            })}
                                        />
                                    </div>
                                    <div>
                                        <SdInput
                                            className="w-100"
                                            form={form}
                                            label='Link Liên kết'
                                            value={menuLink.link}
                                            sdChange={(value) => setMenuLink({
                                                ...menuLink,
                                                link: value
                                            })}
                                        />
                                    </div>
                                    <div>
                                        <SdButton label='Thêm vào menu' onClick={handleAddMenu} />
                                    </div>
                                </div> */}
                            </div>
                        </div>

                    </div>
                    <div className="col-4">
                        <div className="shadow-f1 rounded-8">
                            <div className="bg-blue-100 text-white rounded-top-8 text-18 font-bold py-6 px-10">Menu</div>
                            <div className="p-5 bg-white">
                                <Droppable droppableId="items2" type="PERSON" >
                                    {(provided, snapshot) => {
                                        return (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.droppableProps}
                                                className={cx(
                                                    'wrap-done',
                                                    styles.dropper,
                                                    snapshot.isDraggingOver && styles.dropOver
                                                )}
                                            >
                                                {state?.items2?.map((menu, index) => {
                                                    return (
                                                        <Draggable
                                                            key={menu.referenceId}
                                                            draggableId={menu.referenceId}
                                                            index={index}
                                                        >
                                                            {(provided, snapshot) => {
                                                                return (
                                                                    <div
                                                                        className={cx(
                                                                            styles.dragger,
                                                                            snapshot.isDragging && styles.dragging
                                                                        ) + ' p-10 shadow-f1 rounded-8'}
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                    >
                                                                        <div className={styles.draggerContent}>
                                                                            <DragIndicatorIcon />
                                                                            {menu.title}
                                                                        </div>
                                                                    </div>
                                                                );
                                                            }}
                                                        </Draggable>
                                                    );
                                                })}
                                                {provided.placeholder}
                                            </div>
                                        );
                                    }}
                                </Droppable>
                            </div>
                        </div>
                    </div>
                </div>
            </DragDropContext>
        </div>
    );
};

//
const styles = {
    dragger: (`px-4 py-4 my-2 transition-colors duration-150 ease-in-out bg-white rounded-lg shadow hover:bg-gray-100`),
    dropper: (`w-auto px-4 min-w-1/4 max-w-1/2`),
    draggerContent: (`flex items-center space-x-3 text-base`),
    draggerIcon: (
        `inline-flex items-center justify-center rounded-full p-1.5 text-white bg-teal-100 text-teal-700`
    ),
    dragging: (`bg-gray-300`),
    dropOver: (`bg-gray-100`),
};