import { useState, useEffect } from 'react';
import React, { useContext } from 'react'

import { useRouter } from 'next/router';
import { ConfigurationService } from '../services';
import { ConfigurationDTO } from '@lib/shared/360';
import Head from "next/head";
import { cdn } from '@lib/core/services';
import DepartmentContext from '@lib/admin/context/department-context';


interface Props {
    children?: any;
    routerDefault?: string;
}

export const InitConfigSite = ({ children, routerDefault }: Props): React.ReactElement => {

    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false);
    const [configSite, setConfigSite] = useState<ConfigurationDTO>();

    const department = useContext(DepartmentContext);

    useEffect(() => {
        getConfiguaration();
    }, [department]);

    const getConfiguaration = async () => {
        setIsLoading(true);
        try {
            const config = await ConfigurationService.detailPublic(department.departmentCode);
            setConfigSite(config);
        } finally {
            setIsLoading(false);
        }

    }


    return (
        <>
            {!isLoading && (
                <>
                    <Head>
                        <link rel="preconnect" href="https://cdn.sanity.io/" />
                        <link rel="dns-prefetch" href="https://cdn.sanity.io//" />
                        <title>{configSite?.title}</title>
                        <meta name="description" content={configSite?.faviconInfo} />
                        {/* <link rel="icon" href="/favicon.ico" /> */}
                        <link rel="icon" href="/favicon.svg" />
                        {/* <link rel="icon" href={cdn(configSite?.image)} /> */}
                    </Head>
                    {children}
                </>
            )}

        </>
    );
}

