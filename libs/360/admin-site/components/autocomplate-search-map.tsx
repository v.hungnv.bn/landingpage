



import { useCallback, useEffect, useState } from "react";
import { RegionDTOExtend, TravelCategoryService } from '@lib/360/admin-site/services'
import { GooglePlaceResult } from '@lib/shared/360'
import { Autocomplete, TextField } from '@mui/material';
import React from 'react';
import { debounce } from 'lodash';
import Image from 'next/image'

import GoogleMapReact from "google-map-react";

const GoogleMapSearch = (props: { center?: any, lat?: number, lng?: number, icon?: string, onClick: (data?: any) => void }) => {
    const { icon, lat, lng } = props;
    const [isLoading, setIsLoading] = useState(false);
    const [, setInputValue] = React.useState("");
    const [options, setOptions] = React.useState<GooglePlaceResult[]>([]);

    const [position, setPosition] = useState<any>(null);
    const [center, setCenter] = useState<{ lat: number, lng: number } | null>(null);
    const defaultProps = {
        center: {
            lat: 12.249969820488728,
            lng: 107.5680857232407
        },
        zoom: 15
    };

    useEffect(() => {
        setCenter({
            lat: 12.249969820488728,
            lng: 107.5680857232407
        })
    }, []);

    useEffect(() => {
        setPosition({
            lat: lat,
            lng: lng
        })
    }, [lat]);

    const handleChangeLocation = (event, values) => {
        setInputValue(values?.name || '');
        setCenter(null);
        const timer = setTimeout(() => {
            setCenter({
                ...center,
                lat: values?.geometry?.location?.lat,
                lng: values?.geometry?.location?.lng
            })
        }, 500);
    }

    const fetchDropdownOptions = async (key) => {
        try {
            const res = await TravelCategoryService.searchPlace(key);
            console.log('options', res)
            setOptions(res || []);

        } catch (err) {
            console.log(err)
        }
        finally {
        }
    }

    const debounceDropDown = useCallback(debounce((nextValue) => fetchDropdownOptions(nextValue), 500), [])

    const handleSearch = async event => {
        setInputValue(event.target.value);
        if (event.target.value) {
            debounceDropDown(encodeURIComponent(event.target.value));
            // debounceDropDown((event.target.value));
        }
    };

    const getMapOptions = (maps) => {
        return {
            streetViewControl: true,
            draggableCursor: 'crosshair',
            scaleControl: true,
            fullscreenControl: true,
            zoomControl: true,
            scrollwheel: true,
            styles: [{
                featureType: "poi.business",
                elementType: "labels",
                stylers: [{
                    visibility: "on"
                }]
            }],
            gestureHandling: "greedy",
            disableDoubleClickZoom: false,
            mapTypeControl: true,
            mapTypeId: maps.MapTypeId.ROADMAP,
            mapTypeControlOptions: {
                style: maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: maps.ControlPosition.TOP_LEFT,
                mapTypeIds: [
                    maps.MapTypeId.ROADMAP,
                    maps.MapTypeId.SATELLITE,
                ]
            },
            clickableIcons: true,
        };
    }

    const _onClick = (data) => {

        // console.log(x, y, lat, lng, event);
        // console.log('lat_0', data);
        setPosition({
            ...position,
            lat: data?.lat,
            lng: data?.lng,
        })
        props.onClick(data);

    };

    return (

        <div >
            <div className="mb-16">
                {/* <SdButton label='Thêm vào menu' onClick={handleChangeView} /> */}

                <Autocomplete
                    id="country-select-demo"
                    options={options}
                    getOptionLabel={(option) => option?.formattedAddress}
                    onChange={handleChangeLocation}
                    // options={options.map((option) => option.formattedAddress)}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            size={'small'}
                            label="Tìm kiếm địa chỉ chi tiết"
                            InputProps={{
                                ...params.InputProps,
                                type: 'search',
                            }}
                            onChange={handleSearch}
                        />
                    )}
                />
            </div>

            {center?.lat && center?.lng && (
                <div style={{ height: '600px', width: '100%' }}>
                    <GoogleMapReact
                        bootstrapURLKeys={{
                            key: process.env.NEXT_PUBLIC_GOOGLE_MAP || 'AIzaSyDFSVU6CbF2R1Ni2aps8sO5X9dVmcPUM4U',
                        }}
                        defaultCenter={center}
                        defaultZoom={defaultProps.zoom}
                        yesIWantToUseGoogleMapApiInternals
                        onClick={_onClick}
                        options={getMapOptions}

                    >
                        {position?.lat && position?.lng && (
                            <MyMarker lat={position?.lat} lng={position?.lng} icon={icon}>
                            </MyMarker>
                        )}


                    </GoogleMapReact>
                </div>
            )}


        </div>
    )
}

export default GoogleMapSearch

const MyMarker = (props) => {
    const { show, text, icon } = props;


    return (
        <>
            <div className="relative">
                {/* {text} */}
                <img className="h-maker-icon" src={`/admin/icons/${icon || 'ATM'}.svg`} alt="photo" width="24" height="34" />
            </div>
            {/* <InfoWindow  /> */}
        </>
    );
};
