import React, { useState } from "react";
import { Drag, DragAndDrop, Drop } from "./drag-and-drop";
// import { reorder } from "./helpers.js";
export const reorder = (list: any, startIndex: number, endIndex: number) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const list = [
  {
    id: "q10111",
    name: "Home",
    items: [
      { id: "abc1", name: "First1" },
      { id: "def1", name: "Second1" }
    ]
  },
  {
    id: "wkqx121",
    name: "Liên hệ",
    items: [
      { id: "ghi1", name: "Third1" },
      { id: "jkl1", name: "Fourth2" }
    ]
  }
]
export const NestedListComponent = () => {
  const [categories, setCategories] = useState<any>([
    {
      id: "q101",
      name: "Category 1",
      items: [
        { id: "abc", name: "First" },
        { id: "def", name: "Second" }
      ]
    },
    {
      id: "wkqx",
      name: "Category 2",
      items: [
        { id: "ghi", name: "Third" },
        { id: "jkl", name: "Fourth" }
      ]
    }
  ]);

  const handleDragEnd = (result: { type: any; source: any; destination: any; }) => {
    console.log('result', result);

    const { type, source, destination } = result;
    if (!destination) return;

    const sourceCategoryId = source.droppableId;
    const destinationCategoryId = destination.droppableId;

    // Reordering items
    if (type === "droppable-item") {
      // If drag and dropping within the same category
      if (sourceCategoryId === destinationCategoryId) {
        const catfind = categories.find((category) => category.id === sourceCategoryId);
        const updatedOrder = reorder(catfind?.items || [], source.index, destination.index);
        const updatedCategories = categories.map((category) =>
          category.id !== sourceCategoryId
            ? category
            : { ...category, items: updatedOrder }
        );

        setCategories(updatedCategories);
      } else {
        const sourceOrder = categories.find(
          (category) => category.id === sourceCategoryId
        )?.items || [];
        const destinationOrder = categories.find(
          (category) => category.id === destinationCategoryId
        )?.items || [];

        const [removed] = sourceOrder.splice(source.index, 1);
        destinationOrder.splice(destination.index, 0, removed);

        destinationOrder[removed] = sourceOrder[removed];
        delete sourceOrder[removed];

        const updatedCategories = categories.map((category) =>
          category.id === sourceCategoryId
            ? { ...category, items: sourceOrder }
            : category.id === destinationCategoryId
              ? { ...category, items: destinationOrder }
              : category
        );

        setCategories(updatedCategories);
      }
    }

    // Reordering categories
    if (type === "droppable-category") {
      const updatedCategories = reorder(
        categories,
        source.index,
        destination.index
      );

      setCategories(updatedCategories);
    }

    console.log('type', type)
  };

  return (
    <DragAndDrop onDragEnd={handleDragEnd}>
      <div className="row">
        <div className="col-6">

          {/* <Drop id="droppable1" type="droppable-category-init">
            {list.map((category, categoryIndex) => {
              return (
                <Drag
                  className="draggable-category"
                  key={category.id}
                  idx={categoryIndex}
                  id={category.id}
                  index={categoryIndex}
                >
                  <div className="category-container">
                    <h2 className="item">{category?.name}</h2>

                    <Drop key={category?.id} id={category?.id} type="droppable-item-init">
                      {category?.items?.length > 0 && (
                        <div>
                          {category?.items.map((item, index) => {
                            return (
                              <Drag
                                className="draggable"
                                key={item.id}
                                idx={index}
                                id={item.id}
                                index={index}
                              >
                                <div className="item">{item?.name}</div>
                              </Drag>
                            );
                          })}
                        </div>
                      )}

                    </Drop>
                  </div>
                </Drag>
              );
            })}


          </Drop> */}

        </div>
        <div className="col-12">



          <Drop id="droppable" type="droppable-category">
            <div className="row">
              <div className="col-6">
                {list.map((category, categoryIndex) => {
                  return (
                    <Drag
                      className="draggable-category"
                      key={category.id}
                      idx={categoryIndex}
                      id={category.id}
                      index={categoryIndex}
                    >
                      <div className="category-container">
                        <h2 className="item">{category?.name}</h2>

                        <Drop key={category?.id} id={category?.id} type="droppable-item">
                          {category?.items?.length > 0 && (
                            <div>
                              {category?.items.map((item, index) => {
                                return (
                                  <Drag
                                    className="draggable"
                                    key={item.id}
                                    idx={index}
                                    id={item.id}
                                    index={index}
                                  >
                                    <div className="item">{item?.name}</div>
                                  </Drag>
                                );
                              })}
                            </div>
                          )}

                        </Drop>
                      </div>
                    </Drag>
                  );
                })}
              </div>
              <div className="col-6">
                {categories.map((category, categoryIndex) => {
                  return (
                    <Drag
                      className="draggable-category"
                      key={category.id}
                      idx={categoryIndex}
                      id={category.id}
                      index={categoryIndex}
                    >
                      <div className="category-container">
                        <h2 className="item">{category?.name}</h2>

                        <Drop key={category.id} id={category.id} type="droppable-item">
                          {category?.items?.length > 0 && (
                            <div>
                              {category?.items.map((item, index) => {
                                return (
                                  <Drag
                                    className="draggable"
                                    key={item.id}
                                    idx={index}
                                    id={item.id}
                                    index={index}
                                  >
                                    <div className="item">{item?.name}</div>
                                  </Drag>
                                );
                              })}
                            </div>
                          )}

                        </Drop>
                      </div>
                    </Drag>
                  );
                })}
              </div>
            </div>




          </Drop>
        </div>
      </div>

    </DragAndDrop>
  );
};
