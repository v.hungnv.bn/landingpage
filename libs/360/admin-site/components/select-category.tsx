import { SdInput, SdSelect, SdTreeView, SdTreeViewOptionTransform } from "@lib/core/components";
import { useEffect, useState } from "react";
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import React from "react";
import { TreeData } from "../services/data.model";
import { Controller, useForm } from "react-hook-form";
import { TextField, TextFieldProps } from "@mui/material";
import FormHelperText from '@mui/material/FormHelperText';

export interface SelectCategoryOption<T> {
    value: string;
    display: string;
    data: T;
    children?: SelectCategoryOption<T>[];
}

export interface SelectCategoryProps<T> {
    items: SelectCategoryOption<T>[];
    sdSelect: (item: SelectCategoryOption<T> | null) => void;
    display?: string;
}

export function SelectCategory<T = any>(props: SelectCategoryProps<T> & TextFieldProps) {
    const [options, setOptions] = useState<SelectCategoryOption<T>[]>([]);
    const { items, sdSelect, value, label, display, required } = props;

    useEffect(() => {
        setOptions(items.search('', ['value', 'display'], 'children'));
        console.log('__props', props)
    }, [items]);

   
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        // setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;
    return (
        <div>
            <TextField
                required={required}
                variant="outlined"
                fullWidth
                label={label}
                size="small"
                // onClick={handleClick}
                value={display}
                inputProps={{
                    onClick: (event: any) => {
                        handleClick(event);
                    },
                    onKeyDown: (event) => {
                        event.preventDefault();
                    },
                }}
            ></TextField>

            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                <SdTreeView
                    sx={{
                        height: '500px',
                        width: '300px'
                    }}
                    sdSelect={(item) => {
                        // setAnchorEl(null);
                        console.log('item', item)
                        if (item?.value) {
                            sdSelect(item);
                            setAnchorEl(null);
                        }
                    }}
                    items={items}
                />
            </Popover>
        </div>
    );
}
