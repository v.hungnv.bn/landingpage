import { SdButton, SdInput, SdModal, SdModalRef, SdSelect } from "@lib/core/components"
import { LoadingService, NotifyService } from "@lib/core/services";
import { LinkedCategorySaveReq } from "@lib/shared/360";
import { SaveOutlined } from "@mui/icons-material";
import { TextField, Switch } from "@mui/material";
import { useEffect, useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { LinkedCategoryService } from "../services/linked-category/linked-category.service";



function DepartmentCategoryDetail(props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) {
  const { id, open, onAgree, onClose } = props
  const [openModal, setOpenModal] = useState(false);
  const sdModal = useRef<SdModalRef | null>(null);
  const [req, setReq] = useState<LinkedCategorySaveReq>({
    isActivated: true
  });

  const [type, setType] = useState<string>('1')
  const [listType, setListType] = useState<{
    value: string;
    display: string;
  }[]>([])
  useEffect(() => {
    setOpenModal(open);
  }, [open]);

  const form = useForm();
  const {
    handleSubmit,
  } = form;


  useEffect(() => {
    fetchDetail(id);
  }, [id]);

  const fetchDetail = async (id?: string) => {
    if (!id) {
      setReq({
        isActivated: true
      });
    } else {
      LoadingService.start();
      try {
        const res = await LinkedCategoryService.getLinkedCategoriesDetail(id);
        setReq(res)
      } catch (error) {
        console.log(error)
      } finally {
        LoadingService.stop();

      }
    }
  }
  const submitHandler = async (data: any) => {
    LoadingService.start();
    try {
      if (id) {
        await LinkedCategoryService.update(id, req)
        NotifyService.success('Cập nhật danh mục Thành công!');
        sdModal?.current?.close();
        onAgree();
      } else {
        await LinkedCategoryService.create(req)
        NotifyService.success('Tạo thành công danh mục!');
        sdModal?.current?.close();
        onAgree();
      }
    } catch (error) {
      console.log(error)
    } finally {
      LoadingService.stop();

    }
  }

  return (
    <SdModal opened={openModal} ref={sdModal} width={'25rem'}
      footer={
        <><SdButton className="mr-12" label='Hủy' color="secondary" variant="text" onClick={() => onAgree()} />
          <SdButton icon={<SaveOutlined />} label='Lưu' onClick={handleSubmit(submitHandler)} /></>
      }
      title={id ? 'Cài đặt vùng hiển thị' : 'Tạo mới danh mục đơn vị'} onClose={onClose}>
      <div className="d-flex flex-column align-items-center">
        <div className="mb-16 w-100">
          <SdInput
            className="w-100 mt-10"
            form={form}
            rules={{
              required: true,
              minLength: 1,
            }}
            label='Tên danh mục '
            value={req.name}
            sdChange={(value) => setReq({
              ...req,
              name: value
            })}
          />
        </div>
        <div className="w-100 mb-16">
          <SdSelect
            form={form}
            label="Trạng thái"
            value={req.isActivated}
            items={{
              displayOnTrue: 'Sử dụng',
              displayOnFalse: 'Không sử dụng'
            }}
            sdChange={(item, value) => {
              setReq({
                ...req,
                isActivated: value
              })
            }}
          />
        </div>
      </div>
    </SdModal>
  )
}

export default DepartmentCategoryDetail