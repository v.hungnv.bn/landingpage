import React, { useState } from 'react';

import { useEffect, useRef } from "react";

import { SdButton, SdModal, SdModalRef } from "@lib/core/components";
import { PageHeader } from "@lib/admin/components/PageHeader";
import { SdBadge, SdGrid, SdGridRef, SdInput } from "@lib/core/components";
import { ApiService, LoadingService, NotifyService } from "@lib/core/services";
import { NextPage } from "next"
import AddIcon from '@mui/icons-material/Add';
import { NestedListComponent } from "../components/NestedListComponent";
import { MultiTableDrag } from "../components/DragTest";
import { DragConfigMenu } from "../components/drag-config-menu";
import { MenuDTO, MenuType } from "@lib/shared/360";
import * as uuid from 'uuid';
import { IconButton, Stack } from "@mui/material";
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import DeleteIcon from '@mui/icons-material/Delete';
import { MenuDTOExtend } from "../services";
import { useForm } from "react-hook-form";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';

const host360 = process.env.NEXT_PUBLIC_360_HOST;

const MenuConfigChildren = (props: { open: boolean, onClose: () => void, onAgree: (menus: MenuDTOExtend[]) => void, menu?: MenuDTOExtend, statics, catList, pageList }) => {
  const sdModal = useRef<SdModalRef | null>(null);
  const { menu, onClose, onAgree, statics, catList, pageList } = props;
  const [openModal, setOpenModal] = useState(false);
  const form = useForm();
  const [menuLink, setMenuLink] = useState<MenuDTOExtend>({
    title: '',
    link: ''
  });
  const [menus, setMenus] = useState<MenuDTOExtend[]>([]);
  const [staticPages, setStaticPages] = useState<MenuDTOExtend[]>([]);
  const [categories, setCategories] = useState<MenuDTOExtend[]>([]);
  const [pages, setPages] = useState<MenuDTOExtend[]>([]);
  const [expanded, setExpanded] = useState({
    static: true,
    page: false,
    category: false,
    link: false
  });
  useEffect(() => {
    setStaticPages(statics);
    setCategories(catList);
    setPages(pageList);

    setOpenModal(props.open);

  }, [props.open]);



  const handlerCancel = () => {
    setOpenModal(false)
  }

  const handlerSave = () => {
    sdModal?.current?.close();
    props.onAgree(menus);
  }

  const addMenu = (menu: MenuDTOExtend) => {
    staticPages.forEach(e => {
      if (e.referenceId === menu.referenceId) {
        e.disabled = true;
      }
    });
    setStaticPages(staticPages);

    categories.forEach(e => {
      if (e.referenceId === menu.referenceId) {
        e.disabled = true;
      }
    });
    setCategories(categories);

    pages.forEach(e => {
      if (e.referenceId === menu.referenceId) {
        e.disabled = true;
      }
    });
    setPages(pages);

    setMenus([
      ...menus,
      menu
    ]);

  }

  const removeMenu = (menu: MenuDTOExtend) => {
    const referenceIds = [menu.referenceId];
    if (menu.children) {
      menu.children.forEach(c => {
        referenceIds.push(c.referenceId);
      });
    }

    staticPages.forEach(e => {
      if (referenceIds.includes(e.referenceId)) {
        e.disabled = false;
      }

    });
    setStaticPages(staticPages);

    categories.forEach(e => {
      if (referenceIds.includes(e.referenceId)) {
        e.disabled = false;
      }

    });
    setCategories(categories);

    pages.forEach(e => {
      if (referenceIds.includes(e.referenceId)) {
        e.disabled = false;
      }

    });
    setPages(pages);
    const newMenus = menus?.filter(e => e.referenceId !== menu.referenceId)
    setMenus(newMenus);
  }




  const handleMenuLink = () => {
    if (menuLink?.title && menuLink?.link) {
      const menu = {
        referenceId: uuid.v4(),
        type: MenuType.LINK,
        title: menuLink?.title,
        link: menuLink?.link,
      }
      setMenus([
        ...menus,
        menu
      ]);
    }
  }

  return (
    <SdModal ref={sdModal} opened={openModal} width={'980px'}
      footer={
        <><SdButton className="mr-12" label='Lưu' onClick={handlerSave} /></>
      }
      title={'Cấu hình menu con: ' + menu?.title} onClose={props.onClose}>
      <div className="bg-white rounded-8 shadow-f1 p-20">
        <div className="row">
          <div className="col-4">
            <div className="shadow-f1 rounded-8">
              <div className="bg-blue-100 text-white rounded-top-8 text-18 font-bold py-6 px-10">Danh mục </div>
              <div className="p-5 bg-white">
                <div >
                  <div className="text-16 font-semibold px-10 border-bottom py-10 d-flex justify-content-between align-items-center">
                    Trang mặc định
                    {expanded.static ? (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        static: !expanded.static
                      })}>
                        <KeyboardArrowUpIcon />
                      </IconButton>
                    ) : (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        static: !expanded.static
                      })}>
                        <KeyboardArrowDownIcon />
                      </IconButton>
                    )}

                  </div>
                  <div className={expanded.static ? 'block' : 'hidden'}>
                    {staticPages.map(e => (
                      <div key={e.referenceId} className="p-10 shadow-f1 rounded-8 d-flex justify-content-between align-items-center">
                        {e.title}
                        <IconButton disabled={e.disabled} aria-label="add" onClick={() => addMenu(e)}>
                          <ArrowRightAltIcon />
                        </IconButton>
                      </div>
                    ))}
                  </div>
                </div>

                <div>
                  <div className="text-16 font-semibold px-10 border-bottom py-10 d-flex justify-content-between align-items-center">
                    Trang nội dung
                    {expanded.page ? (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        page: !expanded.page
                      })}>
                        <KeyboardArrowUpIcon />
                      </IconButton>
                    ) : (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        page: !expanded.page
                      })}>
                        <KeyboardArrowDownIcon />
                      </IconButton>
                    )}

                  </div>
                  <div className={expanded.page ? 'block' : 'hidden'}>
                    {pages.map(e => (
                      <div key={e.referenceId} className="p-10 shadow-f1 rounded-8 d-flex justify-content-between align-items-center">
                        {e.title}
                        <IconButton disabled={e.disabled} aria-label="add" onClick={() => addMenu(e)}>
                          <ArrowRightAltIcon />
                        </IconButton>
                      </div>
                    ))}
                  </div>
                </div>

                <div>
                  <div className="text-16 font-semibold px-10 border-bottom py-10 d-flex justify-content-between align-items-center">
                    Trang nội chuyên mục
                    {expanded.category ? (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        category: !expanded.category
                      })}>
                        <KeyboardArrowUpIcon />
                      </IconButton>
                    ) : (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        category: !expanded.category
                      })}>
                        <KeyboardArrowDownIcon />
                      </IconButton>
                    )}

                  </div>
                  <div className={expanded.category ? 'block' : 'hidden'}>
                    {categories?.map(e => (
                      <div key={e.referenceId} className="p-10 shadow-f1 rounded-8 d-flex justify-content-between align-items-center">
                        {e.title}
                        <IconButton disabled={e.disabled} aria-label="add" onClick={() => addMenu(e)}>
                          <ArrowRightAltIcon />
                        </IconButton>
                      </div>
                    ))}
                  </div>
                </div>

                <div>
                  <div className="text-16 font-semibold px-10 border-bottom py-10 d-flex justify-content-between align-items-center">
                    Liên kết tự tạo
                    {expanded.link ? (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        link: !expanded.link
                      })}>
                        <KeyboardArrowUpIcon />
                      </IconButton>
                    ) : (
                      <IconButton aria-label="add" onClick={() => setExpanded({
                        ...expanded,
                        link: !expanded.link
                      })}>
                        <KeyboardArrowDownIcon />
                      </IconButton>
                    )}

                  </div>
                  <div className={expanded.link ? 'block' : 'hidden'}>
                    <div>
                      <SdInput
                        className="w-100"
                        form={form}
                        label='Tên Liên kết'
                        value={menuLink.title}
                        sdChange={(value) => setMenuLink({
                          ...menuLink,
                          title: value
                        })}
                      />
                    </div>
                    <div>
                      <SdInput
                        className="w-100"
                        form={form}
                        label='Link Liên kết'
                        value={menuLink.link}
                        sdChange={(value) => setMenuLink({
                          ...menuLink,
                          link: value
                        })}
                      />
                    </div>
                    <div>
                      <SdButton label='Thêm vào menu' onClick={handleMenuLink} />
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div className="col-6">
            <div className="shadow-f1 rounded-8">
              <div className="bg-blue-100 text-white rounded-top-8 text-18 font-bold py-6 px-10">Menu </div>
              <div className="p-5 bg-white">
                <div className="p-10 shadow-f1 rounded-8 d-flex justify-content-between align-items-center">
                  {menu?.title}
                </div>
                {menus?.length > 0 && (
                  <div className='pl-12'>
                    {menus.map(e => (
                      <div key={e.referenceId} className="p-10 shadow-f1 rounded-8 d-flex justify-content-between align-items-center">
                        {e.title}
                        <Stack direction="row" spacing={1}>
                          <IconButton onClick={() => removeMenu(e)} aria-label="delete">
                            <DeleteIcon />
                          </IconButton>

                        </Stack>


                      </div>
                    ))}
                  </div>
                )}
              </div>
            </div>



          </div>
        </div>

      </div>

    </SdModal>
  )
}
export default MenuConfigChildren