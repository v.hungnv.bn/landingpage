import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect } from "@lib/core/components";
import { TextField } from "@mui/material";
import Image from 'next/image'
import { LoadingService, NotifyService } from "@lib/core/services";
import { TravelCategoryService } from "../services";
import { TravelCategorySaveReq } from "@lib/shared/360";

const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const TravelCategoryIcons = [
    {
        value: 'ANUONG',
        display: 'ANUONG',
    },
    {
        value: 'ATM',
        display: 'ATM',
    },
    {
        value: 'COQUANHANHCHINH',
        display: 'Cơ quan hành chính',
    },
    {
        value: 'CUAHANGTIENLOI',
        display: 'Cửa hàng tiện lợi',
    },
    {
        value: 'DITICH',
        display: 'Di tích',
    },
    {
        value: 'KHACHSAN',
        display: 'Khách sạn',
    },
    {
        value: 'LEHOI',
        display: 'Lế hội',
    },
    {
        value: 'RAPCHIEUPHIM',
        display: 'Rạp chiếu phim',
    },
    {
        value: 'TRAMXANG',
        display: 'Trạm xăng',
    },
    {
        value: 'TTTM',
        display: 'Trung tâm thương mại',
    },
    {
        value: 'TTYT',
        display: 'Trung tâm y tế',
    },
];

const ModalTravelCategory = (props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) => {
    const sdModal = useRef<SdModalRef | null>(null);

    const [openModal, setOpenModal] = useState(false);

    const [req, setReq] = useState<TravelCategorySaveReq>({
        icon: 'ATM',
        isActivated: true
    });


    const form = useForm();

    const {
        register,
        handleSubmit,
        control,
        setValue,
        watch,
        formState: { errors },
    } = form;

    useEffect(() => {
       
        setOpenModal(props.open);

    }, [props.open]);

    useEffect(() => {
        if (props?.id) {
            getDetail(props?.id);
        }
    }, [props.id]);

    useEffect(() => {
        // register("description", { minLength: 15 });


    }, [register]);


    const getDetail = async (id: string) => {
        LoadingService.start();
        try {
            const res = await TravelCategoryService.detail(id);
            setValue('name', res?.name);
            setReq({
                ...req,
                icon: res.icon,
                isActivated: res?.isActivated
            })

        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }


    const handlerCancel = () => {
        setOpenModal(false);
        props.onClose();
    }

    const handlerSave = () => {

    }




    const submitHandler = async (data) => {
        const reqInp: Partial<TravelCategorySaveReq> = {
            name: data.name,
            icon: req.icon,
            isActivated: req.isActivated
        };
        LoadingService.start();
        try {
          if (props?.id) {
            await TravelCategoryService.update(props?.id, reqInp);
            NotifyService.success("Cập nhật danh mục Thành công!");
            sdModal?.current?.close();
            props.onAgree();
          } else {
            await TravelCategoryService.create(reqInp);
            NotifyService.success("Tạo mới danh mục Thành công!");
            sdModal?.current?.close();
            props.onAgree();
          }
        } catch (error) {
          console.log(error);
        } finally {
          LoadingService.stop();
        }
      };

    return (
        <SdModal ref={sdModal} opened={openModal} width={'489px'}
            footer={
                <><SdButton className="mr-12" label='Hủy' variant='text' color='secondary' onClick={handlerCancel} /><SdButton className="mr-12" label='Lưu' onClick={handleSubmit(submitHandler)} /></>
            }
            title={"Thêm danh mục du lịch"} onClose={props.onClose}>
            <form onSubmit={handleSubmit(submitHandler)} >
                <div className="row mt-16 mb-32">

                    <div className="col-12">

                        <div className="mb-30">
                            <Controller
                                name="name"
                                control={control}
                                defaultValue=""
                                rules={{
                                    required: true,
                                }}
                                render={({ field }) => (
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        id="name"
                                        label="Tên danh mục"
                                        size="small"
                                        error={Boolean(errors.name)}
                                        helperText={
                                            errors.name
                                                ? 'Tiêu danh mục bắt buộc'
                                                : ''
                                        }
                                        {...field}
                                    ></TextField>
                                )}
                            ></Controller>
                        </div>
                        <div className=''>
                            <SdSelect
                                form={form}
                                rules={{
                                    required: true,
                                }}
                                value={req?.icon || undefined}
                                items={TravelCategoryIcons}
                                sdChange={(item, value) => setReq({
                                    ...req,
                                    icon: value
                                })}
                            />
                        </div>
                        <div className='mb-16 d-flex align-items-center justify-content-center bg-op-gray py-10'>
                            <Image src={`/admin/icons/${req.icon}.svg`} alt="photo" width="24" height="34" />
                        </div>
                        <div className="w-100 mt-16">
                            <SdSelect
                                form={form}
                                value={req.isActivated}
                                items={{
                                    displayOnTrue: 'Sử dụng',
                                    displayOnFalse: 'Không sử dụng'
                                }}
                                sdChange={(item, value) => {
                                    setReq({
                                        ...req,
                                        isActivated: value
                                    })
                                }}
                            />
                        </div>

                    </div>
                </div>

            </form>

        </SdModal>
    )
}
export default ModalTravelCategory