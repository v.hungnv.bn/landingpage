import { SdButton, SdInput, SdModal, SdModalRef, SdSelect } from "@lib/core/components"
import { LoadingService, NotifyService } from "@lib/core/services";
import { LinkedCategoryDTO, LinkedDepartmentSaveReq } from "@lib/shared/360";
import { SaveOutlined } from "@mui/icons-material";
import { useState, useEffect, useRef } from 'react'
import { useForm } from "react-hook-form";
import { LinkedCategoryService, LinkedDepartmentService } from "../services";

function LinkedDepartmentDetail(props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) {
  const { id, open } = props;
  const sdModal = useRef<SdModalRef | null>(null);
  const [openModal, setOpenModal] = useState(false);
  const [req, setReq] = useState<LinkedDepartmentSaveReq>({
    isActivated: true
  });
  const [linkedCategories, setLinkedCategories] = useState<LinkedCategoryDTO[]>([]);

  const form = useForm();
  const {
    handleSubmit,
  } = form;

  useEffect(() => {
    loadLinkedCategories();
    setOpenModal(open);
  }, [open]);

  useEffect(() => {
    fetchDetail(id);
  }, [id]);

  const fetchDetail = async (id?: string) => {
    if (!id) {
      setReq({
        isActivated: true
      });
    } else {
      LoadingService.start();
      try {
        const linkedDepartment = await LinkedDepartmentService.detail(id);
        setReq(linkedDepartment);
      } catch (error) {
        console.log(error);
      } finally {
        LoadingService.stop();
      }
    }
  }

  const loadLinkedCategories = async () => {
    setLinkedCategories(await LinkedCategoryService.all())
  }

  const submitHandler = async () => {
    LoadingService.start();
    try {
      console.log(req);
      if (id) {
        await LinkedDepartmentService.update(id, req)
        NotifyService.success('Cập nhật Thành công!');
        sdModal?.current?.close();
        props.onAgree();
      } else {
        await LinkedDepartmentService.create(req)
        NotifyService.success('Tạo mới Thành công!');
        sdModal?.current?.close();
        props.onAgree();
      }
    } catch (error) {
      console.error(error)
    } finally {
      LoadingService.stop();
    }
  }

  return (
    <SdModal opened={openModal} ref={sdModal} width={'35rem'}
      footer={
        <><SdButton className="mr-12" label='Hủy' color="secondary" variant="text" onClick={() => props.onAgree()} />
        <SdButton icon={<SaveOutlined />} label='Lưu' onClick={handleSubmit(submitHandler)} /></>
      }
      title={id ? 'Cập nhật đơn vị' : 'Tạo mới đơn vị'} onClose={props.onClose}>
      <div className="d-flex flex-column align-items-center">
        <div className="my-10">
          <SdInput
            form={form}
            rules={{
              required: true
            }}
            label='Tên Đơn vị'
            value={req.name}
            sdChange={(value) => setReq({
              ...req,
              name: value
            })}
            sx={{ width: '30rem' }} />
        </div>
        <div style={{ width: '30rem' }} className="my-10">
          <SdSelect
            label="Danh mục"
            form={form}
            rules={{
              required: true
            }}
            value={req.linkedCategoryId}
            items={linkedCategories.map(e => ({
              value: e.id,
              display: e.name,
              data: e
            }))}
            sdChange={(item, value) => setReq({
              ...req,
              linkedCategoryId: value
            })}
          />
        </div>
        <div className="my-10">
          <SdInput
            form={form}
            label='Liên kết'
            value={req.link}
            sdChange={(value) => setReq({
              ...req,
              link: value
            })}
            sx={{ width: '30rem' }} />
        </div>
        <div style={{ width: '30rem' }} className="my-10">
          <SdSelect
            form={form}
            label="Trạng thái"
            value={req.isActivated}
            items={{
              displayOnTrue: 'Sử dụng',
              displayOnFalse: 'Không sử dụng'
            }}
            sdChange={(item, value) => {
              setReq({
                ...req,
                isActivated: value
              })
            }}
          />
        </div>
      </div>
    </SdModal>
  )
}

export default LinkedDepartmentDetail