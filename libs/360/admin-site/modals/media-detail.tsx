import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect, SdSelectOption, SdTreeViewOptionTransform } from "@lib/core/components";
import { Button, TextField } from "@mui/material";
import IconButton from '@mui/material/IconButton';
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import Image from 'next/image'
import Switch from '@mui/material/Switch';
import { cdn, LoadingService, NotifyService } from "@lib/core/services";
import { SelectCategory } from "../components/select-category";
import { TreeData } from "../services/data.model";
import { SdEditor } from "@lib/core/components/SdEditor/SdEditor";
import { MediaService, PostService } from "../services";
import { MediaSaveReq, MediaType, PostSaveReq } from "@lib/shared/360";

const host360 = process.env.NEXT_PUBLIC_360_HOST;

const ModalMedia = (props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) => {
  const sdModal = useRef<SdModalRef | null>(null);
  const { id, onClose, onAgree } = props;
  const [openModal, setOpenModal] = useState(false);
  const [isPublish, setIsPublish] = useState<string>('0');
  const [file, setFile] = useState<any>();
  const [image, setImage] = useState<any>({
    key: '',
  });
  const [allows, setAllows] = useState<{
    value: string;
    display: string;
    data: any
  }[]>([]);

  const {
    register,
    handleSubmit,
    control,
    setValue,
    watch,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    setAllows([
      {
        value: '1',
        display: 'Sử dụng',
        data: null,
      },
      {
        value: '0',
        display: 'Không sử dụng',
        data: null,
      }
    ])
    setOpenModal(props.open);

  }, [props.open]);

  useEffect(() => {
    if (props?.id) {
      getDetail(props?.id);
    }
    console.log('id', props?.id)
  }, [props.id]);

  useEffect(() => {
    // register("description", { minLength: 15 });


  }, [register]);


  const getDetail = async (id: string) => {
    LoadingService.start();
    try {
      const res = await MediaService.getMediaDetail(id);
      setValue('isPublished', res?.isPublished);
      setValue('url', res?.url);
      setValue('name', res?.name);

      setIsPublish(res?.isPublished ? '1' : '0');

      setFile(cdn(res?.url));

      console.log('res__', res)

    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }

  const editorContent = watch("description");

  const handlerCancel = () => {
    setOpenModal(false);
    props.onClose();
  }

  const handlerSave = () => {

  }

  const submitHandler = async (data: MediaSaveReq) => {
    console.log('data', data);

    LoadingService.start();
    try {
      const req: Partial<MediaSaveReq> = {
        type: MediaType.YOUTUBE,
        isPublished: isPublish === '1' ? true : false,
        url: data?.url || '',
        name: data?.name
      };
      if (props?.id) {
        const res = await MediaService.update(props?.id, req);
        NotifyService.success('Cập nhật Thành công!');
        sdModal?.current?.close();
        props.onAgree();
      } else {
        const res = await MediaService.create(req);
        NotifyService.success('Tạo mới Thành công!');
        sdModal?.current?.close();
        props.onAgree();
      }


    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }

  return (
    <SdModal ref={sdModal} opened={openModal} width={'489px'}
      footer={
        <><SdButton className="mr-12" label='Hủy' variant='text' color='secondary' onClick={handlerCancel} /> <SdButton className="mr-12" label='Lưu' onClick={handleSubmit(submitHandler)} /> </>
      }
      title={id ? 'Cập nhật link Youtube' : 'Tạo mới link Youtube'} onClose={props.onClose}>
      <form onSubmit={handleSubmit(submitHandler)} >
        <div className="row mt-16 mb-32">

          <div className="col-12">
          <div className="mb-30">
              <Controller
                name="name"
                control={control}
                defaultValue=""
                rules={{
                  required: true,
                  minLength: 6,
                }}
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="name"
                    label="Tên video"
                    size="small"
                    {...field}
                  ></TextField>
                )}
              ></Controller>
            </div>

            <div className="mb-30">
              <Controller
                name="url"
                control={control}
                defaultValue=""
                rules={{
                  required: true,
                  minLength: 6,
                }}
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="url"
                    label="Link youtube"
                    size="small"
                    {...field}
                  ></TextField>
                )}
              ></Controller>
            </div>

            <div className="w-100 mt-16">
              <SdSelect
                label="Trạng thái"
                value={isPublish}
                items={allows}
                sdChange={(item => {
                  setIsPublish(item?.value || '0')
                })}

              />
            </div>

          </div>
        </div>
        
      </form>

    </SdModal>
  )
}
export default ModalMedia