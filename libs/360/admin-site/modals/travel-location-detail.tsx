import { SdButton, SdInput, SdModal, SdModalRef, SdSelect } from "@lib/core/components"
import { cdn, LoadingService, NotifyService } from "@lib/core/services";
import { LinkedCategorySaveReq, MenuType, TravelCategoryDTO, TravelLocationSaveReq } from "@lib/shared/360";
import { SaveOutlined } from "@mui/icons-material";
import { TextField, Switch, IconButton } from "@mui/material";
import { useEffect, useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { MenuDTOExtend, PageDTOExtend, PageService, PostService, TravelCategoryService, TravelLocationService } from "../services";
import { LinkedCategoryService } from "../services/linked-category/linked-category.service";
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import GoogleMapSearch from "../components/autocomplate-search-map";

export const LinkTypes = [
    {
        value: 'LINK',
        display: 'Liên kết tự tạo',
    },
    {
        value: 'PAGE',
        display: 'Trang nội dung',
    },
]

function TravelLocationDetail(props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) {
    const { id, open, onAgree, onClose } = props
    const [openModal, setOpenModal] = useState(false);
    const sdModal = useRef<SdModalRef | null>(null);
    const [travelCategories, setTravelCategories] = useState<TravelCategoryDTO[]>([]);
    const [linkType, setLinkType] = useState<string>('LINK');
    const [pages, setPages] = useState<PageDTOExtend[]>([]);
    const [file, setFile] = useState<any>();
    const [icon, setIcon] = useState<string>();
    const [image, setImage] = useState<any>({
        key: '',
    });
    const [req, setReq] = useState<TravelLocationSaveReq>({
        isActivated: true
    });

    const [type, setType] = useState<string>('1')
    const [listType, setListType] = useState<{
        value: string;
        display: string;
    }[]>([])
    useEffect(() => {
        setOpenModal(open);
        fetchData();
    }, [open]);

    const form = useForm();
    const {
        handleSubmit,
    } = form;


    useEffect(() => {
        fetchDetail(id);
    }, [id]);

    const fetchData = async () => {
        setTravelCategories(await TravelCategoryService.all());
        const pags = await PageService.all();
        setPages(pags);
    }

    const fetchDetail = async (id?: string) => {
        if (!id) {
            setReq({
                isActivated: true
            });
        } else {
            LoadingService.start();
            try {
                const res = await TravelLocationService.detail(id);
                setReq(res);
                setFile(cdn(res?.image));
                const cat = travelCategories?.find(e => e.id === res.travelCategoryId);
                setIcon(cat?.icon);
            } catch (error) {
                console.log(error)
            } finally {
                LoadingService.stop();

            }
        }
    }

    const handleChangeFile = async (e: any) => {
        if (e?.target?.files[0]) {
            const file = e.target.files[0];
            if (!["image/png", "image/jpeg", "image/jpg"].some(item => item == file?.type)) {
                NotifyService.warn('file tải lên không đúng định dạng.', null);
                setFile(null);
                return;
            }
            if (file.size > 20000000) {
                NotifyService.warn('Dung lượng ảnh không được quá 20MB.', null);
                setFile(null);
                return;
            }
            setFile(URL.createObjectURL(file));
            LoadingService.start();
            try {
                const res = await PostService.upload(file);
                setImage({
                    ...image,
                    key: res.key
                });
                setReq({
                    ...req,
                    image: res.key
                })
                NotifyService.success('Thành công!');

            } catch (err) {
                console.log(err)

            } finally {
                LoadingService.stop();

            }
        }

    }

    const submitHandler = async (data: any) => {
        LoadingService.start();
        try {
            if (id) {
                await TravelLocationService.update(id, req)
                NotifyService.success('Cập nhật địa điểm Thành công!');
                sdModal?.current?.close();
                onAgree();
            } else {
                await TravelLocationService.create(req)
                NotifyService.success('Tạo thành công địa điểm!');
                sdModal?.current?.close();
                onAgree();
            }
        } catch (error) {
            console.log(error)
        } finally {
            LoadingService.stop();

        }
    }

    return (
        <SdModal opened={openModal} ref={sdModal} width={'1400px'}
            footer={
                <><SdButton className="mr-12" label='Hủy' color="secondary" variant="text" onClick={() => onAgree()} />
                    <SdButton icon={<SaveOutlined />} label='Lưu' onClick={handleSubmit(submitHandler)} /></>
            }
            title={'Thêm một địa điểm'} onClose={onClose}>
            <div className="font-semibold">Thông tin chi tiết về địa điểm</div>
            <div>Cung cấp một số thông tin về địa điểm này. Địa điểm này sẽ xuất hiện công khai trên bản đồ</div>
            <div className="row mt-16">
                <div className="col-3">

                    <div style={{ minHeight: '226px', borderRadius: '16px' }} className='box-shadow-2 d-flex align-items-center justify-content-center py-16'>
                        <div className='text-center'>
                            <IconButton color="primary" aria-label="upload picture" component="label">
                                <div className='thumb-upload-full'>
                                    <div className='thumb-upload-full__wrap'>
                                        {!file ? (
                                            <><AddAPhotoIcon /><div className='text-upload mt-4'>Upload Photo</div></>
                                        ) :
                                            <img className='w-100' src={file} alt="photo" />
                                        }
                                        <input type="file" hidden onChange={handleChangeFile} />

                                    </div>

                                </div>
                            </IconButton>
                            <div className='mt-16 text-center'>Cho phép *.jpeg, *.jpg, *.png.</div>
                            <div className='text-center'>Kích thước tối đa 20 MB</div>
                        </div>

                    </div>
                </div>
                <div className="col-9">
                    <div className="row">
                        <div className="col-6">
                            <div className="">
                                <SdInput
                                    className="w-100"
                                    form={form}
                                    rules={{
                                        required: true,
                                        minLength: 1,
                                    }}
                                    label='Tên địa điểm'
                                    value={req.name}
                                    sdChange={(value) => setReq({
                                        ...req,
                                        name: value
                                    })}
                                />
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="">
                                <SdInput
                                    className="w-100"
                                    form={form}
                                    label='Địa chỉ'
                                    value={req.address}
                                    sdChange={(value) => setReq({
                                        ...req,
                                        address: value
                                    })}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="">
                                <SdInput
                                    className="w-100"
                                    form={form}
                                    label='Số điện thoại'
                                    value={req.phone}
                                    sdChange={(value) => setReq({
                                        ...req,
                                        phone: value
                                    })}
                                />
                            </div>
                        </div>
                        <div className="col-6">
                            <SdSelect
                                label="Danh mục"
                                form={form}
                                rules={{
                                    required: true
                                }}
                                value={req.travelCategoryId}
                                items={travelCategories.map(e => ({
                                    value: e.id,
                                    display: e.name,
                                    data: e
                                }))}
                                sdChange={(item, value) => {
                                    setReq({
                                        ...req,
                                        travelCategoryId: value
                                    });
                                    setIcon(item?.data?.icon);
                                }}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="">
                                <SdSelect
                                    label="Loại hiển thị"
                                    form={form}
                                    rules={{
                                        required: true
                                    }}
                                    value={linkType}
                                    items={LinkTypes.map(e => ({
                                        value: e.value,
                                        display: e.display,
                                        data: e
                                    }))}
                                    sdChange={(item, value) => setLinkType(value || 'LINK')}
                                />
                            </div>
                        </div>
                        <div className="col-6">
                            {linkType === 'LINK' ? (
                                <SdInput
                                    className="w-100"
                                    form={form}
                                    rules={{
                                        required: true,
                                        minLength: 1,
                                    }}
                                    label='Link liên kết'
                                    value={req.link}
                                    sdChange={(value) => setReq({
                                        ...req,
                                        link: value
                                    })}
                                />
                            ) : (
                                <SdSelect
                                    label="Link liên kết"
                                    form={form}
                                    rules={{
                                        required: true
                                    }}
                                    value={req.pageId}
                                    items={pages.map(e => ({
                                        value: e.id,
                                        display: e.name,
                                        data: e
                                    }))}
                                    sdChange={(item, value) => setReq({
                                        ...req,
                                        pageId: value
                                    })}
                                />
                            )}

                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <div className="">
                                <SdInput
                                    className="w-100"
                                    form={form}
                                    rules={{
                                        required: true,
                                        minLength: 1,
                                    }}
                                    label='Nội dung'
                                    value={req.content}
                                    multiline
                                    rows={3}
                                    sdChange={(value) => setReq({
                                        ...req,
                                        content: value
                                    })}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="d-flex flex-column align-items-center">

                        <div className="w-100 mb-16">
                            <SdSelect
                                form={form}
                                label="Trạng thái"
                                value={req.isActivated}
                                items={{
                                    displayOnTrue: 'Sử dụng',
                                    displayOnFalse: 'Không sử dụng'
                                }}
                                sdChange={(item, value) => {
                                    setReq({
                                        ...req,
                                        isActivated: value
                                    })
                                }}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <SdInput
                                className="w-100"
                                form={form}
                                rules={{
                                    required: true,
                                    minLength: 1,
                                }}
                                label='Lat'
                                value={req.lat}
                                sdChange={(value) => setReq({
                                    ...req,
                                    lat: Number(value)
                                })}
                            />
                        </div>
                        <div className="col-6">
                            <SdInput
                                className="w-100"
                                form={form}
                                rules={{
                                    required: true,
                                    minLength: 1,
                                }}
                                label='Long'
                                value={req.lng}
                                sdChange={(value) => setReq({
                                    ...req,
                                    lng: Number(value)
                                })}
                            />
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <GoogleMapSearch icon={icon} lat={req.lat} lng={req.lng} onClick={(data?: any) => {
                    setReq({
                        ...req,
                        lat: data.lat,
                        lng: data.lng
                    })

                }} />
            </div>

        </SdModal>
    )
}

export default TravelLocationDetail