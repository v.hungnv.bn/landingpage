import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect } from "@lib/core/components";
import { TextField } from "@mui/material";
import { cdn, LoadingService, NotifyService } from "@lib/core/services";
import { MemberService, OrganizationDTOExtend, OrganizationService, PostService, PublishList } from "../services";
import { MemberSaveReq, OrganizationSaveReq } from "@lib/shared/360";
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import IconButton from '@mui/material/IconButton';
import Image from 'next/image'

const host360 = process.env.NEXT_PUBLIC_360_HOST;

const ModalMember = (props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) => {
    const sdModal = useRef<SdModalRef | null>(null);

    const [openModal, setOpenModal] = useState(false);
    const [isActivated, setIsActivated] = useState<string>('0');
    const [organizationId, setOrganizationId] = useState<string>('');
    const [organizations, setOrganizations] = useState<OrganizationDTOExtend[]>([]);

    const [title, setTitle] = useState<string>('Thông tin cá nhân  ');
    const [file, setFile] = useState<any>();
    const [image, setImage] = useState<any>({
        key: '',
    });
    const form = useForm();
    const {
        handleSubmit,
        register,
        control,
        setValue,
        watch,
        formState: { errors },
    } = form;

    // const {
    //     register,
    //     handleSubmit,
    //     control,
    //     setValue,
    //     watch,
    //     formState: { errors },
    // } = useForm();

    useEffect(() => {
        setOpenModal(props.open);
        getOrganizations();
    }, [props.open]);

    useEffect(() => {
        if (props?.id) {
            getDetail(props?.id);
        }
        console.log('id', props?.id)
    }, [props.id]);

    useEffect(() => {
        // register("description", { minLength: 15 });


    }, [register]);

    const getOrganizations = async () => {
        LoadingService.start();
        try {
            const res = await OrganizationService.all();
            res?.forEach(e => {
                e.value = e.id;
                e.display = e.name;
            });

            setOrganizations(res);
        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    const getDetail = async (id: string) => {
        LoadingService.start();
        try {
            const res = await MemberService.detail(id);
            setValue('name', res?.name);
            setValue('role', res?.role);
            setValue('email', res?.email);
            setValue('phone', res?.phone);
            setValue('description', res?.description);

            setIsActivated(res?.isActivated ? '1' : '0');
            setOrganizationId(res?.organizationId);
            setFile(cdn(res?.image));

        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    const handlerCancel = () => {
        setOpenModal(false);
        props.onClose();
    }

    const handlerSave = () => {

    }

    const handleChangeFile = async (e: any) => {
        if (e?.target?.files[0]) {
            const file = e.target.files[0];
            if (!["image/png", "image/jpeg", "image/jpg"].some(item => item == file?.type)) {
                NotifyService.warn('file tải lên không đúng định dạng.', null);
                setFile(null);
                return;
            }
            if (file.size > 3000000) {
                NotifyService.warn('Dung lượng ảnh không được quá 3MB.', null);
                setFile(null);
                return;
            }
            setFile(URL.createObjectURL(file));
            LoadingService.start();
            try {
                const res = await PostService.upload(file);
                setImage({
                    ...image,
                    key: res.key
                });

            } catch (err) {
                console.log(err)

            } finally {
                LoadingService.stop();

            }
        }

    }

    const submitHandler = async (data: any) => {
        console.log('data', data);

        LoadingService.start();
        try {
            const req: Partial<MemberSaveReq> = {
                name: data.name,
                role: data.role,
                email: data.email,
                phone: data.phone,
                description: data.description,
                isActivated: isActivated === '1' ? true : false,
                organizationId: organizationId,
                image: image?.key || undefined,
            };
            if (props?.id) {
                const res = await MemberService.update(props?.id, req);
                NotifyService.success('Cập nhật Thành công!');
                sdModal?.current?.close();
                props.onAgree();
            } else {
                const res = await MemberService.create(req);
                NotifyService.success('Tạo mới Thành công!');
                sdModal?.current?.close();
                props.onAgree();
            }


        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    return (
        <SdModal ref={sdModal} opened={openModal} width={'1480px'}
            footer={
                <><SdButton className="mr-12" label='Hủy' variant='text' color='secondary' onClick={handlerCancel} /><SdButton className="ml-12" label='Lưu' onClick={handleSubmit(submitHandler)} /></>
            }
            title={title} onClose={props.onClose}>
            <form onSubmit={handleSubmit(submitHandler)} >
                <div className="row mt-16 mb-32">
                    <div className='col-2'>
                        <div style={{ minHeight: '226px', borderRadius: '16px' }} className='box-shadow-2 d-flex align-items-center justify-content-center py-16'>
                            <div className='text-center'>
                                <IconButton color="primary" aria-label="upload picture" component="label">
                                    <div className='thumb-upload'>
                                        <div className='thumb-upload__wrap'>
                                            {!file ? (
                                                <><AddAPhotoIcon /><div className='text-upload mt-4'>Upload Photo</div></>
                                            ) :
                                                <Image src={file} alt="photo" width="128" height="128" />
                                            }
                                            <input type="file" hidden onChange={handleChangeFile} />

                                        </div>

                                    </div>
                                </IconButton>
                                <div className='mt-16 text-center'>Cho phép *.jpeg, *.jpg, *.png.</div>
                                <div className='text-center'>Kích thước tối đa 20 MB</div>
                            </div>

                        </div>
                    </div>

                    <div className="col-10">
                        <div className='row mb-16'>
                            <div className='col-6'>
                                <Controller
                                    name="name"
                                    control={control}
                                    defaultValue=""
                                    rules={{
                                        required: true,
                                    }}
                                    render={({ field }) => (
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            id="name"
                                            label="Họ và tên"
                                            size="small"
                                            error={Boolean(errors.name)}
                                            helperText={
                                                errors.name
                                                    ? 'Vui lòng điền họ tên'
                                                    : ''
                                            }
                                            {...field}
                                        ></TextField>
                                    )}
                                ></Controller>
                            </div>

                            <div className='col-6'>
                                <Controller
                                    name="role"
                                    control={control}
                                    defaultValue=""
                                    render={({ field }) => (
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            id="role"
                                            label="Chức vụ"
                                            size="small"
                                            {...field}
                                        ></TextField>
                                    )}
                                ></Controller>
                            </div>
                        </div>
                        <div className='row mb-16'>
                            <div className='col-6'>
                                <Controller
                                    name="phone"
                                    control={control}
                                    defaultValue=""
                                    render={({ field }) => (
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            id="phone"
                                            label="Điện thoại"
                                            size="small"
                                            {...field}
                                        ></TextField>
                                    )}
                                ></Controller>
                            </div>

                            <div className='col-6'>
                                <Controller
                                    name="email"
                                    control={control}
                                    defaultValue=""
                                    render={({ field }) => (
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            id="email"
                                            label="Email"
                                            size="small"
                                            {...field}
                                        ></TextField>
                                    )}
                                ></Controller>
                            </div>
                        </div>
                        <div className='row mb-16'>
                            <div className='col-6'>
                                {organizations.length > 0 && (
                                    <SdSelect
                                        form={form}
                                        label="Phòng ban"
                                        value={organizationId}
                                        items={organizations}
                                        rules={{
                                            required: true,
                                        }}
                                        sdChange={(item => {
                                            setOrganizationId(item?.value || '')
                                        })}

                                    />
                                )}

                            </div>

                            <div className='col-6'>
                                <div className="w-100 ">
                                    <SdSelect
                                        label="Trạng thái"
                                        value={isActivated}
                                        items={PublishList}
                                        sdChange={(item => {
                                            setIsActivated(item?.value || '0')
                                        })}

                                    />
                                </div>
                            </div>

                        </div>

                        <div className="mb-30">
                            <Controller
                                name="description"
                                control={control}
                                defaultValue=""
                                render={({ field }) => (
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        size="small"
                                        id="description"
                                        label="Vị trí, chức danh"
                                        multiline
                                        rows={4}
                                        {...field}
                                    />
                                )}
                            ></Controller>
                        </div>

                    </div>
                </div>

            </form>

        </SdModal>
    )
}
export default ModalMember