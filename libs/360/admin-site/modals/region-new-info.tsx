import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef } from "@lib/core/components";
import { Switch, TextField } from "@mui/material";
import IconButton from '@mui/material/IconButton';
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import Image from 'next/image'
import { LoadingService, NotifyService } from "@lib/core/services";
import { PostService } from "../services";
import { BannerType, RegionAdvertisement, RegionInfo } from "@lib/shared/360";
import * as uuid from 'uuid';

const host360 = process.env.NEXT_PUBLIC_360_HOST;


export type RegionInfoExtend = RegionInfo & {
    id?: string;
    uuid?: string;
}

const ModalRegionNewInfo = (props: { open: boolean, title?: string, onClose: () => void, onAgree: (data: RegionInfoExtend) => void, id?: string, data?: RegionInfoExtend }) => {
    const sdModal = useRef<SdModalRef | null>(null);
    const [openModal, setOpenModal] = useState(false);
    const [bannerType, setBannerType] = useState<string>(BannerType.IMAGE);
    const [file, setFile] = useState<any>();
    const [image, setImage] = useState<any>({
        key: '',
    });

    const { title } = props;
    const {
        register,
        handleSubmit,
        control,
        setValue,
        watch,
        formState: { errors },
    } = useForm();

    useEffect(() => {
        setOpenModal(props.open);
        if (props.data) {
            setValue('isActivated', props.data?.isActivated);
            setValue('title', props.data?.title);
            setValue('quote', props.data?.quote);
            setValue('link', props.data?.link);
            setFile(`${host360 + 'aws/' + props.data?.image}`);
        }
    }, [props.open]);

    const handlerClose = () => {
        setOpenModal(false);
        props.onClose();
    }

    const handlerSave = () => {

    }

    const handleChangeFile = async (e: any) => {
        if (e?.target?.files[0]) {
            const file = e.target.files[0];
            if (!["image/png", "image/jpeg", "image/jpg"].some(item => item == file?.type)) {
                NotifyService.warn('file tải lên không đúng định dạng.', null);
                setFile(null);
                return;
            }
            if (file.size > 200000000) {
                NotifyService.warn('Dung lượng ảnh không được quá 20MB.', null);
                setFile(null);
                return;
            }
            setFile(URL.createObjectURL(file));
            LoadingService.start();
            try {
                const res = await PostService.upload(file);
                setImage({
                    ...image,
                    key: res.key
                });

                NotifyService.success('Thành công!');

            } catch (err) {
                console.log(err)

            } finally {
                LoadingService.stop();

            }
        }

    }

    const submitHandler = async (data: RegionInfoExtend) => {
        if (!image?.key && !props.data?.image) {
            NotifyService.warn('Vui lòng thêm hình ảnh', null);
            return;
        }
        const req: Partial<RegionInfoExtend> = {
            title: data?.title,
            isActivated: data?.isActivated,
            quote: data?.quote,
            link: data?.link,
            image: image?.key || props.data?.image,
            uuid: props?.data?.uuid || uuid.v4()
        };
        sdModal?.current?.close();
        // console.log('data', data);

        props.onAgree(req);

    }

    return (
        <SdModal ref={sdModal} opened={openModal} width={'1200px'}
            footer={
                <><SdButton label='Hủy' variant="text" color='secondary' onClick={handlerClose}  /><SdButton className="ml-12" label={props.data?.id ? 'Cập nhật' : 'Lưu'} onClick={handleSubmit(submitHandler)} /></>
            }
            title={title} onClose={props.onClose}>
            <form onSubmit={handleSubmit(submitHandler)} >
                <div className="row mt-16 mb-32">

                    <div className="col-3">
                        <div style={{ minHeight: '226px', borderRadius: '16px' }} className='box-shadow-2 d-flex align-items-center justify-content-center py-16'>
                            <div className='text-center'>
                                <IconButton color="primary" aria-label="upload picture" component="label">
                                    <div className='thumb-upload'>
                                        <div className='thumb-upload__wrap'>
                                            {!file ? (
                                                <><AddAPhotoIcon /><div className='text-upload mt-4'>Upload Photo</div></>
                                            ) :
                                                <Image src={file} alt="photo" width="128" height="128" />
                                            }
                                            <input type="file" hidden onChange={handleChangeFile} />

                                        </div>

                                    </div>
                                </IconButton>
                                <div className='mt-16 text-center'>Cho phép *.jpeg, *.jpg, *.png.</div>
                                <div className='text-center'>Kích thước tối đa 20 MB</div>
                            </div>

                        </div>

                    </div>

                    <div className="col-9">
                        <div className="mb-30">
                            <Controller
                                name="title"
                                control={control}
                                defaultValue=""
                                rules={{
                                    required: true,
                                }}
                                render={({ field }) => (
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        id="title"
                                        label="Tiêu đề"
                                        size="small"
                                        error={Boolean(errors.title)}
                                        helperText={
                                            errors.title
                                                ? 'Vui lòng nhập tiêu đề'
                                                : ''
                                        }
                                        {...field}
                                    ></TextField>
                                )}
                            ></Controller>
                        </div>

                        <div className="mb-30">
                            <Controller
                                name="quote"
                                control={control}
                                defaultValue=""
                                render={({ field }) => (
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        id="quote"
                                        label="Trích dẫn"
                                        size="small"
                                        multiline
                                        rows={3}
                                        {...field}
                                    ></TextField>
                                )}
                            ></Controller>
                        </div>

                        <div className="mb-30">
                            <Controller
                                name="link"
                                control={control}
                                defaultValue=""
                                render={({ field }) => (
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        id="link"
                                        label="Đường dẫn liên kết"
                                        size="small"
                                        {...field}
                                    ></TextField>
                                )}
                            ></Controller>
                        </div>

                        <div className="d-flex align-items-center  my-20">
                            <div>Trạng thái</div>
                            <Controller
                                name="isActivated"
                                control={control}
                                defaultValue={true}
                                render={({ field }) => (
                                    <Switch inputRef={field.ref} checked={field.value} {...field} />
                                )}
                            ></Controller>
                        </div>


                    </div>
                </div>

            </form>

        </SdModal >
    )
}
export default ModalRegionNewInfo