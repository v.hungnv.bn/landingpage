import { SdButton, SdInput, SdModal, SdModalRef, SdSelect } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { useEffect, useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { QuestionGroupService } from "../services/question/question.service";
import TextField from "@mui/material/TextField/TextField";
import { QuestionGroupSaveReq } from "@lib/shared/360";
import { Stack } from "@mui/material";
import { SaveOutlined } from "@mui/icons-material";

function QuestionGroupDetail(props: {
  open: boolean;
  onClose: () => void;
  onAgree: () => void;
  id?: string;
}) {
  const { id, open } = props;
  const sdModal = useRef<SdModalRef | null>(null);
  const [openModal, setOpenModal] = useState(false);
  const [req, setReq] = useState<QuestionGroupSaveReq>({
    isActivated: true
  });
  const form = useForm();
  const {
    handleSubmit,
  } = form;

  useEffect(() => {
    if (open) {
      if (!id) {
        setReq({
          isActivated: true
        })
      } else {
        getDetail(id);
      }
    }
    setOpenModal(open);
  }, [open, id]);

  const getDetail = async (id: string) => {
    LoadingService.start();
    try {
      const questionGroup = await QuestionGroupService.detail(id);
      setReq(questionGroup);
    } catch (error) {
      console.error(error);
    } finally {
      LoadingService.stop();
    }
  };
  const submitHandler = async () => {
    LoadingService.start();
    try {
      if (id) {
        await QuestionGroupService.update(id, req);
        NotifyService.success("Cập nhật Thành công!");
        sdModal?.current?.close();
        props.onAgree();
      } else {
        await QuestionGroupService.create(req);
        NotifyService.success("Tạo mới Thành công!");
        sdModal?.current?.close();
        props.onAgree();
      }
    } catch (error) {
      console.log(error);
    } finally {
      LoadingService.stop();
    }
  };
  return (
    <SdModal
      opened={openModal}
      ref={sdModal}
      width={"400px"}
      footer={
        <Stack my={1} spacing={1} display={'flex'} direction={'row'}>
          <SdButton
            label="Hủy"
            variant="text"
            color="secondary"
            onClick={() => props?.onAgree?.()}
          />
          <SdButton icon={<SaveOutlined />} label="Lưu" onClick={handleSubmit(submitHandler)} />
        </Stack>
      }
      title={id ? 'Cập nhật lĩnh vực' : 'Tạo mới lĩnh vực'}
      onClose={props.onClose}
    >
      <Stack marginTop={2} display={'flex'} direction={'column'} spacing={2} justifyContent={'center'}>
        <SdInput
          form={form}
          label='Tên lĩnh vực'
          rules={{
            required: true
          }}
          value={req.name}
          sdChange={(value) => setReq({
            ...req,
            name: value
          })} />
        <SdSelect
          form={form}
          label='Trạng thái'
          value={req.isActivated}
          items={{
            displayOnTrue: 'Sử dụng',
            displayOnFalse: 'Không sử dụng'
          }}
          sdChange={(item, value) => {
            setReq({
              ...req,
              isActivated: value
            })
          }}
        />
      </Stack>
    </SdModal>
  );
}

export default QuestionGroupDetail;
