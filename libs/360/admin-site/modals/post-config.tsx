import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect, SdTreeViewOptionTransform } from "@lib/core/components";
import { LoadingService, NotifyService } from "@lib/core/services";
import { SelectCategory } from "../components/select-category";
import { ConfigurationService, PostService } from "../services";
import { PostConfigurationSaveReq, PostConfigurationType, PostConfigurationTypes } from "@lib/shared/360";
import { CategoryDTOExtend } from '../services/categories/categories.model';
import { CategoriesService } from '../services/categories/categories.service';

export interface PostConfigurationFormDTO {
  defaultCategoryId?: string;
  outstanding?: boolean;
  type?: string;
}

const ModalPostConfig = (props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) => {
  const sdModal1 = useRef<SdModalRef | null>(null);
  const { id, onClose, onAgree } = props;
  const [openPostConfig, setOpenPostConfig] = useState(false);
  const form = useForm();
  const [categories, setCategories] = useState<CategoryDTOExtend[]>([]);

  const [req, setReq] = useState<PostConfigurationFormDTO>({
    outstanding: true,
    type: PostConfigurationType.AUTO
  });

  const {
    register,
    handleSubmit,
    control,
    setValue,
    watch,
    formState: { errors },
  } = useForm();

  useEffect(() => {

    setOpenPostConfig(props.open);
    getAllCategory();
    getPostConfig();

  }, [props.open]);

  const getPostConfig = async () => {
    LoadingService.start();
    try {
      const res = await ConfigurationService.getPostConfiguration();
      if (res?.defaultCategoryId) {
        setReq({
          ...req,
          type: PostConfigurationType.AUTO,
          defaultCategoryId: res?.defaultCategoryId,
          outstanding: res?.outstanding,
        })
      } else {
        setReq({
          ...req,
          type: PostConfigurationType.MANUAL,
          outstanding: res?.outstanding,
        })
      }
      console.log('res', res)

    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }

  const getAllCategory = async () => {
    LoadingService.start();
    try {
      const res = await CategoriesService.all();
      res?.forEach(e => {
        e.value = e.id;
        e.display = e.name;
      })
      setCategories(res);

    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }


  const handlerCancel = () => {
    setOpenPostConfig(false)
  }


  const submitHandler = async () => {
    LoadingService.start();
    try {
      const reqDTO: Partial<PostConfigurationSaveReq> = {
        defaultCategoryId: req.type === PostConfigurationType.AUTO ? req.defaultCategoryId : undefined,
        outstanding: req.type === PostConfigurationType.AUTO ? req.outstanding : undefined, 
      };
      const res = await ConfigurationService.postConfiguration(reqDTO);
      NotifyService.success('Cấu hình Thành công!');
      sdModal1?.current?.close();
      props.onAgree();

    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }

  return (
    <SdModal ref={sdModal1} opened={openPostConfig} width={'400px'}
      footer={
        <><SdButton className="mr-12" label='Lưu' onClick={handleSubmit(submitHandler)} /></>
      }
      title={'Cấu hình đồng bộ bài viết'} onClose={props.onClose}>
      <div className="my-10">
        <SdSelect
          form={form}
          label="Dạng đồng bộ"
          value={req.type}
          items={PostConfigurationTypes}
          sdChange={(item, value) => {
            setReq({
              ...req,
              type: value
            })
          }}
        />
      </div>

      {req.type === PostConfigurationType.AUTO && (

        <><SdSelect
          form={form}
          label="Tên chuyên mục đơn vị"
          value={req.defaultCategoryId}
          items={categories}
          sdChange={(item, value) => {
            setReq({
              ...req,
              defaultCategoryId: value
            });
          }} /><div className="mb-10">
            <SdSelect
              form={form}
              label="Tin nổi bậc"
              value={req.outstanding}
              items={{
                displayOnTrue: 'Có',
                displayOnFalse: 'Không'
              }}
              sdChange={(item, value) => {
                setReq({
                  ...req,
                  outstanding: value
                });
              }} />
          </div></>
      )
      }



    </SdModal>
  )
}
export default ModalPostConfig