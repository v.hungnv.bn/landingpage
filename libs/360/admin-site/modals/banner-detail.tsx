import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect, SdSelectOption, SdTreeViewOptionTransform } from "@lib/core/components";
import { Button, Stack, TextField } from "@mui/material";
import IconButton from '@mui/material/IconButton';
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import Image from 'next/image'
import Switch from '@mui/material/Switch';
import { cdn, LoadingService, NotifyService } from "@lib/core/services";
import { SelectCategory } from "../components/select-category";
import { TreeData } from "../services/data.model";
import { SdEditor } from "@lib/core/components/SdEditor/SdEditor";
import { BannerService, PostService } from "../services";
import { BannerSaveReq, BannerType, BannerTypes, PostSaveReq } from "@lib/shared/360";
import { CategoriesService } from '../services/categories/categories.service';
import { SaveOutlined } from '@mui/icons-material';

const host360 = process.env.NEXT_PUBLIC_360_HOST;

const ModalBanner = (props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) => {
  const sdModal = useRef<SdModalRef | null>(null);
  const [openModal, setOpenModal] = useState(false);
  const [bannerType, setBannerType] = useState<string>(BannerType.IMAGE);
  const [file, setFile] = useState<any>();
  const [image, setImage] = useState<any>({
    key: '',
  });

  const {
    register,
    handleSubmit,
    control,
    setValue,
    watch,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    setOpenModal(props.open);
  }, [props.open]);

  useEffect(() => {
    if (props?.id) {
      getDetail(props?.id);
    }
  }, [props.id]);

  useEffect(() => {
    // register("description", { minLength: 15 });

  }, [register]);


  const getDetail = async (id: string) => {
    LoadingService.start();
    try {
      const res = await BannerService.getBannerDetail(id);
      setValue('name', res?.name);
      setValue('subTitle', res?.subTitle);

      setValue('link', res?.link);
      setValue('showButton', res?.showButton);
      setValue('isActivated', res?.isActivated);
      // setFile(`${host360 + 'aws/' + res?.image}`);

      // console.log('res__', res)

      if (res.type === BannerType.YOUTUBE) {
        setValue('url', res?.url);
      } else {
        setFile(cdn(res?.url));
      }

      setBannerType(res.type);

    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }

  const editorContent = watch("description");

  const handlerCancel = () => {
    setOpenModal(false)
    props.onClose();

  }

  const handlerSave = () => {
    
  }

  const handleChangeFile = async (e: any) => {
    if (e?.target?.files[0]) {
      const file = e.target.files[0];
      if (!["image/png", "image/jpeg", "image/jpg"].some(item => item == file?.type)) {
        NotifyService.warn('file tải lên không đúng định dạng.', null);
        setFile(null);
        return;
      }
      if (file.size > 20000000) {
        NotifyService.warn('Dung lượng ảnh không được quá 3MB.', null);
        setFile(null);
        return;
      }
      setFile(URL.createObjectURL(file));
      LoadingService.start();
      try {
        const res = await PostService.upload(file);
        setImage({
          ...image,
          key: res.key
        });

        NotifyService.success('Thành công!');

      } catch (err) {
        console.log(err)

      } finally {
        LoadingService.stop();

      }
    }

  }

  const submitHandler = async (data: BannerSaveReq) => {
    console.log('data', data);

    LoadingService.start();
    try {
      const req: Partial<BannerSaveReq> = {
        name: data?.name,
        subTitle: data?.subTitle,
        isActivated: data?.isActivated,
        showButton: data?.showButton,
        link: data?.link,
        type: BannerType[bannerType],
        url: BannerType[bannerType] === 'YOUTUBE' ? data.url : image?.key || undefined,

      };
      if (props?.id) {
        const res = await BannerService.update(props?.id, req);
        NotifyService.success('Cập nhật Thành công!');
        sdModal?.current?.close();
        props.onAgree();
      } else {
        const res = await BannerService.create(req);
        NotifyService.success('Tạo mới Thành công!');
        sdModal?.current?.close();
        props.onAgree();
      }


    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }

  return (
    <SdModal ref={sdModal} opened={openModal} width={'1200px'}
      footer={
        <Stack my={1} spacing={1} display={'flex'} direction={'row'}>
          <SdButton label='Hủy' variant="text" color='secondary' onClick={handlerCancel} />
          <SdButton icon={<SaveOutlined />} label='Lưu' onClick={handleSubmit(submitHandler)} />
        </Stack>
      }
      title={"Thiết lập Banner"} onClose={props.onClose}>
      <form onSubmit={handleSubmit(submitHandler)} >
        <div className="row mt-16 mb-32">

          <div className="col-3">
            <div className="mb-16">
              <SdSelect
                label="Kiểu Banner"
                value={bannerType}
                items={BannerTypes}
                sdChange={(item => {
                  setBannerType(item?.value || '')
                })}
              />
            </div>
            {bannerType === BannerType.YOUTUBE ? (
              <div className="mb-30">
                <Controller
                  name="url"
                  control={control}
                  defaultValue=""
                  render={({ field }) => (
                    <TextField
                      variant="outlined"
                      fullWidth
                      id="url"
                      label="Url youtube"
                      size="small"

                      {...field}
                    ></TextField>
                  )}
                ></Controller>
              </div>

            ) : (
              <div style={{ minHeight: '226px', borderRadius: '16px' }} className='box-shadow-2 d-flex align-items-center justify-content-center py-16'>
                <div className='text-center'>
                  <IconButton color="primary" aria-label="upload picture" component="label">
                    <div className='thumb-upload'>
                      <div className='thumb-upload__wrap'>
                        {!file ? (
                          <><AddAPhotoIcon /><div className='text-upload mt-4'>Upload Photo</div></>
                        ) :
                          <Image src={file} alt="photo" width="128" height="128" />
                        }
                        <input type="file" hidden onChange={handleChangeFile} />

                      </div>

                    </div>
                  </IconButton>
                  <div className='mt-16 text-center'>Cho phép *.jpeg, *.jpg, *.png.</div>
                  <div className='text-center'>Kích thước tối đa 20 MB</div>
                </div>

              </div>
            )}



          </div>

          <div className="col-9">
            <div className="mb-30">
              <Controller
                name="name"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="name"
                    label="Tên hiển thị"
                    size="small"
                    {...field}
                  ></TextField>
                )}
              ></Controller>
            </div>

            {/* <div className="mb-30">
              <Controller
                name="title"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="title"
                    label="Tiêu đề"
                    size="small"
                    {...field}
                  ></TextField>
                )}
              ></Controller>
            </div> */}

            <div className="mb-30">
              <Controller
                name="subTitle"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="subTitle"
                    label="Sub title"
                    size="small"
                    {...field}
                  ></TextField>
                )}
              ></Controller>
            </div>

            <div className="mb-30">
              <Controller
                name="link"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="link"
                    label="Đường dẫn liên kết"
                    size="small"
                    {...field}
                  ></TextField>
                )}
              ></Controller>
            </div>

            <div className="d-flex align-items-center  mt-30">
              <div>Hiển thị button</div>
              {/* <Switch name="isPublished" defaultChecked inputRef={register} /> */}

              <Controller
                name="showButton"
                control={control}
                defaultValue={false}
                render={({ field }) => (
                  <Switch inputRef={field.ref} checked={field.value} {...field} />
                )}
              ></Controller>

            </div>

            <div className="d-flex align-items-center  my-20">
              <div>Trạng thái</div>
              <Controller
                name="isActivated"
                control={control}
                defaultValue={false}
                render={({ field }) => (
                  <Switch inputRef={field.ref} checked={field.value} {...field} />
                )}
              ></Controller>
            </div>


          </div>
        </div>
        
      </form>

    </SdModal >
  )
}
export default ModalBanner