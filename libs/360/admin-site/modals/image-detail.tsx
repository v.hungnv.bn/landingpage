import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect, SdSelectOption, SdTreeViewOptionTransform } from "@lib/core/components";
import { Button, TextField } from "@mui/material";
import IconButton from '@mui/material/IconButton';
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import Image from 'next/image'
import Switch from '@mui/material/Switch';
import { cdn, LoadingService, NotifyService } from "@lib/core/services";
import { SelectCategory } from "../components/select-category";
import { TreeData } from "../services/data.model";
import { SdEditor } from "@lib/core/components/SdEditor/SdEditor";
import { MediaService, PostService } from "../services";
import { MediaSaveReq, MediaType, PostSaveReq } from "@lib/shared/360";

const host360 = process.env.NEXT_PUBLIC_360_HOST;

const ModalImage = (props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) => {
    const sdModal = useRef<SdModalRef | null>(null);

    const [openModal, setOpenModal] = useState(false);
    const [isPublish, setIsPublish] = useState<string>('0');
    const [file, setFile] = useState<any>();
    const [image, setImage] = useState<any>({
        key: '',
    });
    const [allows, setAllows] = useState<{
        value: string;
        display: string;
        data: any
    }[]>([]);

    const {
        register,
        handleSubmit,
        control,
        setValue,
        watch,
        formState: { errors },
    } = useForm();

    useEffect(() => {
        setAllows([
            {
                value: '1',
                display: 'Sử dụng',
                data: null,
            },
            {
                value: '0',
                display: 'Không sử dụng',
                data: null,
            }
        ])
        setOpenModal(props.open);

    }, [props.open]);

    useEffect(() => {
        if (props?.id) {
            getDetail(props?.id);
        }
        console.log('id', props?.id)
    }, [props.id]);

    useEffect(() => {
        // register("description", { minLength: 15 });


    }, [register]);


    const getDetail = async (id: string) => {
        LoadingService.start();
        try {
            const res = await MediaService.getMediaDetail(id);
            setValue('isPublished', res?.isPublished);
            setIsPublish(res?.isPublished ? '1' : '0');
            setFile(cdn(res?.url));

            console.log('res__', res)

        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    const editorContent = watch("description");

    const handlerCancel = () => {
        setOpenModal(false);
        props.onClose();
    }

    const handlerSave = () => {

    }

    const handleChangeFile = async (e: any) => {
        if (e?.target?.files[0]) {
            const file = e.target.files[0];
            if (!["image/png", "image/jpeg", "image/jpg"].some(item => item == file?.type)) {
                NotifyService.warn('file tải lên không đúng định dạng.', null);
                setFile(null);
                return;
            }
            if (file.size > 20000000) {
                NotifyService.warn('Dung lượng ảnh không được quá 20MB.', null);
                setFile(null);
                return;
            }
            setFile(URL.createObjectURL(file));
            LoadingService.start();
            try {
                const res = await PostService.upload(file);
                setImage({
                    ...image,
                    key: res.key
                });

                NotifyService.success('Thành công!');

            } catch (err) {
                console.log(err)

            } finally {
                LoadingService.stop();

            }
        }

    }

    const submitHandler = async (data: any) => {
        console.log('data', data);

        LoadingService.start();
        try {
            const req: Partial<MediaSaveReq> = {
                type: MediaType.IMAGE,
                isPublished: isPublish === '1' ? true : false,
                url: image?.key || undefined,

            };
            if (props?.id) {
                const res = await MediaService.update(props?.id, req);
                NotifyService.success('Cập nhật Thành công!');
                sdModal?.current?.close();
                props.onAgree();
            } else {
                const res = await MediaService.create(req);
                NotifyService.success('Tạo mới Thành công!');
                sdModal?.current?.close();
                props.onAgree();
            }


        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    return (
        <SdModal ref={sdModal} opened={openModal} width={'489px'}
            footer={
                <><SdButton className="mr-12" label='Hủy' variant='text' color='secondary' onClick={handlerCancel} /><SdButton className="mr-12" label='Lưu' onClick={handleSubmit(submitHandler)} /></>
            }
            title={"Thêm hình ảnh"} onClose={props.onClose}>
            <form onSubmit={handleSubmit(submitHandler)} >
                <div className="row mt-16 mb-32">

                    <div className="col-12">
                        <div style={{ minHeight: '226px', borderRadius: '16px' }} className='box-shadow-2 d-flex align-items-center justify-content-center py-16'>
                            <div className='text-center'>
                                <IconButton color="primary" aria-label="upload picture" component="label">
                                    <div className='thumb-upload-full'>
                                        <div className='thumb-upload-full__wrap'>
                                            {!file ? (
                                                <><AddAPhotoIcon /><div className='text-upload mt-4'>Upload Photo</div></>
                                            ) :
                                                <img className='w-100' src={file} alt="photo"  />
                                            }
                                            <input type="file" hidden onChange={handleChangeFile} />

                                        </div>

                                    </div>
                                </IconButton>
                                <div className='mt-16 text-center'>Cho phép *.jpeg, *.jpg, *.png.</div>
                                <div className='text-center'>Kích thước tối đa 20 MB</div>
                            </div>

                        </div>

                        <div className="w-100 mt-16">
                            <SdSelect
                                label="Trạng thái"
                                value={isPublish}
                                items={allows}
                                sdChange={(item => {
                                    setIsPublish(item?.value || '0')
                                })}

                            />
                        </div>

                    </div>
                </div>

            </form>

        </SdModal>
    )
}
export default ModalImage