import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { useForm } from "react-hook-form";

import { SdButton, SdInput, SdModal, SdModalRef, SdSelect } from "@lib/core/components";
import { Stack, TextField } from "@mui/material";
import { LoadingService, NotifyService } from "@lib/core/services";
import { OrganizationService, PublishList } from "../services";
import { OrganizationSaveReq } from "@lib/shared/360";
import { SaveOutlined } from '@mui/icons-material';

const ModalOrganization = (props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) => {
  const sdModal = useRef<SdModalRef | null>(null);
  const { id, open, onAgree, onClose } = props;
  const [openModal, setOpenModal] = useState(false);
  const [req, setReq] = useState<OrganizationSaveReq>({
    isActivated: true
  })
  const form = useForm();
  const {
    handleSubmit,
  } = form;

  useEffect(() => {
    if (open) {
      if (!id) {
        setReq({
          isActivated: true
        })
      } else {
        getDetail(id);
      }
    }
    setOpenModal(open);

  }, [open, id]);

  const getDetail = async (id: string) => {
    LoadingService.start();
    try {
      const entity = await OrganizationService.detail(id);
      setReq(entity);
    } catch (err) {
      console.log(err);
    } finally {
      LoadingService.stop();
    }
  }

  const submitHandler = async () => {
    LoadingService.start();
    try {
      if (id) {
        await OrganizationService.update(id, req);
        NotifyService.success('Cập nhật Thành công!');
        sdModal?.current?.close();
        props.onAgree();
      } else {
        await OrganizationService.create(req);
        NotifyService.success('Tạo mới Thành công!');
        sdModal?.current?.close();
        props.onAgree();
      }

    } catch (err) {
      console.log(err)

    } finally {
      LoadingService.stop();

    }
  }

  return (
    <SdModal ref={sdModal} opened={openModal} width={'400px'}
      footer={
        <Stack my={1} spacing={1} display={'flex'} direction={'row'}>
          <SdButton
            label="Hủy"
            variant="text"
            color="secondary"
            onClick={() => onAgree?.()}
          />
          <SdButton icon={<SaveOutlined />} label="Lưu" onClick={handleSubmit(submitHandler)} />
        </Stack>
      }
      title={id ? 'Cập nhật phòng ban' : 'Tạo mới phòng ban'} onClose={onClose}>
      <form onSubmit={handleSubmit(submitHandler)} >
        <Stack marginTop={2} display={'flex'} direction={'column'} spacing={2} justifyContent={'center'}>
          <SdInput
            form={form}
            label="Tên phòng ban"
            rules={{
              required: true
            }}
            value={req.name}
            sdChange={(value) => setReq({
              ...req,
              name: value
            })} />
          <SdSelect
            label="Trạng thái"
            form={form}
            value={req.isActivated}
            items={{
              displayOnTrue: 'Sử dụng',
              displayOnFalse: 'Không sử dụng'
            }}
            sdChange={(item, value) => {
              setReq({
                ...req,
                isActivated: value
              })
            }}
          />
        </Stack>
      </form>

    </SdModal>
  )
}
export default ModalOrganization