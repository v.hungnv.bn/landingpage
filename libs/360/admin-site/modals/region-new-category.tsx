import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect } from "@lib/core/components";
import { Switch } from "@mui/material";
import { NotifyService } from "@lib/core/services";
import { RegionCategoryExtend } from "../services";
import { CategoryDTOExtend } from '../services/categories/categories.model';
import * as uuid from 'uuid';
import { CategoriesService } from '../services/categories/categories.service';

export interface CatDTO {
  value?: string;
  display?: string;
}
const ModalRegionNewCategory = (props: { open: boolean, onClose: () => void, onAgree: (data: RegionCategoryExtend) => void, id?: string, data?: RegionCategoryExtend }) => {
  const sdModal = useRef<SdModalRef | null>(null);
  const [openModal, setOpenModal] = useState(false);
  const [categories, setCategories] = useState<CategoryDTOExtend[]>([]);
  const [cat, setCat] = useState<CatDTO>(
    {
      value: '',
      display: ''
    }
  );
  const form = useForm();
  // const {
  //   handleSubmit,
  // } = form;

  const {
    register,
    handleSubmit,
    control,
    setValue,
    watch,
    formState: { errors },
  } = useForm();

  console.log('props', props)
  useEffect(() => {
    setOpenModal(props.open);
    getAllCategory();
    if (props.data) {
      setValue('isActivated', props.data?.isActivated);
      setValue('id', props.data?.id);

      setCat({
        value: props.data?.id,
        display: props?.data?.name || ''
      });
    }
  }, [props.open]);


  useEffect(() => {

  }, []);

  const getAllCategory = async () => {
    CategoriesService.all().then(categories => {
      const updateCategories = categories.map((e) => {
        if (e?.level == 1) {
          return { ...e, name: `-${e?.name}` }
        } else if (e?.level == 2) {
          return { ...e, name: `--${e?.name}` }
        } else return { ...e }
      })
      setCategories(updateCategories);
    })
  }


  const handlerClose = () => {
    setOpenModal(false);
    props.onClose();

  }

  const handlerSave = () => {

  }


  const submitHandler = async (data) => {
    if (!cat.value) {
      NotifyService.warn('Vui lòng chọn chuyên mục!');

      return;
    }
    const param = {
      id: cat.value,
      isActivated: data?.isActivated,
      display: cat?.display,
      name: cat?.display,
      uuid: props?.data?.uuid || uuid.v4()
    }
    sdModal?.current?.close();
    props.onAgree(param);
    console.log('param', param);

  }

  return (
    <SdModal ref={sdModal} opened={openModal} width={'400px'}
      footer={
        <><SdButton label='Hủy' variant="text" color='secondary' onClick={handlerClose} /><SdButton className="ml-12" label={props.data?.id ? 'Cập nhật' : 'Thêm'} onClick={handleSubmit(submitHandler)} /></>
      }
      title={props.data?.id ? 'Cập nhật chuyên mục' : 'Tạo mới chuyên mục'} onClose={props.onClose}>
      <form onSubmit={handleSubmit(submitHandler)} >
        <div className="row mt-16 mb-32">

          <div className="col-12">
            <div className="mb-16">
              <SdSelect
                form={form}
                value={cat?.value}
                label="Chọn chuyên mục"
                rules={{
                  required: true
                }}
                items={categories.map(e => ({
                  value: e.id,
                  display: e.name,
                  data: e
                }))}
                sdChange={(item, value) => setCat({
                  ...cat,
                  value: item?.value || '',
                  display: item?.display || ''
                })}

              />

            </div>
            <div className="">
              <div>Trạng thái</div>
              <Controller
                name="isActivated"
                control={control}
                defaultValue={true}
                render={({ field }) => (
                  <Switch inputRef={field.ref} checked={field.value} {...field} />
                )}
              ></Controller>
            </div>

          </div>
        </div>

      </form>

    </SdModal>
  )
}
export default ModalRegionNewCategory