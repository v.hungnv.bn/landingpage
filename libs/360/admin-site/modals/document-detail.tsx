import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect } from "@lib/core/components";
import { TextField } from "@mui/material";
import IconButton from '@mui/material/IconButton';
import Image from 'next/image'
import { LoadingService, NotifyService } from "@lib/core/services";
import { DocumentGroupService, PostService, PublishList } from "../services";
import { DocumentGroupFor, DocumentSaveReq } from "@lib/shared/360";
import SdDate from '@lib/core/components/SdDate/SdDate';
import CloseIcon from '@mui/icons-material/Close';
import { DocumentService } from '../services/document/document.service';
const host360 = process.env.NEXT_PUBLIC_360_HOST;

const ModalDocument = (props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) => {
    const sdModal = useRef<SdModalRef | null>(null);
    const [publishDate, setPublishDate] = React.useState<string>('');
    const [toDate, setToDate] = React.useState<string>('');

    const [openModal, setOpenModal] = useState(false);
    const [isActivated, setisActivated] = useState<string>('0');
    const [file, setFile] = useState<any>();
    const [files, setFiles] = useState<{ name: string, url: string, size?: number }[]>([]);

    const [image, setImage] = useState<any>({
        key: '',
    });

    const [entity, setEntity] = useState<Partial<DocumentSaveReq>>();
    const [documentCategories, setDocumentCategories] = React.useState<any[]>([]);
    const [documentTypes, setDocumentTypes] = React.useState<any[]>([]);
    const [documentDepartments, setDocumentDepartments] = React.useState<any[]>([]);


    React.useEffect(() => {
        getAllDocumentGroup();
    }, []);

    const getAllDocumentGroup = async () => {
        LoadingService.start();

        try {
            const res = await DocumentGroupService.all();
            const cats = res?.filter(e => e.groupFor === DocumentGroupFor.CATEGORY && e.isActivated)?.map(e => {
                return {
                    ...e,
                    value: e.id,
                    display: e.name
                }
            });
            setDocumentCategories(cats);
            const types = res?.filter(e => e.groupFor === DocumentGroupFor.TYPE && e.isActivated)?.map(e => {
                return {
                    ...e,
                    value: e.id,
                    display: e.name
                }
            });
            setDocumentTypes(types);
            const departments = res?.filter(e => e.groupFor === DocumentGroupFor.DEPARTMENT && e.isActivated)?.map(e => {
                return {
                    ...e,
                    value: e.id,
                    display: e.name
                }
            });
            setDocumentDepartments(departments);

        } catch (err) {
            console.log(err)
        } finally {
            LoadingService.stop();
        }
    }
    const form = useForm();

    const {
        handleSubmit,
        register,
        control,
        setValue,
        watch,
        formState: { errors },
    } = form;

    // const {
    //     register,
    //     handleSubmit,
    //     control,
    //     setValue,
    //     watch,
    //     formState: { errors },
    // } = useForm();

    useEffect(() => {
        setOpenModal(props.open);
    }, [props.open]);

    useEffect(() => {
        if (props?.id) {
            getDetail(props?.id);
        }
    }, [props.id]);

    useEffect(() => {
        // register("description", { minLength: 15 });

    }, [register]);


    const getDetail = async (id: string) => {
        LoadingService.start();
        try {
            const res = await DocumentService.detail(id);
            setValue('title', res?.title);
            setValue('symbol', res?.symbol);
            setValue('signBy', res?.signBy);

            setValue('title', res?.title);
            setValue('title', res?.title);
            setValue('title', res?.title);


            setPublishDate(Date.toFormat(res?.publishDate, 'MM/dd/yyyy'));
            setisActivated(res?.isActivated ? '1' : '0');
            setFiles(res.files || []);

            setEntity({
                ...entity,
                documentCategoryId: res?.documentCategoryId,
                documentTypeId: res?.documentTypeId,
                documentDepartmentId: res?.documentDepartmentId
            })

        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    const handlerCancel = () => {
        setOpenModal(false)
    }

    const handlerRemoveDocument = (index: number) => {
        files.splice(index, 1);
        setFiles(
            [
                ...files,
            ]
        );
    }

    const getExtFile = (fileName: string) => {
        const lastDot = fileName.lastIndexOf('.');
        if (lastDot === -1) {
            throw new Error(`File không đúng định dạng`);
        }
        const ext = fileName.substring(lastDot + 1);
        return ext;
    }

    const handleChangeFile = async (e: any) => {
        if (e?.target?.files[0]) {
            const file = e.target.files[0];
            const ext = getExtFile(file?.name);


            console.log('file?.type', file);
            if (!["application/docx", "application/doc", "application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"].some(item => item == file?.type)) {
                NotifyService.warn('file tải lên không đúng định dạng pdf, doc, docx.', null);
                setFile(null);
                return;
            }
            if (file.size > 200000000) {
                NotifyService.warn('Dung lượng ảnh không được quá 20MB.', null);
                setFile(null);
                return;
            }
            setFile(URL.createObjectURL(file));
            LoadingService.start();
            try {
                const res = await PostService.upload(file);

                // setImage({
                //     ...image,
                //     key: res.key
                // });

                if (files?.length > 0) {
                    const filter = files.filter(e => getExtFile(e?.url) !== ext);
                    setFiles(
                        [
                            ...filter,
                            { url: res.key, name: file?.name || '', size: Math.round((file.size) / 1000) }
                        ]
                    );
                } else {
                    setFiles(
                        [
                            ...files,
                            { url: res.key, name: file?.name || '', size: Math.round((file.size) / 1000) }
                        ]
                    );
                }

                NotifyService.success('Thành công!');

            } catch (err) {
                console.log(err)

            } finally {
                LoadingService.stop();

            }
        }

    }

    const submitHandler = async (data: any) => {

        LoadingService.start();
        try {
            const req: Partial<DocumentSaveReq> = {
                title: data.title,
                symbol: data.symbol,
                signBy: data.signBy,
                documentCategoryId: entity?.documentCategoryId || '',
                documentTypeId: entity?.documentTypeId || '',
                documentDepartmentId: entity?.documentDepartmentId || '',
                isActivated: isActivated === '1' ? true : false,
                publishDate: new Date(publishDate),
                files: files

            };
            if (props?.id) {
                const res = await DocumentService.update(props?.id, req);
                NotifyService.success('Cập nhật Thành công!');
                sdModal?.current?.close();
                props.onAgree();
            } else {
                const res = await DocumentService.create(req);
                NotifyService.success('Tạo mới Thành công!');
                sdModal?.current?.close();
                props.onAgree();
            }


        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    return (
        <SdModal ref={sdModal} opened={openModal} width={'980px'}
            footer={
                <><SdButton className="mr-12" label='Lưu' onClick={handleSubmit(submitHandler)} /></>
            }
            title={"Thông tin chung"} onClose={props.onClose}>
            <form onSubmit={handleSubmit(submitHandler)} >
                <div className="row mt-16 mb-16">
                    <div className="col-12">
                        <div className="mb-16">
                            <Controller
                                name="title"
                                control={control}
                                defaultValue=""
                                rules={{
                                    required: true,
                                    minLength: 6,
                                }}
                                render={({ field }) => (
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        id="title"
                                        label="Tiêu đề"
                                        size="small"
                                        error={Boolean(errors.title)}
                                        helperText={
                                            errors.title
                                                ? 'Vui lòng nhập tiêu đề'
                                                : ''
                                        }
                                        {...field}
                                    ></TextField>
                                )}
                            ></Controller>
                        </div>

                        <div className='row mb-16'>
                            <div className="col-6">
                                <Controller
                                    name="symbol"
                                    control={control}
                                    defaultValue=""
                                    render={({ field }) => (
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            id="symbol"
                                            label="Số hiệu"
                                            size="small"

                                            {...field}
                                        ></TextField>
                                    )}
                                ></Controller>
                            </div>
                            <div className="col-6">
                                <SdSelect
                                    form={form}
                                    label="Danh mục văn bản"
                                    value={entity?.documentCategoryId || ''}
                                    items={documentCategories}
                                    rules={{
                                        required: true
                                    }}
                                    sdChange={(item => {
                                        setEntity({
                                            ...entity,
                                            documentCategoryId: item?.value
                                        })
                                    })}

                                />
                            </div>
                        </div>

                        <div className='row mb-16'>
                            <div className="col-6">
                                <SdSelect
                                    form={form}
                                    label="Loại văn bản"
                                    value={entity?.documentTypeId || ''}
                                    items={documentTypes}
                                    rules={{
                                        required: true
                                    }}
                                    sdChange={(item => {
                                        setEntity({
                                            ...entity,
                                            documentTypeId: item?.value
                                        })
                                    })}

                                />
                            </div>
                            <div className="col-6">
                                <SdSelect
                                    form={form}
                                    label="Cơ quan ban hành"
                                    value={entity?.documentDepartmentId || ''}
                                    items={documentDepartments}
                                    rules={{
                                        required: true
                                    }}
                                    sdChange={(item => {
                                        setEntity({
                                            ...entity,
                                            documentDepartmentId: item?.value
                                        })
                                    })}

                                />
                            </div>
                        </div>

                        <div className='row mb-16'>
                            <div className="col-6">
                                <SdDate
                                    label="Ngày ban hành"
                                    value={publishDate}
                                    required={false}
                                    sdChange={(item => {
                                        setPublishDate(Date.toFormat(item, 'MM/dd/yyyy'))
                                    })}

                                />
                            </div>
                            <div className="col-6">
                                <Controller
                                    name="signBy"
                                    control={control}
                                    defaultValue=""

                                    render={({ field }) => (
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            id="signBy"
                                            label="Người ký"
                                            size="small"

                                            {...field}
                                        ></TextField>
                                    )}
                                ></Controller>
                            </div>
                        </div>

                        <div className="w-100 mb-16">
                            <SdSelect
                                label="Trạng thái"
                                value={isActivated}
                                items={PublishList}
                                sdChange={(item => {
                                    setisActivated(item?.value || '0')
                                })}

                            />
                        </div>
                        <IconButton className='w-100 mb-20 rounded-8 border-dash-gray  py-40 mt-30 bg-gray-102' color="primary" aria-label="upload picture" component="label">
                            <div className='media align-items-center'>
                                <div className='mr-40'><Image src="/admin/select-file.png" alt="photo" width="216" height="160" /></div>
                                <div className='media-body'>
                                    <div className='text-20 font-bold mb-8 text-left'>Chọn File</div>
                                    <div className='text-14  mb-8 text-left'>Giới hạn dung lượng 20Mb</div>
                                </div>
                            </div>
                            <input type="file" hidden onChange={handleChangeFile} />

                        </IconButton>
                        <div>
                            {files?.map((document, index) => (
                                <div className='position-relative media align-items-center py-8 px-16 border-gray rounded-8 mb-12' key={index}>
                                    <div className='mr-16'>
                                        <Image src="/Vector.png" alt="photo" width="22" height="28" />
                                    </div>
                                    <div className='media-body'>
                                        <div className='text-14 font-semibold mb-2'>{document.name}</div>
                                        <div className='text-14'>{document.size} KB</div>
                                    </div>
                                    <IconButton className='position-absolute top-5 right-2' aria-label="delete" onClick={() => handlerRemoveDocument(index)}>
                                        <CloseIcon />
                                    </IconButton>
                                </div>
                            ))}
                        </div>

                    </div>
                </div>

            </form>

        </SdModal>
    )
}
export default ModalDocument
// border: 1px solid rgba(145, 158, 171, 0.32);