import React, { useState } from 'react';

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect, SdSelectOption, SdTreeViewOptionTransform } from "@lib/core/components";
import { Button, TextField } from "@mui/material";
import IconButton from '@mui/material/IconButton';
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import Image from 'next/image'
import Switch from '@mui/material/Switch';
import { LoadingService, NotifyService } from "@lib/core/services";
import { SelectCategory } from "../components/select-category";
import { TreeData } from "../services/data.model";
import { SdEditor } from "@lib/core/components/SdEditor/SdEditor";
import { MediaService, PostService, PublishList, ScheduleService } from "../services";
import { MediaSaveReq, MediaType, PostSaveReq, ScheduleSaveReq } from "@lib/shared/360";
import SdDate from '@lib/core/components/SdDate/SdDate';
import dayjs from 'dayjs';
import CloseIcon from '@mui/icons-material/Close';
const host360 = process.env.NEXT_PUBLIC_360_HOST;

const ModalSchedule = (props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) => {
    const sdModal = useRef<SdModalRef | null>(null);
    const [fromDate, setFromDate] = React.useState<string>('');
    const [toDate, setToDate] = React.useState<string>('');

    const [openModal, setOpenModal] = useState(false);
    const [isActivated, setisActivated] = useState<string>('0');
    const [file, setFile] = useState<any>();
    const [files, setFiles] = useState<any>();
    const [documents, setDocuments] = useState<{ name: string, url: string, size?: number }[]>([]);

    const [image, setImage] = useState<any>({
        key: '',
    });

    const {
        register,
        handleSubmit,
        control,
        setValue,
        watch,
        formState: { errors },
    } = useForm();

    useEffect(() => {
        setOpenModal(props.open);
    }, [props.open]);

    useEffect(() => {
        if (props?.id) {
            getDetail(props?.id);
        }
    }, [props.id]);

    useEffect(() => {
        // register("description", { minLength: 15 });

    }, [register]);


    const getDetail = async (id: string) => {
        LoadingService.start();
        try {
            const res = await ScheduleService.detail(id);
            setValue('name', res?.name);
            setFromDate(Date.toFormat(res?.fromDate, 'MM/dd/yyyy'));
            setToDate(Date.toFormat(res?.toDate, 'MM/dd/yyyy'));
            setisActivated(res?.isActivated ? '1' : '0');
            setDocuments(res.documents || []);

        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    const handlerCancel = () => {
        setOpenModal(false)
    }

    const handlerRemoveDocument = (index: number) => {
        documents.splice(index, 1);
        setDocuments(
            [
                ...documents,
            ]
        );
    }

    const handleChangeFile = async (e: any) => {
        if (e?.target?.files[0]) {
            const file = e.target.files[0];
            console.log('file', file)
            if (!["application/pdf"].some(item => item == file?.type)) {
                NotifyService.warn('file tải lên không đúng định dạng pdf', null);
                setFile(null);
                return;
            }
            if (file.size > 200000000) {
                NotifyService.warn('Dung lượng file không được quá 20MB.', null);
                setFile(null);
                return;
            }
            setFile(URL.createObjectURL(file));
            LoadingService.start();
            try {
                const res = await PostService.upload(file);

                // setImage({
                //     ...image,
                //     key: res.key
                // });

                console.log('documents', documents)
                const doc = { url: res.key, name: file?.name || '', size: Math.round((file.size) / 1000) };
                setDocuments([doc]);
                

                NotifyService.success('Thành công!');

            } catch (err) {
                console.log(err)

            } finally {
                LoadingService.stop();

            }
        }

    }

    const submitHandler = async (data: any) => {
        console.log('data', data);

        LoadingService.start();
        try {
            const req: Partial<ScheduleSaveReq> = {
                name: data.name,
                isActivated: isActivated === '1' ? true : false,
                fromDate: new Date(fromDate),
                toDate: new Date(toDate),
                documents: documents

            };
            if (props?.id) {
                const res = await ScheduleService.update(props?.id, req);
                NotifyService.success('Cập nhật Thành công!');
                sdModal?.current?.close();
                props.onAgree();
            } else {
                const res = await ScheduleService.create(req);
                NotifyService.success('Tạo mới Thành công!');
                sdModal?.current?.close();
                props.onAgree();
            }


        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    return (
        <SdModal ref={sdModal} opened={openModal} width={'980px'}
            footer={
                <><SdButton className="mr-12" label='Lưu' onClick={handleSubmit(submitHandler)} /></>
            }
            title={"Thông tin chung"} onClose={props.onClose}>
            <form onSubmit={handleSubmit(submitHandler)} >
                <div className="row mt-16 mb-16">
                    <div className="col-12">
                        <div className="mb-16">
                            <Controller
                                name="name"
                                control={control}
                                defaultValue=""
                                rules={{
                                    required: true,
                                    minLength: 6,
                                }}
                                render={({ field }) => (
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        id="name"
                                        label="Tiêu đề (*)"
                                        size="small"
                                        error={Boolean(errors.name)}
                                        helperText={
                                            errors.name
                                                ? errors.name.type === 'minLength'
                                                    ? 'Tiêu đề bài viết tối thiểu 5 ký tự'
                                                    : 'Tiêu đề bài viết bắt buộc'
                                                : ''
                                        }
                                        {...field}
                                    ></TextField>
                                )}
                            ></Controller>
                        </div>

                        <div className='row mb-16'>
                            <div className="col-6">
                                <SdDate
                                    label="Từ ngày"
                                    value={fromDate}
                                    required={false}
                                    sdChange={(item => {
                                        setFromDate(Date.toFormat(item, 'MM/dd/yyyy'))
                                    })}

                                />
                            </div>
                            <div className="col-6">
                                <SdDate
                                    label="Đến ngày"
                                    value={toDate}
                                    required={false}
                                    sdChange={(item => {
                                        setToDate(Date.toFormat(item, 'MM/dd/yyyy'))
                                    })}

                                />
                            </div>
                        </div>

                        <div className="w-100 mb-16">
                            <SdSelect
                                label="Trạng thái"
                                value={isActivated}
                                items={PublishList}
                                sdChange={(item => {
                                    setisActivated(item?.value || '0')
                                })}

                            />
                        </div>
                        <IconButton className='w-100 mb-20 rounded-8 border-dash-gray  py-40 mt-30 bg-gray-102' color="primary" aria-label="upload picture" component="label">
                            <div className='media align-items-center'>
                                <div className='mr-40'><Image src="/admin/select-file.png" alt="photo" width="216" height="160" /></div>
                                <div className='media-body'>
                                    <div className='text-20 font-bold mb-8 text-left'>Chọn File</div>
                                    <div className='text-14  mb-8 text-left'>Giới hạn dung lượng 20Mb</div>
                                </div>
                            </div>
                            <input type="file" hidden onChange={handleChangeFile} />

                        </IconButton>
                        <div>
                            {documents?.map((document, index) => (
                                <div className='position-relative media align-items-center py-8 px-16 border-gray rounded-8 mb-12' key={index}>
                                    <div className='mr-16'>
                                        <Image src="/Vector.png" alt="photo" width="22" height="28" />
                                    </div>
                                    <div className='media-body'>
                                        <div className='text-14 font-semibold mb-2'>{document.name}</div>
                                        <div className='text-14'>{document.size} KB</div>
                                    </div>
                                    <IconButton className='position-absolute top-5 right-2' aria-label="delete" onClick={() => handlerRemoveDocument(index)}>
                                        <CloseIcon />
                                    </IconButton>
                                </div>
                            ))}
                        </div>

                    </div>
                </div>
               
            </form>

        </SdModal>
    )
}
export default ModalSchedule
// border: 1px solid rgba(145, 158, 171, 0.32);