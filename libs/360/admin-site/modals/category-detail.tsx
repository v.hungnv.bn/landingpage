import { SdButton, SdInput, SdModal, SdModalRef, SdSelect } from "@lib/core/components";
import React, { useState, useEffect, useRef } from "react";
import { useForm } from "react-hook-form";
import { Stack, Switch } from "@mui/material";
import { CategoryDTO, CategorySaveReq, CategoryType, CategoryTypes } from "libs/shared/360";
import { LoadingService, NotifyService } from "@lib/core/services";
import { CategoriesService } from "../services/categories/categories.service";
import { SaveOutlined } from "@mui/icons-material";

const DEFAULT: CategorySaveReq = {
  type: CategoryType.STATIC,
  publicRss: false,
  synchronized: true
};

function CategoryDetail(props: {
  open: boolean;
  onClose: () => void;
  onAgree: () => void;
  id?: string;
}) {
  const { id, open, onClose, onAgree } = props;
  const sdModal = useRef<SdModalRef | null>(null);
  const [openModal, setOpenModal] = useState(false);
  const [categories, setCategories] = useState<CategoryDTO[]>([]);
  const [req, setReq] = useState<CategorySaveReq>({ ...DEFAULT });
  const [check, setCheck] = useState<boolean>(false);
  const form = useForm();
  const {
    handleSubmit,
  } = form;

  useEffect(() => {
    CategoriesService.all().then(categories => {
      setCategories(categories);
      setCheck(!check)
    })
  }, []);

  useEffect(() => {
    if (open) {
      if (!id) {
        setReq({ ...DEFAULT });
      } else {
        getDetail(id);
      }
    }
    setOpenModal(open);
  }, [open, id]);

  const getDetail = async (id: string) => {
    LoadingService.start();
    try {
      setReq(await CategoriesService.detail(id));
    } catch (error) {
      console.error(error);
    } finally {
      LoadingService.stop();
    }
  };

  useEffect(() => {
    const editCategories = categories.map((e) => {
      if (e?.level == 1) {
        return { ...e, name: `-${e?.name}` }
      } else if (e?.level == 2) {
        return { ...e, name: `--${e?.name}` }
      } else return { ...e }
    })
    setCategories(editCategories)
  }, [categories, check]);

  const submitHandler = async () => {
    LoadingService.start();
    try {
      if (id) {
        await CategoriesService.update(id, req);
        NotifyService.success("Cập nhật chuyên mục Thành công!");
        sdModal?.current?.close();
        onAgree();
      } else {
        await CategoriesService.create(req);
        NotifyService.success("Tạo mới chuyên mục Thành công!");
        sdModal?.current?.close();
        onAgree();
      }
    } catch (error) {
      console.log(error);
    } finally {
      LoadingService.stop();
    }
  };
  return (
    <SdModal
      opened={openModal}
      ref={sdModal}
      width={"50rem"}
      footer={
        <Stack my={1} spacing={1} display={'flex'} direction={'row'}>
          <SdButton
            label="Hủy"
            variant="text"
            color="secondary"
            onClick={() => onAgree()}
          />
          <SdButton icon={<SaveOutlined />} label="Lưu" onClick={handleSubmit(submitHandler)} />
        </Stack>
      }
      title={props?.id ? "Cập nhật chuyên mục" : "Tạo mới chuyên mục"}
      onClose={onClose}
    >
      <div className="d-flex justify-between align-items-center my-10">
        <div>Tên chuyên mục (*)</div>
        <SdInput
          form={form}
          rules={{
            required: true
          }}
          value={req.name}
          sdChange={(value) => setReq({
            ...req,
            name: value
          })}
          sx={{ width: '30rem' }} />
      </div>
      <div className="d-flex justify-between align-items-center my-10">
        <div>Chuyên mục cha</div>
        <SdSelect
          sx={{ width: '30rem' }}
          form={form}
          value={req.parentId}
          disabled={!!id}
          items={categories.map(e => ({
            value: e.id,
            display: e.name,
            data: e
          }))}
          sdChange={(item, value) => setReq({
            ...req,
            parentId: value
          })}
        />
      </div>

      <div className="d-flex justify-between align-items-center my-10">
        <div>Loại chuyên mục (*)</div>
        <div style={{ width: "30rem" }}>
          <SdSelect
            form={form}
            disabled={!!id}
            rules={{
              required: true,
            }}
            value={req.type}
            items={CategoryTypes}
            sdChange={(item, value) => setReq({
              ...req,
              type: value as CategoryType
            })}
          />
        </div>
      </div>
      {req.type === CategoryType.STATIC &&
        <div className="d-flex justify-between align-items-center my-10">
          <div>Công khai RSS</div>
          <SdSelect
            sx={{ width: '30rem' }}
            form={form}
            value={req.publicRss}
            items={{
              displayOnTrue: 'Có',
              displayOnFalse: 'Không'
            }}
            sdChange={(item, value) => {
              setReq({
                ...req,
                publicRss: value
              })
            }}
          />
        </div>
      }

      {req.type === CategoryType.RSS && (
        <>
          <div className="d-flex justify-between align-items-center my-10">
            <div>Link RSS</div>
            <SdInput
              form={form}
              rules={{
                required: true
              }}
              value={req.rss}
              sdChange={(value) => setReq({
                ...req,
                rss: value
              })}
              sx={{ width: '30rem' }} />
          </div>
          <div className="d-flex justify-between align-items-center my-10">
            <div>Tuần xuất lấy tin (phút)(*)</div>
            <SdInput
              form={form}
              type='number'
              rules={{
                required: true
              }}
              value={req.rssDelayMinute}
              sdChange={(value) => setReq({
                ...req,
                rssDelayMinute: +value || 0
              })}
              sx={{ width: '30rem' }} />
          </div>
        </>
      )}
      <div className="d-flex justify-between align-items-center my-10">
        <div>Trạng thái</div>
        <SdSelect
          sx={{ width: '30rem' }}
          form={form}
          value={req.synchronized}
          items={{
            displayOnTrue: 'Sử dụng',
            displayOnFalse: 'Không sử dụng'
          }}
          sdChange={(item, value) => {
            setReq({
              ...req,
              synchronized: value
            })
          }}
        />
      </div>
    </SdModal>
  );
}

export default CategoryDetail;
