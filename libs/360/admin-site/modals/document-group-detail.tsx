import React, { useState } from "react";

import { useEffect, useRef } from "react";
import { Controller, useForm } from "react-hook-form";

import { SdButton, SdModal, SdModalRef, SdSelect } from "@lib/core/components";
import { TextField } from "@mui/material";
import { LoadingService, NotifyService } from "@lib/core/services";
import { DocumentGroupService, PublishList } from "../services";
import { DocumentGroupFor, DocumentGroupSaveReq } from "@lib/shared/360";
import SaveIcon from '@mui/icons-material/Save';
const host360 = process.env.NEXT_PUBLIC_360_HOST;

const ModalDocumentGroup = (props: {
  open: boolean;
  onClose: () => void;
  onAgree: () => void;
  id?: string;
  groupFor: DocumentGroupFor;
}) => {
  const sdModal = useRef<SdModalRef | null>(null);
  const { groupFor } = props;
  const [openModal, setOpenModal] = useState(false);
  const [isActivated, setIsActivated] = useState<string>("1");
  const [title, setTitle] = useState<string>("Tạo mới danh mục văn bản");
  const [labelName, setLabelName] = useState<string>("Tên danh mục");

  const {
    register,
    handleSubmit,
    control,
    setValue,
    watch,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    setOpenModal(props.open);
    if (groupFor === DocumentGroupFor.CATEGORY) {
      setTitle("Tạo mới danh mục văn bản");
      setLabelName("Tên danh mục");
    }
    if (groupFor === DocumentGroupFor.DEPARTMENT) {
      setTitle("Cơ quan ban hành");
      setLabelName("Tên cơ quan ");
    }
    if (groupFor === DocumentGroupFor.TYPE) {
      setTitle("Tạo mới loại văn bản");

      setLabelName("Tên loại văn bản");
    }
  }, [props.open]);

  useEffect(() => {
    if (props?.id) {
      getDetail(props?.id);
    }
  }, [props.id]);

  useEffect(() => {
    // register("description", { minLength: 15 });
  }, [register]);

  const getDetail = async (id: string) => {
    LoadingService.start();
    try {
      const res = await DocumentGroupService.detail(id);
      setValue("name", res?.name);
      setIsActivated(res?.isActivated ? "1" : "0");

      console.log("res__", res);
    } catch (err) {
      console.log(err);
    } finally {
      LoadingService.stop();
    }
  };

  const handlerCancel = () => {
    setOpenModal(false);
    props.onClose();
  };

  const handlerSave = () => { };

  const submitHandler = async (data: any) => {
    console.log("data", data);

    LoadingService.start();
    try {
      const req: Partial<DocumentGroupSaveReq> = {
        name: data.name,
        isActivated: isActivated === "1" ? true : false,
        groupFor: groupFor,
      };
      if (props?.id) {
        const res = await DocumentGroupService.update(props?.id, req);
        NotifyService.success("Cập nhật Thành công!");
        sdModal?.current?.close();
        props.onAgree();
      } else {
        const res = await DocumentGroupService.create(req);
        NotifyService.success("Tạo mới Thành công!");
        sdModal?.current?.close();
        props.onAgree();
      }
    } catch (err) {
      console.log(err);
    } finally {
      LoadingService.stop();
    }
  };

  return (
    <SdModal
      ref={sdModal}
      opened={openModal}
      width={"400px"}
      footer={
        <>
          <SdButton
            className=""
            label="Hủy"
            variant={'text'}
            color={'secondary'}
            onClick={handlerCancel}
          />
          <SdButton
          icon={<SaveIcon />} 
            className="ml-12"
            label="Lưu"
            onClick={handleSubmit(submitHandler)}
          />
        </>
      }
      title={title}
      onClose={props.onClose}
    >
      <form onSubmit={handleSubmit(submitHandler)}>
        <div className="row mt-16 mb-32">
          <div className="col-12">
            <div className="mb-30">
              <Controller
                name="name"
                control={control}
                rules={{
                  required: true,
                  minLength: 1,
                }}
                defaultValue=""
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="name"
                    required
                    label={labelName}
                    error={Boolean(errors.name)}
                    helperText={errors.name ? "Tên danh mục bắt buộc" : ""}
                    size="small"
                    {...field}
                  ></TextField>
                )}
              ></Controller>
            </div>

            <div className="w-100 mt-16">
              <SdSelect
                label="Trạng thái"
                value={isActivated}
                items={PublishList}
                sdChange={(item) => {
                  setIsActivated(item?.value || "0");
                }}
              />
            </div>
          </div>
        </div>
        
      </form>
    </SdModal>
  );
};
export default ModalDocumentGroup;
