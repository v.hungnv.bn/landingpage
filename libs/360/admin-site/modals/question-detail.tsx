import { SdButton, SdInput, SdModal, SdModalRef, SdSelect } from "@lib/core/components"
import { LoadingService, NotifyService } from "@lib/core/services";
import { QuestionSaveReq } from "@lib/shared/360";
import { useEffect, useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { QuestionService } from "../services/question/question.service";
import { SdEditor } from "@lib/core/components/SdEditor/SdEditor";
import { SaveOutlined } from "@mui/icons-material";


function QuestionDetail(props: { open: boolean, onClose: () => void, onAgree: () => void, id?: string }) {
    const { id, open, onAgree, onClose } = props
    const sdModal = useRef<SdModalRef | null>(null);
    const [openModal, setOpenModal] = useState(false);
    const [req, setReq] = useState<QuestionSaveReq>({
        isActivated: true
    });

    useEffect(() => {
        setOpenModal(open);
    }, [open]);

    useEffect(() => {
        fetchDetail(id);
    }, [id]);

    const fetchDetail = async (id?: string) => {
        if (!id) {
            setReq({
                isActivated: true
            });
        } else {
            LoadingService.start();
            try {
                const res = await QuestionService.getQuestionDetail(id);
                setReq(res);
                setValue('anwser', res?.anwser);

            } catch (error) {
                console.log(error)
            } finally {
                LoadingService.stop();

            }
        }

    }

    const submitHandler = async (data?: QuestionSaveReq) => {
        LoadingService.start();
        try {
            req.anwser = data?.anwser;
            if (id) {
                await QuestionService.update(id, req)
                NotifyService.success('Cập nhật Thành công!');
                sdModal?.current?.close();
                onAgree();
            } else {
                await QuestionService.create(req)
                NotifyService.success('Tạo mới Thành công!');
                sdModal?.current?.close();
                onAgree();
            }
        } catch (error) {
            console.log(error)
        } finally {

            LoadingService.stop();

        }
    }

    const form = useForm();


    const {
        handleSubmit,
        register,
        control,
        setValue,
        watch,
        formState: { errors },
    } = form;
    // useEffect(() => {
    //     register("anwser", { minLength: 5, required: true });


    // }, [register]);
    // const editorContent = watch("anwser");

    return (
        <SdModal opened={openModal} ref={sdModal} width={'1240px'}
            footer={
                <>
                    <div>
                        <SdButton className="mr-12" label='Hủy' color="secondary" variant="text" onClick={() => onAgree()} />
                        <SdButton icon={<SaveOutlined />} label='Lưu' onClick={handleSubmit(submitHandler)} />
                    </div>
                </>
            }
            title={'Thông tin câu hỏi'} onClose={onClose}>

            <form onSubmit={handleSubmit(submitHandler)} >
                <div className="row mb-16 pt-20">
                    <div className="col-12">
                        <SdInput
                            form={form}
                            rules={{
                                required: true
                            }}
                            disabled={id ? true : false}
                            label='Tiêu đề câu hỏi'
                            value={req.title}
                            sdChange={(value) => setReq({
                                ...req,
                                title: value
                            })}
                            sx={{ width: '100%' }} />
                    </div>
                </div>
                <div className="row mb-16">
                    <div className="col-6">
                        <SdInput
                            form={form}
                            rules={{
                                required: true
                            }}
                            disabled={id ? true : false}
                            label='Họ và tên'
                            value={req.fullName}
                            sdChange={(value) => setReq({
                                ...req,
                                fullName: value
                            })}
                            sx={{ width: '30rem' }} />
                    </div>
                    <div className="col-6">
                        <SdInput
                            form={form}
                            rules={{
                                required: true
                            }}
                            disabled={!!id}
                            label='Địa chỉ'
                            value={req.address}
                            sdChange={(value) => setReq({
                                ...req,
                                address: value
                            })}
                            sx={{ width: '30rem' }} />
                    </div>
                </div>
                <div className="row mb-16">
                    <div className="col-6">
                        <SdInput
                            form={form}
                            disabled={!!id}
                            label='Số điện thoại'
                            value={req.phone}
                            sdChange={(value) => setReq({
                                ...req,
                                phone: value
                            })}
                            sx={{ width: '30rem' }} />
                    </div>
                    <div className="col-6">
                        <SdInput
                            form={form}
                            disabled={id ? true : false}
                            label='Email'
                            value={req.email}
                            sdChange={(value) => setReq({
                                ...req,
                                email: value
                            })}
                            sx={{ width: '30rem' }} />
                    </div>
                </div>

                <div className="row mb-16">
                    <div className="col-12">
                        <SdInput
                            form={form}
                            rules={{
                                required: true
                            }}
                            multiline
                            disabled
                            rows={4}
                            label='Nội dung'
                            value={req.content}
                            sdChange={(value) => setReq({
                                ...req,
                                content: value
                            })}
                            sx={{ width: '100%' }} />
                    </div>
                </div>
                <div className="row mb-16">
                    <div className="col-12">
                        <div className="font-bold text-18 mb-6">Thông tin câu trả lời</div>
                        {/* <SdEditor
                            value={req.anwser}
                            placeholder={"Trả lời..."}

                            onChange={(value) => setReq({
                                ...req,
                                anwser: value
                            })}
                        /> */}

                        <Controller
                            name="anwser"
                            control={control}
                            rules={{
                                required: true,
                                minLength: 12
                            }}
                            render={({ field }) => {
                                return (
                                    <div>
                                        <SdEditor
                                            {...field}
                                            placeholder={"Trả lời..."}
                                            onChange={(text) => {
                                                field.onChange(text);
                                            }}
                                        />
                                        {Boolean(errors.anwser) && (
                                            <p className="MuiFormHelperText-root Mui-error MuiFormHelperText-sizeSmall MuiFormHelperText-contained MuiFormHelperText-filled css-k4qjio-MuiFormHelperText-root">
                                                {errors?.anwser?.type === 'required' && 'Dữ liệu không được để trống!'}
                                                {errors?.anwser?.type === 'minLength' && 'Tối thiểu 5 ký tự!'}
                                            </p>

                                        )}
                                    </div>
                                )
                            }}
                        />

                        {/* <SdEditor
                            placeholder={'Nội dung...'}
                            value={editorContent}
                            onChange={(html) => setValue("anwser1", html)}
                        /> */}

                    </div>
                </div>
                <div className="row mb-16">
                    <div className="col-4">
                        <div className="w-100">
                            <SdSelect
                                form={form}
                                label="Trạng thái"
                                value={req.isActivated}
                                items={{
                                    displayOnTrue: 'Đã duyệt',
                                    displayOnFalse: 'Chưa duyệt'
                                }}
                                sdChange={(item, value) => {
                                    setReq({
                                        ...req,
                                        isActivated: value
                                    })
                                }}
                            />
                        </div>
                    </div>
                </div>
            </form>
        </SdModal>
    )
}

export default QuestionDetail