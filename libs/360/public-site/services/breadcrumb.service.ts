import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

export interface BreadCrumbDTO  {
    title?: string;
    subTitle?: string;
    slug?: string;
}

export class BreadCrumbService {
  static alertSubject = new Subject();

  static onLoader = () => {
    return BreadCrumbService.alertSubject.asObservable();
  }

  static info = (info: BreadCrumbDTO) => {
    BreadCrumbService.alertSubject.next(info);
  }

  static title = (title: string) => {
    BreadCrumbService.alertSubject.next(title);
  }
 
  static stop = () => {
    BreadCrumbService.alertSubject.next(false);
  }
}