export const fakeCategories = [
    {
        Id: "a",
        Slug: "tin-tuc-su-kien",
        Name: "Tin tức - sự kiện",
        posts: [0, 1]
    },
    {
        Id: "g",
        Slug: "gold",
        Name: "Gold",
        posts: [2]
    }
];

export interface BlogDTO {
    Id: string;
    Slug: string;
    Title: string;
    Text: string;
    category: any;
  }

export interface CategoryDTO {
    Id: string;
    Slug: string;
    Name: string;
    posts: BlogDTO[]
  }

export const fakePosts = [
    {
        Id: 0,
        Slug: "khai-mac-hoi-thao",
        Title: "Khai mạc Hội thao Người giáo viên nhân dân toàn quốc",
        Text: "People talk loud when they wanna act smart, right?",
        category: "a"
    },
    {
        Id: 1,
        Slug: "another-post",
        Title: "Another Post",
        Text: "It's not just a boulder... It's a rock!",
        category: "a"
    },
    {
        Id: 2,
        Slug: "the-newest-post",
        Title: "The Newest Post",
        Text: "Can't have dirty garbage!",
        category: "g"
    }
];

export function getDenormalizedPosts() {
    return fakePosts.map((post) => {
        return {
            ...post,
            category: fakeCategories.find((c) => c.Id === post.category)
        };
    });
}

export function getDenormalizedCategories() {
    return fakeCategories.map((category) => {
        return {
            ...category,
            posts: category.posts.map((post) => fakePosts.find((p) => p.Id === post))
        };
    });
}
