import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Breadcrumb } from "../components/breadcrumb";
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';
import { PostService } from "@lib/360/admin-site/services";
import { CategoryDTO, ConfigurationTemplate, MenuType, PostDTO, PostPublicFindReq } from "@lib/shared/360";
import { ListPostLayout2 } from "../components/list-post-layout-2";
import { ListPostStaticLayout1 } from "../components/list-post-static";
import { Pagination, Stack } from "@mui/material";
import CategoryLayout from "../components/category-layout";
import { BreadCrumbService } from "../services/breadcrumb.service";

const host360 = process.env.NEXT_PUBLIC_360_HOST;


export default function AllCateNews() {
    const [categories, setCategories] = useState<CategoryDTO[]>([]);
    const [posts, setPosts] = useState<PostDTO[]>([]);

    const [total, setTotal] = useState<number>(0);

    const [filter, setFilter] = useState<PostPublicFindReq>({
        pageNumber: 0,
        pageSize: 10,
    });

    const router = useRouter();
    const { slug } = router.query;

    const template = localStorage.getItem('TEMPLATE');

    useEffect(() => {
        BreadCrumbService.info({
            title: 'Tin tức sự kiện',
            subTitle: 'Tin tức sự kiện',
            slug: '/tin-tuc-su-kien'
        });

        getPosts();

    }, [filter]);

    const getPosts = async (catId?: string) => {
        try {
            const res = await PostService.findPublish(filter);
            setPosts(res?.items);
            setTotal(res?.total);
        } catch (err) {
            console.log(err)
        } finally {
        }
    }

    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setFilter({
            ...filter,
            pageNumber: value - 1
        });
    };

    const renderTreeItem = (treeItems) => {
        if (!treeItems?.length) {
            return null;
        }
        return treeItems.map(treeItem => {
            return (

                <Link className="pointer" href={`/tin-tuc-su-kien/${treeItem.slug}`}>
                    <TreeItem
                        key={treeItem.id}
                        nodeId={treeItem.id}
                        label={treeItem.name}>

                        {
                            renderTreeItem(treeItem.children)
                        }

                    </TreeItem>
                </Link>
            )
        })
    }

    return (
        <div>
            {template === ConfigurationTemplate.TEMPLATE2 && (
                <div><ListPostLayout2 articles={posts} /></div>
            )}
            {template === ConfigurationTemplate.TEMPLATE1 && (
                <ListPostStaticLayout1 articles={posts} />
            )}

            {total > 0 && (
                <div className="d-flex justify-content-end">
                    <Stack spacing={2}>
                        <Pagination count={Math.ceil(total / filter.pageSize)} color="primary" page={filter.pageNumber + 1} onChange={handleChange} />
                    </Stack>
                </div>
            )}
        </div>

    );
}




