import Link from "next/link";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import { useEffect, useState } from "react";
import { Breadcrumb } from "../components/breadcrumb";
import { BlogDTO, fakeCategories, fakePosts, getDenormalizedCategories } from "../services/datafake.model";
import Image from "next/image";
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';
import { SdInput, SdSelect, SdTreeView, SdTreeViewOptionTransform } from "@lib/core/components";
import { PostService } from "@lib/360/admin-site/services";
import { ApiService, cdn } from "@lib/core/services";
import { CategoryDTO, PostDTO } from "@lib/shared/360";
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

const host360 = process.env.NEXT_PUBLIC_360_HOST;


export default function Category() {
    const [categories, setCategories] = useState<CategoryDTO[]>([]);
    const [posts, setPosts] = useState<PostDTO[]>([]);
    const [items, setItems] = useState<PostDTO[]>([]);

    const [category, setCategory] = useState<CategoryDTO>();
    const [curentPage, setCurrenPage] = useState<number>(1);
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(5);

    const router = useRouter();
    const { slug } = router.query;

    console.log('slug', slug);
    useEffect(() => {
        setPage(1);
        if (typeof slug === 'string') {
            if (slug !== 'tin-tuc-su-kien') {
                getCategoryDetail(slug);
            } else {
                getPostByCategory();
            }

        }
    }, [slug]);

    useEffect(() => {
        if (ApiService.departmentCode) {
            getAllCategory();
        }
    }, [slug, ApiService.departmentCode]);

    const getCategoryDetail = async (slug?: string) => {
        try {
            const cat = await PostService.getCategory(slug || '');
            setCategory(cat);
            getPostByCategory(cat?.id);
        } catch (err) {
            console.log(err);
            getListPost();
        } finally {
        }
    }

    const getAllCategory = async () => {
        try {
            const res = await PostService.getAllCategory();
            setCategories(res);

        } catch (err) {
            console.log(err)
        } finally {

        }
    }

    const getPostByCategory = async (catId?: string) => {
        try {
            const res = await PostService.allPostPublish(catId || '');
            console.log('res__', res)
            setPosts(res);
        } catch (err) {
            console.log(err)
        } finally {
        }
    }

    const getListPost = async () => {
        try {
            const req = {
                pageSize: 10,
                pageNumber: 0
            }
            const res = await PostService.find(req);
            setPosts(res.items || []);

            console.log('res', res)

        } catch (err) {
            console.log(err)
        } finally {
        }
    }

    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        console.log('value', value)

        setPage(value);

    };

    const renderTreeItem = (treeItems) => {
        if (!treeItems?.length) {
            return null;
        }
        return treeItems.map(treeItem => {
            return (
                <Link className="pointer" href={treeItem.slug}>
                    <TreeItem
                        key={treeItem.id}
                        nodeId={treeItem.id}
                        label={treeItem.name}>
                        {
                            renderTreeItem(treeItem.children)
                        }

                    </TreeItem>
                </Link>
            )
        })
    }

    return (

        <div >
            {/* <Breadcrumb title={category?.name} /> */}

            <main className="container-fluid py-30">
                <div className="row">
                    <div className="col-3">
                        <div className="category-tree-component mt-8">
                            <div className="category-title">Danh mục </div>
                            <TreeView
                                aria-label="file system navigator"
                                defaultCollapseIcon={<ExpandMoreIcon />}
                                defaultExpandIcon={<ChevronRightIcon />}
                            >
                                {renderTreeItem(categories)}

                            </TreeView>
                        </div>

                    </div>
                    <div className="col-9">
                        {/* <h1>{category?.Name}</h1> */}
                        {posts.length > 0 && (
                            <>
                                <div className="mt-8">
                                    {posts?.map((post, index) => {
                                        if (((index + 1) <= (pageSize * page)) && ((index + 1) > (pageSize * page - pageSize))) {
                                            return (
                                                <Link className="pointer" key={post.id} href={`/bai-viet/${post.slug}`}>
                                                    <div className="media shadow-f1 mb-16 rounded-8 pointer">
                                                        <div className="entry-thumb mr-16">
                                                            <Image src={cdn(post.image) || '/front/no-image.png'} alt="photo" width="243" height="156" />
                                                        </div>
                                                        <div className="media-body entry-content">
                                                            <div className="text-18 font-bold mb-10">{post.name}</div>
                                                            <div className="text-14 text-gray-101 mb-10">{Date.toFormat(post.isPublished, 'dd/MM/yyyy')}</div>
                                                            <div className="text-14 mb-20">{post.quote}</div>
                                                            <div className="entry-author media align-items-center">
                                                                <div className="entry-avatar mr-6">
                                                                    <Image
                                                                        src={post?.creatorAvatar || "/front/entry/avatar.png"}
                                                                        alt="logo" width="26" height="26" />
                                                                </div>
                                                                <div className="text-13 font-bold media-body">{post?.creator}</div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </Link>
                                            )
                                        }

                                    })}


                                </div>
                                <div className="d-flex justify-content-end">
                                    <Stack spacing={2}>
                                        <Pagination count={Math.ceil(posts.length / pageSize)} color="primary" page={page} onChange={handleChange} />
                                    </Stack>
                                </div>
                            </>
                        )}
                    </div>
                </div>

            </main>

        </div>
    );
}




