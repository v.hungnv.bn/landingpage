


import Head from 'next/head'
import Image from 'next/image'
import { HomeSlider } from '../components/home-slider'

import styles from '../styles/Home.module.css'
import { useEffect, useRef, useState } from "react";
import { BlogDTO, CategoryDTO, fakeCategories, fakePosts } from '../services/datafake.model';
import Link from 'next/link'
import { GetStaticProps, InferGetStaticPropsType } from 'next';
import { FeaturedNew } from '../components/featured-new'
import { OurService } from '../components/our-service'
import { NewTabs } from '../components/new-tabs'
import { SwiperGalleryAdvertisement } from '../components/swiper-gallery'
import { TabNewsSchool } from '../components/tab-news-school'
import { SwiperGalleryPhoto } from '../components/swiper-gallery-photo'
import { SwiperGalleryVideo } from '../components/swiper-gallery-video'
import { TabDepartmentLink } from '../components/tab-department-link'
import { ApiService } from '@lib/core/services'
import { RegionDTOExtend, RegionService } from '@lib/360/admin-site/services'
import { RegionType } from '@lib/shared/360'
import { GMap } from '../components/map'
import { DomainService } from '@lib/admin/services'
import { IconLinkList } from '../components/icon-link-list';
import { PortfolioImage } from '../components/portfolio-link';
import { BlogDuLich } from '../components/blog-du-lich';
import { TourHot } from '../components/tour-noi-bat';
import BoxMap from '../components/box-map';


const Template2 = () => {
    const [regions, setRegions] = useState<RegionDTOExtend[]>([]);

    useEffect(() => {
        getAllRegion();
    }, []);

    const getAllRegion = async () => {
        try {
            const res = await RegionService.allPublish();
            setRegions(res);

        } catch (err) {
            console.log(err)
        } finally {

        }
    }

    return (

        <div className={styles.container}>
            <HomeSlider />
            <IconLinkList title={'Điểm đến'}/>
            <FeaturedNew title={'Nổi bật'}/>

            <PortfolioImage title={'Điểm đến du lịch'}/>
            <BlogDuLich title={'Blog du lịch'}/>
            <TourHot title={'Tour nổi bật'}/>
            {regions?.map((e: RegionDTOExtend, index: number) => (
                <div key={e.id}>

                    {e.type === RegionType.CATEGORY && (
                        <NewTabs title={e.name} categories={e.categories} />
                    )}

                    {e.type === RegionType.LINK_PAGE && (
                        <OurService data={e.data} title={e.name} />
                    )}

                    {e.type === RegionType.ADVERTISE && (
                        <SwiperGalleryAdvertisement data={e.data} title={e.name} />
                    )}
                </div>
            ))}


            <BoxMap />

        </div>
    )
}

export default Template2