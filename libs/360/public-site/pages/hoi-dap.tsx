import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Breadcrumb } from "../components/breadcrumb";
import { SdButton, SdSelect } from "@lib/core/components";
import SearchIcon from '@mui/icons-material/Search';
import { Controller, useForm } from "react-hook-form";
import { Pagination, Stack, TextField } from "@mui/material";
import React from "react";
import VisibilityIcon from '@mui/icons-material/Visibility';

import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import { ApiService, LoadingService, NotifyService } from "@lib/core/services";
import { QuestionGroupService, QuestionService } from "@lib/360/admin-site/services/question/question.service";
import { QuestionDTOExtend, QuestionGroupDTOExtend } from "@lib/360/admin-site/services/question/question.model";
import { FindDTOReq } from "@lib/360/admin-site/services";
import { QuestionPublicFindReq, QuestionSaveReq } from "@lib/shared/360";
import QuestionDetailView from "../modals/question-detail";
import { BreadCrumbService } from "../services/breadcrumb.service";


function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}


export default function AnswerQuestion() {
    const [openModal, setOpenModal] = useState(false);

    const [tab, setTab] = React.useState(0);
    const [questionGroups, setQuestionGroups] = useState<QuestionGroupDTOExtend[]>([]);
    const [currentGroup, setCurrentGroup] = useState<string>('');
    const [questions, setQuestions] = useState<QuestionDTOExtend[]>([]);
    const [questionGroupId, setQuestionGroupId] = useState<string>('');
    const [item, setItem] = useState<QuestionDTOExtend>();
    const [total, setTotal] = useState<number>(0);


    const [filter, setFilter] = useState<QuestionPublicFindReq>({
        pageNumber: 0,
        pageSize: 5,
        searchText: '',
        address: '',
        fullName: '',
    });

    const handleChange = (event, newValue) => {
        setTab(newValue);
    };

    const form = useForm();
    const {
        handleSubmit,
        control,
        formState: { errors },
    } = form;

    const {
        register,
        setValue,
        watch,
    } = useForm();

    // const {
    //     register,
    //     handleSubmit,
    //     control,
    //     setValue,
    //     watch,
    //     formState: { errors },
    // } = useForm();

    const router = useRouter();
    const { slug } = router.query;

    useEffect(() => {
        getAllQuestionGroup();
    }, []);

    useEffect(() => {
        BreadCrumbService.info({
            title: 'Hỏi đáp',
            subTitle: 'Hỏi đáp',
            slug: '/hoi-dap'
        });
    }, []);


    useEffect(() => {
        getQuestions();
    }, [filter]);

    const getAllQuestionGroup = async () => {
        try {
            const res = await QuestionGroupService.allPublish();
            res?.forEach(e => {
                e.value = e.id;
                e.display = e.name;
            })
            setQuestionGroups(res);

        } catch (err) {
            console.log(err)
        } finally {

        }
    }

    const getQuestions = async () => {
        try {
            const res = await QuestionService.findPublish(filter);
            setQuestions(res.items || []);
            setTotal(res?.total);

        } catch (err) {
            console.log(err)
        } finally {

        }
    }


    const submitHandler = async (data: QuestionSaveReq) => {

        LoadingService.start();
        try {
            const req: Partial<QuestionSaveReq> = {
                title: data.title,
                address: data.address,
                phone: data.phone,
                email: data.email,
                fullName: data.fullName,
                content: data.content,
                questionGroupId: questionGroupId
            };
            const res = await QuestionService.create(req);
            NotifyService.success('Gửi câu hỏi thành công, câu hỏi của bạn sẽ được hiển thị khi QTV duyệt');

            form.reset();
            setQuestionGroupId('');

        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    const handleClick = (item: QuestionGroupDTOExtend) => {
        setCurrentGroup(item.id);
        setFilter({
            ...filter,
            questionGroupId: item.id
        })
        router.push({
            pathname: '/hoi-dap',
            query: { groupId: item.id }
        })
    }

    const handleOpenDetail = (item: QuestionDTOExtend) => {
        setItem(item);
        setOpenModal(true);
    }

    const handleClose = () => {
        setOpenModal(false);
    }

    const handleChangePage = (event: React.ChangeEvent<unknown>, value: number) => {
        setFilter(
            {
                ...filter,
                pageNumber: value - 1
            }
        );
    };

    const search = async (data) => {
        setFilter({
            ...filter,
            searchText: data.searchText,
            fullName: data.fullName,
            address: data.address
        })
        // getQuestions();
    }

    const reset = async () => {
        setFilter({
            searchText: '',
            pageNumber: 0,
            pageSize: 5
        });
        form.reset();
    }
    return (

        <div >
            {/* <Breadcrumb title={'Hỏi đáp'} /> */}

            <main className="container-fluid pt-16 pb-50">

                <div className="row">
                    <div className="col-md-3 mb-16">
                        <div className="shadow-f1 rounded-8">
                            <div className="bg-blue-100 text-white rounded-top-8 text-18 font-bold py-6 px-10">Lĩnh vực </div>
                            {questionGroups?.length > 0 && (
                                <div className="document-cat mb-5">
                                    {questionGroups.map(e => (

                                        <div key={e.id} onClick={() => handleClick(e)} className={e.id === currentGroup ? 'document-cat__item pointer  p-10 my-5 active' : 'document-cat__item pointer  p-10 my-5'} >{e.name}</div>

                                    ))}

                                </div>
                            )}


                        </div>
                    </div>
                    <div className="col-md-9 mb-16">
                        <Box sx={{ width: '100%' }}>
                            <Box className="px-24">
                                <Tabs value={tab} onChange={handleChange} aria-label="basic tabs example">
                                    <Tab label="Hỏi đáp" {...a11yProps(0)} />
                                    <Tab label="Đặt câu hỏi" {...a11yProps(1)} />

                                </Tabs>
                            </Box>
                            <TabPanel value={tab} index={0}>
                                <div className="document-search shadow-f1 p-10 rounded-8 mb-20">
                                    <div className="media flex-wrap">
                                        <div className="media-body" style={{minWidth: 140}}>
                                            <form onSubmit={handleSubmit(search)} >
                                                <div className="mb-20">
                                                    <Controller
                                                        name="searchText"
                                                        control={control}
                                                        defaultValue=""
                                                        render={({ field }) => (
                                                            <TextField
                                                                variant="outlined"
                                                                fullWidth
                                                                id="searchText"
                                                                label="Tiêu đề câu hỏi"
                                                                size="small"
                                                                {...field}
                                                            ></TextField>
                                                        )}
                                                    ></Controller>
                                                </div>
                                                <div className="row mb-20">
                                                    <div className="col-md-6 mb-16">
                                                        <Controller
                                                            name="fullName"
                                                            control={control}
                                                            defaultValue=""
                                                            render={({ field }) => (
                                                                <TextField
                                                                    variant="outlined"
                                                                    fullWidth
                                                                    id="fullName"
                                                                    label="Người gửi"
                                                                    size="small"
                                                                    {...field}
                                                                ></TextField>
                                                            )}
                                                        ></Controller>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <Controller
                                                            name="address"
                                                            control={control}
                                                            defaultValue=""
                                                            render={({ field }) => (
                                                                <TextField
                                                                    variant="outlined"
                                                                    fullWidth
                                                                    id="address"
                                                                    label="Địa chỉ"
                                                                    size="small"
                                                                    {...field}
                                                                ></TextField>
                                                            )}
                                                        ></Controller>
                                                    </div>
                                                </div>


                                            </form>

                                        </div>
                                        <div className="media-button d-flex align-items-center ml-8 ml-md-60">
                                            <div className="ml-20">
                                                <SdButton color={'primary'} variant={'outlined'} label="Làm mới" onClick={reset} />
                                            </div>
                                            <div className="ml-20">
                                                <SdButton icon={<SearchIcon />} color={'primary'} label="Tìm kiếm" onClick={handleSubmit(search)} />

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {questions.length > 0 && (
                                    <><div className="ans-list">
                                        {questions.map(e => (
                                            <div key={e.id} className="px-30 shadow-f1 py-10 mb-18 rounded-8">
                                                <div className="text-16 mb-10"><strong>Câu hỏi:</strong> {e.title}</div>
                                                <div className="mb-10 d-flex align-items-center">
                                                    <div className="mr-36"><strong>Người gửi:</strong> {e.fullName}</div>
                                                    <div><strong>Địa chỉ:</strong> {e.address}</div>
                                                </div>
                                                <div className="d-flex align-items-center text-blue-100 pointer mr-32" onClick={() => handleOpenDetail(e)}>
                                                    <div className="mr-12"><VisibilityIcon /></div>
                                                    <div>Xem chi tiết</div>
                                                </div>
                                            </div>
                                        ))}



                                        {openModal && <QuestionDetailView item={item} open={openModal}
                                            onClose={handleClose}
                                            onAgree={() => {
                                                setOpenModal(false);
                                            }} />}

                                    </div><div className="d-flex justify-content-end">
                                            <Stack spacing={2}>
                                                <Pagination count={Math.ceil(total / filter.pageSize)} color="primary" page={filter.pageNumber + 1} onChange={handleChangePage} />
                                            </Stack>
                                        </div></>
                                )}

                            </TabPanel>
                            <TabPanel value={tab} index={1}>
                                <div className="text-18 font-bold my-16">Thông tin câu hỏi</div>
                                <form onSubmit={handleSubmit(submitHandler)} >
                                    <div className="mb-20">
                                        <Controller
                                            name="title"
                                            control={control}
                                            defaultValue=""
                                            rules={{
                                                required: true,
                                            }}
                                            render={({ field }) => (
                                                <TextField
                                                    variant="outlined"
                                                    fullWidth
                                                    id="title"
                                                    label="Tiêu đề câu hỏi"
                                                    size="small"
                                                    error={Boolean(errors.title)}
                                                    helperText={
                                                        errors.title
                                                            ? 'Tiêu đề bài viết bắt buộc'
                                                            : ''
                                                    }
                                                    {...field}
                                                ></TextField>
                                            )}
                                        ></Controller>
                                    </div>
                                    <div className="row mb-20">
                                        <div className="col-6">
                                            <Controller
                                                name="fullName"
                                                control={control}
                                                defaultValue=""
                                                rules={{
                                                    required: true,
                                                }}
                                                render={({ field }) => (
                                                    <TextField
                                                        variant="outlined"
                                                        fullWidth
                                                        id="fullName"
                                                        label="Họ và tên (*)"
                                                        size="small"
                                                        error={Boolean(errors.fullName)}
                                                        helperText={
                                                            errors.fullName
                                                                ? 'Họ và tên bắt buộc'
                                                                : ''
                                                        }
                                                        {...field}
                                                    ></TextField>
                                                )}
                                            ></Controller>
                                        </div>
                                        <div className="col-6">
                                            <Controller
                                                name="address"
                                                control={control}
                                                defaultValue=""
                                                rules={{
                                                    required: true,
                                                }}
                                                render={({ field }) => (
                                                    <TextField
                                                        variant="outlined"
                                                        fullWidth
                                                        id="address"
                                                        label="Địa chỉ "
                                                        size="small"
                                                        error={Boolean(errors.address)}
                                                        helperText={
                                                            errors.address
                                                                ? 'Vui lòng nhập địa chỉ'
                                                                : ''
                                                        }
                                                        {...field}
                                                    ></TextField>
                                                )}
                                            ></Controller>
                                        </div>
                                    </div>
                                    <div className="row mb-20">
                                        <div className="col-6">
                                            <Controller
                                                name="phone"
                                                control={control}
                                                defaultValue=""
                                                render={({ field }) => (
                                                    <TextField
                                                        variant="outlined"
                                                        fullWidth
                                                        id="phone"
                                                        label="Số điện thoại"
                                                        size="small"
                                                        {...field}
                                                    ></TextField>
                                                )}
                                            ></Controller>
                                        </div>
                                        <div className="col-6">
                                            <Controller
                                                name="email"
                                                control={control}
                                                defaultValue=""
                                                render={({ field }) => (
                                                    <TextField
                                                        variant="outlined"
                                                        fullWidth
                                                        id="email"
                                                        label="Email"
                                                        size="small"
                                                        {...field}
                                                    ></TextField>
                                                )}
                                            ></Controller>
                                        </div>

                                    </div>

                                    <div className="row mb-20">
                                        <div className="col-6">
                                            <SdSelect
                                                form={form}
                                                label="Lĩnh vực"
                                                value={questionGroupId}
                                                items={questionGroups}
                                                rules={{
                                                    required: true,
                                                }}
                                                sdChange={(item => {
                                                    setQuestionGroupId(item?.value || '');
                                                })}

                                            />

                                            {/* <SdSelect
                                                form={form}
                                                disabled={id ? true : false}
                                                rules={{
                                                    required: true,
                                                }}
                                                value={req.type}
                                                items={CategoryTypes}
                                                sdChange={(item, value) => setReq({
                                                    ...req,
                                                    type: value as CategoryType
                                                })}
                                            /> */}
                                        </div>

                                    </div>

                                    <div className="row mb-20">
                                        <div className="col-12">
                                            <Controller
                                                name="content"
                                                control={control}
                                                rules={{
                                                    required: true,
                                                }}
                                                defaultValue=""
                                                render={({ field }) => (
                                                    <TextField
                                                        variant="outlined"
                                                        fullWidth
                                                        size="small"
                                                        id="content"
                                                        label="Nội dung câu hỏi"
                                                        multiline
                                                        rows={5}
                                                        error={Boolean(errors.content)}
                                                        helperText={
                                                            errors.content
                                                                ? 'Họ và tên bắt buộc'
                                                                : ''
                                                        }
                                                        {...field}
                                                    />
                                                )}
                                            ></Controller>
                                        </div>
                                    </div>

                                    <div className="d-flex justify-content-end">
                                        <div className="ml-20">
                                            <SdButton color={'secondary'} variant={'text'} label="Huỷ bỏ" onClick={() => null} />
                                        </div>
                                        <div className="ml-20">
                                            <SdButton color={'primary'} label="Gửi câu hỏi" onClick={handleSubmit(submitHandler)} />

                                        </div>

                                    </div>


                                </form>

                            </TabPanel>
                        </Box>


                    </div>
                </div>

            </main>

        </div>
    );
}

