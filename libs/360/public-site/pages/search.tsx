import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Breadcrumb } from "../components/breadcrumb";
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';
import { PostService } from "@lib/360/admin-site/services";
import { CategoryDTO, ConfigurationTemplate, MenuType, PostDTO, PostPublicFindReq } from "@lib/shared/360";
import { ListPostLayout2 } from "../components/list-post-layout-2";
import { ListPostStaticLayout1 } from "../components/list-post-static";
import { Pagination, Stack } from "@mui/material";
import CategoryLayout from "../components/category-layout";
import { BreadCrumbService } from "../services/breadcrumb.service";
import { cdn } from "@lib/core/services";

const host360 = process.env.NEXT_PUBLIC_360_HOST;


export default function SearchPage() {
    const [categories, setCategories] = useState<CategoryDTO[]>([]);
    const [posts, setPosts] = useState<PostDTO[]>([]);

    const [total, setTotal] = useState<number>(0);

    const [filter, setFilter] = useState<PostPublicFindReq | undefined>();

    const router = useRouter();
    const { slug } = router.query;
    const { keyword } = router.query;

    useEffect(() => {
        BreadCrumbService.info({
            title: 'Tìm kiếm',
            subTitle: 'Tin tức sự kiện',
            slug: '/tin-tuc-su-kien'
        });
        if (typeof keyword === 'string') {
            setFilter({
                pageNumber: 0,
                pageSize: 50,
                searchText: keyword
            })
        }
    }, [keyword]);

    useEffect(() => {
        getPosts();

    }, [filter]);

    const getPosts = async () => {
        try {
            if (filter) {
                const res = await PostService.findPublish(filter);
                setPosts(res?.items);
                setTotal(res?.total);
            }

        } catch (err) {
            console.log(err)
        } finally {
        }
    }

    return (
        <div className="section-search py-50">
            <div className="container">
                <div className="page-title text-24 font-bold mb-30">Kết quả tìm kiếm cho: <span className="text-18 text-gray-101">{keyword}</span></div>
                <div className="list-result">
                    {posts?.map(e => (
                        <div key={e.id} className="result-item media">
                            <div className="thumb">
                                <img src={cdn(e.image) || '/front/no-image.png'} alt="" />
                            </div>
                            <div className="content media-body">
                                <h3 className="mt-0"><a href={`/bai-viet/${e.slug}`}>{e.name}</a></h3>
                                <p dangerouslySetInnerHTML={{ __html: e.quote?.replace(/\n/g, "<br />") || '' }}></p>

                            </div>
                        </div>
                    ))}


                </div>
            </div>

        </div>

    );
}




