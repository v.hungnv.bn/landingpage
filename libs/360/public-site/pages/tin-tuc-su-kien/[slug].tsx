import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';
import { PostService } from "@lib/360/admin-site/services";
import { CategoryDTO, CategoryType, ConfigurationTemplate, MenuType, PostDTO, PostPublicFindReq } from "@lib/shared/360";
import { Breadcrumb } from "../../components/breadcrumb";
import { ListPostLayout2 } from "../../components/list-post-layout-2";
import { ListPostStaticLayout1 } from "../../components/list-post-static";
import { ListPostRssLayout1 } from "../../components/list-post-rss";
import { ListPostLayout2Rss } from "../../components/list-post-layout-2-rss";
import { Pagination, Stack } from "@mui/material";
import CategoryLayout from "../../components/category-layout";
import { BreadCrumbService } from "../../services/breadcrumb.service";




export default function CategoryDetail(props: { onClick?: (data: CategoryDTO) => void}) {
    const [categories, setCategories] = useState<CategoryDTO[]>([]);
    const [posts, setPosts] = useState<PostDTO[]>([]);

    const [category, setCategory] = useState<CategoryDTO>();
    const [total, setTotal] = useState<number>(0);

    const [filter, setFilter] = useState<PostPublicFindReq>({
        pageNumber: 0,
        pageSize: 10,
    });

    const router = useRouter();
    const { slug } = router.query;

    const template = localStorage.getItem('TEMPLATE');

    useEffect(() => {
        // getAllCategory();
        if (typeof slug === 'string') {
            getCategoryDetail(slug);
        }
    }, [slug]);


    useEffect(() => {
        if (category?.type === CategoryType.STATIC) {
            getPosts();
        }
    }, [filter]);

    const getCategoryDetail = async (slug?: string) => {
        try {
            const res = await PostService.getCategory(slug);
            setCategory(res);
            BreadCrumbService.info({
                title: res?.name,
                subTitle: 'Tin tức sự kiện',
                slug: '/tin-tuc-su-kien'
            });
            if (res?.type !== CategoryType.RSS) {
                setFilter({
                    ...filter,
                    categorySlugOrId: slug
                });
            }
        } catch (err) {
            console.log(err);
        } finally {
        }
    }

    const getPosts = async () => {
        try {
            const res = await PostService.findPublish(filter);
            setPosts(res?.items);
            setTotal(res?.total);
        } catch (err) {
            console.log(err)
        } finally {
        }
    }


    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setFilter({
            ...filter,
            pageNumber: value - 1
        });
    };

    const renderTreeItem = (treeItems) => {
        if (!treeItems?.length) {
            return null;
        }
        return treeItems.map(treeItem => {
            return (
                <Link className="pointer" href={treeItem.slug}>
                    <TreeItem
                        key={treeItem.id}
                        nodeId={treeItem.id}
                        label={treeItem.name}>
                        {
                            renderTreeItem(treeItem.children)
                        }

                    </TreeItem>
                </Link>
            )
        })
    }

    return (
        <div>
            {template === ConfigurationTemplate.TEMPLATE2 && (
                <>
                    {category?.type == CategoryType.RSS ? (
                        <div><ListPostLayout2Rss articles={category.rssInfo?.items} /></div>
                    ) : (
                        <div>
                            <ListPostLayout2 articles={posts} />
                            {total > 0 && (
                                <div className="d-flex justify-content-end">
                                    <Stack spacing={2}>
                                        <Pagination count={Math.ceil(total / filter.pageSize)} color="primary" page={filter.pageNumber + 1} onChange={handleChange} />
                                    </Stack>
                                </div>
                            )}

                        </div>

                    )}
                </>

            )}
            {template === ConfigurationTemplate.TEMPLATE1 && (
                <>
                    {category?.type == CategoryType.RSS ? (
                        <ListPostRssLayout1 articles={category.rssInfo?.items} />
                    ) : (
                        <div>
                            <ListPostStaticLayout1 articles={posts} />
                            {total > 0 && (
                                <div className="d-flex justify-content-end">
                                    <Stack spacing={2}>
                                        <Pagination count={Math.ceil(total / filter.pageSize)} color="primary" page={filter.pageNumber + 1} onChange={handleChange} />
                                    </Stack>
                                </div>
                            )}
                        </div>
                    )}
                </>
            )}
        </div>


    );
}




