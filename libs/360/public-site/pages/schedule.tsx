import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Breadcrumb } from "../components/breadcrumb";
import { SdButton, SdInput } from "@lib/core/components";
import SearchIcon from '@mui/icons-material/Search';
import { Controller, useForm } from "react-hook-form";
import { Pagination, Stack, TextField } from "@mui/material";
import React from "react";
import VisibilityIcon from '@mui/icons-material/Visibility';
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import { TopNew } from "../components/top-new";
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import { FindDTOReq, ScheduleDTOExtend, ScheduleService } from "@lib/360/admin-site/services";
import { ApiService, cdn } from "@lib/core/services";
import { MenuType, ScheduleDTO, SchedulePublicFindReq } from "@lib/shared/360";
import { FilterOperator } from "@lib/core/models";
import { BreadCrumbService } from "../services/breadcrumb.service";

const host360 = process.env.NEXT_PUBLIC_360_HOST;

export default function ScheduleList() {
    const [list, setList] = useState<ScheduleDTOExtend[]>([]);
    const [total, setTotal] = useState<number>(0);

    const [filter, setFilter] = useState<SchedulePublicFindReq>({
        pageNumber: 0,
        pageSize: 10,
        searchText: ''
    });
    const form = useForm();
    const {
        handleSubmit,
    } = form;


    const router = useRouter();
    const { slug } = router.query;


    useEffect(() => {
        getSchedule();
    }, [router.pathname, filter]);

    useEffect(() => {
        BreadCrumbService.info({
            title: 'Lịch công tác',
            subTitle: 'Lịch công tác',
            slug: '/schedule'
        });
    }, []);


    const getSchedule = async () => {
        try {
            const res = await ScheduleService.findPublish(filter);
            setList(res.items);
            setTotal(res.total);
        } catch (err) {
            console.log(err)
        } finally {

        }
    }
    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setFilter(
            {
                ...filter,
                pageNumber: value - 1
            }
        );
    };

    const search = async () => {
        getSchedule();
    }

    const reset = async () => {
        setFilter({
            searchText: '',
            pageNumber: 0,
            pageSize: 10
        });
    }

    const download = (item: ScheduleDTO) => {
        ApiService.download(cdn(item.documents[0]?.url), `${item.documents[0]?.name || 'Schedule_' + item.name}`);
    }

    const submitHandler = async (data: any) => {
    }

    return (

        <div >
            {/* <Breadcrumb title={'Lịch công tác'} pageType={MenuType.SCHEDULE} /> */}

            <main className="container-fluid pt-16 pb-50">

                <div className="row">

                    <div className="col-md-9 mb-16">
                        <div className="document-search p-10 rounded-8 mb-20">
                            <div className="text-24 font-bold mb-20 text-blue-100 heading-line">Tìm kiếm</div>
                            <div className="media flex-wrap">
                                <div className="media-body">
                                    <form onSubmit={handleSubmit(submitHandler)} >
                                        <div className="" style={{minWidth: 120}}>
                                            <SdInput
                                                className="w-100"
                                                form={form}
                                                label='Từ khoá tìm kiếm'
                                                value={filter.searchText}
                                                sdChange={(value) => setFilter({
                                                    ...filter,
                                                    searchText: value
                                                })}
                                            />
                                        </div>

                                    </form>

                                </div>
                                <div className="media-button d-flex align-items-center ml-8 ml-md-60">
                                    <div className="ml-20">
                                        <SdButton color={'primary'} variant={'outlined'} label="Làm mới" onClick={reset} />
                                    </div>
                                    <div className="ml-20">
                                        <SdButton icon={<SearchIcon />} color={'primary'} label="Tìm kiếm" onClick={handleSubmit(search)} />

                                    </div>
                                </div>
                            </div>
                        </div>

                        {list?.length > 0 && (
                            <><div className="schedule-list">
                                {list?.map((item, index: number) => (
                                    <div key={item.id} className="border-bottom rounded-8 px-20 pb-10 mb-20">
                                        <Link href={`/lich-cong-tac/${item.id}`}>
                                            <div className="text-24 font-bold mb-16 pointer">{item.name}</div>
                                        </Link>
                                        <div className="d-flex align-items-center">
                                            <div className="d-flex align-items-center text-gray-101 mr-32">
                                                <div className="mr-12"><CalendarTodayIcon /></div>
                                                <div>{Date.toFormat(item.fromDate, 'dd/MM/yyyy')} - {Date.toFormat(item.toDate, 'dd/MM/yyyy')}</div>
                                            </div>
                                            <Link href={`/lich-cong-tac/${item.id}`}>
                                                <div className="d-flex align-items-center text-gray-101 pointer mr-32">
                                                    <div className="mr-12"><VisibilityIcon /></div>
                                                    <div>Xem chi tiết</div>
                                                </div>
                                            </Link>

                                            {item?.documents?.length > 0 && (
                                                <div className="d-flex align-items-center text-gray-101 pointer" onClick={() => download(item)}>
                                                    <div className="mr-12"><FileDownloadIcon /></div>
                                                    <div>Tải về</div>
                                                </div>
                                            )}

                                        </div>

                                    </div>
                                ))}

                            </div><div className="d-flex justify-content-end">
                                    <Stack spacing={2}>
                                        <Pagination count={Math.ceil(total  / filter.pageSize)} color="primary" page={filter.pageNumber + 1} onChange={handleChange} />
                                    </Stack>
                                </div></>
                        )}


                    </div>

                    <div className="col-md-3 mb-16">
                        <TopNew />
                    </div>
                </div>

            </main>

        </div>
    );
}

