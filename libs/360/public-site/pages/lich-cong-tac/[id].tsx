import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Breadcrumb } from "../../components/breadcrumb";
import Image from "next/image";

import * as React from 'react';
import { TopNew } from "../../components/top-new";
import { ScheduleService } from "@lib/360/admin-site/services";
import { MenuType, ScheduleDTO } from "@lib/shared/360";
import { ApiService, cdn, getUrl360 } from "@lib/core/services";

const host360 = process.env.NEXT_PUBLIC_360_HOST;

export default function Schedule() {

    const [post, setPost] = useState<ScheduleDTO>();


    const router = useRouter();
    const { id } = router.query;
    useEffect(() => {
        if (typeof id === 'string') {
            getDetail(id);
        }
    }, [id]);

    const getDetail = async (id: string) => {
        try {
            const res = await ScheduleService.detail(id);

            setPost(res);

            console.log('res__', res)

        } catch (err) {
            console.log(err)

        } finally {

        }
    }

    const download = (item: ScheduleDTO) => {
        ApiService.download(getUrl360(item.documents?.[0]?.url), `${item.documents[0]?.name || 'Schedule_' + item.name}`);
    }

    return (
            <div >
                {/* <Breadcrumb title={'Lịch công tác'} pageType={MenuType.SCHEDULE} idOrSlug={post?.id}/> */}
                <main className="container-fluid pt-16 pb-50">

                    <div className="row">

                        <div className="col-md-9 col-sm-12 mb-16">
                            {post?.id && (
                                <div>
                                    <div className="text-24 font-bold mb-16 pointer">{post.name}</div>
                                    <div className="file-wrap">
                                        <div className="media align-items-center mb-20">
                                            <div className="mr-30">
                                                <Image
                                                    src="/front/pdf-thumb.png"
                                                    alt="logo" width="29" height="37"
                                                />
                                            </div>
                                            <div className="media-body d-flex align-items-center">
                                                <div className="mr-24">{post?.documents[0]?.name}</div>
                                                <div className="text-15 font-bold text-blue-100 pointer" onClick={() => download(post)}>Tải về</div>
                                            </div>
                                        </div>

                                    </div>

                                    <div>
                                        <iframe src={getUrl360(post?.documents?.[0]?.url)} width="100%" height="800px"></iframe>
                                    </div>
                                </div>
                            )}




                        </div>

                        <div className="col-md-3 col-sm-12 mb-16">
                            <TopNew />
                        </div>
                    </div>

                </main>

            </div>


    );
}

