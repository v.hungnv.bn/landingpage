import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Breadcrumb } from "../../components/breadcrumb";
import Image from "next/image";
import { PageDTOExtend, PageService } from "@lib/360/admin-site/services";

import moment from 'moment';
import 'moment/locale/vi';
import { TopNew } from "../../components/top-new";
import { BreadCrumbService } from "../../services/breadcrumb.service";
moment.locale('vi');

const host360 = process.env.NEXT_PUBLIC_360_HOST;

export default function ContentPage() {

    const [post, setPost] = useState<PageDTOExtend | null>();
    const router = useRouter();
    const { slug } = router.query;

    useEffect(() => {
        if (typeof slug === 'string') {
            getDetail(slug);
        }
    }, [slug]);

    const getDetail = async (slug: string) => {
        try {
            const res = await PageService.getPageDetail(slug);
            setPost(res);

            BreadCrumbService.info({
                title: res?.name,
                subTitle: 'Nội dung',
                // slug: '/about'
            });
        } catch (err) {
            console.log(err)

        } finally {

        }
    }


    return (
            <div >
                {/* <Breadcrumb title={post?.name} /> */}
                {post && (
                    <>
                        <main className="container-fluid">
                            <div className="row">
                                <div className="col-md-9 mb-16">
                                    <article className="mt-16">
                                        <div className="d-flex align-items-center text-14 text-gray-101 mb-24">
                                            <div className="mr-20">Cập nhật: {post?.publishDate ? moment(post?.publishDate).format('HH:mm dddd, DD/MM/YYYY'): ''}</div>
                                            <div>Lượt xem: 400</div>
                                        </div>
                                        <div className="py-10">
                                            {post.quote}
                                        </div>
                                        <div className="entry-content ql-editor px-0">
                                            <div dangerouslySetInnerHTML={{ __html: post.content || '' }}></div>
                                        </div>

                                    </article>

                                </div>

                                <div className="col-md-3">
                                    <TopNew />

                                </div>
                            </div>


                        </main></>
                )}

            </div>




    );
}

