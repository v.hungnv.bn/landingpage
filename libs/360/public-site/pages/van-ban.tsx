import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import { Breadcrumb } from "../components/breadcrumb";
import { SdButton, SdFilterOption, SdGrid, SdGridRef, SdInput, SdSelect } from "@lib/core/components";
import SearchIcon from '@mui/icons-material/Search';
import { useForm } from "react-hook-form";
import SdDate from "@lib/core/components/SdDate/SdDate";
import React from "react";
import VisibilityIcon from '@mui/icons-material/Visibility';
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import { ApiService, cdn } from "@lib/core/services";
import { DocumentGroupService } from "@lib/360/admin-site/services";
import { DocumentDTO, DocumentGroupFor, DocumentPublicFindReq, MenuType } from "@lib/shared/360";
import { DocumentService } from "@lib/360/admin-site/services/document/document.service";
import { DocumentDTOExtend } from "@lib/360/admin-site/services/document/document.model";
import { BreadCrumbService } from "../services/breadcrumb.service";

const host360 = process.env.NEXT_PUBLIC_360_HOST;

export default function DocumentCategory() {
    const [documentCategories, setDocumentCategories] = React.useState<any[]>([]);
    const [documentTypes, setDocumentTypes] = React.useState<any[]>([]);
    const [documentDepartments, setDocumentDepartments] = React.useState<any[]>([]);
    const [currentCategory, setCurrenCategory] = useState<string>('');
    const [filter, setFilter] = useState<Partial<DocumentPublicFindReq>>({
        pageSize: 10,
        pageNumber: 0,
        publishDateFrom: '',
        publishDateTo: ''
    });
    const grid = useRef<SdGridRef | null>(null);

    const form = useForm();
    const {
        handleSubmit,
    } = form;


    const router = useRouter();
    const { slug, documentCategoryId } = router.query;

    useEffect(() => {

        setFilter({
            ...filter,
            documentCategoryId: typeof documentCategoryId === 'string' ? documentCategoryId : ''
        })

    }, [router.query]);

    useEffect(() => {
        grid.current?.reload();

    }, [filter]);

    useEffect(() => {
        BreadCrumbService.info({
            title: 'Văn bản quy phạm pháp luật',
            subTitle: 'Văn bản',
            slug: '/van-ban'
        });
        getAllCategory();
    }, []);



    const getAllCategory = async () => {
        try {
            const res = await DocumentGroupService.allPublish();
            const cats = res?.filter(e => e.groupFor === DocumentGroupFor.CATEGORY && e.isActivated)?.map(e => {
                return {
                    ...e,
                    value: e.id,
                    display: e.name
                }
            });
            setDocumentCategories(cats);

            const types = res?.filter(e => e.groupFor === DocumentGroupFor.TYPE && e.isActivated)?.map(e => {
                return {
                    ...e,
                    value: e.id,
                    display: e.name
                }
            });
            setDocumentTypes(types);
            const departments = res?.filter(e => e.groupFor === DocumentGroupFor.DEPARTMENT && e.isActivated)?.map(e => {
                return {
                    ...e,
                    value: e.id,
                    display: e.name
                }
            });
            setDocumentDepartments(departments);

        } catch (err) {
            console.log(err)
        } finally {

        }
    }

    const items = async (args?: SdFilterOption) => {
        let req: DocumentPublicFindReq = {
            pageSize: args?.pageSize || filter.pageSize || 10,
            pageNumber: args?.pageNumber || filter.pageNumber || 0,
            searchText: filter?.searchText || undefined,
            documentCategoryId: currentCategory || undefined,
            documentDepartmentId: filter?.documentDepartmentId || undefined,
            documentTypeId: filter?.documentTypeId || undefined,
            publishDateFrom: filter?.publishDateFrom || undefined,
            publishDateTo: filter?.publishDateTo || undefined,
        }

        const res = await DocumentService.findPublish(req);
        const data = res?.items?.map((e, index) => {
            return {
                ...e,
                stt: index + 1
            }
        })
        return {
            items: data || [],
            total: res?.total || 0
        }
    }

    const submitHandler = async () => {
    }

    const search = async () => {
        grid?.current?.reload();

    }

    const reset = async () => {
        setFilter({
            searchText: ''
        });
        grid?.current?.reload();
    }

    const download = (item: DocumentDTOExtend) => {
        ApiService.download(cdn(item.files[0]?.url), `${item.files[0]?.name || 'Doc_' + item.title}`);
    }

    const handleClick = (item?: DocumentDTOExtend) => {
        setCurrenCategory(item?.id || '');
        router.push({
            pathname: '/van-ban',
            query: { documentCategoryId: item?.id || '' }
        })
        // grid.current?.reload();
    }

    return (

        <div >
            {/* <Breadcrumb title={'Văn bản quy phạm pháp luật'} pageType={MenuType.DOCUMENT} /> */}

            <main className="container-fluid pt-16 pb-50">

                <div className="row">
                    <div className="col-md-3 mb-16">
                        <div className="shadow-f1 rounded-8">
                            <div className="bg-blue-100 text-white rounded-top-8 text-18 font-bold py-6 px-10">Danh mục </div>
                            <div className="document-cat mb-5">
                                <div className={'document-cat__item pointer  p-10 my-5'} onClick={() => handleClick()}>Tất cả</div>
                                {documentCategories?.map((e, index) => (
                                    <div key={index} onClick={() => handleClick(e)} className={e.id === currentCategory ? 'document-cat__item pointer  p-10 my-5 active' : 'document-cat__item pointer  p-10 my-5'} >{e.display}</div>
                                ))}



                            </div>

                        </div>
                    </div>
                    <div className="col-md-9 mb-16">
                        <div className="document-search shadow-f1 p-10 rounded-8 mb-20">
                            <div className="text-24 font-bold mb-20 text-blue-100 heading-line">Tìm kiếm văn bản</div>
                            <div className="media flex-wrap">
                                <div className="media-body">
                                    <form onSubmit={handleSubmit(submitHandler)} >
                                        <div className="">
                                            <SdInput
                                                className="w-100"
                                                form={form}
                                                label='Từ khoá tìm kiếm'
                                                value={filter.searchText}
                                                sdChange={(value) => setFilter({
                                                    ...filter,
                                                    searchText: value
                                                })}
                                            />
                                        </div>
                                        <div className="row mb-20">
                                            <div className="col-md-6 mb-16">
                                                <SdSelect
                                                    label="Loại văn bản"
                                                    value={filter.documentTypeId}
                                                    items={documentTypes}
                                                    sdChange={(item => {
                                                        setFilter({
                                                            ...filter,
                                                            documentTypeId: item?.value
                                                        })
                                                    })}

                                                />
                                            </div>
                                            <div className="col-md-6 mb-16">
                                                <SdSelect
                                                    label="Cơ quan ban hành"
                                                    value={filter.documentDepartmentId}
                                                    items={documentDepartments}
                                                    sdChange={(item => {
                                                        setFilter({
                                                            ...filter,
                                                            documentDepartmentId: item?.value
                                                        })
                                                    })}

                                                />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 mb-16">
                                                <SdDate
                                                    label="Từ ngày ban hành"
                                                    value={filter?.publishDateFrom || ''}
                                                    required={false}
                                                    sdChange={(item => {
                                                        setFilter({
                                                            ...filter,
                                                            publishDateFrom: Date.toFormat(item, 'MM/dd/yyyy')
                                                        })
                                                    })}

                                                />
                                                {/* <SdDate
                                                    label="Ngày ban hành"
                                                    value={publishDate}
                                                    required={false}
                                                    sdChange={(item => {
                                                        setPublishDate(Date.toFormat(item, 'MM/dd/yyyy'))
                                                    })}

                                                /> */}
                                            </div>
                                            <div className="col-md-6 mb-16">
                                                <SdDate
                                                    label="Đến ngày ban hành"
                                                    value={filter?.publishDateTo || ''}
                                                    required={false}
                                                    sdChange={(item => {
                                                        setFilter({
                                                            ...filter,
                                                            publishDateTo: Date.toFormat(item, 'MM/dd/yyyy')
                                                        })
                                                    })}

                                                />
                                            </div>
                                        </div>

                                    </form>

                                </div>
                                <div className="media-button d-flex align-items-center ml-8 ml-md-60">
                                    <div className="ml-20">
                                        <SdButton color={'primary'} variant={'outlined'} label="Làm mới" onClick={reset} />
                                    </div>
                                    <div className="ml-20">
                                        <SdButton icon={<SearchIcon />} color={'primary'} label="Tìm kiếm" onClick={handleSubmit(search)} />

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="document-table w-100 ">
                            <div className="scroll-horirontal">
                                <SdGrid
                                    pagingServer
                                    ref={grid}
                                    items={async (args: SdFilterOption) => {
                                        const res = await items(args);
                                        return {
                                            items: res?.items || [],
                                            total: res?.total || 0
                                        }
                                    }}

                                    columns={[{
                                        field: 'stt',
                                        label: 'STT',
                                        type: 'string',
                                        filter: {
                                            disabled: true
                                        }
                                    }, {
                                        field: 'title',
                                        label: 'Tên văn bản',
                                        type: 'string',
                                    }, {
                                        field: 'symbol',
                                        label: 'Số hiệu',
                                        type: 'string',
                                    }, {
                                        field: 'publishDate',
                                        label: 'Ngày ban hành',
                                        type: 'string',
                                        template: (value, item) => (
                                            <div style={{ width: '100px' }}>
                                                {Date.toFormat(value, 'dd/MM/yyyy')}
                                            </div>
                                        )
                                    },]}
                                    commands={[{
                                        title: 'Chi tiết',
                                        icon: <VisibilityIcon />,
                                        onClick: (item) => {
                                            router.push(`/van-ban/${item.id}`);
                                        },
                                    }, {
                                        title: 'Tải xuống',
                                        icon: <FileDownloadIcon />,
                                        onClick: (item) => {
                                            download(item);
                                        },
                                    }]}
                                />
                            </div>
                        </div>

                    </div>
                </div>

            </main>

        </div>
    );
}

