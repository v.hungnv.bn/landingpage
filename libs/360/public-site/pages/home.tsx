


import { HomeSlider } from '../components/home-slider'

import styles from '../styles/Home.module.css'
import { useEffect, useState } from "react";
import { FeaturedNew } from '../components/featured-new'
import { OurService } from '../components/our-service'
import { NewTabs } from '../components/new-tabs'
import { SwiperGalleryAdvertisement } from '../components/swiper-gallery'
import { SwiperGalleryPhoto } from '../components/swiper-gallery-photo'
import { SwiperGalleryVideo } from '../components/swiper-gallery-video'
import { TabDepartmentLink } from '../components/tab-department-link'
import { RegionDTOExtend, RegionService } from '@lib/360/admin-site/services'
import { RegionType } from '@lib/shared/360'
import { GMap } from '../components/map'
import { IconLinkList } from '../components/icon-link-list';
import { PortfolioImage } from '../components/portfolio-link';
import { BlogDuLich } from '../components/blog-du-lich';
import { TourHot } from '../components/tour-noi-bat';
import BoxMap from '../components/box-map';
import { GetServerSideProps, GetStaticProps, InferGetServerSidePropsType, InferGetStaticPropsType, NextPage } from 'next';

interface Props {
  userAgent?: string;
}
const HomePage: NextPage<Props> = ({ userAgent }) => {

  // const HomePage: NextPage = () => {

  // const HomePage: NextPage = (order: Order) => {
  const [categories, setCategories] = useState<any>();
  const [posts, setPosts] = useState<any>();
  const [regions, setRegions] = useState<RegionDTOExtend[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const template = localStorage.getItem('TEMPLATE');


  useEffect(() => {
    getAllRegion();
  }, []);

  const getAllRegion = async () => {
    try {
      const res = await RegionService.allPublish();
      setRegions(res);

    } catch (err) {
      console.log(err)
    } finally {

    }
  }



  return (

    <div className={styles.container}>
      <HomeSlider />

      {regions?.map((e: RegionDTOExtend, index: number) => (
        <div key={e.id}>

          {e.type === RegionType.TIN_TUC_SU_KIEN && (
            <FeaturedNew title={e.name} />
          )}

          {e.type === RegionType.CATEGORY && (
            <NewTabs title={e.name} categories={e.categories} />
          )}

          {e.type === RegionType.LINK_PAGE && (
            <OurService data={e.data} title={e.name} />
          )}

          {e.type === RegionType.ADVERTISE && (
            <SwiperGalleryAdvertisement data={e.data} title={e.name} />
          )}

          {e.type === RegionType.DIEM_DEN && (
            <IconLinkList data={e.data} title={e.name} />
          )}

          {e.type === RegionType.DIEM_DEN_DU_LICH && (
            <PortfolioImage data={e.data} title={e.name} />
          )}

          {e.type === RegionType.BLOG_DU_LICH && (
            <BlogDuLich data={e.data} title={e.name} />
          )}

          {e.type === RegionType.TOUR_NOI_BAT && (
            <TourHot data={e.data} title={e.name} />
          )}

          {e.type === RegionType.IMAGE && (
            <SwiperGalleryPhoto title={e.name} />
          )}
          {e.type === RegionType.VIDEO && (
            <SwiperGalleryVideo title={e.name} />
          )}
          {e.type === RegionType.DON_VI_LIEN_KET && (
            <TabDepartmentLink title={e.name} />
          )}
          {e.type === RegionType.GOOGLE_MAP && (
            <BoxMap title={e.name} />
          )}
        </div>
      ))}

      {/* {template === 'TEMPLATE2' && (
        <GMap />
      )} */}


    </div>
  )
}

HomePage.getInitialProps = async ({ req }) => {

  console.log('vo day', '');
  const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
  return { userAgent }
}


export default HomePage
