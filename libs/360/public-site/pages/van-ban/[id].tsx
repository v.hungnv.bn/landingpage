import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Breadcrumb } from "../../components/breadcrumb";
import Image from "next/image";

import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import { SdFilterOption, SdGrid, SdGridRef } from "@lib/core/components";
import VisibilityIcon from '@mui/icons-material/Visibility';
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import { DocumentService } from "@lib/360/admin-site/services/document/document.service";
import { ApiService, getUrl360 } from "@lib/core/services";
import { DocumentGroupService } from "@lib/360/admin-site/services";
import { DocumentDTO, DocumentGroupFor, DocumentPublicFindReq } from "@lib/shared/360";
import { DocumentDTOExtend } from "@lib/360/admin-site/services/document/document.model";


function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function Document() {
    const [post, setPost] = useState<DocumentDTO>();
    const [value, setValue] = React.useState(0);
    const [documentCategories, setDocumentCategories] = React.useState<any[]>([]);
    const [documentDepartments, setDocumentDepartments] = React.useState<any[]>([]);
    const [currentCategory, setCurrenCategory] = useState<string>('');
    const [documentTypes, setDocumentTypes] = React.useState<any[]>([]);
    const [pdfFile, setPdfFile] = React.useState<any>();
    const grid = React.useRef<SdGridRef | null>(null);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const router = useRouter();

    const { id } = router.query;
    useEffect(() => {
        if (typeof id === 'string') {
            getDetail(id);
        }
    }, [id]);
    useEffect(() => {
        getAllCategory();
    }, []);

    const getAllCategory = async () => {
        try {
            const res = await DocumentGroupService.allPublish();
            const cats = res?.filter(e => e.groupFor === DocumentGroupFor.CATEGORY && e.isActivated)?.map(e => {
                return {
                    ...e,
                    value: e.id,
                    display: e.name
                }
            });
            setDocumentCategories(cats);

            const types = res?.filter(e => e.groupFor === DocumentGroupFor.TYPE && e.isActivated)?.map(e => {
                return {
                    ...e,
                    value: e.id,
                    display: e.name
                }
            });
            setDocumentTypes(types);
            const departments = res?.filter(e => e.groupFor === DocumentGroupFor.DEPARTMENT && e.isActivated)?.map(e => {
                return {
                    ...e,
                    value: e.id,
                    display: e.name
                }
            });
            setDocumentDepartments(departments);

        } catch (err) {
            console.log(err)
        } finally {

        }
    }

    const getExtFile = (fileName: string) => {
        const lastDot = fileName.lastIndexOf('.');
        if (lastDot === -1) {
            throw new Error(`File không đúng định dạng`);
        }
        const ext = fileName.substring(lastDot + 1);
        return ext;
    }

    const getDetail = async (id: string) => {
        try {
            const res = await DocumentService.detail(id);
            const pdfFilter = res?.files?.filter(file => getExtFile(file?.url) === 'pdf');
            setPost(res);
            setPdfFile(pdfFilter[0])
            setCurrenCategory(res.documentCategoryId);

        } catch (err) {
            console.log(err)

        } finally {

        }
    }
    const handleClick = (item: DocumentDTOExtend) => {
        setCurrenCategory(item.id);

        router.push({
            pathname: '/van-ban',
            query: { documentCategoryId: item.id }
        })
    }

    const download = (file) => {
        ApiService.download(getUrl360(file?.url), `${file?.name || 'Doc_' + file.name}`);
    }
    return (
        <div >
            {/* <Breadcrumb title={'Văn bản quy phạm pháp luật'} /> */}

            <main className="container-fluid pt-16 pb-50">

                <div className="row">
                    <div className="col-md-3 mb-16">
                        <div className="shadow-f1 rounded-8">
                            <div className="bg-blue-100 text-white rounded-top-8 text-18 font-bold py-6 px-10">Danh mục </div>
                            <div className="document-cat mb-5">
                                {documentCategories?.map((e, index) => (
                                    <div key={index} onClick={() => handleClick(e)} className={e.id === currentCategory ? 'document-cat__item pointer  p-10 my-5 active' : 'document-cat__item pointer  p-10 my-5'} >{e.display}</div>
                                ))}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-9 mb-16">
                        {post?.id && (
                            <Box sx={{ width: '100%' }}>
                                <Box className="px-24">
                                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" variant="scrollable"
                                        allowScrollButtonsMobile>
                                        <Tab label="Tóm tắt" {...a11yProps(0)} />
                                        <Tab label="Văn bản gốc" {...a11yProps(1)} />
                                        <Tab label="Tải về" {...a11yProps(2)} />
                                        <Tab label="Văn bản liên quan" {...a11yProps(3)} />

                                    </Tabs>
                                </Box>
                                <TabPanel value={value} index={0}>
                                    <div className="rounded-8 shadow-f1 p-10">
                                        <div className="text-18 font-bold mb-20">{post?.title}</div>
                                        <div className="document-content">
                                            <div className="row mx-0">
                                                <div className="col-md-6 px-0">
                                                    <div className="media">
                                                        <div className="wi-200 p-10 bg-gray-102 text-gray-1021 font-semibold">Cơ quan ban hành:</div>
                                                        <div className="media-body px-20 py-10">{post?.documentDepartmentName}</div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6 px-0">
                                                    <div className="media">
                                                        <div className="wi-200 p-10 bg-gray-102 text-gray-1021 font-semibold">Loại văn bản:</div>
                                                        <div className="media-body px-20 py-10">{post?.documentTypeName}</div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6 px-0">
                                                    <div className="media">
                                                        <div className="wi-200 p-10 bg-gray-102 text-gray-1021 font-semibold">Ngày ban hành:</div>
                                                        <div className="media-body px-20 py-10">{Date.toFormat(post?.publishDate, 'dd/MM/yyyy')}</div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6 px-0">
                                                    <div className="media">
                                                        <div className="wi-200 p-10 bg-gray-102 text-gray-1021 font-semibold">Số hiệu:</div>
                                                        <div className="media-body px-20 py-10">{post?.symbol}</div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6 px-0">
                                                    <div className="media">
                                                        <div className="wi-200 p-10 bg-gray-102 text-gray-1021 font-semibold">Người ký:</div>
                                                        <div className="media-body px-20 py-10">{post?.signBy}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </TabPanel>
                                <TabPanel value={value} index={1}>
                                    <div>
                                        <iframe src={getUrl360(pdfFile?.url)} width="100%" height="800px"></iframe>

                                        {/* <iframe src="https://www.africau.edu/images/default/sample.pdf" width="100%" height="800px"></iframe> */}
                                    </div>
                                </TabPanel>
                                <TabPanel value={value} index={2}>
                                    {post?.files?.length > 0 && (
                                        <div className="file-wrap">
                                            {post?.files?.map((file, index) => (
                                                <div key={index} className="media align-items-center mb-20">
                                                    <div className="mr-30">
                                                        {getExtFile(file.url) == 'pdf' ? (
                                                            <Image
                                                                src="/front/pdf-thumb.png"
                                                                alt="logo" width="29" height="37"
                                                            />
                                                        ) : (
                                                            <Image
                                                                src="/front/word-thumb.png"
                                                                alt="logo" width="29" height="37"
                                                            />
                                                        )}

                                                    </div>
                                                    <div className="media-body d-flex align-items-center">
                                                        <div className="mr-24">{file?.name}</div>
                                                        <div className="text-15 font-bold text-blue-100 pointer" onClick={() => download(file)}>Tải về</div>
                                                    </div>
                                                </div>
                                            ))}

                                        </div>
                                    )}

                                </TabPanel>
                                <TabPanel value={value} index={3}>
                                    <div className="document-table scroll-horirontal" >
                                        <SdGrid
                                            pagingServer
                                            ref={grid}
                                            items={async (args: SdFilterOption) => {
                                                let req: DocumentPublicFindReq = {
                                                    pageSize: args?.pageSize || 10,
                                                    pageNumber: args?.pageNumber || 0,
                                                    documentCategoryId: post?.documentCategoryId || undefined,
                                                    documentTypeId: post?.documentTypeId || undefined,
                                                }

                                                const res = await DocumentService.findPublish(req);
                                                const data = res?.items?.filter(f => f.id !== post.id)?.map((e, index) => {
                                                    return {
                                                        ...e,
                                                        stt: index + 1
                                                    }
                                                })
                                                return {
                                                    items: data || [],
                                                    total: res?.total || 0
                                                }
                                            }}
                                            columns={[{
                                                field: 'stt',
                                                label: 'STT',
                                                type: 'string',
                                            }, {
                                                field: 'title',
                                                label: 'Tên văn bản',
                                                type: 'string',
                                            }, {
                                                field: 'symbol',
                                                label: 'Số hiệu',
                                                type: 'string',
                                            }, {
                                                field: 'publishDate',
                                                label: 'Ngày ban hành',
                                                type: 'string',
                                                template: (value) => (
                                                    <div style={{ width: '100px' }}>
                                                        {Date.toFormat(value, 'dd/MM/yyyy')}
                                                    </div>
                                                )
                                            },]}
                                            commands={[{
                                                title: 'Chi tiết',
                                                icon: <VisibilityIcon />,
                                                onClick: (item) => {
                                                    router.push(`/van-ban/${item.id}`);
                                                },
                                            }, {
                                                title: 'Tải xuống',
                                                icon: <FileDownloadIcon />,
                                                onClick: (item) => {
                                                    ApiService.download(getUrl360(item?.files[0]?.url), `${item?.files[0]?.name || 'Doc_' + item?.files[0]?.name}`);
                                                },
                                            }]}
                                        />
                                    </div>
                                </TabPanel>
                            </Box>
                        )}


                    </div>
                </div>

            </main>

        </div>


    );
}

