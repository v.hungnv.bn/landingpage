import Link from "next/link";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import { useEffect, useState } from "react";
import { Breadcrumb } from "../components/breadcrumb";
import { BlogDTO, CategoryDTO, fakeCategories, fakePosts, getDenormalizedCategories } from "../services/datafake.model";
import Image from "next/image";
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';
import { SdInput, SdSelect, SdTreeView, SdTreeViewOptionTransform } from "@lib/core/components";
import { TopNew } from "../components/top-new";
import { PageService } from "@lib/360/admin-site/services";
import { PageDTO } from "@lib/shared/360";
import { BreadCrumbService } from "../services/breadcrumb.service";


export default function Contact() {
    const [data, setData] = useState<Partial<PageDTO>>({
        content: ''
    });
    const router = useRouter();
    const { slug } = router.query;
    
    useEffect(() => {
        getDetail('a94fad48-39de-4f23-8d6f-5e207ca92a43');
    }, [slug]);

    useEffect(() => {
       
    }, []);

    const getDetail = async (id: string) => {
        try {
            const res = await PageService.getPageDetail(id);
            setData(res);
            BreadCrumbService.info({
                title: res?.name,
                subTitle: 'Liên hệ',
                slug: '/contact'
            });

        } catch (err) {
            console.log(err)
        } finally {
        }
    }

    useEffect(() => {
    }, []);

    return (

            <div >
                {/* <Breadcrumb title={data.name} /> */}

                <main className="container-fluid py-50">
                    <div className="row">
                        <div className="col-10">
                            <div className="text-14 text-gray-101 d-flex align-items-center mb-16">
                                <div className="p-10">Cập nhật: 14:40 Thứ hai, 01/11/2022</div>

                                <div className="p-10">Lượt xem: 400</div>
                            </div>

                            <div dangerouslySetInnerHTML={{__html: data.content || ''}}></div>

                            {/* <div>
                                <div className="text-16 font-bold mb-16">PHÒNG GIÁO DỤC TIỂU HỌC SỞ GIÁO DỤC VÀ ĐÀO TẠO THÀNH PHỐ HỒ CHÍ MINH</div>

                                <p className="mb-16">Địa chỉ liên hệ: </p>

                                <p className="mb-16">Tầng 9, Phòng 9.1 toà nhà Sở Giáo dục và Đào tạo TP.HCM</p>

                                <p className="mb-16">68 Lê Thánh Tôn Phường Bến Nghé Quận 1 - Thành phố Hồ Chí Minh</p>

                                <p className="mb-16">Điện thoại: (028) 38229361 - 38224618</p>


                            </div>

                            <div className="position-relative" style={{ paddingTop: '56.25%' }}>
                                <iframe className="position-absolute" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d29792.983903516117!2d105.8341598!3d21.0277644!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1670561249133!5m2!1svi!2s" width="1140" height="640" style={{ top: 0, left: 0, border: 0, width: '100%', height: '100%' }} allowFullScreen loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>

                            </div> */}



                        </div>
                        <div className="col-2">
                            <TopNew />
                        </div>

                    </div>

                </main>

            </div>
    );
}

