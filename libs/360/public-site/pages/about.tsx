import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Breadcrumb } from "../components/breadcrumb";
import { SdSelect } from "@lib/core/components";
import { OrganizationDTOExtend, OrganizationService } from "@lib/360/admin-site/services";
import { MemberDTO } from "@lib/shared/360";
import { cdn } from "@lib/core/services";
import { styled, Tooltip, tooltipClasses, TooltipProps } from "@mui/material";
import { BreadCrumbService } from "../services/breadcrumb.service";


const host360 = process.env.NEXT_PUBLIC_360_HOST;

const NoMaxWidthTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
))({
    [`& .${tooltipClasses.tooltip}`]: {
        maxWidth: 800,
    },
});

export default function About() {
    const [list, setList] = useState<OrganizationDTOExtend[]>([]);
    const [organizationSelected, setOrganizationSelected] = useState<OrganizationDTOExtend>();

    const router = useRouter();
    const { slug } = router.query;

    useEffect(() => {
        getOrganization();

       
    }, [router.pathname]);

    

    const getOrganization = async () => {
        try {
            const res = await OrganizationService.allPublish();
            res?.forEach(e => {
                e.value = e.id;
                e.display = e.name;
                e.activatedMembers = e.activatedMembers || [];
                e.data = e;
            });
            setList(res);
            setOrganizationSelected(res[0]);
            BreadCrumbService.info({
                title: res[0]?.name,
                subTitle: 'Cơ cấu tổ chức',
                slug: '/about'
            });
        } catch (err) {
            console.log(err)
        } finally {

        }
    }

    return (

        <div >
            {/* <Breadcrumb title={organizationSelected?.name} /> */}

            <main className="container-fluid py-50">
                <div className="d-flex justify-content-end mb-30">
                    <div className="" style={{ width: 320 }}>
                        <SdSelect
                            label="Cơ cấu tổ chức"
                            value={organizationSelected?.value || ''}
                            items={list}
                            sdChange={((item) => {
                                setOrganizationSelected(item?.data || undefined);
                                BreadCrumbService.info({
                                    title: item?.name,
                                    subTitle: 'Cơ cấu tổ chức',
                                    slug: '/about'
                                });
                            })}
                        />
                    </div>

                </div>
                {organizationSelected?.activatedMembers && organizationSelected?.activatedMembers?.length > 0 && (
                    <div className="row align-items-stretch">
                        {organizationSelected?.activatedMembers?.map((member: MemberDTO, index: number) => (
                            <div key={member.id} className="col-md-6 col-12 mb-30">
                                <div className="media py-10 pl-10 shadow-f1 rounded-8 h-100">
                                    <div className="about-avatar mr-30">
                                        {/* <Image className="rounded-full overflow-hidden entry-thumb-135-135" src={cdn(member.image)} width="135" height="135" /> */}
                                        <img className="rounded-circle overflow-hidden entry-thumb-135-135" src={cdn(member.image)} />


                                    </div>
                                    <div className="media-body  ">
                                        <div className="media align-items-stretch">
                                            <div className="about-info  border-left-gray border-right-gray px-16 media-body" >
                                                <div className="intro-content" style={{ minWidth: 250 }}>
                                                    <div className="text-18 font-bold mb-10">{member.name}</div>
                                                    <div className="mb-10 text-gray-101">{member.role}</div>
                                                    <div className="mb-10">{member.phone}</div>
                                                    <div>{member.email}</div>
                                                </div>

                                            </div>
                                            <div className="about-description pr-12 ml-16">

                                                <NoMaxWidthTooltip title={member.description}>
                                                    <div className="content entry-quote-6" dangerouslySetInnerHTML={{ __html: member.description?.replace(/\n/g, "<br />") || '' }}></div>
                                                </NoMaxWidthTooltip>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        ))}

                    </div>
                )}


            </main>

        </div>
    );
}

