import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Breadcrumb } from "../../components/breadcrumb";
import Image from "next/image";
import { CommentService, PostService } from "@lib/360/admin-site/services";
import { Controller, useForm } from "react-hook-form";
import { TextField } from "@mui/material";
import { SdButton } from "@lib/core/components";
import SendIcon from '@mui/icons-material/Send';
import { LoadingService, NotifyService } from "@lib/core/services";
import { CommentDTO, CommentSaveReq, MenuType, PostDTO } from "@lib/shared/360";
import {
    FacebookShareButton,
    FacebookIcon,
} from 'next-share';
import {
    TwitterShareButton,
    TwitterIcon,
} from 'next-share';
import moment from 'moment';
import 'moment/locale/vi';
import { TopNew } from "../../components/top-new";
import { BreadCrumbService } from "../../services/breadcrumb.service";
moment.locale('vi');

const host360 = process.env.NEXT_PUBLIC_360_HOST;

export default function Post() {
    const [categories, setCategories] = useState<any>();
    const [posts, setPosts] = useState<any>();
    const [category, setCategory] = useState<any>();
    const [post, setPost] = useState<PostDTO | null>();
    const [comments, setComments] = useState<CommentDTO[]>([]);

    const router = useRouter();
    const { slug } = router.query;
    const { asPath } = useRouter();
    const cleanPath = asPath.split('#')[0].split('?')[0];
    // const form = useForm();

    const {
        register,
        handleSubmit,
        control,
        setValue,
        watch,
        reset,
        formState: { errors },
    } = useForm();

    useEffect(() => {
        if (typeof slug === 'string') {
            getDetail(slug);
            updateViewPost(slug);
        }
    }, [slug]);

    const getDetail = async (slug: string) => {
        try {
            const res = await PostService.getPostDetail(slug);

            if (res?.canComment) {
                const resComments = await CommentService.allPublish(res.id);
                setComments(resComments);
            }
            setPost(res);

            BreadCrumbService.info({
                title: res.name,
                subTitle: 'Tin tức sự kiện',
                slug: `/tin-tuc-su-kien`
            });
        } catch (err) {
            console.log(err)

        } finally {

        }
    }
    const updateViewPost = async (slug: string) => {
        try {
            const res = await PostService.view(slug);

        } catch (err) {
            console.log(err)

        } finally {

        }
    }


    const submitHandler = async (data: any) => {
        LoadingService.start();
        try {
            const req: Partial<CommentSaveReq> = {
                postId: post?.id || '',
                content: data?.content,
                fullName: data?.fullName,
            };
            const res = await CommentService.create(req);
            NotifyService.success('Gửi bình luận thành công, bình luận của bạn sẽ được hiển thị khi QTV duyệt');
            reset();
        } catch (err) {
            console.log(err)

        } finally {
            LoadingService.stop();

        }
    }

    return (
        <div >
            {/* <Breadcrumb title={post?.name} pageType={MenuType.CATEGORY} idOrSlug={post?.categoryId} slugName={post?.categoryName} /> */}
            {post && (
                <>

                    <main className="container-fluid">
                        <div className="row">
                            <div className="col-md-9 mb-16">
                                <article className="mt-16">
                                    <div className="d-flex align-items-center text-14 text-gray-101 mb-24">
                                        <div className="mr-20">Cập nhật: {post?.publishDate ? moment(post?.publishDate).format('HH:mm dddd, DD/MM/YYYY') : ''}</div>
                                        <div>Lượt xem: 400</div>
                                    </div>
                                    {/* <div className="py-10 entry-quote text-18 font-bold">
                                        {post.quote}
                                    </div> */}
                                    <div className="py-10 entry-quote text-18 font-bold" dangerouslySetInnerHTML={{ __html: post.quote?.replace(/\n/g, "<br />") || '' }}></div>

                                    <div className="entry-content ql-editor px-0">
                                        <div dangerouslySetInnerHTML={{ __html: post.content || '' }}></div>


                                    </div>

                                </article>

                                <div className="d-flex justify-content-between border-top border-bottom p-10">
                                    <div className="media align-items-center">
                                        <div className="mr-6">Chia sẻ bài viết</div>
                                        <div className="media-body d-flex">
                                            <div className="mr-6">
                                                <FacebookShareButton
                                                    url={`${host360}${cleanPath}`}
                                                    quote={post.name}
                                                >
                                                    <FacebookIcon size={32} round />
                                                </FacebookShareButton>
                                            </div>


                                            <TwitterShareButton
                                                url={`${host360}${cleanPath}`}
                                                title={post.name}
                                            >
                                                <TwitterIcon size={32} round />
                                            </TwitterShareButton>

                                        </div>
                                    </div>
                                    <div></div>

                                </div>

                                {post.canComment && (
                                    <div>
                                        <div className="heading-line text-blue-100 my-24">Bình luận</div>
                                        <div className="mb-30">
                                            <form onSubmit={handleSubmit(submitHandler)} >
                                                <div className="mb-16">
                                                    <Controller
                                                        name="content"
                                                        control={control}
                                                        defaultValue=""
                                                        rules={{
                                                            required: true,
                                                        }}
                                                        render={({ field }) => (
                                                            <TextField
                                                                variant="outlined"
                                                                fullWidth
                                                                size="small"
                                                                required
                                                                id="content"
                                                                label="Nội dung bình luận"
                                                                multiline
                                                                rows={4}
                                                                error={Boolean(errors.content)}
                                                                helperText={
                                                                    errors.content
                                                                        ? 'Vui lòng nhập nội dung'
                                                                        : ''
                                                                }
                                                                {...field}
                                                            />
                                                        )}
                                                    ></Controller>
                                                </div>
                                                <div className="mb-30">
                                                    <Controller
                                                        name="fullName"
                                                        control={control}
                                                        defaultValue=""
                                                        rules={{
                                                            required: true,
                                                        }}
                                                        render={({ field }) => (
                                                            <TextField
                                                                variant="outlined"
                                                                fullWidth
                                                                required
                                                                size="small"
                                                                id="fullName"
                                                                label="Họ và tên"
                                                                error={Boolean(errors.fullName)}
                                                                helperText={
                                                                    errors.fullName
                                                                        ? 'Vui lòng điền họ tên'
                                                                        : ''
                                                                }
                                                                {...field}
                                                            />
                                                        )}
                                                    ></Controller>
                                                </div>
                                                <div>
                                                    <SdButton icon={<SendIcon />} label='Gửi bình luận' onClick={handleSubmit(submitHandler)} />
                                                </div>
                                            </form>
                                        </div>

                                        {comments?.length > 0 && (
                                            <div className="py-24 border-top">
                                                {comments?.map((comment: CommentDTO, index: number) => (
                                                    <div key={index} className={index % 2 === 0 ? "media entry-comment align-items-center" : "media entry-comment align-items-center odd"} >
                                                        <div className="entry-avatar mr-20">
                                                            {comment.fullName.charAt(0)}
                                                        </div>
                                                        <div className="media-body">
                                                            <div className="text-18 font-bold mb-5">{comment.fullName}</div>
                                                            <div className="text-16 mb-5">{comment.content}</div>
                                                            <div className="text-14 text-gray-101"> Cập nhật: {moment(comment.createdDate).format('HH:mm dddd, DD/MM/YYYY')}</div>
                                                        </div>
                                                    </div>
                                                ))}
                                            </div>
                                        )}
                                    </div>
                                )}


                            </div>

                            <div className="col-md-3">
                                <TopNew />

                            </div>
                        </div>


                    </main></>
            )}

        </div>




    );
}

