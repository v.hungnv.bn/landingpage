import { SdButton, SdModal, SdModalRef, SdSelect } from "@lib/core/components"
import { LoadingService, NotifyService } from "@lib/core/services";
import { QuestionSaveReq } from "@lib/shared/360";
import { TextField } from "@material-ui/core";
import { useEffect, useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { SdEditor } from "@lib/core/components/SdEditor/SdEditor";
import { QuestionDTOExtend } from "@lib/360/admin-site/services/question/question.model";


function QuestionDetailView(props: { open: boolean, onClose: () => void, onAgree: () => void, item?: QuestionDTOExtend }) {
    const sdModal = useRef<SdModalRef | null>(null);
    const [openModal, setOpenModal] = useState(false);
    const [isActivated, setIsActivated] = useState<string>('');
    const { item } = props;

    const [isActivateds, setIsActivateds] = useState<{
        value: string;
        display: string;
        data: any
    }[]>([]);
    const {
        register,
        handleSubmit,
        control,
        setValue,
        watch,
        formState: { errors },
    } = useForm();
    useEffect(() => {
        setIsActivateds([
            {
                value: '1',
                display: 'Đã duyệt',
                data: null,
            },
            {
                value: '0',
                display: 'Chưa duyệt',
                data: null,
            }
        ])
        setOpenModal(props.open);
    }, [props.open]);

    useEffect(() => {
        console.log('item', item);
        if (item?.id) {
            setValue('title', item?.title)
            setValue('content', item?.content)
            setValue('anwser', item?.anwser)
        }
    }, []);


    return (
        <SdModal sx={{ width: '80%' }} opened={openModal} ref={sdModal}
            footer={
                <>
                    <div>
                        <SdButton className="mr-12" label='Đóng' color="secondary" variant="text" onClick={() => props.onAgree()} />
                    </div>

                </>
            }
            title={'Thông tin câu hỏi'} onClose={props.onClose}>

            <div >
                <div className="row mb-16 pt-20">
                    <div className="col-12">
                        <Controller
                            name="title"
                            control={control}
                            defaultValue=""
                            render={({ field }) => (
                                <TextField
                                    label="Tiêu đề câu hỏi"
                                    variant="outlined"
                                    fullWidth
                                    disabled
                                    id="title"
                                    size="small"
                                    {...field}
                                ></TextField>
                            )}
                        />
                    </div>
                </div>


                <div className="row mb-16">
                    <div className="col-12">
                        <Controller
                            name="content"
                            control={control}
                            defaultValue=""
                            render={({ field }) => (
                                <TextField
                                    label="Nội dung"
                                    variant="outlined"
                                    fullWidth
                                    id="content"
                                    disabled
                                    size="small"
                                    multiline
                                    rows={4}
                                    {...field}
                                ></TextField>
                            )}
                        />
                    </div>
                </div>
                <div className="row mb-16">
                    <div className="col-12">
                        <div className="font-bold text-18 mb-6">Thông tin câu trả lời</div>

                        <Controller
                            name="anwser"
                            control={control}
                            render={({ field }) => (
                                <SdEditor
                                    {...field}
                                    readOnly
                                    placeholder={"Trả lời..."}
                                    onChange={(text) => {
                                        field.onChange(text);
                                    }}
                                />
                            )}
                        />
                    </div>
                </div>



            </div>
        </SdModal>
    )
}

export default QuestionDetailView