import React, { useState, useEffect } from "react";
export const Catalogues = [
    {
        id: "01",
        name: "Cơ quan hành chính"
    },
    {
        id: "02",
        name: "Khách sạn"
    },
];

export const CategoryMap = (props: { categories, onChange: (isCheck) => void }) => {
    const [isCheckAll, setIsCheckAll] = useState(false);
    const [isCheck, setIsCheck] = useState<any[]>([]);
    const [list, setList] = useState<any[]>([]);

    useEffect(() => {
        setList(props.categories);
        setIsCheckAll(true);
        setIsCheck(list.map(li => li.id));
        
    }, [list]);

    const handleSelectAll = e => {
        setIsCheckAll(!isCheckAll);
        setIsCheck(list.map(li => li.id));
        if (isCheckAll) {
            setIsCheck([]);
        }
    };

    const handleClick = e => {
        const { id, checked } = e.target;
        setIsCheck([...isCheck, id]);
        if (!checked) {
            setIsCheck(isCheck.filter(item => item !== id));
        }
    };

    // console.log(isCheck);
    // props.onChange(isCheck);
    const catalog = list.map(({ id, name }) => {
        return (
            <><li>
                <Checkbox
                    key={id}
                    type="checkbox"
                    name={name}
                    id={id}
                    handleClick={handleClick}
                    isChecked={isCheck.includes(id)}
                />
                {name}
            </li>
            </>
        );
    });

    return (
        <ul className="list-none">
            <li>
                <Checkbox
                    type="checkbox"
                    name="selectAll"
                    id="selectAll"
                    handleClick={handleSelectAll}
                    isChecked={isCheckAll}
                />
                Chọn tất cả
            </li>
            {catalog}
        </ul>
    );
};

export const Checkbox = ({ id, type, name, handleClick, isChecked }) => {
    return (
        <input
            id={id}
            name={name}
            type={type}
            onChange={handleClick}
            checked={isChecked}
        />
    );
};