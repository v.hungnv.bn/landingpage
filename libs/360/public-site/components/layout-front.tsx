import React, { LegacyRef, ReactNode, useContext, useEffect, useState } from "react";
import Head from "next/head";
import { NextSeo } from "next-seo";
import { Footer } from "./footer";
import { Navbar } from "./navbar";
import BackToTopButton from "./back-to-top";
import { DomainService } from "@lib/admin/services";
import { ApiService, cdn } from "@lib/core/services";
import { ConfigurationService, MenuDTOExtend } from "@lib/360/admin-site/services";
import { ConfigurationDTO, ConfigurationTemplate } from "@lib/shared/360";
import Script from "next/script";
import { useRouter } from "next/router";
import CategoryLayout from "./category-layout";
import { Breadcrumb } from "./breadcrumb";
import DepartmentContext from "@lib/admin/context/department-context";
import type { GetServerSideProps } from 'next'

// import defaultOG from "../public/img/og-default.jpg";

// import PopupWidget from "../components/popupWidget";

interface Props {
    title?: string;
    description?: string;
    url?: string;
    children?: ReactNode;
    menus?: MenuDTOExtend[];
    // order?: Order;
}

export const LayoutFront = (props: Props): React.ReactElement => {
    const { children } = props;
// console.log('props111', props)
    const [isLoading, setIsLoading] = useState(false);
    const [menus, setMenus] = useState<MenuDTOExtend[]>([]);
    const [configSite, setConfigSite] = useState<ConfigurationDTO>();
    const [template, setTemplate] = useState<ConfigurationTemplate>(ConfigurationTemplate.TEMPLATE1);

    const ogimage = "";
    const router = useRouter();
    const department = useContext(DepartmentContext);

    useEffect(() => {
        // domainByHost();

        getConfiguaration();
    }, []);

    const getConfiguaration = async () => {
        try {
            const config = await ConfigurationService.detailPublic(department.departmentCode);
            localStorage.setItem('TEMPLATE', config?.template || template);
            setTemplate(config?.template || ConfigurationTemplate.TEMPLATE1)

            setConfigSite(config);
            setMenus(config?.menus || []);

        } catch {
        }
        finally {
        }
    }
  
    return (
        <>
            <Head>
                <link rel="preconnect" href="https://cdn.sanity.io/" />
                <link rel="dns-prefetch" href="https://cdn.sanity.io//" />
                <title>{configSite?.title}</title>
                <meta name="description" content={configSite?.faviconInfo} />
                <link rel="icon" href="/favicon.svg" />
                {/* <link rel="icon" href={cdn(configSite?.image)} /> */}

            </Head>

            <NextSeo
                title={configSite?.title}
                description={configSite?.title}
                canonical={props.url}
                openGraph={{
                    url: props.url,
                    title: props.title,
                    description: props.description,
                    images: [
                        {
                            url: ogimage,
                            width: 800,
                            height: 600,
                            alt: props.title
                        }
                    ],
                    site_name: props.title
                }}
                twitter={{
                    handle: "@surjithctly",
                    site: "@surjithctly",
                    cardType: "summary_large_image"
                }}
            />

            <div className={template}>
                <Navbar  {...props} menus={menus} configSite={configSite} />
                {router.pathname !== '/' && (
                    <Breadcrumb title={'Tin tức sự kiện'} />
                )}

                {configSite?.title && (
                    <div className="main-container">
                        {(router.pathname === '/tin-tuc-su-kien/[slug]' || router.pathname === '/tin-tuc-su-kien') ? (
                            <div className="container-fluid py-30">
                                <div className="row">
                                    <div className="col-md-3 mb-16">
                                        <CategoryLayout />
                                    </div>
                                    <div className="col-md-9 mb-16">
                                        {children}
                                    </div>
                                </div>

                            </div>

                        ) : (
                            <>{children}</>
                        )}

                    </div>
                )}
                <Footer {...props} configSite={configSite} />
                <BackToTopButton />
            </div>

        </>
    );
}
