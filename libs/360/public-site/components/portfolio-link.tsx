import { cdn } from "@lib/core/services";
import { RegionData } from "@lib/shared/360";
import Image from "next/image";
import Link from "next/link";
const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const PortfolioImage = (props: { title?: string, data?: RegionData[] }): React.ReactElement => {
    const { title, data } = props;

    return (
        <section className="section-portfolio my-25">
            <div className="container-fluid">
                <div className="text-24 text-center mb-20 text-blue-100 font-bold heading-line d-flex justify-content-center">{title}</div>

                <div className="d-flex align-items-center justify-content-center">
                    <div className="gallery">
                        {data?.map((e, index) => (
                            <div key={index} className="ima-item">
                                <img src={cdn(e.image)} alt="" />
                                <div className="ima-info text-center">
                                    <div className="text-18 font-bold mb-62 text-white md:text-48">{e.title}</div>
                                    <a className="button-link bg-blue-100 rounded px-8 md:px-30 py-8 text-white text-15 font-bold " target="_blank" href={e?.link}>Khám phá ngay</a>
                                </div>
                            </div>
                        ))}


                    </div>
                </div>
            </div>
        </section>

    );
}


