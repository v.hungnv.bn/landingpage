import * as React from 'react';

import Image from "next/image";
import Link from "next/link";
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import { PostDTOExtend, PostService, RegionCategoryExtend } from "@lib/360/admin-site/services";
import { cdn } from '@lib/core/services';
import { CategoryDTO, CategoryType, PostPublicFindReq } from '@lib/shared/360';
import { RssPostDTO } from './list-post-rss';
import { A11y, Autoplay, Navigation, Pagination, Scrollbar } from 'swiper';
import { useContext, useEffect, useState } from 'react';
import DepartmentContext from '@lib/admin/context/department-context';
const host360 = process.env.NEXT_PUBLIC_360_HOST;

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}


export const NewTabs = (props: { title: string, categories?: RegionCategoryExtend[] }): React.ReactElement => {
    const { title, categories } = props;
    const [value, setValue] = React.useState(0);
    const [category, setCategory] = React.useState<CategoryDTO>();

    const [posts, setPosts] = React.useState<PostDTOExtend[]>([]);
    const [rssPost, setRssPost] = React.useState<RssPostDTO[]>([]);

    const [total, setTotal] = useState<number>(0);

    const [filter, setFilter] = useState<PostPublicFindReq>({
        pageNumber: 0,
        pageSize: 50,
    });

    const department = useContext(DepartmentContext);

    
    useEffect(() => {
        if (props?.categories?.length) {
            getCategoryDetail(categories?.[0]?.id);
        }
    }, [categories]);


    useEffect(() => {
        if (category?.type === CategoryType.STATIC) {
            getPosts();
        }
    }, [filter]);

    const getCategoryDetail = async (idOrSlug?: string) => {
        try {
            const res = await PostService.getCategory(idOrSlug);
            setCategory(res);
            if (res?.type !== CategoryType.RSS) {
                setFilter({
                    ...filter,
                    categorySlugOrId: idOrSlug
                });
                setRssPost([]);
            } else {
                setRssPost(res?.rssInfo?.items);
                setTotal(res?.rssInfo?.items?.length);
                setPosts([]);
            }
        } catch (err) {
            console.log(err);
        } finally {
        }
    }

    const getPosts = async () => {
        try {
            const res = await PostService.findPublish(filter, department.departmentCode);
            setPosts(res?.items);
            setTotal(res?.items?.length);
        } catch (err) {
            console.log(err)
        } finally {
        }
    }

    const handleChange = (event, newValue) => {
        setValue(newValue);
        getCategoryDetail(categories?.[newValue]?.id);
        // getPostByCategory(categories?.[newValue]?.id);
    };


    return (
        <div className="container-fluid tab-new mb-40">
            <div className="text-24 text-center mb-20 text-blue-100 font-bold heading-line d-flex align-items-center justify-content-center">{title} </div>

            <Box sx={{ width: '100%' }}>
                <Box >
                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" centered variant="scrollable"
                        allowScrollButtonsMobile>
                        {categories?.map((e, index) => (
                            <Tab key={e.id} label={e.name || 'Danh mục ' + (index + 1)} {...a11yProps(index)} />
                        ))}
                    </Tabs>
                </Box>
                {total > 0 && (
                    <Box >
                        {categories?.map((e, index) => (
                            <TabPanel key={e.id} value={value} index={index}>
                                <Swiper
                                    modules={[Autoplay]}
                                    initialSlide={1}
                                    direction={'horizontal'}
                                    slidesPerView={"auto"}
                                    spaceBetween={24}
                                    autoplay={{ delay: 5000 }}
                                    loopedSlides={total}
                                    loop={true}
                                    className="swiper-tab-new"
                                ><>
                                        {category?.type == CategoryType.RSS ? (
                                            <>
                                                {rssPost?.map((e: RssPostDTO, index: number) => (
                                                    <SwiperSlide className="rounded-8 shadow-f1" key={index} style={{ width: 305 }}>
                                                        <div className="entry-item " >
                                                            <div className="entry-thumb mb-8 box-thumb">
                                                                {e.content?.match(/<img/)
                                                                    ? (
                                                                        <div className="rss-content" dangerouslySetInnerHTML={{ __html: e.content || '' }}></div>

                                                                    ) : (
                                                                        <a href={e.link} target='_blank'>
                                                                            <img
                                                                                src={'/front/no-image.png'}
                                                                                alt="logo"
                                                                            />
                                                                        </a>
                                                                    )}


                                                            </div>
                                                            <div className="entry-content p-10">
                                                                <div className="entry-title text-18 mb-10 entry-quote-2">
                                                                    <a className="text-18" href={e.link} target={'_blank'}>{e.title}</a>
                                                                </div>
                                                                <div className="entry-date text-14 mb-10 text-gray-101">{Date.toFormat(e.pubDate, 'dd/MM/yyyy')}</div>
                                                                <div className="entry-text text-14 mb-8 entry-quote-5">
                                                                    {e.contentSnippet}
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </SwiperSlide>
                                                ))}
                                            </>
                                        ) : (
                                            <>
                                                {posts?.map((e: PostDTOExtend, index: number) => (
                                                    <SwiperSlide className="rounded-8 shadow-f1" key={e.id} style={{ width: 305 }}>
                                                        <div className="entry-item " >
                                                            <div className="entry-thumb mb-8 box-thumb">
                                                                <Link href={`/bai-viet/${e.slug}`}>
                                                                    <Image
                                                                        src={cdn(e.image)}
                                                                        alt="logo" width="305" height="184"
                                                                    />
                                                                </Link>
                                                            </div>
                                                            <div className="entry-content p-10">
                                                                <div className="entry-title text-18 mb-10 entry-quote-2">
                                                                    <Link href={`/bai-viet/${e.slug}`}>
                                                                        <a className="text-18">{e.name}</a>
                                                                    </Link>
                                                                </div>
                                                                <div className="entry-date text-14 mb-10 text-gray-101">{Date.toFormat(e.publishDate, 'dd/MM/yyyy')}</div>
                                                                <div className="entry-text text-14 mb-8 entry-quote-5">
                                                                    {e.quote}
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </SwiperSlide>
                                                ))}
                                            </>
                                        )}
                                    </>



                                </Swiper>

                            </TabPanel>
                        ))}
                    </Box>
                )}



            </Box>

        </div >
    );
}


