// import Container from "@components/container";
// import ThemeSwitch from "@components/themeSwitch";
import { cdn } from "@lib/core/services";
import { ConfigurationDTO } from "@lib/shared/360";
import Image from "next/image";
// import { myLoader } from "@utils/all";

interface Props {
  host?: string;
  copyright?: string;
  description?: string;
  configSite?: ConfigurationDTO
}

export const Footer = (props: Props): React.ReactElement => {
  const { configSite } = props;

  return (
    <div className="bg-blue-100 p-20 text-white">
      <div className="container-fluid relative">
        <div className="media">
          <div className="logo-footer mr-34">
            {/* <Image src={cdn(configSite?.image)} alt="Logo" width={145} height={145} /> */}
            <img className="entry-thum-145-145" src={cdn(configSite?.image)} alt="photo"  />

          </div>
          <div className="media-body">
            <div className="md:text-24 text-18 font-bold mb-15">{configSite?.title}</div>
            <div className="content" dangerouslySetInnerHTML={{ __html: configSite?.footer?.replace(/\n/g, "<br />") || '' }}></div>

            {/* <div className="text-26 ">Bản quyền thuộc về: Sở giáo dục và Đào tạo tỉnh Đắk Nông</div>
            <div className="text-26 ">Địa chỉ: số 35 đường ABC, Đắk Nông</div>
            <div className="text-26 ">Điện thoại: 024.387682</div>
            <div className="text-26 ">Email: sogddaknong@gmail.com</div>
            <div className="text-26 ">Giấy phép số 184/GP-BC do Bộ Văn hoá Thông tin cấp</div> */}
          </div>
        </div>
        {configSite?.ipV6 && (
          // <a className="absolute top-0 right-0 pointer" target={'_blank'} href={configSite?.ipV6}> <Image src={'/front/ipv6.png'} alt="Logo" width={86} height={35} /></a>
          <a className="absolute top-0 right-0 pointer" href={`http://ipv6-test.com/validate.php?url=${configSite?.ipV6}`}  target={'_blank'} ><img src='http://ipv6-test.com/button-ipv6-small.png' alt='ipv6 ready' title='ipv6 ready' /></a>
        )}

      </div>
    </div>
  );
}


