// import Container from "@components/container";
// import ThemeSwitch from "@components/themeSwitch";
import { PostService } from "@lib/360/admin-site/services";
import { cdn } from "@lib/core/services";
import { PostDTO } from "@lib/shared/360";
import { IconButton, Pagination, Stack } from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import React, { useEffect, useState } from "react";
// import { myLoader } from "@utils/all";
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Scrollbar, EffectFade, A11y } from 'swiper';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';


// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

export const ListPostLayout2Rss = (props: { title?: string, articles?: any[] }): React.ReactElement => {
    const { title, articles } = props;
    const [posts, setPosts] = useState<PostDTO[]>([]);
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(6);
    const [preview, setPreview] = useState<PostDTO | undefined>();
    const [previewIndex, setPreviewIndex] = useState<number>(0);

    useEffect(() => {
        const items = articles?.slice(0, 3) as PostDTO[];
        setPosts(items || []);
        setPreview(items?.[previewIndex]);
    }, [articles, previewIndex]);

    useEffect(() => {
        setPreview(posts?.[previewIndex]);
    }, [previewIndex]);


    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setPage(value);
    };
    const handleNextItem = () => {
        setPreviewIndex(previewIndex + 1);
    };
    const handlePrevItem = () => {
        setPreviewIndex(previewIndex - 1);
    };
    return (
        <div className="">

            <div className="preview-post mb-30 shadow-f1 rounded-8">
                {preview && (
                    <div className="row ">
                        <div className="col-md-8 ">
                            <img className="entry-thumb-h-400 rounded-8 object-position-top-center" src={cdn(preview.image) || '/front/no-image.png'} alt="photo" />
                        </div>
                        <div className="col-md-4 ">
                            <div className="d-flex flex-column justify-content-between h-100">

                                <div>
                                    <div className="mb-16">
                                        <IconButton className="button-nav-orange mr-12" disabled={previewIndex <= 0} color="primary" aria-label="" component="label" onClick={handlePrevItem}>
                                            <ArrowBackIosNewIcon />
                                        </IconButton>
                                        <IconButton className="button-nav-orange" disabled={previewIndex >= posts.length - 1} color="primary" aria-label="" component="label" onClick={handleNextItem}>
                                            <ArrowForwardIosIcon />
                                        </IconButton>
                                    </div>
                                    <div className="text-18 font-bold mb-10 ">{preview.name}</div>
                                    <div className="text-14 text-gray-101 mb-10">{Date.toFormat(preview.publishDate, 'dd/MM/yyyy')}</div>
                                    {/* <div className="text-14 mb-20 entry-quote-3">{post.quote}</div> */}
                                    <div className="text-14 mb-20 entry-quote-3" dangerouslySetInnerHTML={{ __html: preview.quote?.replace(/\n/g, "<br />") || '' }}></div>
                                </div>
                                <div>
                                    {/* <div className="entry-author media align-items-center">
                                        <div className="entry-avatar mr-6">
                                            <Image
                                                src="/front/entry/avatar.png"
                                                alt="logo" width="26" height="26" />
                                        </div>
                                        <div className="text-13 font-bold media-body">admin</div>

                                    </div> */}
                                </div>

                            </div>


                        </div>
                    </div>

                )}


            </div>

            <div className="row align-content-stretch">
                {articles?.map((post, index) => {
                    if (((index + 1) <= (pageSize * page)) && ((index + 1) > (pageSize * page - pageSize))) {
                        return (
                            <div key={index} className="col-md-4 mb-30">
                                <Link href={`${post.link}`}>
                                    <div className="h-100 pointer border-bottom pb-8 post-block">

                                        <div className="entry-thumb-rss  rounded-8 ">
                                            <div className="rss-content thumb-h250" dangerouslySetInnerHTML={{ __html: post.content || '' }}></div>
                                        </div>
                                        <div className=" entry-content">
                                            <div className="text-18 font-bold mb-10 entry-quote-2 entry-title">{post.title}</div>
                                            <div className="text-14 text-gray-101 mb-10">{Date.toFormat(post.pubDate, 'dd/MM/yyyy')}</div>
                                            <div className="text-14 mb-20 entry-quote-3">{post.contentSnippet}</div>


                                        </div>

                                    </div>
                                </Link>
                            </div>
                        )
                    }

                })}
            </div>
            <div className="d-flex justify-content-end">
                <Stack spacing={2}>
                    <Pagination count={Math.ceil(articles?.length || 0 / pageSize)} color="primary" page={page} onChange={handleChange} />
                </Stack>
            </div>
        </div>

    );
}


