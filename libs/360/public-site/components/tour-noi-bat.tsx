import { cdn } from "@lib/core/services";
import { RegionData } from "@lib/shared/360";
import Image from "next/image";
import Link from "next/link";
import { Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const TourHot = (props: { title?: string, data?: RegionData[] }): React.ReactElement => {
    const { title, data } = props;

    return (
        <section className="section-tour my-25">
            <div className="container-fluid">
                <div className="text-24 text-center mb-20 text-blue-100 font-bold heading-line d-flex justify-content-center">{title}</div>

                <div className="d-flex align-items-center justify-content-center">
                    {data?.length && data?.length > 0 && (
                        <Swiper

                            modules={[Autoplay]}
                            initialSlide={1}
                            direction={'horizontal'}
                            slidesPerView={"auto"}
                            spaceBetween={34}
                            autoplay={{ delay: 5000 }}
                            loopedSlides={data.length}
                            loop={true}

                            className=""
                        >
                            {data?.map((e, index) => (
                                <SwiperSlide className="rounded-8" style={{ width: 500 }}>
                                    <a className="entry-item relative pointer" >
                                        <div className="entry-thumb box-thumb">
                                            <img className="entry-thumb-500-308" src={cdn(e.image)} alt="" />
                                        </div>
                                        <div className="entry-title text-white bg-organge-op-1 text-18 font-bold d-flex  p-10 absolute bottom-0 w-100" style={{ minHeight: 80 }}>{e.title}</div>
                                    </a>
                                </SwiperSlide>
                            ))}

                        </Swiper>
                    )}


                </div>
            </div>
        </section>

    );
}


