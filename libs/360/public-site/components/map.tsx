import * as React from 'react';

import { RegionCategoryExtend } from "@lib/360/admin-site/services";
import GoogleMapReact from "google-map-react";
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';
import { IconButton } from '@mui/material';
import { GooglePlaceResult, TravelCategoryDTO, TravelLocationDTO } from '@lib/shared/360';
import { cdn } from '@lib/core/services';
import Image from 'next/image'

const host360 = process.env.NEXT_PUBLIC_360_HOST;


export const GMap = (props: { title?: string, categories?: RegionCategoryExtend[], travelCategories?: TravelCategoryDTO[], center?: any, lat?: number, lng?: number }): React.ReactElement => {

  const { travelCategories } = props;
  const [, setInputValue] = React.useState("");
  const [options, setOptions] = React.useState<GooglePlaceResult[]>([]);


  const [isCheckAll, setIsCheckAll] = useState(false);
  const [isCheck, setIsCheck] = useState<TravelCategoryDTO[]>([]);
  const [makers, setMakers] = useState<TravelLocationDTO[]>([]);

  const [list, setList] = useState<TravelCategoryDTO[]>([]);
  const [state, setState] = useState<any>({
    apiReady: false,
    map: null,
    maps: null
  });

  useEffect(() => {
    setList(travelCategories || []);
    setIsCheckAll(true);
    setIsCheck(travelCategories || []);
    let marks: TravelLocationDTO[] = [];
    travelCategories?.forEach(e => {
      if (e?.travelLocations?.length) {
        marks = [...marks, ...e.travelLocations];
      }
    });
    setMakers(marks);

  }, [travelCategories]);

  useEffect(() => {
    handleFilterMakers();

  }, [isCheck]);

  const handleSelectAll = e => {
    setIsCheckAll(!isCheckAll);
    setIsCheck(list);
    if (isCheckAll) {
      setIsCheck([]);
    }

  };

  const handleFilterMakers = () => {
    let marks: TravelLocationDTO[] = [];
    isCheck?.forEach(e => {
      if (e?.travelLocations?.length) {
        marks = [...marks, ...e.travelLocations];
      }
    });
    setMakers(marks);
  }
  const handleClick = e => {
    const { id, checked } = e.target;
    const find = list?.find(e => e.id === id) as TravelCategoryDTO;
    setIsCheck([...isCheck, find]);
    if (!checked) {
      setIsCheck(isCheck?.filter(item => item.id !== id));
    }
  };

  const { title, categories, lat, lng } = props;
  const defaultProps = {
    center: {
      lat: 21.028511,
      lng: 105.804817
    },
    zoom: 15
  };
  const [cordinates, setCordinates] = useState({
    center: {
      lat: lat,
      lng: lng
    },
  });

  useEffect(() => {
    setCordinates({
      center: {
        lat: lat,
        lng: lng
      },
    });
  }, [props])

  console.log('cordinates', cordinates)

  const catalog = list?.map(({ id, name }) => {
    return (
      <><li>
        <Checkbox
          key={id}
          type="checkbox"
          name={name}
          id={id}
          handleClick={handleClick}
          isChecked={isCheck?.some(e => e.id === id)}
        />
        {name}
      </li>
      </>
    );
  });

  const getMapOptions = (maps) => {

    return {
      gestureHandling: 'cooperative',
      streetViewControl: true,
      scaleControl: false,
      fullscreenControl: true,
      zoomControl: true,
      styles: [{
        featureType: "poi.business",
        elementType: "labels",
        stylers: [{
          visibility: "on"
        }]
      }],
      disableDoubleClickZoom: false,
      mapTypeControl: true,
      mapTypeId: maps.MapTypeId.ROADMAP,
      mapTypeControlOptions: {
        style: maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: maps.ControlPosition.TOP_LEFT,
        mapTypeIds: [
          maps.MapTypeId.ROADMAP,
          maps.MapTypeId.SATELLITE,
        ]
      },
      clickableIcons: true,
    };
  }

  const apiIsLoaded = (map, maps) => {
    setState({
      ...state,
      apiReady: true,
      map: map,
      maps: maps
    })


  };

  const _onClick = (x, y, lat, lng, event) => {

    console.log(x, y, lat, lng, event)

  };


  return (
    <div className="">


      <div className='categoy-map '>
        <div className='cat-map-title'>Danh sách điểm đến</div>
        <ul className="list-none">
          <li>
            <Checkbox
              type="checkbox"
              name="selectAll"
              id="selectAll"
              handleClick={handleSelectAll}
              isChecked={isCheckAll}
            />
            Chọn tất cả
          </li>
          {catalog}
        </ul>
      </div>

      <div style={{ height: '70vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{
            key: process.env.NEXT_PUBLIC_GOOGLE_MAP || 'AIzaSyDFSVU6CbF2R1Ni2aps8sO5X9dVmcPUM4U',
          }}
          defaultCenter={cordinates.center}
          defaultZoom={defaultProps.zoom}

          yesIWantToUseGoogleMapApiInternals
          options={getMapOptions}
          onClick={_onClick}
          onGoogleApiLoaded={({ map, maps }) => apiIsLoaded(map, maps)}
        >
          {makers?.map((e, index) => {
            return (
              // <MyMarker key={id} lat={lat} lng={lng} text={id} tooltip={title} />
              <Marker
                key={e.id}
                lat={e.lat}
                lng={e.lng}
                name={e.name}
                link={e.link}
                content={e.content}
                image={e.image}
                pageId={e.pageId}
                travelCategoryIcon={e.travelCategoryIcon}
              />
            );
          })}


        </GoogleMapReact>
      </div>
    </div>
  );
}


// Marker component
const Marker = (props) => {
  const { name, link, image, pageId, content, travelCategoryIcon } = props;
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <>
      <div>
        <IconButton onClick={handleClick} >
          {/* <Image src={`/admin/${travelCategoryIcon}.svg`} alt="photo" width={24} height={34} /> */}
          <img className="h-maker-icon" src={`/admin/icons/${travelCategoryIcon || 'ATM'}.svg`} alt="photo" width="24" height="34" />

          {/* <Image src={`/admin/icons/${travelCategoryIcon}.svg`} alt="photo" width="24" height="34" /> */}
        </IconButton>

        <Popover
          id={id}
          className="wrap-dialog-map"
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <div id='map'><div id="content">
            <div className="cover">
              <img className='entry-img-h152' src={cdn(image)} alt="" />
            </div>
            <div id="siteNotice"></div>
            <h1 id="firstHeading" className="firstHeading">
              <a href={link || `/noi-dung/${pageId}`} target={'_blank'}>{name}</a>
            </h1>
            <div id="bodyContent">
              <p className="description">
                {content}
              </p>
            </div>
          </div>
          <div className='map-entry-arrow'></div>
          </div>
        </Popover>
      </div>
      {/* <InfoWindow  /> */}
    </>
  );
};

export const Checkbox = ({ id, type, name, handleClick, isChecked }) => {
  return (
    <input
      id={id}
      name={name}
      type={type}
      onChange={handleClick}
      checked={isChecked}
    />
  );
};

