import { cdn } from "@lib/core/services";
import { PostDTO } from "@lib/shared/360";
import { Pagination, Stack, styled, Tooltip, tooltipClasses, TooltipProps } from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import React, { useEffect, useState } from "react";

const NoMaxWidthTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
))({
    [`& .${tooltipClasses.tooltip}`]: {
        maxWidth: 800,
    },
});


export interface RssPostDTO {
    title: string;
    link: string;
    description?: string;
    content?: string;
    pubDate?: string;
    contentSnippet?: string;
    image?: {
        url?: string;
        title: string;
        link: string;
    };
}

export const ListPostRssLayout1 = (props: { title?: string, articles?: any[] }): React.ReactElement => {
    const { title, articles } = props;
    const [posts, setPosts] = useState<RssPostDTO[] | undefined>([]);
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(6);

    useEffect(() => {
        setPosts(articles);
        
    }, []);

    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setPage(value);
    };

    return (
        <div>
            <div className="mt-8">
                {articles?.map((post, index) => {
                    if (((index + 1) <= (pageSize * page)) && ((index + 1) > (pageSize * page - pageSize))) {
                        return (

                            <div  key={index} className="media shadow-f1 mb-16 rounded-8 ">
                                <div className="entry-thumb-rss  mr-16" style={{maxWidth: 243}}>
                                    <div className="rss-content" dangerouslySetInnerHTML={{ __html: post.content || '' }}></div>

                                    
                                </div>
                                <div className="media-body entry-content">
                                    <div className="text-18 font-bold mb-10 entry-quote-2 entry-title"><Link className="pointer" key={index} href={post.link} target="_blank"><a className="pointer">{post.title}</a></Link></div>
                                    <div className="text-14 text-gray-101 mb-10">{Date.toFormat(post?.pubDate, 'dd/MM/yyyy')}</div>

                                    <NoMaxWidthTooltip title={post.contentSnippet}>
                                        <div className="text-14 mb-20 entry-quote-2">{post.contentSnippet}</div>
                                    </NoMaxWidthTooltip>


                                </div>

                            </div>


                        )
                    }

                })}


            </div>
            <div className="d-flex justify-content-end">
                <Stack spacing={2}>
                    <Pagination count={Math.ceil(articles?.length || 0 / pageSize)} color="primary" page={page} onChange={handleChange} />
                </Stack>
            </div>
        </div>

    );
}


