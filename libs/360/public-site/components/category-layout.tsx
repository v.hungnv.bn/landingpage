import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';
import { PostService } from "@lib/360/admin-site/services";
import { CategoryDTO } from "@lib/shared/360";

const host360 = process.env.NEXT_PUBLIC_360_HOST;

export default function CategoryLayout(): React.ReactElement {
    const [categories, setCategories] = useState<CategoryDTO[]>([]);
    const [expanded, setExpanded] = useState<string[]>([]);

    const router = useRouter();
    const { slug } = router.query;

    useEffect(() => {
        getAllCategory();

    }, []);

    useEffect(() => {
        if (typeof slug === 'string' && categories.length) {
            const cat = findNode(slug);
            if (cat?.parentId) {
                findParent(cat);
            }
        }
    }, [router.query, slug, categories]);

    const findNode = (slug) => {
        const node = categories?.find(e => e.slug === slug);
        if (node) {
            return node;
        } else {
            for (let i = 0; i < categories.length; i++) {
                const nodeF1 = categories[i]?.children.find(f1 => f1.slug === slug);
                if (nodeF1) {
                    return nodeF1;
                } else {
                    for (let j = 0; j < categories[i]?.children.length; i++) {
                        const nodeF2 = categories[i]?.children[j].children.find(f2 => f2.slug === slug);
                        if (nodeF2) {
                            return nodeF2;
                        }
                    }
                }
            }

        }

    }

    const findParent = (item: CategoryDTO) => {
        categories?.forEach(e => {

            if (item.parentId === e.id) {
                setExpanded([e.slug]);
            } else {
                let exps: string[] = [];
                e.children.forEach(f => {
                    if (item.parentId === f.id) {
                        exps = [e.slug];
                        exps = [...exps, f.slug];
                        setExpanded(exps);
                    }
                })
            }

        })

    }

    const getAllCategory = async () => {
        try {
            const res = await PostService.getAllCategory();
            setCategories(res);


        } catch (err) {
            console.log(err)
        } finally {

        }
    }

    const handleToggle = (event, nodeIds) => {
        event.preventDefault();
        if (event.target.closest(".MuiTreeItem-iconContainer")) {
            setExpanded(nodeIds);
        }

    };

    const handleSelect = (event, nodeId) => {
        event.preventDefault();
        if (event.target.closest(".MuiTreeItem-label")) {
            const href = (nodeId && nodeId !== '8585c953-a815-42b8-8dd5-99fb64a43d55') ? `/tin-tuc-su-kien/${nodeId}` : `/tin-tuc-su-kien`;
            router.push(href);
        }

    };

    const renderTreeItem = (treeItems) => {
        if (!treeItems?.length) {
            return null;
        }
        return treeItems.map(treeItem => {
            return (
                <TreeItem
                    className="pointer"
                    key={treeItem.slug}
                    nodeId={treeItem.slug}
                    label={treeItem.name}
                >
                    {
                        renderTreeItem(treeItem.children)
                    }

                </TreeItem>
            )
        })
    }

    return (

        <div >
            <div className="category-tree-component mt-8">
                <div className="category-title">Chuyên mục </div>
                <TreeView
                    aria-label="file system navigator"
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpandIcon={<ChevronRightIcon />}
                    selected={[typeof slug === 'string' ? slug : '8585c953-a815-42b8-8dd5-99fb64a43d55']}
                    defaultExpanded={expanded}
                    expanded={expanded}
                    onNodeToggle={handleToggle}
                    onNodeSelect={handleSelect}
                >
                    <TreeItem
                        className="pointer"
                        nodeId={'8585c953-a815-42b8-8dd5-99fb64a43d55'}
                        label={'Tất cả'}>
                    </TreeItem>
                    {renderTreeItem(categories)}

                </TreeView>
            </div>
        </div>
    );
}




