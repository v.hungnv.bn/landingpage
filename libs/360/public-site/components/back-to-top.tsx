import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import classNames from 'classnames';
import { debounce } from 'lodash';
import { useEffect, useState } from 'react';

function BackToTopButton() {
  const [isShow, setIsShow] = useState(false);

  useEffect(() => {
    const handleScrollListener = debounce((e) => {
      const currentScrollY = window.scrollY; // vị trí scroll hiện tại
      const windowHeight = window.innerHeight; // chiều cao có thể hiển thị content của browser
      const totalScrollHeight = document.body.scrollHeight; // tổng chiều cao của toàn bộ nội dung web (tương đương chiều cao thẻ body)

      /*
        Nếu scroll đang ở vị trí <= 100px đầu trang hoặc
        scroll đang ở vị trí >= 20px cuối trang thì sẽ cho ẩn
        button tránh che nội dung người đọc
      */
      if (
        currentScrollY <= 100 ||
        currentScrollY >= totalScrollHeight - windowHeight - 20
      ) {
        setIsShow(false);
        return;
      }

      setIsShow(true);
    }, 100);

    window.addEventListener('scroll', handleScrollListener);

    return () => {
      window.removeEventListener('scroll', handleScrollListener);
    };
  }, []);

  const handleClick = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  return (
    <div
      className={classNames(
        'h-back-top fixed bottom-4 right-4 z-10  flex items-center justify-center  cursor-pointer rounded-full ring-2 ring-white/30 transition-opacity duration-200',
        {
          'opacity-0 pointer-events-none': !isShow,
        },
      )}
      onClick={handleClick}
    >
      <div className="text-white  mr-6 ">
        <ArrowUpwardIcon />
      </div>
      <div className='text-white text-shadow '>Quay lại đầu trang</div>
    </div>
  );
}

export default BackToTopButton;
