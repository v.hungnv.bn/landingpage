// import Container from "@components/container";
// import ThemeSwitch from "@components/themeSwitch";

import React from "react";
import { BreadCrumbService } from "../services/breadcrumb.service";
import Link from "next/link";



// export const Breadcrumb = (props: { title?: string, pageType?: MenuType, idOrSlug?: string, slugName?: string }): React.ReactElement => {
//   const { title, pageType, idOrSlug, slugName } = props;
//   const router = useRouter();
//   const { slug } = router.query;

//   useEffect(() => {

// }, [router.query]);


//   return (
//     <div className="section-pageheader">
//       <div className="container-fluid">
//         <div className="text-32 font-bold mb-12">{title}</div>
//         <div className="text-14 font-bold mb-12">
//           <Link href={`/`}>
//             <a className="text-white pointer">Home / </a>
//           </Link>
//           {pageType === MenuType.DOCUMENT && (
//             <Link href={idOrSlug ? '/van-ban' : '/van-ban'}>
//               <a className="text-white pointer"> Văn bản </a>
//             </Link>
//           )}
//           {pageType === MenuType.SCHEDULE && (
//             <Link href={'/lich-cong-tac'}>
//               <a className="text-white pointer"> Lịch công tác </a>
//             </Link>
//           )}
//           {pageType === MenuType.QUESTION && (
//             <Link href={'/hoi-dap'}>
//               <a className="text-white pointer"> Hỏi đáp </a>
//             </Link>
//           )}
//           {pageType === MenuType.ORGANIZATION && (
//             <Link href={'/about'}>
//               <a className="text-white pointer"> Cơ cấu tổ chức </a>
//             </Link>
//           )}
//           {pageType === MenuType.CATEGORY && (
//             <>
//               {!!slugName ? (
//                 <>
//                   <Link href={`/tin-tuc-su-kien/${idOrSlug}`}>
//                     <a className="text-white pointer"> {slugName} </a>
//                   </Link>
//                 </>
//               ) : (
//                 <>
//                   <Link href={'/tin-tuc-su-kien'}>
//                     <a className="text-white pointer"> Tin tức sự kiện </a>
//                   </Link>
//                 </>
//               )}
//             </>

//           )}
//           {/* Home / Tin tức - sự kiện */}
//         </div>

//       </div>
//     </div>
//   );
// }


class Breadcrumb extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      subTitle: '',
      slug: ''
    };
  }

  componentDidMount() {
    this.subscription = BreadCrumbService.onLoader()
      .subscribe((info) => {
        this.setState({
          title: info.title,
          subTitle: info.subTitle,
          slug: info.slug
        });

      });
  }

  componentWillUnmount() {
    this.subscription.unsubscribe();
  }

  reset() {
    this.setState(null)
  }

  render() {
    return (
      <div className="section-pageheader">
        <div className="container-fluid">
          <div className="text-18 md:text-32 font-bold mb-12">{this.state.title}</div>
          <div className="text-14 font-bold mb-12">
            <Link href={`/`}>
              <a className="text-white pointer">Home / </a>
            </Link>
            {this.state.slug ? (
              <Link href={this.state.slug}>
                <a className="text-white pointer"> {this.state.subTitle} </a>
              </Link>
            ) : (
              <a className="text-white pointer"> {this.state.subTitle} </a>
            )}

            {/* {pageType === MenuType.DOCUMENT && (
              <Link href={idOrSlug ? '/van-ban' : '/van-ban'}>
                <a className="text-white pointer"> Văn bản </a>
              </Link>
            )}
            {pageType === MenuType.SCHEDULE && (
              <Link href={'/lich-cong-tac'}>
                <a className="text-white pointer"> Lịch công tác </a>
              </Link>
            )}
            {pageType === MenuType.QUESTION && (
              <Link href={'/hoi-dap'}>
                <a className="text-white pointer"> Hỏi đáp </a>
              </Link>
            )}
            {pageType === MenuType.ORGANIZATION && (
              <Link href={'/about'}>
                <a className="text-white pointer"> Cơ cấu tổ chức </a>
              </Link>
            )}
            {pageType === MenuType.CATEGORY && (
              <>
                {!!slugName ? (
                  <>
                    <Link href={`/tin-tuc-su-kien/${idOrSlug}`}>
                      <a className="text-white pointer"> {slugName} </a>
                    </Link>
                  </>
                ) : (
                  <>
                    <Link href={'/tin-tuc-su-kien'}>
                      <a className="text-white pointer"> Tin tức sự kiện </a>
                    </Link>
                  </>
                )}
              </>

            )} */}
            {/* Home / Tin tức - sự kiện */}
          </div>

        </div>
      </div>
    );
  }
}


export { Breadcrumb };