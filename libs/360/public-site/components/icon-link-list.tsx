import { cdn } from "@lib/core/services";
import { RegionData } from "@lib/shared/360";
import Image from "next/image";
import Link from "next/link";
const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const IconLinkList = (props: { title: string, data?: RegionData[] }): React.ReactElement => {
    const { title, data } = props;

    return (
        <section className="section-shortcut-link my-25">
            <div className="container">
                <div className="text-24 text-center mb-20 text-blue-100 font-bold heading-line d-flex justify-content-center">{title}</div>
                <div className="content-wrap mt-60">
                    <div className="d-flex align-content-center justify-content-center flex-wrap bd-highlight mb-3">
                        {data?.map((e, index) => (
                            <a key={index} className="entry-item mb-16" href={e.link || '#'} target={'_blank'}>
                                <div className="into-icon mb-6">
                                    <img src={cdn(e.image)} />
                                </div>
                                <div className="intro-title text-16 text-white " >{e.title}</div>

                            </a>
                        ))}



                    </div>

                </div>
            </div>
        </section>

    );
}


