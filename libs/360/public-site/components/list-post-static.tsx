import { cdn } from "@lib/core/services";
import { PostDTO } from "@lib/shared/360";
import { Pagination, Stack, styled, Tooltip, tooltipClasses, TooltipProps } from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import React, { useEffect, useState } from "react";

const hostUser = process.env.NEXT_PUBLIC_USER_HOST;


const NoMaxWidthTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
))({
    [`& .${tooltipClasses.tooltip}`]: {
        maxWidth: 800,
    },
});

export const ListPostStaticLayout1 = (props: { title?: string, articles?: PostDTO[] }): React.ReactElement => {
    const { title, articles } = props;
    const [posts, setPosts] = useState<PostDTO[] | undefined>([]);
    const [page, setPage] = useState<number>(1);
    const [pageSize, setPageSize] = useState<number>(6);

    useEffect(() => {
        setPosts(articles);
    }, []);



    return (
        <div>
            <div className="mt-8">
                {articles?.map((post, index) => {
                    return (

                        <div key={post.id} className="media shadow-f1 mb-16 rounded-8 pointer">
                            <div className="entry-thumb mr-16">
                                <Link href={`/bai-viet/${post.slug || post.id}`}>
                                    <a className="pointer">
                                        <img className="entry-thumb-243-156" src={cdn(post.image) || '/front/no-image.png'} alt="photo" />
                                    </a>
                                </Link>
                            </div>
                            <div className="media-body entry-content">
                                <div className="text-18 font-bold mb-10 entry-quote-2 entry-title">
                                    <Link href={`/bai-viet/${post.slug || post.id}`}><a className="pointer">{post.name}</a>
                                    </Link>
                                </div>
                                <div className="text-14 text-gray-101 mb-10">{Date.toFormat(post.publishDate, 'dd/MM/yyyy')}</div>

                                <NoMaxWidthTooltip title={post.quote}>
                                    <div className="text-14 mb-20 entry-quote-2">{post.quote}</div>

                                </NoMaxWidthTooltip>
                                <div className="entry-author media align-items-center">
                                    <div className="entry-avatar mr-6">
                                        <Image
                                            src={post?.creatorAvatar ? `${hostUser + 'aws/' + post?.creatorAvatar}` : "/front/entry/avatar.png"}
                                            alt="logo" width="26" height="26" />
                                    </div>
                                    <div className="text-13 font-bold media-body">{post?.creator}</div>

                                </div>

                            </div>

                        </div>

                    )

                })}


            </div>

        </div>

    );
}


