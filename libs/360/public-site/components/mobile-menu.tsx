
import * as React from 'react';

import { useState } from "react";
import { MenuDTOExtend } from "@lib/360/admin-site/services";
import { ConfigurationDTO } from "@lib/shared/360";
import { useRouter } from "next/router";

import Box from '@mui/material/Box';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import { Collapse, IconButton } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { ExpandLess, ExpandMore } from '@mui/icons-material';

type Anchor = 'top' | 'left' | 'bottom' | 'right';

interface Props {
    logo?: string;
    copyright?: string;
    description?: string;
    menus?: MenuDTOExtend[];
    configSite?: ConfigurationDTO
}
const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const MobileMenu = (props: Props): React.ReactElement => {

    const { menus, configSite } = props;
    const [data, setData] = useState<MenuDTOExtend[]>([]);

    const router = useRouter();
    const { slug } = router.query;

    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    React.useEffect(() => {
        if (menus) {
            setData(menus);
        }
    }, [props]);

    const handleRouter = (menu: MenuDTOExtend) => {

        if (menu.link) {
            router.push(menu.link);
        }
        setState({ ...state, ['right']: false });
    };

    const toogleCollapse = (menu: MenuDTOExtend) => {
        const listmenu = data?.map(e => {
            if (e.referenceId === menu.referenceId) {
                e.open = !e.open;
            }
            return e;
        })
        setData(listmenu);
    };

    const toggleDrawer =
        (anchor: Anchor, open: boolean) =>
            (event: React.KeyboardEvent | React.MouseEvent) => {
                if (
                    event &&
                    event.type === 'keydown' &&
                    ((event as React.KeyboardEvent).key === 'Tab' ||
                        (event as React.KeyboardEvent).key === 'Shift')
                ) {
                    return;
                }

                setState({ ...state, [anchor]: open });
            };

    const list = (anchor: Anchor) => (
        <Box
            className='BoxBox'
            sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 300 }}
            role="presentation"
        // onClick={toggleDrawer(anchor, false)}
        // onKeyDown={toggleDrawer(anchor, false)}
        >
            <List>
                {data?.map((menu, index) => (
                    <ListItem key={menu.referenceId} disablePadding>
                        <ListItemButton className='d-flex flex-column justify-content-start align-items-start' >
                            <div className='d-flex justify-content-between w-100 align-items-center'>
                                <ListItemText onClick={() => handleRouter(menu)} primary={menu.title} />
                                {menu.children && menu.children.length > 0 && (
                                    <>{!!menu.open ? <ExpandLess onClick={() => toogleCollapse(menu)} /> : <ExpandMore onClick={() => toogleCollapse(menu)} />}</>
                                )}
                            </div>

                            {menu.children && menu.children.length > 0 && (
                                <Collapse in={menu.open} timeout="auto" unmountOnExit>
                                    <List component="div" disablePadding>
                                        {menu?.children?.map((child, idx) => (
                                            <ListItemButton key={child.referenceId} sx={{ pl: 4 }}>
                                                <ListItemText onClick={() => handleRouter(child)} primary={child.title} />
                                            </ListItemButton>
                                        ))}

                                    </List>
                                </Collapse>
                            )}

                        </ListItemButton>


                    </ListItem>
                ))}
            </List>
        </Box>
    );
    return (
        <div>
            <React.Fragment key={'right'}>
                <IconButton className="d-lg-none d-inline-block" aria-label="" onClick={toggleDrawer('right', true)}>
                    <MenuIcon />
                </IconButton>
                <SwipeableDrawer
                    anchor={'right'}
                    open={state['right']}
                    onClose={toggleDrawer('right', false)}
                    onOpen={toggleDrawer('right', true)}
                    disableScrollLock={true}
                >
                    {list('right')}
                </SwipeableDrawer>
            </React.Fragment> 
        </div>

    );
}

