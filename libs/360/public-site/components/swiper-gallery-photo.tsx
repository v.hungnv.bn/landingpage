import Image from "next/image";
import Link from "next/link";
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from 'swiper';
import 'swiper/css';
import { useEffect, useState } from "react";
import { MediaService } from "@lib/360/admin-site/services";
import { MediaDTO, MediaType } from "@lib/shared/360";
import { cdn } from "@lib/core/services";

export const SwiperGalleryPhoto = (props: { title?: string}): React.ReactElement => {
    const { title } = props;
    const [photos, setPhotos] = useState<MediaDTO[]>([]);

    useEffect(() => {
        getMedias();
    }, []);


    const getMedias = async () => {
        try {
            const res = await MediaService.allPublic(MediaType.IMAGE);
            setPhotos(res);
        } catch (err) {
            console.log(err)
        } finally {

        }
    }
    return (
        <div className="container-fluid section-gallery bg-photo mb-40">
            <div className="text-24 text-center mb-0 text-white font-bold pt-10 heading-line-white d-flex justify-content-center">{title || 'Thư viện hình ảnh'} </div>

            <div className="bk-slider ">
                <Swiper
                    modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
                    initialSlide={1}
                    spaceBetween={0}
                    slidesPerView={1}
                    navigation
                    onSlideChange={() => console.log('slide change')}
                    onSwiper={(swiper) => console.log(swiper)}
                    centeredSlides={true}
                    autoplay={{delay: 2000}}
                    loop={true}
                    breakpoints={{
                        768: {
                          slidesPerView: 3,
                        },
                    }}
                >
                    {photos?.map((e: MediaDTO, index: number) => (
                        <SwiperSlide key={e.id}>
                            <div className="entry-item">
                                <Image
                                    src={cdn(e.url)}
                                    alt="logo" width="608" height="345"
                                />

                            </div>

                        </SwiperSlide>
                    ))}


                </Swiper>
            </div>

        </div>

    );
}


