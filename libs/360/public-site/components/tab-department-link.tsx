import Image from "next/image";
import Link from "next/link";
import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { LinkedCategoryService } from "@lib/360/admin-site/services";
import { LinkedCategoryDTO, LinkedDepartmentDTO } from "@lib/shared/360";


function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}


export const TabDepartmentLink = (props: { title?: string }): React.ReactElement => {
    const { title } = props;
    const [value, setValue] = React.useState(0);
    const [tabs, setTabs] = React.useState<LinkedCategoryDTO[]>([]);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    React.useEffect(() => {
        getDepartmentLinks();
    }, []);

    const getDepartmentLinks = async (catId?: string) => {
        try {
            const res = await LinkedCategoryService.allPublish();
            setTabs(res);
        } catch (err) {
            console.log(err)
        } finally {
        }
    }


    return (
        <div className="container-fluid mb-40">
            <div className="text-24 text-center mb-20 text-blue-100 font-bold">{title || 'Đơn vị liên kết'} </div>

            <Box sx={{ width: '100%' }}>
                <Box >
                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" centered variant="scrollable"
                        allowScrollButtonsMobile>
                        {tabs.map((e: LinkedCategoryDTO, index: number) => (
                            <Tab key={e.id} label={e.name} {...a11yProps(index)} />
                        ))}
                    </Tabs>
                </Box>

                {tabs.map((e: LinkedCategoryDTO, index: number) => (
                    <TabPanel key={e.id} value={value} index={index}>
                        <div className="department-wrapper">
                            <ul className="list-none d-flex p-0  flex-wrap">
                                {e.linedDepartments?.map((child: LinkedDepartmentDTO, index: number) => (
                                    <li key={child.id} className="mb-12">

                                        <a className="text-14" target="_blank" href={child.link} rel="noopener noreferrer">{child.name}</a>
                                    </li>
                                ))}

                            </ul>
                        </div>


                    </TabPanel>
                ))}

            </Box>

        </div>
    );
}


