import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { ApiService } from '@lib/core/services';
import { DomainService } from '@lib/admin/services';


interface Props {
  children?: any;
  routerDefault?: string;
}

function RouteSiteGuard({ children, routerDefault }: Props) {
  const router = useRouter();
  const [authorized, setAuthorized] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  // const [departmentCode, setDepartmentCode] = useState('DAKNONG-1-01');
  const [departmentCode, setDepartmentCode] = useState('DAKNONG-3-07');

  useEffect(() => {
    domainByHost();
  }, []);

  const domainByHost = async () => {
    setIsLoading(true);
    try {
      const domain = await DomainService.byHost();
      if (domain?.departmentCode) {
        setDepartmentCode(domain?.departmentCode);
        ApiService.setDepartmentCode(domain?.departmentCode);

      } else {
        ApiService.setDepartmentCode(departmentCode);

      }
      // setIsLoading(false);
    } finally {
      setIsLoading(false);
    }

  }


  return (!isLoading && ApiService.departmentCode && children);
}

export { RouteSiteGuard };