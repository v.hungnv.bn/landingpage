import Image from "next/image";
import Link from "next/link";
import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';


function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}


export const TabNewsSchool = (): React.ReactElement => {
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };


    return (
        <div className="container-fluid tab-new mb-40">
            <div className="text-24 text-center mb-20 text-blue-100 font-bold">Tin tức trường học </div>

            <Box sx={{ width: '100%' }}>
                <Box >
                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" centered>
                        <Tab label="THPT" {...a11yProps(0)} />
                        <Tab label="THCS" {...a11yProps(1)} />
                        <Tab label="TIỂU HỌC" {...a11yProps(2)} />
                        <Tab label="MẦM NON" {...a11yProps(3)} />

                    </Tabs>
                </Box>
                <TabPanel value={value} index={0}>
                    <div>
                        <ul className="list-none d-flex tab-new-wrap">
                            <li>
                                <div className="entry-item" style={{ width: 305 }}>
                                    <div className="entry-thumb mb-8">
                                        <Link href={`/`}>
                                            <Image
                                                src="https://znews-photo.zingcdn.me/w960/Uploaded/gtntnn/2022_07_21/NTH_9078.jpg"
                                                alt="logo" width="305" height="184"
                                            />
                                        </Link>
                                    </div>
                                    <div className="entry-content">
                                        <div className="entry-title text-18 mb-10">
                                            <Link href={`/`}>
                                                <a className="text-18">Bộ trưởng Nguyễn Kim Sơn dự Lễ kỷ niệm Quốc khánh Tây Ban Nha</a>
                                            </Link>
                                        </div>
                                        <div className="entry-date text-14 mb-10">31/10/2022</div>
                                        <div className="entry-text text-14">
                                            Tối 27/10, Bộ trưởng Bộ Giáo dục và Đào tạo Nguyễn Kim Sơn thay mặt Chính phủ Việt Nam tới dự và phát biểu tại Lễ kỷ niệm Quốc khánh Vương quốc Tây Ban Nha và 45 năm thiết lập quan hệ ngoại giao Việt Nam ...
                                        </div>
                                    </div>

                                </div>
                            </li>

                            <li>
                                <div className="entry-item" style={{ width: 305 }}>
                                    <div className="entry-thumb mb-8">
                                        <Link href={`/`}>
                                            <Image
                                                src="https://znews-photo.zingcdn.me/w1920/Uploaded/mdf_uswreo/2022_12_02/manga_football_japan_nss_12.jpg"
                                                alt="logo" width="305" height="184"
                                            />
                                        </Link>
                                    </div>
                                    <div className="entry-content">
                                        <div className="entry-title text-18 mb-10">
                                            <Link href={`/`}>
                                                <a className="text-18">Bộ trưởng Nguyễn Kim Sơn dự Lễ kỷ niệm Quốc khánh Tây Ban Nha</a>
                                            </Link>
                                        </div>
                                        <div className="entry-date text-14 mb-10">31/10/2022</div>
                                        <div className="entry-text text-14">
                                            Tối 27/10, Bộ trưởng Bộ Giáo dục và Đào tạo Nguyễn Kim Sơn thay mặt Chính phủ Việt Nam tới dự và phát biểu tại Lễ kỷ niệm Quốc khánh Vương quốc Tây Ban Nha và 45 năm thiết lập quan hệ ngoại giao Việt Nam ...
                                        </div>
                                    </div>

                                </div>
                            </li>

                            <li>
                                <div className="entry-item" style={{ width: 305 }}>
                                    <div className="entry-thumb mb-8">
                                        <Link href={`/`}>
                                            <Image
                                                src="https://znews-photo.zingcdn.me/w960/Uploaded/aohunkx/2021_08_11/atm_agribank_1.jpg"
                                                alt="logo" width="305" height="184"
                                            />
                                        </Link>
                                    </div>
                                    <div className="entry-content">
                                        <div className="entry-title text-18 mb-10">
                                            <Link href={`/`}>
                                                <a className="text-18">Bộ trưởng Nguyễn Kim Sơn dự Lễ kỷ niệm Quốc khánh Tây Ban Nha</a>
                                            </Link>
                                        </div>
                                        <div className="entry-date text-14 mb-10">31/10/2022</div>
                                        <div className="entry-text text-14">
                                            Tối 27/10, Bộ trưởng Bộ Giáo dục và Đào tạo Nguyễn Kim Sơn thay mặt Chính phủ Việt Nam tới dự và phát biểu tại Lễ kỷ niệm Quốc khánh Vương quốc Tây Ban Nha và 45 năm thiết lập quan hệ ngoại giao Việt Nam ...
                                        </div>
                                    </div>

                                </div>
                            </li>

                            <li>
                                <div className="entry-item" style={{ width: 305 }}>
                                    <div className="entry-thumb mb-8">
                                        <Link href={`/`}>
                                            <Image
                                                src="https://znews-photo.zingcdn.me/w660/Uploaded/sotntb/2022_12_01/Anh_3_Hoai_Nhon_gui_zing_1.jpg"
                                                alt="logo" width="305" height="184"
                                            />
                                        </Link>
                                    </div>
                                    <div className="entry-content">
                                        <div className="entry-title text-18 mb-10">
                                            <Link href={`/`}>
                                                <a className="text-18">Bộ trưởng Nguyễn Kim Sơn dự Lễ kỷ niệm Quốc khánh Tây Ban Nha</a>
                                            </Link>
                                        </div>
                                        <div className="entry-date text-14 mb-10">31/10/2022</div>
                                        <div className="entry-text text-14">
                                            Tối 27/10, Bộ trưởng Bộ Giáo dục và Đào tạo Nguyễn Kim Sơn thay mặt Chính phủ Việt Nam tới dự và phát biểu tại Lễ kỷ niệm Quốc khánh Vương quốc Tây Ban Nha và 45 năm thiết lập quan hệ ngoại giao Việt Nam ...
                                        </div>
                                    </div>

                                </div>
                            </li>

                            <li>
                                <div className="entry-item" style={{ width: 305 }}>
                                    <div className="entry-thumb mb-8">
                                        <Link href={`/`}>
                                            <Image
                                                src="https://znews-photo.zingcdn.me/w360/Uploaded/ofh_huqfztmf/2022_11_24/2022_11_23T152511Z_54035492_UP1EIBN151UTE_RTRMADP_3_SOCCER_WORLDCUP_GER_JPN_REPORT_1.JPG"
                                                alt="logo" width="305" height="184"
                                            />
                                        </Link>
                                    </div>
                                    <div className="entry-content">
                                        <div className="entry-title text-18 mb-10">
                                            <Link href={`/`}>
                                                <a className="text-18">Bộ trưởng Nguyễn Kim Sơn dự Lễ kỷ niệm Quốc khánh Tây Ban Nha</a>
                                            </Link>
                                        </div>
                                        <div className="entry-date text-14 mb-10">31/10/2022</div>
                                        <div className="entry-text text-14">
                                            Tối 27/10, Bộ trưởng Bộ Giáo dục và Đào tạo Nguyễn Kim Sơn thay mặt Chính phủ Việt Nam tới dự và phát biểu tại Lễ kỷ niệm Quốc khánh Vương quốc Tây Ban Nha và 45 năm thiết lập quan hệ ngoại giao Việt Nam ...
                                        </div>
                                    </div>

                                </div>
                            </li>

                            <li>
                                <div className="entry-item" style={{ width: 305 }}>
                                    <div className="entry-thumb mb-8">
                                        <Link href={`/`}>
                                            <Image
                                                src="https://znews-photo.zingcdn.me/w360/Uploaded/ofh_huqfztmf/2022_11_24/2022_11_23T152511Z_54035492_UP1EIBN151UTE_RTRMADP_3_SOCCER_WORLDCUP_GER_JPN_REPORT_1.JPG"
                                                alt="logo" width="305" height="184"
                                            />
                                        </Link>
                                    </div>
                                    <div className="entry-content">
                                        <div className="entry-title text-18 mb-10">
                                            <Link href={`/`}>
                                                <a className="text-18">Bộ trưởng Nguyễn Kim Sơn dự Lễ kỷ niệm Quốc khánh Tây Ban Nha</a>
                                            </Link>
                                        </div>
                                        <div className="entry-date text-14 mb-10">31/10/2022</div>
                                        <div className="entry-text text-14">
                                            Tối 27/10, Bộ trưởng Bộ Giáo dục và Đào tạo Nguyễn Kim Sơn thay mặt Chính phủ Việt Nam tới dự và phát biểu tại Lễ kỷ niệm Quốc khánh Vương quốc Tây Ban Nha và 45 năm thiết lập quan hệ ngoại giao Việt Nam ...
                                        </div>
                                    </div>

                                </div>
                            </li>
                            
                        
                        </ul>
                    </div>


                </TabPanel>
                <TabPanel value={value} index={1}>
                    Item Two
                </TabPanel>
                <TabPanel value={value} index={2}>
                    Item Three
                </TabPanel>
                <TabPanel value={value} index={3}>
                    Item Three
                </TabPanel>
            </Box>

        </div>
    );
}


