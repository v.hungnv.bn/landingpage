import { cdn } from "@lib/core/services";
import { RegionData } from "@lib/shared/360";
import Image from "next/image";
import Link from "next/link";
import { Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const BlogDuLich = (props: { title?: string, data?: RegionData[] }): React.ReactElement => {
    const { title, data } = props;

    return (
        <section className="section-blog-dulich my-25">
            <div className="container-fluid">
                <div className="text-24 text-center mb-20 text-blue-100 font-bold heading-line d-flex justify-content-center">{title}</div>

                <div className="d-flex align-items-center justify-content-center">
                    {data?.length && data?.length > 0 && (
                        <Swiper
                            modules={[Autoplay]}
                            initialSlide={1}
                            direction={'horizontal'}
                            slidesPerView={"auto"}
                            spaceBetween={34}
                            autoplay={{ delay: 5000 }}
                            loopedSlides={data.length}
                            loop={true}
                            className=""
                        >
                            {data?.map((e, index: number) => (
                                <SwiperSlide key={index} className="rounded-8" style={{ width: 234 }}>
                                    <a className="entry-item relative pointer" >
                                        <div className="entry-thumb box-thumb">
                                            <img className="entry-thumb-308-234" src={cdn(e.image)} alt="" />
                                        </div>
                                        <div className="entry-title text-18 p-5 entry-quote-3 absolute">
                                            <a className="text-18" href={e.link} target={'_blank'}>{e.title}</a>
                                        </div>
                                    </a>

                                </SwiperSlide>
                            ))}


                        </Swiper>
                    )}




                </div>
            </div>
        </section>

    );
}


