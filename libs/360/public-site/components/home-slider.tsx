import Image from "next/image";

import { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';



// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import { ApiService, cdn } from "@lib/core/services";
import { useEffect, useState } from "react";
import { BannerDTOExtend, BannerService } from "@lib/360/admin-site/services";
import { BannerType, ConfigurationTemplate } from "@lib/shared/360";
// import 'swiper/css/scrollbar';

const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const getIdUrlYoutube = (url: string) => {
    let video_id = url?.split('v=')[1];
    const ampersandPosition = video_id?.indexOf('&');
    if (ampersandPosition != -1) {
        video_id = video_id?.substring(0, ampersandPosition);
    }
    return video_id;
}


export const HomeSlider = (): React.ReactElement => {
    const [banners, setBanners] = useState<BannerDTOExtend[]>([]);
    const template = localStorage.getItem('TEMPLATE');

    const config = {
        autoRotate: -2
    };

    useEffect(() => {
        getAllBanner();
    }, []);

    const getAllBanner = async () => {
        try {
            const res = await BannerService.getAllBanner();
            console.log('banner', res);
            setBanners(res);

        } catch (err) {
            console.log(err);
        } finally {
        }
    }


    return (
        <div className="home-slider">
            {/* <iframe width="100%" height="800" allowFullScreen src="https://cdn.pannellum.org/2.5/pannellum.htm#panorama=https://pannellum.org/images/cerro-toco-0.jpg&amp;autoLoad=true&amp;autoRotate=-2"></iframe> */}

            <Swiper
                // install Swiper modules
                modules={[Navigation, Pagination, Scrollbar, A11y]}

                spaceBetween={0}
                slidesPerView={1}
                navigation
                pagination={{ clickable: true, enabled: template === ConfigurationTemplate.TEMPLATE1 }}
                onSwiper={(swiper) => console.log(swiper)}
                onSlideChange={() => console.log('slide change')}
            >
                {banners?.length > 0 && (
                    <>
                        {banners?.map((item, index) => {

                            switch (item.type) {
                                case BannerType.IMAGE360:
                                    return (
                                        <SwiperSlide key={item.id} className="" >
                                            <iframe width="100%" height="800" allowFullScreen src={`https://cdn.pannellum.org/2.5/pannellum.htm#panorama=${cdn(item.url)}&autoLoad=true&autoRotate=-2`}></iframe>
                                        </SwiperSlide>
                                    )
                                case BannerType.YOUTUBE:
                                    return (
                                        <SwiperSlide key={item.id} className="">
                                            <div className="intro-slider" style={{ 'backgroundImage': `url(https://img.youtube.com/vi/${getIdUrlYoutube(item.url)}/maxresdefault.jpg)` }}>
                                                <div className="entry-info text-center">
                                                    <div className="text-32 font-bold mb-46">{item.name}</div>
                                                    <a className="button-link bg-blue-100 rounded px-40 py-8 text-white text-15 font-bold" target={'_blank'} href={item.link}>Xem thêm</a>
                                                </div>
                                            </div>
                                        </SwiperSlide>
                                    )
                                default:
                                    return (
                                        <>
                                            {template === ConfigurationTemplate.TEMPLATE2 ? (<SwiperSlide key={item.id} className={item.name ? ' has-overlay' : ''}>
                                                <div className="intro-slider" style={{ 'backgroundImage': `url(${cdn(item.url)})` }}>
                                                    <div className="entry-info text-center w-100 p-100">
                                                        <div className=" mb-20 text-center font-bold" style={{ fontSize: 68 }}>{item.name}</div>
                                                        {item?.subTitle && (<div className=" mb-30 sub-tittle text-center" style={{ fontSize: 40 }}>{item?.subTitle}</div>)}
                                                        <a className="button-home-slider" target={'_blank'} href={item.link}>Xem thêm</a>
                                                    </div>
                                                </div>
                                            </SwiperSlide>) : (
                                                <SwiperSlide key={item.id} className={item.name ? ' has-overlay' : ''}>
                                                    <div className="intro-slider" style={{ 'backgroundImage': `url(${cdn(item.url)})` }}>
                                                        <div className="entry-info text-center">
                                                            <div className="text-32 font-bold mb-46">{item.name}</div>
                                                            {item?.subTitle && (<div className=" mb-30 sub-tittle text-center " style={{ fontSize: 40 }}>{item?.subTitle}</div>)}

                                                            <a className="button-link bg-blue-100 rounded px-40 py-8 text-white text-15 font-bold " target={'_blank'} href={item.link}>Xem thêm</a>
                                                        </div>
                                                    </div>
                                                </SwiperSlide>)}
                                        </>
                                    )
                            }


                        })}
                    </>
                )}

            </Swiper>
        </div>
    );
}


