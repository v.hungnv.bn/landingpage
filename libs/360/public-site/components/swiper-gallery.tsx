import Image from "next/image";
import Link from "next/link";
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from 'swiper';
import 'swiper/css';
import { RegionAdvertisement } from "@lib/shared/360";
import { cdn } from "@lib/core/services";
const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const SwiperGalleryAdvertisement = (props: { title: string, data?: RegionAdvertisement[] }): React.ReactElement => {
    const { title, data } = props;
    return (
        <div className="container-fluid section-gallery bg-ads mb-40">
            <div className="text-24 text-center mb-0 text-white font-bold pt-10 heading-line-white d-flex justify-content-center"> {title || ''}</div>

            <div className="bk-slider">
                <Swiper
                    modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
                    initialSlide={1}
                    spaceBetween={0}
                    slidesPerView={1}
                    navigation
                    onSlideChange={() => console.log('slide change')}
                    onSwiper={(swiper) => console.log(swiper)}
                    centeredSlides={true}
                    autoplay={{delay: 2000}}
                    loop={true}
                    breakpoints={{
                        768: {
                          slidesPerView: 3,
                        },
                    }}
                >

                    {data?.map((e: RegionAdvertisement, index: number) => (
                        <SwiperSlide key={index + 'Advertisement'}>
                            <div className="entry-item">
                                <Image
                                    src={cdn(e.image)}
                                    alt="logo" width="608" height="345"
                                />
                                <div className="entry-content">
                                    <div className="text-16 mb-18">{e.title}</div>
                                    <div className="text-14 mb-18">{e.quote}</div>
                                    {e.link && (
                                        <a className="button-link bg-blue-100 rounded px-20 py-8 text-white text-13 font-bold" href={e.link} target={'_blank'}>Xem thêm</a>
                                    )}
                                </div>
                            </div>

                        </SwiperSlide>
                    ))}

                </Swiper>
            </div>

        </div>

    );
}


