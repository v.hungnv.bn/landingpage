// import Container from "@components/container";
// import ThemeSwitch from "@components/themeSwitch";
import { PostService } from "@lib/360/admin-site/services";
import DepartmentContext from "@lib/admin/context/department-context";
import { cdn } from "@lib/core/services";
import { PostDTO } from "@lib/shared/360";
import Image from "next/image";
import Link from "next/link";
import React, { useContext, useState } from "react";
// import { myLoader } from "@utils/all";


export const TopNew = (props: { title?: string }): React.ReactElement => {
    const { title } = props;
    const [posts, setPosts] = useState<PostDTO[]>([]);
    const department = useContext(DepartmentContext);
    React.useEffect(() => {
        getTopView();
    }, []);

    const getTopView = async () => {
        try {
            const res = await PostService.topView(department.departmentCode);
            setPosts(res);
        } catch (err) {
            console.log(err)
        } finally {
        }
    }

    return (
        <div className="box-news rounded-8 shadow-f1 mt-12">
            <div className="text-18 font-bold p-10">Tin xem nhiều</div>
            <div className="entry-list px-10">
                {posts?.map((e: PostDTO, index: number) => (
                    <Link key={e.id} href={`/bai-viet/${e.slug}`}>
                        
                        <div key={e.id} className="media mb-24 pointer">
                            <div className="mr-16 entry-thumb-83">
                                {/* <Image src={cdn(e?.image)} alt="vi" width={72} height={83} /> */}
                                <img className="entry-thumb-83" src={cdn(e?.image)} />
                            </div>
                            <div className="media-body">
                                <div className="text-16 font-bold mb-16">{e.name}</div>
                                <div className="text-14 text-gray-101">{Date.toFormat(e.publishDate, 'dd/MM/yyyy')}</div>
                            </div>
                        </div>
                    </Link>

                ))}
            </div>
        </div>

    );
}


