import React, { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { ConfigurationService, MenuDTOExtend } from "@lib/360/admin-site/services";
import { ConfigurationDTO, MenuType } from "@lib/shared/360";
import { cdn } from "@lib/core/services";
import { useRouter } from "next/router";
import MenuIcon from '@mui/icons-material/Menu';
import { IconButton, InputBase, TextField } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import CloseIcon from '@mui/icons-material/Close';
import { Controller, useForm } from "react-hook-form";
import { MobileMenu } from "./mobile-menu";
interface Props {
  logo?: string;
  copyright?: string;
  description?: string;
  menus?: MenuDTOExtend[];
  configSite?: ConfigurationDTO
}
const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const Navbar = (props: Props): React.ReactElement => {

  const { menus, configSite } = props;
  const [data, setData] = useState<MenuDTOExtend[]>([]);
  const [state, setState] = useState<{ open: boolean }>({
    open: false
  });

  const router = useRouter();
  const { slug } = router.query;

  const form = useForm();

  const {
    handleSubmit,
    register,
    control,
    setValue,
    watch,
    formState: { errors },
  } = form;

  useEffect(() => {
    const items = props?.menus || [];
    items?.forEach(e => {
      if (e.type === MenuType.NEWS) {
        e.link = '/tin-tuc-su-kien';
      }
      if (e.type === MenuType.DOCUMENT) {
        e.link = '/van-ban';
      }
      if (e.type === MenuType.ORGANIZATION) {
        e.link = '/about';
      }
      if (e.type === MenuType.SCHEDULE) {
        e.link = '/schedule';
      }
      if (e.type === MenuType.QUESTION) {
        e.link = '/hoi-dap';
      }
      if (e.type === MenuType.CATEGORY) {
        e.link = `/tin-tuc-su-kien/${e.slug}`;
      }
      if (e.type === MenuType.PAGE) {
        e.link = `/noi-dung/${e.slug}`;
      }
      if (e.children?.length) {
        e.children?.forEach(child => {
          if (child.type === MenuType.NEWS) {
            child.link = '/tin-tuc-su-kien';
          }
          if (child.type === MenuType.DOCUMENT) {
            child.link = '/van-ban';
          }
          if (child.type === MenuType.ORGANIZATION) {
            child.link = '/about';
          }
          if (child.type === MenuType.SCHEDULE) {
            child.link = '/schedule';
          }
          if (child.type === MenuType.QUESTION) {
            child.link = '/hoi-dap';
          }
          if (child.type === MenuType.CATEGORY) {
            child.link = `/tin-tuc-su-kien/${child.slug}`;
          }
          if (child.type === MenuType.PAGE) {
            child.link = `/noi-dung/${child.slug}`;
          }

        });
      }

    });
    setData(items);
  }, [props.menus]);


  const googleTranslateElementInit = () => {
    new (window as any).google.translate.TranslateElement(
      {
        pageLanguage: "vi",
        autoDisplay: false
      },
      "google_translate_element"
    );
  };
  useEffect(() => {
    var addScript = document.createElement("script");
    addScript.setAttribute(
      "src",
      "//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"
    );
    document.body.appendChild(addScript);
    (window as any).googleTranslateElementInit = googleTranslateElementInit;
  }, []);

  const changeLanguageByButtonClick = (language: string) => {
    const selectField: any = document.querySelector("#google_translate_element select");
    console.log('selectField', selectField)
    for (var i = 0; i < selectField.children.length; i++) {
      var option = selectField.children[i];
      // find desired langauge and change the former language of the hidden selection-field 
      if (option.value == language) {
        selectField.selectedIndex = i;
        // trigger change event afterwards to make google-lib translate this side
        selectField.dispatchEvent(new Event('change'));
        break;
      }
    }
  };

  const toogleSearch = () => {
    setState({
      open: !state.open
    })
  }

  const submitHandler = async (data: any) => {
    if (data.searchText) {
      router.push({
        pathname: '/search',
        query: { keyword: data.searchText }
      });
      setState({
        open: !state.open
      });
      setValue('searchText', '');
    }
  }

  return (
    <div className="sticky top-0 z-30 w-full px-2 py-4 bg-white sm:px-4 shadow-header">
      <div className="container-fluid">
        <div style={{ maxWidth: 1368 }} className="mx-auto">
          <div className="d-flex  align-items-center py-4">
            <span >
              {/* <Image src="/front/360-logo.png" alt="Logo" width={65} height={65} /> */}
              {/* <Image src={cdn(configSite?.image)} alt="Logo" width={65} height={65} /> */}
              <img className="entry-logo" src={cdn(configSite?.image)} alt="Logo" />

            </span>
            {!state.open && (
              <div className="media-body d-flex justify-content-end align-items-center">

                <nav className="main-menu">
                  <ul className="d-lg-flex d-none list-none  align-items-center mb-0 p-0">
                    <li className="menu-item relative  mx-4">
                      <Link href={'/'}>
                        <a className="text-18  pointer px-8 py-8">Home</a>
                      </Link>
                    </li>
                    {data.length > 0 && data.map((menu, idx) => {
                      return (
                        <li key={menu.referenceId || idx} className={router.pathname == menu.link ? "active menu-item relative  mx-4" : " menu-item relative  mx-4"}>

                          {menu.type === MenuType.LINK ? (
                            <a href={menu?.link || ''} className="text-18  pointer px-8 py-8" target={'_blank'}>{menu.title}</a>
                          ) : (
                            <Link href={menu?.link || ''}>
                              <a className="text-18  pointer px-8 py-8">{menu.title}</a>
                            </Link>
                          )}

                          {menu?.children && menu?.children?.length > 0 && (
                            <ul className="menu-children  list-none ">
                              {menu?.children?.map((level2, idx2) => {
                                return (
                                  <>
                                    {level2.type === MenuType.LINK ? (
                                      <li key={level2.referenceId} className="menu-item relative pointer">
                                        <a href={level2?.link || ''} className="text-16  px-16 py-10 d-flex" target={'_blank'}>{level2?.title || ''}</a>
                                      </li>
                                    ) : (

                                      <Link key={level2.referenceId} href={level2?.link || ''}>
                                        <li className="menu-item relative pointer">
                                          <a className="text-16  px-16 py-10 d-flex">{level2?.title || ''}</a>
                                        </li>
                                      </Link>
                                    )}
                                  </>

                                );
                              })}
                            </ul>
                          )}

                        </li>
                      );
                    })}

                  </ul>
                  {/* <IconButton className="d-lg-none d-inline-block" aria-label="add">
                    <MenuIcon />
                  </IconButton> */}
                </nav>

                <MobileMenu menus={data}/>
                
                <div className="d-flex align-items-center ml-16">
                  <a className="mr-24 d-flex align-items-center pointer" onClick={() => changeLanguageByButtonClick('vi')}>
                    <Image src="/front/vi.png" alt="vi" width={30} height={19} />

                  </a>
                  <a className="d-flex align-items-center pointer" onClick={() => changeLanguageByButtonClick('en')}>
                    <Image src="/front/en.png" alt="en" width={30} height={19} />
                  </a>
                  <div id="google_translate_element" className="opacity-0 fixed" style={{ zIndex: -999 }}></div>
                  {/* <input value="en" id="language" /> */}

                </div>

                <IconButton className="ml-16" aria-label="search" onClick={toogleSearch}>
                  <SearchIcon />
                </IconButton>

                {router.pathname == '/tin-tuc-su-kien/[slug]' && slug && (
                  <div className="ml-20">
                    <a href={`${host360}category/rss/${slug}/feed`} target="_blank">
                      <img className="entry-thumb-h-25" src="/rss.png" alt="vi" />
                    </a>
                  </div>
                )}
              </div>
            )}

            {state.open && (
              <div className="media-body d-flex justify-content-end align-items-center">
                <form className="w-100" onSubmit={handleSubmit(submitHandler)}>
                  <div className="d-flex">
                    <IconButton className="ml-16" aria-label="search" onClick={handleSubmit(submitHandler)}>
                      <SearchIcon />
                    </IconButton>
                    <Controller
                      name="searchText"
                      control={control}
                      defaultValue=""
                      render={({ field }) => (
                        // <TextField
                        //   variant="standard"
                        //   fullWidth
                        //   id="name"
                        //   size="small"
                        //   {...field}
                        // ></TextField>
                        <InputBase
                          sx={{ ml: 1, flex: 1 }}
                          placeholder="Nhập từ khoá tìm kiếm và nhấn enter ...."
                          inputProps={{ 'aria-label': 'Nhập từ khoá tìm kiếm và nhấn enter ....' }}
                          {...field}
                        />
                      )}
                    ></Controller>
                    <IconButton className="ml-16" aria-label="search" onClick={toogleSearch}>
                      <CloseIcon />
                    </IconButton>
                  </div>
                </form>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
