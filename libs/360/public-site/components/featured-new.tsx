import { PostService } from "@lib/360/admin-site/services";
import { cdn } from "@lib/core/services";
import { PostDTO } from "@lib/shared/360";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";

const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const FeaturedNew = (props: { title?: string}): React.ReactElement => {
  const { title } = props;
  const [posts, setPosts] = useState<PostDTO[]>([]);


  useEffect(() => {
    getPostByCategory('');
  }, []);

  const getPostByCategory = async (catId?: string) => {
    try {
      const res = await PostService.allPostPublish(catId || '');
      setPosts(res);
    } catch (err) {
      console.log(err)
    } finally {
    }
  }
  return (
    <div className="container-fluid featured-new my-40">
      <div className="d-flex justify-content-center">
        <div className="text-24 text-center mb-20 text-blue-100 heading-line">{title || 'Tin tức sự kiện'}</div>

      </div>
      <div className="row">
        <div className="col-md-8">
          {posts?.length > 0 && (
            <div className="row mx-n10">

              {posts?.map((article, index) => {
                if (index <= 2) {
                  return (
                    <div key={article.id} className="col-sm-6 col-md-4 px-10 mb-16">
                      <div className="entry-item ">
                        <div className="entry-thumb mb-8 box-thumb">
                          <Link href={`/bai-viet/${article.slug}`}>
                            <img className="w-100" src={cdn(article.image)} />
                          </Link>
                        </div>
                        <div className="entry-content">
                          <div className="entry-title entry-quote-3 text-18 mb-10">
                            <Link href={`/bai-viet/${article.slug}`}>
                              <a className="text-18">{article.name}</a>
                            </Link>
                          </div>
                          <div className="entry-date text-14 mb-10">{Date.toFormat(article.publishDate, 'dd/MM/yyyy')}</div>

                          <div className="entry-quote-5 text-14">
                            {article.quote}
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                }

              })}

            </div>
          )}



        </div>
        <div className="col-md-4">
          {posts?.length > 2 && (
            <div className="sidebar-new">
              {posts?.map((article, index) => {
                if (index > 2 && index < 7) {
                  return (
                    <div key={article.id} className="media mb-20">
                      <div className="d-flex mr-8 rounded-lg overflow-hidden box-thumb" style={{ width: 125 }} >

                        <Link href={`/bai-viet/${article.slug}`}>
                          <img  className="w-100 entry-thumb-h74" src={cdn(article.image)} />
                          {/* <Image
                                                        src="https://znews-photo.zingcdn.me/w480/Uploaded/spluaaa/2022_11_24/6a94779004030ee61bdab0905f7f4e15.jpg"
                                                        alt="logo" width="125" height="78"
                                                    /> */}
                        </Link>
                      </div>
                      <div className="media-body">
                        <Link href={`/bai-viet/${article.slug}`}>
                          <a className="text-18">{article.name}</a>
                        </Link>
                      </div>
                    </div>
                  )
                }

              })}

              {posts?.length > 7 && (
                <Link href={`/tin-tuc-su-kien`}>
                  <a className="text-15 font-bold pointer text-center d-block text-gray-101">Xem thêm</a>
                </Link>
              )}

            </div>
          )}

        </div>
      </div>
    </div>
  );
}


