import Image from "next/image";
import Link from "next/link";
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from 'swiper';
import 'swiper/css';
import ReactImageVideoLightbox from "react-image-video-lightbox";
import { useEffect, useState } from "react";
import { MediaService } from "@lib/360/admin-site/services";
import { MediaDTO, MediaType } from "@lib/shared/360";
import { cdn } from "@lib/core/services";
import { getIdUrlYoutube } from "./home-slider";


export interface MediaVideo {
    type?: string;

    name?: string;

    url?: string;
    isPublished?: boolean;
    id: string;
    videoId?: string;
}
export const SwiperGalleryVideo = (props: { title?: string}): React.ReactElement => {
    const { title } = props;

    const [open, setopen] = useState(false);

    const [currentIndex, setCurrentIndex] = useState(0);
    const [videos, setVideos] = useState<MediaVideo[]>([]);

    useEffect(() => {
        getMedias();
    }, []);


    const getMedias = async () => {
        try {
            const list: MediaVideo[] = [];
            const res = await MediaService.allPublic(MediaType.YOUTUBE);
            if (res?.length) {
                res.forEach(e => {
                    const entry = {
                        id: e.id,
                        url: e.url,
                        videoId: getIdUrlYoutube(e.url || '')
                    }
                    list.push(entry);
                })
            }
            setVideos(list);

        } catch (err) {
            console.log(err)
        } finally {

        }
    }

    const openlightbox = (index) => {
        console.log(index);
        setCurrentIndex(index);
        setopen(true);
    };

    return (
        <div className="container-fluid section-gallery video bg-video mb-40">
            <div className="text-24 text-center mb-0 text-white font-bold pt-10 heading-line-white d-flex justify-content-center"> {title || 'Thư viện video'}</div>

            <div className="bk-slider">
                <Swiper
                   modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
                   initialSlide={1}
                   spaceBetween={0}
                   slidesPerView={1}
                   navigation
                   onSlideChange={() => console.log('slide change')}
                   onSwiper={(swiper) => console.log(swiper)}
                   centeredSlides={true}
                   autoplay={{delay: 2000}}
                   loop={true}
                   breakpoints={{
                       768: {
                         slidesPerView: 3,
                       },
                   }}
                >
                    {videos?.map((e: MediaVideo, index: number) => (
                        <SwiperSlide key={e.id}>
                            <div className="entry-item pointer" >
                                <iframe src={`https://www.youtube.com/embed/${e.videoId}`}
                                    width="608"
                                    height="345"
                                    frameBorder='0'
                                    allow='autoplay; encrypted-media'
                                    allowFullScreen
                                    title='video'
                                />
                                {/* <Image
                                    src={`https://img.youtube.com/vi/${getIdUrlYoutube(e.url || '')}/maxresdefault.jpg`}
                                    alt="logo" width="608" height="345"
                                /> */}

                            </div>

                        </SwiperSlide>
                    ))}

                </Swiper>
            </div>

            {open && (
                <div className="lightBoxModal">
                    <ReactImageVideoLightbox
                        data={videos}
                        startIndex={currentIndex}
                        showResourceCount={true}
                        onCloseCallback={() => setopen(false)}
                        onNavigationCallback={(currentIndex) =>
                            console.log(`Current index: ${currentIndex}`)
                        }
                    />
                </div>

            )}

        </div>

    );
}


