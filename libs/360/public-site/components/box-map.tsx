



import { useCallback, useEffect, useState } from "react";
import { RegionDTOExtend, TravelCategoryService } from '@lib/360/admin-site/services'
import { GooglePlaceResult, TravelCategoryDTO } from '@lib/shared/360'
import { GMap } from '../components/map'
import React from 'react';
import { debounce } from 'lodash';


const BoxMap = (props: { title?: string}) => {
    const { title } = props;

    const [categories, setCategories] = useState<any>();
    const [posts, setPosts] = useState<any>();
    const [regions, setRegions] = useState<RegionDTOExtend[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const template = localStorage.getItem('TEMPLATE');
    const [, setInputValue] = React.useState("");
    const [options, setOptions] = React.useState<GooglePlaceResult[]>([]);
    const [lat, setLat] = useState<number | undefined>(12.249969820488728);
    const [lng, setLng] = useState<number | undefined>(107.5680857232407);
    const [travelCategories, setTravelCategories] = React.useState<TravelCategoryDTO[]>([]);

    const [location, setLocation] = React.useState<any>({
        center: {
            lat: 21.028511,
            lng: 105.804817
        }
    });

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const res = await TravelCategoryService.allPublish();
        setTravelCategories(res);
    }

    const handleChangeLocation = (event, values) => {
        setLat(undefined)
        setLng(undefined)
        setInputValue(values?.name || '');

        const timer = setTimeout(() => {
            setLat(values?.geometry?.location?.lat)
            setLng(values?.geometry?.location?.lng)
        }, 500);


    }

    const fetchDropdownOptions = async (key) => {
        try {
            const res = await TravelCategoryService.searchPlace(key);
            setOptions(res || []);

        } catch (err) {
            console.log(err)
        }
        finally {
        }
    }

    const debounceDropDown = useCallback(debounce((nextValue) => fetchDropdownOptions(nextValue), 500), [])

    const handleSearch = async event => {
        setInputValue(event.target.value);
        debounceDropDown(event.target.value);
    };


    return (

        <div className="container-fluid section-google-map mb-40 relative">
            <div className="text-24 text-center mb-20 text-blue-100 font-bold heading-line d-flex justify-content-center">{title}</div>

            <div>
                {/* <SdButton label='Thêm vào menu' onClick={handleChangeView} /> */}

                {/* <Autocomplete
                    id="country-select-demo"
                    options={options}
                    getOptionLabel={(option) => option?.formattedAddress}
                    onChange={handleChangeLocation}
                    // options={options.map((option) => option.formattedAddress)}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            size={'small'}
                            label="Tìm kiếm địa chỉ chi tiết"
                            InputProps={{
                                ...params.InputProps,
                                type: 'search',
                              }}
                            onChange={handleSearch}
                        />
                    )}
                /> */}
            </div>

            {lat && lng && travelCategories?.length > 0 && (<GMap lat={lat} lng={lng} travelCategories={travelCategories} />)}


        </div>
    )
}

export default BoxMap