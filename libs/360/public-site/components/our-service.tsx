import { cdn } from "@lib/core/services";
import { RegionData } from "@lib/shared/360";
import Image from "next/image";
import Link from "next/link";
const host360 = process.env.NEXT_PUBLIC_360_HOST;

export const OurService = (props: { title: string, data?: RegionData[] }): React.ReactElement => {
    const { title, data } = props;

    return (
        <div className="container-fluid  mb-40">
            <div className="text-24 text-center mb-20 text-blue-100 font-bold heading-line d-flex justify-content-center">{title}</div>
            <div className="row">
                {data?.map((e: RegionData, index: number) => (
                    <div key={e.title + '_' + index} className="col-6 col-md-3 mb-30">
                        <Link href={e.link}>
                            <div className="pointer relative box-thumb">
                                <img
                                    className="w-100"
                                    src={cdn(e.image)}
                                    alt="logo"
                                />
                                <div className="entry-title text-white bg-blue-op-1 text-16 md:text-24  font-bold d-flex align-items-center justify-content-center p-10 absolute bottom-0 w-100" >{e.title}</div>
                            </div>

                        </Link>
                    </div>
                ))}

            </div>
        </div>
    );
}


