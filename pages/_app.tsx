import '../styles/globals.css';
import '../libs/core/styles/core/index.scss';
import "../libs/360/public-site/styles/index.scss";
import "../libs/360/admin-site/styles/index.scss";
import '../styles/quill.core.css';
import '../styles/quill.bubble.css';
import '../styles/quill.snow.css';


import type { AppProps } from 'next/app';
import { Alert } from '../libs/core/components/Alert/Alert';
import { ModalConfirm } from '../libs/core/components/Confirm/Confirm';
import { StyledEngineProvider } from '@mui/material/styles';
import { Loader } from '../libs/core/components/BackDropLoading/BackDropLoading';
import HomeIcon from '@mui/icons-material/Home';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import DomainIcon from '@mui/icons-material/Domain';
import { Layout, Menu, Meta, RouteGuard } from '@lib/admin/components';
import { AppConfig } from 'src/utils/AppConfig';
import { ApiService } from '@lib/core/services';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { orange } from '@mui/material/colors';
import PermMediaIcon from '@mui/icons-material/PermMedia';
import ArticleIcon from '@mui/icons-material/Article';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import { LayoutFront } from '@lib/360/public-site/components/layout-front';
import { DomainGuard } from '@lib/admin/components/domain-guard';
import { InitConfigSite } from '@lib/360/admin-site/components/init-config-info-site';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import MapIcon from '@mui/icons-material/Map';



declare module '@mui/material/styles' {
  interface Theme {
    status: {
      danger: string;
    };
  }
  // allow configuration using `createTheme`
  interface ThemeOptions {
    status?: {
      danger?: string;
    };
  }
}

const theme = createTheme({
  status: {
    danger: orange[500],
  },
  palette: {
    primary: {
      main: "#2962FF"
    },
    secondary: {
      main: "#637381"
    }
  },
});

export const Menus: Menu[] = [
  {
    id: '14a811c8-8910-40c9-9eab-1ff9cb37d2d0',
    icon: <HomeIcon />,
    level: 1,
    title: 'Hệ thống',
    children: [{
      id: '1deb8a1d-e3f1-4b6a-8baf-fcbc15492db6',
      level: 2,
      path: '/system/department',
      title: 'Cơ quan đơn vị',
      hidden: () => !ApiService.portalPage?.department?.view
    }, {
      id: '85a4b01a-6041-492b-8047-0f1d5bdd4fd1',
      path: '/system/domain',
      title: 'Tên miền',
      level: 2,
      hidden: () => !ApiService.portalPage?.domain?.view
    }, {
      id: '758d25f6-8ced-4e95-9848-c9c6c4b20c8d',
      path: '/system/permission',
      title: 'Quyền',
      level: 2,
      hidden: () => !ApiService.portalPage?.permission?.view
    }, {
      id: 'e13a0cfc-90ff-4bf7-a70a-41fda004c239',
      path: '/system/group-permission',
      title: 'Nhóm quyền',
      level: 2,
      hidden: () => !ApiService.portalPage?.group?.view
    }, {
      id: '2a2af15e-321e-4afb-a129-0bd765c2db10',
      path: '/system/role',
      title: 'Vai trò',
      level: 2,
      hidden: () => !ApiService.portalPage?.role?.view
    }]
  }, {
    id: 'a8896190-801e-4339-a887-ee08d6dc19fe',
    icon: <AccountBoxIcon />,
    path: '/system/account',
    title: 'Tài khoản',
    level: 1,
    hidden: () => !ApiService.portalPage?.user?.view
  },
  {
    id: '53aacbb5-164c-4b4e-8634-9dfa167620d2',
    icon: <ArticleIcon />,
    path: 'https://cskh-vnagroup.gitbook.io/huong-dan-su-dung-cong-thong-tin-dien-tu/',
    title: 'Hướng dẫn sử dụng',
    isExternal: true,
    level: 1,
  },
  {
    id: '05360f0b-306a-4a1b-af9a-83d8641fd2fe',
    icon: <DarkModeIcon />,
    path: '/admin-360/theme',
    title: 'Tuỳ chỉnh theme',
    level: 1,
  },
  {
    id: '2b7ae67d-687b-4061-be3d-90e6573c97fb',
    icon: <DomainIcon />,
    path: '/admin-360/article',
    title: 'Tin tức',
    level: 1,
    children: [
      {
        id: 'ae64bb40-4108-457e-bb52-b0cd2bfd6a4b',
        path: '/admin-360/category',
        title: 'Chuyên mục',
        level: 2,
      },
      {
        id: '67ddc63b-ee94-410a-8a50-51b09d4fc26a',
        path: '/admin-360/article',
        title: 'Bài viết',
        level: 2,
      },
      {
        id: 'c3118e3d-3162-4333-8344-e5fefa9bf27b',
        path: '/admin-360/article/detail',
        title: 'Tạo mới bài viết',
        level: 2,
        hidden: () => true
      },
      {
        id: '51cbc6b9-057b-4c1f-be06-ab3857d87928',
        path: '/admin-360/content',
        title: 'Trang nội dung',
        level: 2,
      },
      {
        id: 'ed570453-38c5-42c0-b51f-7f1287ef8016',
        path: '/admin-360/content/detail',
        title: 'Tạo mới nội dung',
        level: 2,
        hidden: () => true
      },

    ]
  },
  {
    id: 'ca96e149-d937-4821-8881-c57747f5b765',
    icon: <DomainIcon />,
    path: '/admin-360/department-category',
    title: 'Đơn vị liên kết',
    level: 1,
    children: [
      {
        id: '62cc0cc1-1c80-4b83-b75f-aa26e8153293',
        path: '/admin-360/department-category',
        title: 'Danh mục đơn vị',
        level: 2,
      },
      {
        id: '1fdc9687-58b8-423f-a616-e3265c32adf3',
        path: '/admin-360/department-360',
        title: 'Đơn vị',
        level: 2,
      }
    ]
  },
  {
    id: 'múagd78-a729-41dc-bb2f-97e1e2eca978',
    icon: <PermMediaIcon />,
    path: '/admin-360/image',
    title: 'Đa phương tiện',
    level: 1,
    children: [
      {
        id: 'áda-1c80-4b83-b75f-aa26e8153293',
        path: '/admin-360/image',
        title: 'Hình ảnh',
        level: 2,
      },
      {
        id: '1fdc9687-58b8-423f-a616-vgctr1',
        path: '/admin-360/media',
        title: 'Video',
        level: 2,
      }
    ]
  },
  {
    id: '86bba649-83c7-41af-8565-8c65e653d2c0',
    icon: <ArticleIcon />,
    path: '/admin-360/image',
    title: 'Quản lý văn bản',
    level: 1,
    children: [
      {
        id: '1594a2e9-2728-439f-99a9-83e4d2bc0b11',
        path: '/admin-360/document-group',
        title: 'Danh mục văn bản',
        level: 2,
      },
      {
        id: '6336bcd4-adc4-41b4-913c-2cdab425048b',
        path: '/admin-360/document',
        title: 'Danh sách văn bản',
        level: 2,
      },
    ]
  },
  {
    id: 'c2a48e9d-8562-4ae7-af91-a5db23feedcb',
    icon: <ArticleIcon />,
    path: '/admin-360/organization',
    title: 'Cơ cấu tổ chức',
    level: 1,
    children: [
      {
        id: '53e98456-8202-4024-92ed-5be3b21ce870',
        path: '/admin-360/organization',
        title: 'Phòng ban',
        level: 2,
      },
      {
        id: 'fff43b30-489f-4957-836a-b19957a1c2d9',
        path: '/admin-360/member',
        title: 'Thành viên',
        level: 2,
      },
    ]
  },
  {
    id: 'dbbade0e-a729-41dc-bb2f-kjkasd',
    icon: <ArticleIcon />,
    path: '/admin-360/image',
    title: 'Giao diện',
    level: 1,
    children: [
      {
        id: '62cc0cc1-56ada-4b83-b75f-aa26e8153293',
        path: '/admin-360/info',
        title: 'Thông tin website',
        level: 2,
      },
      {
        id: '1fdc9687-kljl-423f-a616-e3265c32adf3',
        path: '/admin-360/banner',
        title: 'Banner',
        level: 2,
      },
      {
        id: '1fdc9687-58b8-423f-a616-adasd',
        path: '/admin-360/menu-config',
        title: 'Cấu hình menu',
        level: 2,
      },
      {
        id: 'f8fa6079-8d31-487b-b6a3-bbe7993b3fc4',
        path: '/admin-360/region',
        title: 'Cài đặt vùng hiển thị',
        level: 2,
      },
      {
        id: 'f8fa6079-8d31-487b-b6a3-bbe7993b3fc4',
        path: '/admin-360/region/detail',
        title: 'Thêm vùng hiển thị',
        level: 2,
        hidden: () => true

      }
    ]
  },

  {
    id: 'ccea19d5-259c-4881-a863-efd342fbb986',
    icon: <CalendarMonthIcon />,
    path: '/admin-360/schedule',
    title: 'Quản lý lịch công tác',
    level: 1,
  },

  {
    id: '9feca0bb-50e2-4213-b874-b677051e8c20',
    icon: <ArticleIcon />,
    path: '/admin-360/comment',
    title: 'Hỏi đáp - bình luận',
    level: 1,
    children: [
      {
        id: 'd31c5d48-d023-41de-be5b-aa975f74201b',
        path: '/admin-360/question',
        title: 'Hỏi đáp',
        level: 2,
      },
      {
        id: 'f1a215c0-8e21-442b-a147-ecdea54f8d71',
        path: '/admin-360/comment',
        title: 'Bình luận',
        level: 2,
      },
    ]
  },
  {
    id: 'be3787c0-9d42-492e-bd67-a11bf4de5627',
    icon: <MapIcon />,
    path: '/admin-360/travel-category',
    title: 'Bản đồ',
    level: 1,
    children: [
      {
        id: 'fe527fa3-e7a9-44f9-9339-b3aca38e0581',
        path: '/admin-360/travel-category',
        title: 'Danh mục du lịch',
        level: 2,
      },
      {
        id: '01b19301-cfb7-425e-aacc-98b4805649e1',
        path: '/admin-360/travel-location',
        title: 'Địa điểm du lịch',
        level: 2,
      },
    ]
  },

];

function MyApp({ Component, pageProps, router }: AppProps) {

  ApiService.setAdminDefaultPage('/admin-360/article');

  const isLogin = router.pathname === '/login';

  const isAdminSite = Menus.some(e => e.path === router.pathname || e?.children?.some(f => f.path === router.pathname));

  return (
    <StyledEngineProvider injectFirst><Meta title={AppConfig.title} description={AppConfig.description} /><Alert /><Loader /> <ModalConfirm />
      <DomainGuard>
        {(isLogin) && (<Component {...pageProps} />)}
        {isAdminSite && (<RouteGuard>
          <ThemeProvider theme={theme}>
            <InitConfigSite>
              <Layout host={process.env.NEXT_PUBLIC_360_HOST || ''} menus={Menus} >
                <Component {...pageProps} />
              </Layout>
            </InitConfigSite>
          </ThemeProvider>
        </RouteGuard>)
        }
        {!isLogin && !isAdminSite && (<ThemeProvider theme={theme}><LayoutFront><Component {...pageProps.order} /></LayoutFront></ThemeProvider>)}
      </DomainGuard>
    </StyledEngineProvider >
  )
}

export default MyApp
